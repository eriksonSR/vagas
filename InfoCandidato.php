<?php
session_start();

date_default_timezone_set('America/Sao_Paulo');	
include 'Banco/Conexao.class.php';
include 'Model/InfoCandidato.class.php';
include 'Model/User.class.php';
include 'Persistencia/CursoDAO.class.php';
include 'Persistencia/ExperienciaDAO.class.php';
include 'Model/Curso.class.php';
include 'Model/Experiencia.class.php';
include 'Persistencia/UserDAO.class.php';
include 'Model/AreaInteresse.class.php';
include 'Persistencia/AreaInteresseDAO.class.php';

if(isset($_SESSION['foco_candidato']) && (isset($_SESSION['ct_user']) || isset($_SESSION['ct_empresa']) || isset($_SESSION['ct_admin']))){
	$infoCandidato = unserialize($_SESSION['foco_candidato']);
}else{
	header('location:index.php');
}
?>
<!DOCTYPE HTML>
<html><!-- InstanceBegin template="/Templates/InfoCandidatoTemplate.dwt" codeOutsideHTMLIsLocked="false" --> 
<head>

<title>Bem-vindo</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600" rel="stylesheet" type="text/css" />

<link rel="stylesheet" type="text/css" href="css/table/table.css">

<link href="css/table/style.css" rel="stylesheet" type="text/css" />
<link href="css/table/form/screen.css" rel="stylesheet" type="text/css" />

<!--[if lte IE 8]><script src="js/html5shiv.js"></script><![endif]-->
<script src="js/request/AjaxRequest.js"></script> 

<script src="js/jquery.min.js"></script>
<script src="js/skel.min.js"></script>
<script src="js/skel-panels.min.js"></script>
<script src="js/init.js"></script>

<!-- Fim da popup -->

<noscript>
<link rel="stylesheet" href="css/skel-noscript.css" />
<link rel="stylesheet" href="css/style.css" />
<link rel="stylesheet" href="css/style-wide.css" />
</noscript>
<!--[if lte IE 9]><link rel="stylesheet" href="css/ie9.css" /><![endif]-->
<!--[if lte IE 8]><link rel="stylesheet" href="css/ie8.css" /><![endif]-->
</head>
<body>

<div id="pop"> <img src="images/ajax-loader.gif" > </div>

<div id='alerta'> </div>
<!-- Header -->
<div id="header" class="skel-panels-fixed">
  <div class="top">
    <div id="logo"> 
      <h1 id="title"><!-- InstanceBeginEditable name="EditRegion1" --> 
	  <?php
	  echo $infoCandidato[0]->nome;	  	  
	  ?><!-- InstanceEndEditable --></h1>     
    </div>
    <!-- Nav -->
    <nav id="nav">
      <ul>
        <li><a href="#info" id="info-link" class="skel-panels-ignoreHref"><span class="fa fa-info-circle">Informações pessoais</span></a></li>      
        <li><a href="getPdf.php" target="_blank"><span class="fa fa-file-o">Gerar currículo em PDF</span></a></li>  
        
        
        <!-- InstanceBeginEditable name="EditRegion6" -->
        <?php
		if(isset($_SESSION['ct_admin'])){
		?>					            
            <li><a href="Controller/Admin.controller.php?op=<?php echo sha1(5) ?>" class="skel-panels-ignoreHref"><span class="fa fa-folder-open-o">Acessar como</span></a></li>  			            
        <?php
		}				
		?>
		<!-- InstanceEndEditable -->
        
        <li style="border-bottom:1px #999999 groove; margin-top:7px;"> </li>
        
        <!-- InstanceBeginEditable name="EditRegion10" -->
         <?php
        	if(isset($_SESSION['ct_empresa'])){
				echo '<li><a href="PerfilEmpresa.php"><span class="fa fa-home">Voltar ao perfil inicial</span></a></li>';
			}else if(isset($_SESSION['ct_user'])){
				echo '<li><a href="PerfilCandidato.php"><span class="fa fa-home">Voltar ao perfil inicial</span></a></li>';
			}else if(isset($_SESSION['ct_admin'])){
				echo '<li><a href="PerfilAdmin.php"><span class="fa fa-home">Voltar ao perfil inicial</span></a></li>';
			}
		 ?>
        <!-- InstanceEndEditable -->
        
        <li><a href="Controller/Logoff.controller.php"><span class="fa fa-lock">Sair do sistema</span></a></li>
      </ul>
    </nav>
  </div>
  <div class="bottom">
    <!-- Social Icons -->
    <ul class="icons">
      <li><a href="#" class="fa fa-facebook solo"><span>Facebook</span></a></li>
      <li><a href="#" class="fa fa-envelope solo"><span>Email</span></a></li>
    </ul>
  </div>
</div>
<!-- Main -->
<div id="main">
	
  <!-- Intro -->
  <section id="info" class="one">
    <div class="container">
        <!--  <a  class="image featured"><img src="../banner.jpg" alt="" /></a> -->
        <!-- InstanceBeginEditable name="EditRegion4" -->
        <header id='nome_vaga'>
          <h2 class="alt"><strong><?php echo $infoCandidato[0]->nome; ?></strong></h2>
        </header>

		<div class="container" style="margin-bottom:-50px;">       
            <form onSubmit="return false" id="alt_nota">
                <div class="row half">
                    <div class="12u">
                        <textarea name="txtNota" placeholder="Crie uma anotação sobre o candidato" onBlur='cadastroRapido("Controller/Candidato.controller.php?op=3","alerta","alt_nota")'><?php 
						if(strlen($infoCandidato[0]->anotacao) > 0){
							echo $infoCandidato[0]->anotacao;
						} ?></textarea>
                    </div>
                </div>
            </form>
        </div>
        

        <!-- InstanceEndEditable -->
    </div>
  </section> 
  
  <section id="info" class="two">
    <div class="container">
    
      <header>
        <h2>Informações do candidato</h2>       
        <p>Convide este candidato para uma de suas vagas!!!</p>
      </header> 
      
	  <div id="dados_vaga"> 
		  <!-- InstanceBeginEditable name="EditRegion5" -->
          	<div class="row half" style="margin-top:-15px;">
              <div class="12u" id='input_full2'> <b style="color:#000000;">Auto imagem</b> <br />
               <?php
			   if(strlen($infoCandidato[0]->auto_imagem) > 0){
               	echo $infoCandidato[0]->auto_imagem;
			   }else{
			   	echo " -- ";
			   }
               ?>
              </div>          
            </div>
            <div class="row half" style="margin-top:-15px;">
              <div class="6u" id='input_full'> <b style="color:#000000;">Facebook</b> <br />
               <?php
               echo $infoCandidato[0]->facebook;
               ?>
              </div>          
            
              <div class="6u" id='input_full'> <b style="color:#000000;">Linkedin</b> <br />
               <?php
               echo $infoCandidato[0]->linkedin;
               ?>
              </div>
            </div>
            
            <div class="row half">
              <div class="6u" id='input_full'> <b style="color:#000000;">Data de nascimento</b> <br />
               <?php
               if(strlen($infoCandidato[0]->nascimento) > 0){
                echo date('d/m/Y',strtotime($infoCandidato[0]->nascimento));
               }
               ?>
              </div>          
            
              <div class="6u" id='input_full'> <b style="color:#000000;">Conhecimento em informatica</b> <br />
               <?php
               echo $infoCandidato[0]->informatica;
               ?>
              </div>
            </div>
            
            <div class="row half">
              <div class="6u" id='input_full'> <b style="color:#000000;">CNH</b> <br />
               <?php
                echo substr($infoCandidato[0]->cnh,0,strlen($infoCandidato[0]->cnh) - 1);   
               ?>
              </div>          
            
              <div class="6u" id='input_full'> <b style="color:#000000;">Idiomas que domina</b> <br />
               <?php
                echo substr($infoCandidato[0]->idioma,0,strlen($infoCandidato[0]->idioma) - 1);   
               ?>
              </div>
            </div>
            
            <div class="row half">
              <div class="6u" id='input_full'> <b style="color:#000000;">Estado civil</b> <br />
               <?php
               echo $infoCandidato[0]->estado_civil;
               ?>
              </div>          
            
              <div class="6u" id='input_full'> <b style="color:#000000;">Escolaridade</b> <br />
               <?php
               echo $infoCandidato[0]->escolaridade;
               ?>
              </div>
            </div>
            
            <div class="row half">
              <div class="6u" id='input_full'> <b style="color:#000000;">Possui filhos</b> <br />
               <?php
               echo $infoCandidato[0]->filho_pequeno;
               ?>
              </div>          
            
              <div class="6u" id='input_full'> <b style="color:#000000;">Sexo</b> <br />
               <?php
               echo $infoCandidato[0]->sexo;
               ?>
              </div>
            </div>
            
            <div class="row half">
              <div class="6u" id='input_full'> <b style="color:#000000;">Dias disponíveis</b> <br />
               <?php
               $infoCandidato[0]->dia_disponivel = utf8_encode($infoCandidato[0]->dia_disponivel);
               
               echo substr($infoCandidato[0]->dia_disponivel,0,strlen($infoCandidato[0]->dia_disponivel) - 2);   
             
               ?>
              </div>          
            
              <div class="6u" id='input_full'> <b style="color:#000000;">Turnos disponíveis</b> <br />
               <?php
               echo substr($infoCandidato[0]->turno_disponivel,0,strlen($infoCandidato[0]->turno_disponivel) - 1);      
               ?>
              </div>
            </div>
            
            <div class="row half">
              <div class="6u" id='input_full'> <b style="color:#000000;">Estado</b> <br />
               <?php
               echo $infoCandidato[0]->estado;
               ?>
              </div>          
            
              <div class="6u" id='input_full'> <b style="color:#000000;">Cidade</b> <br />
               <?php
               echo $infoCandidato[0]->cidade;
               ?>
              </div>
            </div>
            
            <div class="row half">
              <div class="6u" id='input_full'> <b style="color:#000000;">Bairro</b> <br />
               <?php
               echo $infoCandidato[0]->bairro;
               ?>
              </div>          
            
              <div class="6u" id='input_full'> <b style="color:#000000;">Rua / Número</b> <br />
               <?php
               echo $infoCandidato[0]->rua;
               ?>
              </div>
            </div>
                        
            <br />
            
            <?php
            $cursoDAO = new CursoDAO();
            $userx = new User();
            
            $userx->id = $infoCandidato[0]->id_user;
            
            $list_curso = $cursoDAO->getAll($userx);
            
            echo " <table>
                        <caption><h3> Cursos cadastrados</h3></caption>
                    
                    <thead>";
                        
                    if(count($list_curso) > 0){
                        echo "<tr>
                                <th>Nível</th>
                                <th>Curso</th>
                                <th>Escola</th>
                                <th>Descrição</th>
                                <th>Data inicio</th>
                                <th>Data termino</th>
                              </tr>";						
                    }else{
                        echo "<tr>
                                <th>Aviso</th>
                              </tr>";
                    }						
                                                    
            echo "	</thead>
                    <tbody>";
                    if(count($list_curso) > 0){
                        foreach($list_curso as $list){
                            echo "<tr>
                                    <th>".$list->nivel."</th>
                                    <th>".$list->curso."</th>
                                    <th>".$list->escola."</th>";
                                    if(strlen($list->descricao) > 0){
                                        echo "<th>".$list->descricao."</th>";
                                    }else{
                                        echo "<th> - - - </th>";
                                    }
                            echo "	<th>".date('d/m/Y',strtotime($list->data_inicio))."</th>
                                    <th>".date('d/m/Y',strtotime($list->data_fim))."</th>";
        
                            echo "</tr>";		
                        }						
                    }else{
                        echo "<tr>
                                <th>Não há cursos cadastrados.</th>
                              </tr>";
                            
                    }
            echo "	</tbody>
                 </table>";
            
            $experienciaDAO = new ExperienciaDAO();		
            
            $list_experiencia = $experienciaDAO->getAll($userx);
            
            echo "	 <table>
                        <caption><h3>Experiências profissionais</h3></caption>";
                                            
                        if(isset($aviso)){
                            echo "<caption><h3>".$aviso."</h3></caption>";
                        }
                            
            echo "			<thead>";
                            
                            if(count($list_experiencia) > 0){
                                echo "<tr>
                                        <th>Empresa</th>
                                        <th>Cargo</th>
                                        <th>Descrição</th>
                                        <th>Data inicio</th>
                                        <th>Data termino</th>
                                      </tr>";						
                            }else{
                                echo "<tr>
                                        <th>Aviso</th>
                                      </tr>";
                            }						
                                                        
            echo "			</thead>
                            <tbody>";
                            if(count($list_experiencia) > 0){
                                foreach($list_experiencia as $list){
                                    echo "<tr>
                                            <th>".$list->empresa."</th>
                                            <th>".$list->cargo."</th>";
                                            if(strlen($list->descricao) > 0){
                                                echo "<th>".$list->descricao."</th>";
                                            }else{
                                                echo "<th> - - - </th>";
                                            }
                                    echo "	<th>".date('d/m/Y',strtotime($list->data_inicio))."</th>
                                            <th>".date('d/m/Y',strtotime($list->data_fim))."</th>";
        
                                    echo "</tr>";		
                                }						
                            }else{
                                echo "<tr>
                                        <th>Não há experiencias cadastradas.</th>
                                      </tr>";
                                    
                            }
            echo "			</tbody>
                         </table>";		
						 
						 
			// Interesse profissional
			
			$areaInteresseDAO = new AreaInteresseDAO();		
            
            $list_interesse = $areaInteresseDAO->getAll($userx);
            
            echo "	 <table>
                        <caption><h3>Interesse profissional</h3></caption>";
                                            
                        if(isset($aviso)){
                            echo "<caption><h3>".$aviso."</h3></caption>";
                        }
                            
            echo "			<thead>";
                            
                            if(count($list_interesse) > 0){
                                echo "<tr>
                                        <th>Àrea de interesse</th>
                                        <th>Função</th>
                                      </tr>";						
                            }else{
                                echo "<tr>
                                        <th>Aviso</th>
                                      </tr>";
                            }						
                                                        
            echo "			</thead>
                            <tbody>";
                            if(count($list_interesse) > 0){
                                foreach($list_interesse as $list){
                                    echo "<tr>
                                            <th>".$list->cargo."</th>
                                            <th>".$list->funcao."</th>";                                            
                                    echo "</tr>";		
                                }						
                            }else{
                                echo "<tr>
                                        <th>Não há interesses profissionais cadastradas.</th>
                                      </tr>";
                                    
                            }
            echo "			</tbody>
                         </table>";		 
            ?>
		  <!-- InstanceEndEditable -->   
          
          <div class="row">
			  <!-- InstanceBeginEditable name="EditRegion7" -->
             <div class="12u" id='botao_bloqueio'> 
              	<?php
				if(isset($_SESSION['ct_admin'])){
				
					$userDAO = new UserDAO();
															
					$user = $userDAO->getId($infoCandidato[0]->id_user);
					
					if($user[0]->status == 0){
						?>                	
    	            	<a class="button submit" onClick='getId("Controller/Admin.controller.php?op=<?php echo sha1(4) ?>&id=<?php echo base64_encode($infoCandidato[0]->id_user) ?>&status=<?php echo base64_encode(1) ?>&tp=user","botao_bloqueio")'>Bloquear usuário</a> 		                
						<?php
					}else{
						?>                	
    	            	<a class="button submit" onClick='getId("Controller/Admin.controller.php?op=<?php echo sha1(4) ?>&id=<?php echo base64_encode($infoCandidato[0]->id_user) ?>&status=<?php echo base64_encode(0) ?>&tp=user","botao_bloqueio")'>Liberar usuário</a> 		                
						<?php
					}										
				}else{
				?>
                	<a class="button submit" onclick='getId("Controller/Vaga.controller.php?op=10","alerta")'>Convidar para uma vaga</a> 
                <?php
				}
				?>
              </div>
              <!-- InstanceEndEditable -->
          </div>
        
      </div> 
      
    </div>
  </section>    
 
</div>
<!-- Footer -->
<div id="footer">
  <!-- Copyright -->
  <div class="copyright">
    <ul class="menu">
      <li>AST Facilities.</li>
    </ul>
  </div>
</div>
</body>
<!-- InstanceEnd --></html>
