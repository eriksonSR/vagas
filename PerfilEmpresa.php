<?php
session_start();
include 'Banco/Conexao.class.php';
include 'Model/User.class.php';
include 'Model/InfoEmpresa.class.php';
include 'Persistencia/InfoEmpresaDAO.class.php';
include 'Model/Recrutador.class.php';
include 'Persistencia/RecrutadorDAO.class.php';
include 'Model/Vaga.class.php';
include 'Persistencia/VagaDAO.class.php';
include 'Model/InfoCandidato.class.php';
include 'Persistencia/InfoCandidatoDAO.class.php';

if(isset($_SESSION['ct_empresa'])){
	
	$user = unserialize($_SESSION['ct_empresa']);
	
	$infoEmpresaDAO = new InfoEmpresaDAO();
	
	$infoEmpresa = $infoEmpresaDAO->getUser($user[0]->id);
			
}else{
	session_unset();
	header('location:index.php');
}
?>
<!DOCTYPE HTML>
<html>
<head>
<title>Bem-vindo</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="css/table/table.css">
<link href="css/table/style.css" rel="stylesheet" type="text/css" />
<link href="css/table/form/screen.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="css/table/tabulacao.css">

<!--[if lte IE 8]><script src="js/html5shiv.js"></script><![endif]-->
<script src="js/request/AjaxRequest.js"></script>
<script src="js/jquery.min.js"></script>
<script src="js/skel.min.js"></script>
<script src="js/skel-panels.min.js"></script>
<script src="js/init.js"></script>
<script>
function mascara(o,f){
	v_obj=o 
	v_fun=f
	setTimeout("execmascara()",1)
}

function execmascara(){
	v_obj.value=v_fun(v_obj.value)
}

function leech(v){
	v=v.replace(/o/gi,"0")
	v=v.replace(/i/gi,"1")
	v=v.replace(/z/gi,"2")
	v=v.replace(/e/gi,"3")
	v=v.replace(/a/gi,"4")
	v=v.replace(/s/gi,"5")
	v=v.replace(/t/gi,"7")
	return v
}

///////////////////////////////////////////////////

function numeros(v){
   return v.replace(/\D/g,"")
}

function telefone(v){
	v=v.replace(/\D/g,"")                 //Remove tudo o que não é dígito
	v=v.replace(/^(\d\d)(\d)/g,"($1) $2") //Coloca parênteses em volta dos dois primeiros dígitos
	v=v.replace(/(\d{4})(\d)/,"$1-$2")    //Coloca hífen entre o quarto e o quinto dígitos
	return v
}       

function data(v){
	v=v.replace(/\D/g,"")                    //Remove tudo o que não é dígito
	v=v.replace(/(\d{2})(\d)/,"$1/$2")       //Coloca um ponto entre o terceiro e o quarto dígitos
	v=v.replace(/(\d{2})(\d)/,"$1/$2")       //Coloca um ponto entre o terceiro e o quarto dígitos
											 //de novo (para o segundo bloco de números)
	return v
}

function cpf(v){ 
	v=v.replace(/\D/g,"")                    //Remove tudo o que não é dígito 
	v=v.replace(/(\d{3})(\d)/,"$1.$2")       //Coloca um ponto entre o terceiro e o quarto dígitos 
	v=v.replace(/(\d{3})(\d)/,"$1.$2")       //Coloca um ponto entre o terceiro e o quarto dígitos 
											 //de novo (para o segundo bloco de números) 
	v=v.replace(/(\d{3})(\d{1,2})$/,"$1-$2") //Coloca um hífen entre o terceiro e o quarto dígitos 
	return v 
} 

function moeda(z){  
	v = z.value;
	v=v.replace(/\D/g,"")  //permite digitar apenas números
	v=v.replace(/[0-9]{12}/,"inválido")   //limita pra máximo 999.999.999,99
	v=v.replace(/(\d{1})(\d{8})$/,"$1.$2")  //coloca ponto antes dos últimos 8 digitos
	v=v.replace(/(\d{1})(\d{5})$/,"$1.$2")  //coloca ponto antes dos últimos 5 digitos
	v=v.replace(/(\d{1})(\d{1,2})$/,"$1,$2")	//coloca virgula antes dos últimos 2 digitos
		z.value = v;
}


</script>

<!-- Popup utilizada no primeiro login depois do cadastro -->
<style>
#popup_pf {
	display: none; /* Hide the DIV */
	position: absolute;
	left: 47%;
	top: 4%;
	width: 350px;
	background: #FFFFFF;
	z-index: 100; /* Layering ( on-top of others), if you have lots of layers: I just maximized, you can change it yourself */
	/* additional features, can be omitted */
	border: 2px solid #CCCCCC;
	padding: 15px;
	font-size: 15px;
	-moz-box-shadow: 0 0 5px #CCCCCC;
	-webkit-box-shadow: 0 0 5px #CCCCCC;
	box-shadow: 0 0 2px #999999;
}
</style>
<script type="text/javascript"> 
	
$(document).ready( function() {
	
	// When site loaded, load the Popupbox First
	loadPopupBox();

	$('#popupBoxClose').click( function() {			
		unloadPopupBox();									  			 
	});
	
	function unloadPopupBox() {	// TO Unload the Popupbox
		$('#popup_pf').fadeOut("slow");											 		
	}	
	
	function loadPopupBox() {	// To Load the Popupbox				
		$('#popup_pf').fadeIn("slow");
	}
	/**********************************************************/

});
</script>
<!-- Fim da popup -->

<noscript>
<link rel="stylesheet" href="css/skel-noscript.css" />
<link rel="stylesheet" href="css/style.css" />
<link rel="stylesheet" href="css/style-wide.css" />
</noscript>
<!--[if lte IE 9]><link rel="stylesheet" href="css/ie9.css" /><![endif]-->
<!--[if lte IE 8]><link rel="stylesheet" href="css/ie8.css" /><![endif]-->
</head><body>
<div id='alerta'>
  <?php
if(isset($_SESSION['aviso_login'])){
	echo "<div id='popup_pf'>                    
			 <a id='popupBoxClose'>Fechar</a>
			 ".$_SESSION['aviso_login']."
		  </div>";
	
	unset($_SESSION['aviso_login']);
}
?>
</div>

<?php
if(isset($_SESSION['filtro_vaga_emp'])){
	include '../Include/ListVaga.view.php';	
}
?>
<!-- Header -->
<div id="header" class="skel-panels-fixed">
  <div class="top">
    <div id="logo">
      <h1 id="title">
      <?php
        if(count($infoEmpresa) > 0){
            if(strlen($infoEmpresa[0]->nome) > 0){
                echo $infoEmpresa[0]->nome;
            }else{								
                $posicao = strpos($user[0]->email, '@');								
                echo substr($user[0]->email, 0, $posicao);
            }
        }else{			
            $posicao = strpos($user[0]->email, '@');								
            echo substr($user[0]->email, 0, $posicao);
        }
      ?>
      <span class="byline"> <a onClick='getId("Controller/Cadastro.controller.php?op=1","alerta")' style="font-weight:bold;" href="#">Alterar senha</a> </span> </div>
    <!-- Nav -->
    <nav id="nav">
      <ul>
        <li><a href="#top" id="top-link" class="skel-panels-ignoreHref"><span class="fa fa-home">Home</span></a></li>
        <li><a href="#info" id="info-link" class="skel-panels-ignoreHref"><span class="fa fa-check">Informações da empresa</span></a></li>
        <!-- <li><a href="#recrutador" id="vaga-link" class="skel-panels-ignoreHref"><span class="fa fa-list-alt">Gestor de Recrutador</span></a></li> -->
        <li><a href="#vaga" id="vaga-link" class="skel-panels-ignoreHref"><span class="fa fa-list-alt">Gestor de vagas</span></a></li>
        <li><a href="#candidato" id="candidato-link" class="skel-panels-ignoreHref"><span class="fa fa-search">Pesquisar candidatos</span></a></li>
        <li style="border-bottom:1px #999999 groove; margin-top:7px;"> </li>
        <li><a href="#vaga_pendente" id="candidato-link" class="skel-panels-ignoreHref"><span class="fa fa-search">Vagas pendentes</span></a></li>
        <li><a href="Controller/Logoff.controller.php"><span class="fa fa-lock">Sair do sistema</span></a></li>
      </ul>
    </nav>
  </div>
  <!--
  <div class="bottom">
    <ul class="icons">
      <li><a href="#" class="fa fa-facebook solo"><span>Facebook</span></a></li>
      <li><a href="#" class="fa fa-envelope solo"><span>Email</span></a></li>
    </ul>
  </div>
  --> 
</div>
<!-- Main -->
<div id="main">

<!-- Intro -->
<section id="top" class="one">
  <div class="container"> <br />
    <?php		
		if(isset($_SESSION['ct_admin'])){        
			echo "<header>
					<h2 class='alt'><a href='Controller/Admin.controller.php?op=".sha1(6)."'> Voltar ao <strong>perfil inicial </a></strong> </h2>
				  </header>
				  <p>Utilize o link a cima para retornoar ao seu perfil.</p>";
        
		}else{
			echo "<header>
					<h2 class='alt'><strong>Bem vindo</strong> </h2>
				  </header>
				  <p>Obrigado por participar do Portal Estágios, um serviço que conecta jovens com vontade de crescer às vagas das melhores empresas do mercado!.</p>";
		}
	   ?>
  </div>
</section>

<!-- Contact -->
<section id="info" class="two">
  <div class="container">
    <header>
      <h2>Algumas informações da empresa</h2>
    </header>
    <form onSubmit="return false" id="info_candidato">
      <header>
        <div class="row half">
          <div class="6u">
            <input  type="text" class="text" name="txtRazaoSocial" placeholder="Razão social" onBlur='cadastroRapido("Controller/Empresa.controller.php?op=1","alerta","info_candidato")' value="<?php 			
			if(count($infoEmpresa) > 0){
				echo $infoEmpresa[0]->razao_social;		
			}
			?>"/>
          </div>
          <div class="6u">
            <input type="text" class="text" name="txtNome" placeholder="Nome fantasia" onBlur='cadastroRapido("Controller/Empresa.controller.php?op=1","alerta","info_candidato")' value="<?php 			
			if(count($infoEmpresa) > 0){
				echo $infoEmpresa[0]->nome;
			}			
			?>"/>
          </div>
        </div>
        <div class="row half">
          <div class="6u">
            <input  type="text" class="text" name="txtRamu" placeholder="Ramu de atuação" onBlur='cadastroRapido("Controller/Empresa.controller.php?op=1","alerta","info_candidato")' value="<?php 			
			if(count($infoEmpresa) > 0){
				echo $infoEmpresa[0]->ramu_atuacao;
			}		
			?>"/>
          </div>
          <div class="6u">
            <select name="cbPorte" onChange='cadastroRapido("Controller/Empresa.controller.php?op=1","alerta","info_candidato")'>
              <?php
				$porte = array('Pequeno','Médio','Grande');
				
				if(count($infoEmpresa) > 0 && strlen($infoEmpresa[0]->porte) > 0){	
					echo "<option value='".$infoEmpresa[0]->porte."'>".$infoEmpresa[0]->porte."</option>";
				
					foreach($porte as $pt){
						if($pt != $infoEmpresa[0]->porte){								
							echo "<option value='".$pt."'>".$pt."</option>";								
						} 
					}
				}else{								
					echo "<option value=''>Porte da empresa</option>
                		  <option value='Pequeno'>Pequeno</option>
						  <option value='Médio'>Médio</option>
               			  <option value='Grande'>Grande</option>";
				} 
			   ?>
            </select>
          </div>
        </div>
        <div class="row half">
          <div class="6u">
            <input type="text" class="text" name="txtSite" placeholder="Website" onBlur='cadastroRapido("Controller/Empresa.controller.php?op=1","alerta","info_candidato")' value="<?php 			
			if(count($infoEmpresa) > 0){
				echo $infoEmpresa[0]->site;
			}			
			?>"/>
          </div>
          <div class="6u">
            <input  type="text" class="text" name="txtEmail" placeholder="E-mail" onBlur='cadastroRapido("Controller/Empresa.controller.php?op=1","alerta","info_candidato")' value="<?php 			
			echo $user[0]->email;		
			?>"/>
          </div>
        </div>
        <div class="row half">
          <div class="6u">
            <input onkeypress='mascara(this,telefone)' maxlength='14' type="text" class="text" name="txtTel" placeholder="Telefone" onBlur='cadastroRapido("Controller/Empresa.controller.php?op=1","alerta","info_candidato")' value="<?php 			
			if(count($infoEmpresa) > 0){
				echo $infoEmpresa[0]->tel;
			}			
			?>"/>
          </div>
          <div class="6u">
            <input onkeypress='mascara(this,telefone)' maxlength='14' type="text" class="text" name="txtCel" placeholder="Celular" onBlur='cadastroRapido("Controller/Empresa.controller.php?op=1","alerta","info_candidato")' value="<?php 			
			if(count($infoEmpresa) > 0){
				echo $infoEmpresa[0]->cel;
			}			
			?>"/>
          </div>
        </div>
        <div class="row half">
          <div class="6u">
            <input type="text" class="text" name="txtFace" placeholder="Facebook" onBlur='cadastroRapido("Controller/Empresa.controller.php?op=1","alerta","info_candidato")' value="<?php 			
			if(count($infoEmpresa) > 0){
				echo $infoEmpresa[0]->facebook;
			}			
			?>"/>
          </div>
          <div class="6u">
            <input  type="text" class="text" name="txtTwitter" placeholder="Twitter" onBlur='cadastroRapido("Controller/Empresa.controller.php?op=1","alerta","info_candidato")' value="<?php 			
			if(count($infoEmpresa) > 0){
				echo $infoEmpresa[0]->twitter;
			}			
			?>"/>
          </div>
        </div>
      </header>
      <header>
        <h2>Representante do RH</h2>
        <p>As suas informações de contato são fundamentais para que possamos nos comunicar com você! Por favor, mantenha seus dados atualizados. Estas informações não são divulgadas aos candidatos cadastrados.</p>
      </header>
      <header>
        <div class="row half">
          <div class="6u">
            <input  type="text" class="text" name="txtNomeRh" placeholder="Representante do RH" onBlur='cadastroRapido("Controller/Empresa.controller.php?op=1","alerta","info_candidato")' value="<?php 			
			if(count($infoEmpresa) > 0){
				echo $infoEmpresa[0]->nome_rh;
			}	
			?>"/>
          </div>
          <div class="6u">
            <input type="text" class="text" name="txtTelRh" placeholder="Telefone" onBlur='cadastroRapido("Controller/Empresa.controller.php?op=1","alerta","info_candidato")' value="<?php 			
			if(count($infoEmpresa) > 0){
				echo $infoEmpresa[0]->tel_rh;
			}			
			?>"/>
          </div>
        </div>
        <div class="row half">
          <div class="6u">
            <input type="text" class="text" name="txtCelRh" placeholder="Celular" onBlur='cadastroRapido("Controller/Empresa.controller.php?op=1","alerta","info_candidato")' value="<?php 			
			if(count($infoEmpresa) > 0){
				echo $infoEmpresa[0]->cel_rh;
			}			
			?>"/>
          </div>
          <div class="6u">
            <input  type="text" class="text" name="txtEmailRh" placeholder="E-mail" onBlur='cadastroRapido("Controller/Empresa.controller.php?op=1","alerta","info_candidato")' value="<?php 			
			if(count($infoEmpresa) > 0){
				echo $infoEmpresa[0]->email_rh;
			}			
			?>"/>
          </div>
        </div>
      </header>
      <header>
        <h2>Endereço da empresa</h2>
        <p>Seus dados de contato são essenciais para que a administração do Portal Estágio possa entrar em contato com você!.</p>
      </header>
      <header>
        <div class="row half">
          <div class="6u">
            <input type="text" class="text" name="txtEstado" placeholder="Estado" onBlur='cadastroRapido("Controller/Empresa.controller.php?op=1","alerta","info_candidato")' value="Rio Grande do Sul" disabled/>
          </div>
          <div class="6u">
            <select name="txtCidade" onChange='cadastroRapido("Controller/Empresa.controller.php?op=1","alerta","info_candidato")'>
              <?php 			
			  if(count($infoEmpresa) > 0){
			  	echo "<option value='".$infoEmpresa[0]->cidade."'>".$infoEmpresa[0]->cidade."</option>";;
			  }else{
			  	echo "<option value=''>Selecione a cidade</option>";
			  }		
			  ?>
              <?php 			
			  if(count($infoEmpresa) > 0){
			  	
			  	if($infoEmpresa[0]->cidade != "Porto Alegre"){
					echo '<option value="Porto Alegre">Porto Alegre</option>';
				}
			  }else{			  	
			  	echo '<option value="Porto Alegre">Porto Alegre</option>';
			  }		
			  ?>
              <option value="Aceguá">Aceguá</option>
              <option value="Afonso Rodrigues">Afonso Rodrigues</option>
              <option value="Aguapés">Aguapés</option>
              <option value="Água Santa">Água Santa</option>
              <option value="Águas Claras">Águas Claras</option>
              <option value="Agudo">Agudo</option>
              <option value="Ajuricaba">Ajuricaba</option>
              <option value="Albardão">Albardão</option>
              <option value="Alecrim">Alecrim</option>
              <option value="Alegrete">Alegrete</option>
              <option value="Alegria">Alegria</option>
              <option value="Alfredo Brenner">Alfredo Brenner</option>
              <option value="Almirante Tamandaré do Sul">Almirante Tamandaré do Sul</option>
              <option value="Alpestre">Alpestre</option>
              <option value="Alto Alegre">Alto Alegre</option>
              <option value="Alto da União">Alto da União</option>
              <option value="Alto Feliz">Alto Feliz</option>
              <option value="Alto Paredão">Alto Paredão</option>
              <option value="Alto Recreio">Alto Recreio</option>
              <option value="Alto Uruguai">Alto Uruguai</option>
              <option value="Alvorada">Alvorada</option>
              <option value="Amaral Ferrador">Amaral Ferrador</option>
              <option value="Ametista do Sul">Ametista do Sul</option>
              <option value="André da Rocha">André da Rocha</option>
              <option value="Anta Gorda">Anta Gorda</option>
              <option value="Antônio Kerpel">Antônio Kerpel</option>
              <option value="Antônio Prado">Antônio Prado</option>
              <option value="Arambaré">Arambaré</option>
              <option value="Araricá">Araricá</option>
              <option value="Aratiba">Aratiba</option>
              <option value="Arco-Íris">Arco-Íris</option>
              <option value="Arco Verde">Arco Verde</option>
              <option value="Arroio Canoas">Arroio Canoas</option>
              <option value="Arroio do Meio">Arroio do Meio</option>
              <option value="Arroio do Padre">Arroio do Padre</option>
              <option value="Arroio do Sal">Arroio do Sal</option>
              <option value="Arroio do Só">Arroio do Só</option>
              <option value="Arroio dos Ratos">Arroio dos Ratos</option>
              <option value="Arroio do Tigre">Arroio do Tigre</option>
              <option value="Arroio Grande">Arroio Grande</option>
              <option value="Árvore Só">Árvore Só</option>
              <option value="Arvorezinha">Arvorezinha</option>
              <option value="Atafona">Atafona</option>
              <option value="Atiaçu">Atiaçu</option>
              <option value="Augusto Pestana">Augusto Pestana</option>
              <option value="Áurea">Áurea</option>
              <option value="Avelino Paranhos">Avelino Paranhos</option>
              <option value="Azevedo Sodré">Azevedo Sodré</option>
              <option value="Bacupari">Bacupari</option>
              <option value="Bagé">Bagé</option>
              <option value="Baliza">Baliza</option>
              <option value="Balneário Pinhal">Balneário Pinhal</option>
              <option value="Banhado do Colégio">Banhado do Colégio</option>
              <option value="Barão de Cotegipe">Barão de Cotegipe</option>
              <option value="Barão do Triunfo">Barão do Triunfo</option>
              <option value="Barão">Barão</option>
              <option value="Barracão">Barracão</option>
              <option value="Barra do Guarita">Barra do Guarita</option>
              <option value="Barra do Ouro">Barra do Ouro</option>
              <option value="Barra do Quaraí">Barra do Quaraí</option>
              <option value="Barra do Ribeiro">Barra do Ribeiro</option>
              <option value="Barra do Rio Azul">Barra do Rio Azul</option>
              <option value="Barra Funda">Barra Funda</option>
              <option value="Barreirinho">Barreirinho</option>
              <option value="Barreiro">Barreiro</option>
              <option value="Barro Preto">Barro Preto</option>
              <option value="Barros Cassal">Barros Cassal</option>
              <option value="Barro Vermelho">Barro Vermelho</option>
              <option value="Basílio">Basílio</option>
              <option value="Bela Vista">Bela Vista</option>
              <option value="Beluno">Beluno</option>
              <option value="Benjamin Constant do Sul">Benjamin Constant do Sul</option>
              <option value="Bento Gonçalves">Bento Gonçalves</option>
              <option value="Bexiga">Bexiga</option>
              <option value="Boa Esperança">Boa Esperança</option>
              <option value="Boa Vista das Missões">Boa Vista das Missões</option>
              <option value="Boa Vista do Buricá">Boa Vista do Buricá</option>
              <option value="Boa Vista do Cadeado">Boa Vista do Cadeado</option>
              <option value="Boa Vista do Incra">Boa Vista do Incra</option>
              <option value="Boa Vista do Sul">Boa Vista do Sul</option>
              <option value="Boa Vista">Boa Vista</option>
              <option value="Boca do Monte">Boca do Monte</option>
              <option value="Boi Preto">Boi Preto</option>
              <option value="Bojuru">Bojuru</option>
              <option value="Bom Jardim">Bom Jardim</option>
              <option value="Bom Jesus">Bom Jesus</option>
              <option value="Bom Princípio">Bom Princípio</option>
              <option value="Bom Progresso">Bom Progresso</option>
              <option value="Bom Retiro do Guaíba">Bom Retiro do Guaíba</option>
              <option value="Bom Retiro do Sul">Bom Retiro do Sul</option>
              <option value="Bom Retiro">Bom Retiro</option>
              <option value="Bonito">Bonito</option>
              <option value="Boqueirão do Leão">Boqueirão do Leão</option>
              <option value="Boqueirão">Boqueirão</option>
              <option value="Bororé">Bororé</option>
              <option value="Borussia">Borussia</option>
              <option value="Bossoroca">Bossoroca</option>
              <option value="Botucaraí">Botucaraí</option>
              <option value="Bozano">Bozano</option>
              <option value="Braga">Braga</option>
              <option value="Brochier">Brochier</option>
              <option value="Buriti">Buriti</option>
              <option value="Butiá">Butiá</option>
              <option value="Butiás">Butiás</option>
              <option value="Caçapava do Sul">Caçapava do Sul</option>
              <option value="Cacequi">Cacequi</option>
              <option value="Cachoeira do Sul">Cachoeira do Sul</option>
              <option value="Cachoeirinha">Cachoeirinha</option>
              <option value="Cacique Doble">Cacique Doble</option>
              <option value="Cadorna">Cadorna</option>
              <option value="Caibaté">Caibaté</option>
              <option value="Caiçara">Caiçara</option>
              <option value="Camaquã">Camaquã</option>
              <option value="Camargo">Camargo</option>
              <option value="Cambará do Sul">Cambará do Sul</option>
              <option value="Campestre Baixo">Campestre Baixo</option>
              <option value="Campestre da Serra">Campestre da Serra</option>
              <option value="Campina das Missões">Campina das Missões</option>
              <option value="Campina Redonda">Campina Redonda</option>
              <option value="Campinas do Sul">Campinas do Sul</option>
              <option value="Campinas">Campinas</option>
              <option value="Campo Bom">Campo Bom</option>
              <option value="Campo Branco">Campo Branco</option>
              <option value="Campo do Meio">Campo do Meio</option>
              <option value="Campo Novo">Campo Novo</option>
              <option value="Campo Santo">Campo Santo</option>
              <option value="Campos Borges">Campos Borges</option>
              <option value="Campo Seco">Campo Seco</option>
              <option value="Campo Vicente">Campo Vicente</option>
              <option value="Candelária">Candelária</option>
              <option value="Cândido Freire">Cândido Freire</option>
              <option value="Cândido Godói">Cândido Godói</option>
              <option value="Candiota">Candiota</option>
              <option value="Canela">Canela</option>
              <option value="Canguçu">Canguçu</option>
              <option value="Canhembora">Canhembora</option>
              <option value="Canoas">Canoas</option>
              <option value="Canudos do Vale">Canudos do Vale</option>
              <option value="Capané">Capané</option>
              <option value="Capão Bonito do Sul">Capão Bonito do Sul</option>
              <option value="Capão Bonito">Capão Bonito</option>
              <option value="Capão Comprido">Capão Comprido</option>
              <option value="Capão da Canoa">Capão da Canoa</option>
              <option value="Capão da Porteira">Capão da Porteira</option>
              <option value="Capão do Cedro">Capão do Cedro</option>
              <option value="Capão do Cipó">Capão do Cipó</option>
              <option value="Capão do Leão">Capão do Leão</option>
              <option value="Capela de Santana">Capela de Santana</option>
              <option value="Capela Velha">Capela Velha</option>
              <option value="Capinzal">Capinzal</option>
              <option value="Capitão">Capitão</option>
              <option value="Capivari do Sul">Capivari do Sul</option>
              <option value="Capivarita">Capivarita</option>
              <option value="Capoeira Grande">Capoeira Grande</option>
              <option value="Capo-Erê">Capo-Erê</option>
              <option value="Caraá">Caraá</option>
              <option value="Carajá Seival">Carajá Seival</option>
              <option value="Carazinho">Carazinho</option>
              <option value="Carlos Barbosa">Carlos Barbosa</option>
              <option value="Carlos Gomes">Carlos Gomes</option>
              <option value="Carovi">Carovi</option>
              <option value="Casca">Casca</option>
              <option value="Cascata">Cascata</option>
              <option value="Caseiros">Caseiros</option>
              <option value="Castelinho">Castelinho</option>
              <option value="Catimbau">Catimbau</option>
              <option value="Catuípe">Catuípe</option>
              <option value="Cavajureta">Cavajureta</option>
              <option value="Caverá">Caverá</option>
              <option value="Caxias do Sul">Caxias do Sul</option>
              <option value="Cazuza Ferreira">Cazuza Ferreira</option>
              <option value="Cedro Marcado">Cedro Marcado</option>
              <option value="Centenário">Centenário</option>
              <option value="Centro Linha Brasil">Centro Linha Brasil</option>
              <option value="Cerrito Alegre">Cerrito Alegre</option>
              <option value="Cerrito do Ouro ou Vila do Cerrito">Cerrito do Ouro ou Vila do Cerrito</option>
              <option value="Cerrito">Cerrito</option>
              <option value="Cerro Alto">Cerro Alto</option>
              <option value="Cerro Branco">Cerro Branco</option>
              <option value="Cerro Claro">Cerro Claro</option>
              <option value="Cerro do Martins">Cerro do Martins</option>
              <option value="Cerro do Roque">Cerro do Roque</option>
              <option value="Cerro Grande do Sul">Cerro Grande do Sul</option>
              <option value="Cerro Grande">Cerro Grande</option>
              <option value="Cerro Largo">Cerro Largo</option>
              <option value="Chapada">Chapada</option>
              <option value="Charqueadas">Charqueadas</option>
              <option value="Charrua">Charrua</option>
              <option value="Chiapetta">Chiapetta</option>
              <option value="Chicolomã">Chicolomã</option>
              <option value="Chimarrão">Chimarrão</option>
              <option value="Chorão">Chorão</option>
              <option value="Chuí">Chuí</option>
              <option value="Chuvisca">Chuvisca</option>
              <option value="Cidreira">Cidreira</option>
              <option value="Cinqüentenário">Cinqüentenário</option>
              <option value="Ciríaco">Ciríaco</option>
              <option value="Clara">Clara</option>
              <option value="Clemente Argolo">Clemente Argolo</option>
              <option value="Coimbra">Coimbra</option>
              <option value="Colinas">Colinas</option>
              <option value="Colônia das Almas">Colônia das Almas</option>
              <option value="Colônia Medeiros">Colônia Medeiros</option>
              <option value="Colônia Municipal">Colônia Municipal</option>
              <option value="Colônia Nova">Colônia Nova</option>
              <option value="Colônia São João">Colônia São João</option>
              <option value="Colônia Z-3">Colônia Z-3</option>
              <option value="Coloninha">Coloninha</option>
              <option value="Colorado">Colorado</option>
              <option value="Comandai">Comandai</option>
              <option value="Condor">Condor</option>
              <option value="Consolata">Consolata</option>
              <option value="Constantina">Constantina</option>
              <option value="Coqueiro Baixo">Coqueiro Baixo</option>
              <option value="Coqueiros do Sul">Coqueiros do Sul</option>
              <option value="Cordilheira">Cordilheira</option>
              <option value="Coroados">Coroados</option>
              <option value="Coronel Barros">Coronel Barros</option>
              <option value="Coronel Bicaco">Coronel Bicaco</option>
              <option value="Coronel Finzito">Coronel Finzito</option>
              <option value="Coronel Pilar">Coronel Pilar</option>
              <option value="Coronel Teixeira">Coronel Teixeira</option>
              <option value="Cortado">Cortado</option>
              <option value="Costa da Cadeia">Costa da Cadeia</option>
              <option value="Costão">Costão</option>
              <option value="Cotiporã">Cotiporã</option>
              <option value="Coxilha Grande">Coxilha Grande</option>
              <option value="Coxilha">Coxilha</option>
              <option value="Cr-1">Cr-1</option>
              <option value="Crissiumal">Crissiumal</option>
              <option value="Cristal do Sul">Cristal do Sul</option>
              <option value="Cristal">Cristal</option>
              <option value="Criúva">Criúva</option>
              <option value="Cruz Alta">Cruz Alta</option>
              <option value="Cruzaltense">Cruzaltense</option>
              <option value="Cruzeiro">Cruzeiro</option>
              <option value="Cruzeiro do Sul">Cruzeiro do Sul</option>
              <option value="Curral Alto">Curral Alto</option>
              <option value="Curumim">Curumim</option>
              <option value="Daltro Filho">Daltro Filho</option>
              <option value="Dário Lassance">Dário Lassance</option>
              <option value="David Canabarro">David Canabarro</option>
              <option value="Delfina">Delfina</option>
              <option value="Deodoro">Deodoro</option>
              <option value="Depósito">Depósito</option>
              <option value="Derrubadas">Derrubadas</option>
              <option value="Dezesseis de Novembro">Dezesseis de Novembro</option>
              <option value="Dilermando de Aguiar">Dilermando de Aguiar</option>
              <option value="Divino">Divino</option>
              <option value="Dois Irmãos das Missões">Dois Irmãos das Missões</option>
              <option value="Dois Irmãos">Dois Irmãos</option>
              <option value="Dois Lajeados">Dois Lajeados</option>
              <option value="Dom Feliciano">Dom Feliciano</option>
              <option value="Dom Pedrito">Dom Pedrito</option>
              <option value="Dom Pedro de Alcântara">Dom Pedro de Alcântara</option>
              <option value="Dona Francisca">Dona Francisca</option>
              <option value="Dona Otília">Dona Otília</option>
              <option value="Dourado">Dourado</option>
              <option value="Doutor Edgardo Pereira Velho">Doutor Edgardo Pereira Velho</option>
              <option value="Doutor Maurício Cardoso">Doutor Maurício Cardoso</option>
              <option value="Doutor Ricardo">Doutor Ricardo</option>
              <option value="Durasnal">Durasnal</option>
              <option value="Eldorado do Sul">Eldorado do Sul</option>
              <option value="Eletra">Eletra</option>
              <option value="Encantado">Encantado</option>
              <option value="Encruzilhada do Sul">Encruzilhada do Sul</option>
              <option value="Encruzilhada">Encruzilhada</option>
              <option value="Engenho Velho">Engenho Velho</option>
              <option value="Entre-Ijuís">Entre-Ijuís</option>
              <option value="Entrepelado">Entrepelado</option>
              <option value="Entre Rios do Sul">Entre Rios do Sul</option>
              <option value="Erebango">Erebango</option>
              <option value="Erechim">Erechim</option>
              <option value="Ernestina">Ernestina</option>
              <option value="Ernesto Alves">Ernesto Alves</option>
              <option value="Erval Grande">Erval Grande</option>
              <option value="Erval Seco">Erval Seco</option>
              <option value="Erveiras">Erveiras</option>
              <option value="Esmeralda">Esmeralda</option>
              <option value="Esperança do Sul">Esperança do Sul</option>
              <option value="Esperança">Esperança</option>
              <option value="Espigão Alto">Espigão Alto</option>
              <option value="Espigão">Espigão</option>
              <option value="Espinilho Grande">Espinilho Grande</option>
              <option value="Espírito Santo">Espírito Santo</option>
              <option value="Espumoso">Espumoso</option>
              <option value="Esquina Araújo">Esquina Araújo</option>
              <option value="Esquina Bom Sucesso">Esquina Bom Sucesso</option>
              <option value="Esquina Gaúcha">Esquina Gaúcha</option>
              <option value="Esquina Ipiranga">Esquina Ipiranga</option>
              <option value="Esquina Piratini">Esquina Piratini</option>
              <option value="Estação">Estação</option>
              <option value="Estância Grande">Estância Grande</option>
              <option value="Estância Velha">Estância Velha</option>
              <option value="Esteio">Esteio</option>
              <option value="Esteira">Esteira</option>
              <option value="Estreito">Estreito</option>
              <option value="Estrela">Estrela</option>
              <option value="Estrela Velha">Estrela Velha</option>
              <option value="Eugênio de Castro">Eugênio de Castro</option>
              <option value="Evangelista">Evangelista</option>
              <option value="Fagundes Varela">Fagundes Varela</option>
              <option value="Fão">Fão</option>
              <option value="Faria Lemos">Faria Lemos</option>
              <option value="Farinhas">Farinhas</option>
              <option value="Farrapos">Farrapos</option>
              <option value="Farroupilha">Farroupilha</option>
              <option value="Faxinal do Soturno">Faxinal do Soturno</option>
              <option value="Faxinal">Faxinal</option>
              <option value="Faxinalzinho">Faxinalzinho</option>
              <option value="Fazenda Fialho">Fazenda Fialho</option>
              <option value="Fazenda Souza">Fazenda Souza</option>
              <option value="Fazenda Vilanova">Fazenda Vilanova</option>
              <option value="Feliz">Feliz</option>
              <option value="Ferreira">Ferreira</option>
              <option value="Flores da Cunha">Flores da Cunha</option>
              <option value="Floresta">Floresta</option>
              <option value="Floriano Peixoto">Floriano Peixoto</option>
              <option value="Flórida">Flórida</option>
              <option value="Fontoura Xavier">Fontoura Xavier</option>
              <option value="Formigueiro">Formigueiro</option>
              <option value="Formosa">Formosa</option>
              <option value="Forninho">Forninho</option>
              <option value="Forquetinha">Forquetinha</option>
              <option value="Fortaleza dos Valos">Fortaleza dos Valos</option>
              <option value="Frederico Westphalen">Frederico Westphalen</option>
              <option value="Freire">Freire</option>
              <option value="Frei Sebastião">Frei Sebastião</option>
              <option value="Garibaldina">Garibaldina</option>
              <option value="Garibaldi">Garibaldi</option>
              <option value="Garruchos">Garruchos</option>
              <option value="Gaurama">Gaurama</option>
              <option value="General Câmara">General Câmara</option>
              <option value="Gentil">Gentil</option>
              <option value="Getúlio Vargas">Getúlio Vargas</option>
              <option value="Giruá">Giruá</option>
              <option value="Glória">Glória</option>
              <option value="Glorinha">Glorinha</option>
              <option value="Goio-En">Goio-En</option>
              <option value="Gramado dos Loureiros">Gramado dos Loureiros</option>
              <option value="Gramado">Gramado</option>
              <option value="Gramado São Pedro">Gramado São Pedro</option>
              <option value="Gramado Xavier">Gramado Xavier</option>
              <option value="Gravataí">Gravataí</option>
              <option value="Guabiju">Guabiju</option>
              <option value="Guaíba">Guaíba</option>
              <option value="Guajuviras">Guajuviras</option>
              <option value="Guaporé">Guaporé</option>
              <option value="Guarani das Missões">Guarani das Missões</option>
              <option value="Guassu Boi">Guassu Boi</option>
              <option value="Guassupi">Guassupi</option>
              <option value="Harmonia">Harmonia</option>
              <option value="Herval">Herval</option>
              <option value="Herveiras">Herveiras</option>
              <option value="Hidráulica">Hidráulica</option>
              <option value="Horizontina">Horizontina</option>
              <option value="Hulha Negra">Hulha Negra</option>
              <option value="Humaitá">Humaitá</option>
              <option value="Ibarama">Ibarama</option>
              <option value="Ibaré">Ibaré</option>
              <option value="Ibiaçá">Ibiaçá</option>
              <option value="Ibiraiaras">Ibiraiaras</option>
              <option value="Ibirapuitã">Ibirapuitã</option>
              <option value="Ibirubá">Ibirubá</option>
              <option value="Igrejinha">Igrejinha</option>
              <option value="Ijucapirama">Ijucapirama</option>
              <option value="Ijuí">Ijuí</option>
              <option value="Ilha dos Marinheiros">Ilha dos Marinheiros</option>
              <option value="Ilópolis">Ilópolis</option>
              <option value="Imbé">Imbé</option>
              <option value="Imigrante">Imigrante</option>
              <option value="Independência">Independência</option>
              <option value="Inhacorá">Inhacorá</option>
              <option value="Inhanduí">Inhanduí</option>
              <option value="Ipê">Ipê</option>
              <option value="Ipiranga do Sul">Ipiranga do Sul</option>
              <option value="Ipiranga">Ipiranga</option>
              <option value="Ipuaçu">Ipuaçu</option>
              <option value="Iraí">Iraí</option>
              <option value="Iruí">Iruí</option>
              <option value="Itaara">Itaara</option>
              <option value="Itacolomi">Itacolomi</option>
              <option value="Itacurubi">Itacurubi</option>
              <option value="Itaimbezinho">Itaimbezinho</option>
              <option value="Itaí">Itaí</option>
              <option value="Itão">Itão</option>
              <option value="Itapororó">Itapororó</option>
              <option value="Itapuã">Itapuã</option>
              <option value="Itapucá">Itapucá</option>
              <option value="Itaqui">Itaqui</option>
              <option value="Itatiba do Sul">Itatiba do Sul</option>
              <option value="Itati">Itati</option>
              <option value="Itaúba">Itaúba</option>
              <option value="Ituim">Ituim</option>
              <option value="Ivaí">Ivaí</option>
              <option value="Ivorá">Ivorá</option>
              <option value="Ivoti">Ivoti</option>
              <option value="Jaboticaba">Jaboticaba</option>
              <option value="Jacuizinho">Jacuizinho</option>
              <option value="Jacutinga">Jacutinga</option>
              <option value="Jaguarão">Jaguarão</option>
              <option value="Jaguarete">Jaguarete</option>
              <option value="Jaguari">Jaguari</option>
              <option value="Jansen">Jansen</option>
              <option value="Jaquirana">Jaquirana</option>
              <option value="Jari">Jari</option>
              <option value="Jazidas ou Capela São Vicente">Jazidas ou Capela São Vicente</option>
              <option value="João Arregui">João Arregui</option>
              <option value="João Rodrigues">João Rodrigues</option>
              <option value="Joça Tavares">Joça Tavares</option>
              <option value="Jóia">Jóia</option>
              <option value="José Otávio">José Otávio</option>
              <option value="Juá">Juá</option>
              <option value="Júlio de Castilhos">Júlio de Castilhos</option>
              <option value="Lagoa Bonita do Sul">Lagoa Bonita do Sul</option>
              <option value="Lagoa dos Patos">Lagoa dos Patos</option>
              <option value="Lagoa dos Três Cantos">Lagoa dos Três Cantos</option>
              <option value="Lagoão">Lagoão</option>
              <option value="Lagoa Vermelha">Lagoa Vermelha</option>
              <option value="Lajeado Bonito">Lajeado Bonito</option>
              <option value="Lajeado Cerne">Lajeado Cerne</option>
              <option value="Lajeado do Bugre">Lajeado do Bugre</option>
              <option value="Lajeado Grande">Lajeado Grande</option>
              <option value="Lajeado">Lajeado</option>
              <option value="Lara">Lara</option>
              <option value="Laranjeira">Laranjeira</option>
              <option value="Lava-Pés">Lava-Pés</option>
              <option value="Lavras do Sul">Lavras do Sul</option>
              <option value="Leonel Rocha">Leonel Rocha</option>
              <option value="Liberato Salzano">Liberato Salzano</option>
              <option value="Lindolfo Collor">Lindolfo Collor</option>
              <option value="Linha Comprida">Linha Comprida</option>
              <option value="Linha Nova">Linha Nova</option>
              <option value="Linha Vitória">Linha Vitória</option>
              <option value="Loreto">Loreto</option>
              <option value="Maçambará">Maçambará</option>
              <option value="Machadinho">Machadinho</option>
              <option value="Magistério">Magistério</option>
              <option value="Mampituba">Mampituba</option>
              <option value="Manchinha">Manchinha</option>
              <option value="Mangueiras">Mangueiras</option>
              <option value="Manoel Viana">Manoel Viana</option>
              <option value="Maquiné">Maquiné</option>
              <option value="Maratá">Maratá</option>
              <option value="Marau">Marau</option>
              <option value="Marcelino Ramos">Marcelino Ramos</option>
              <option value="Marcorama">Marcorama</option>
              <option value="Mariana Pimentel">Mariana Pimentel</option>
              <option value="Mariano Moro">Mariano Moro</option>
              <option value="Mariante">Mariante</option>
              <option value="Mariápolis">Mariápolis</option>
              <option value="Marques de Souza">Marques de Souza</option>
              <option value="Matarazzo">Matarazzo</option>
              <option value="Mata">Mata</option>
              <option value="Mato Castelhano">Mato Castelhano</option>
              <option value="Mato Grande">Mato Grande</option>
              <option value="Mato Leitão">Mato Leitão</option>
              <option value="Mato Perso">Mato Perso</option>
              <option value="Mato Queimado">Mato Queimado</option>
              <option value="Mauá">Mauá</option>
              <option value="Maximiliano de Almeida">Maximiliano de Almeida</option>
              <option value="Medianeira">Medianeira</option>
              <option value="Minas do Leão">Minas do Leão</option>
              <option value="Miraguaia">Miraguaia</option>
              <option value="Miraguaí">Miraguaí</option>
              <option value="Mirim">Mirim</option>
              <option value="Montauri">Montauri</option>
              <option value="Monte Alegre">Monte Alegre</option>
              <option value="Monte Alegre dos Campos">Monte Alegre dos Campos</option>
              <option value="Monte Alverne">Monte Alverne</option>
              <option value="Monte Belo do Sul">Monte Belo do Sul</option>
              <option value="Monte Bonito">Monte Bonito</option>
              <option value="Montenegro">Montenegro</option>
              <option value="Mormaço">Mormaço</option>
              <option value="Morrinhos do Sul">Morrinhos do Sul</option>
              <option value="Morrinhos">Morrinhos</option>
              <option value="Morro Alto">Morro Alto</option>
              <option value="Morro Azul">Morro Azul</option>
              <option value="Morro Redondo">Morro Redondo</option>
              <option value="Morro Reuter">Morro Reuter</option>
              <option value="Morungava">Morungava</option>
              <option value="Mostardas">Mostardas</option>
              <option value="Muçum">Muçum</option>
              <option value="Muitos Capões">Muitos Capões</option>
              <option value="Muliterno">Muliterno</option>
              <option value="Não-Me-Toque">Não-Me-Toque</option>
              <option value="Nazaré">Nazaré</option>
              <option value="Nicolau Vergueiro">Nicolau Vergueiro</option>
              <option value="Nonoai">Nonoai</option>
              <option value="Nossa Senhora Aparecida">Nossa Senhora Aparecida</option>
              <option value="Nossa Senhora da Conceição">Nossa Senhora da Conceição</option>
              <option value="Nova Alvorada">Nova Alvorada</option>
              <option value="Nova Araçá">Nova Araçá</option>
              <option value="Nova Bassano">Nova Bassano</option>
              <option value="Nova Boa Vista">Nova Boa Vista</option>
              <option value="Nova Bréscia">Nova Bréscia</option>
              <option value="Nova Candelária">Nova Candelária</option>
              <option value="Nova Esperança do Sul">Nova Esperança do Sul</option>
              <option value="Nova Hartz">Nova Hartz</option>
              <option value="Nova Milano">Nova Milano</option>
              <option value="Nova Pádua">Nova Pádua</option>
              <option value="Nova Palma">Nova Palma</option>
              <option value="Nova Petrópolis">Nova Petrópolis</option>
              <option value="Nova Prata">Nova Prata</option>
              <option value="Nova Ramada">Nova Ramada</option>
              <option value="Nova Roma do Sul">Nova Roma do Sul</option>
              <option value="Nova Santa Rita">Nova Santa Rita</option>
              <option value="Nova Sardenha">Nova Sardenha</option>
              <option value="Novo Barreiro">Novo Barreiro</option>
              <option value="Novo Cabrais">Novo Cabrais</option>
              <option value="Novo Hamburgo">Novo Hamburgo</option>
              <option value="Novo Horizonte">Novo Horizonte</option>
              <option value="Novo Machado">Novo Machado</option>
              <option value="Novo Planalto">Novo Planalto</option>
              <option value="Novo Tiradentes">Novo Tiradentes</option>
              <option value="Novo Xingu">Novo Xingu</option>
              <option value="Oralina">Oralina</option>
              <option value="Osório">Osório</option>
              <option value="Osvaldo Cruz">Osvaldo Cruz</option>
              <option value="Osvaldo Kroeff">Osvaldo Kroeff</option>
              <option value="Otávio Rocha">Otávio Rocha</option>
              <option value="Pacheca">Pacheca</option>
              <option value="Padilha">Padilha</option>
              <option value="Padre Gonzales">Padre Gonzales</option>
              <option value="Paim Filho">Paim Filho</option>
              <option value="Palmares do Sul">Palmares do Sul</option>
              <option value="Palmas">Palmas</option>
              <option value="Palmeira das Missões">Palmeira das Missões</option>
              <option value="Palmitinho">Palmitinho</option>
              <option value="Pampeiro">Pampeiro</option>
              <option value="Panambi">Panambi</option>
              <option value="Pântano Grande">Pântano Grande</option>
              <option value="Paraí">Paraí</option>
              <option value="Paraíso do Sul">Paraíso do Sul</option>
              <option value="Pareci Novo">Pareci Novo</option>
              <option value="Parobé">Parobé</option>
              <option value="Passa Sete">Passa Sete</option>
              <option value="Passinhos">Passinhos</option>
              <option value="Passo Burmann">Passo Burmann</option>
              <option value="Passo da Areia">Passo da Areia</option>
              <option value="Passo da Caveira">Passo da Caveira</option>
              <option value="Passo das Pedras">Passo das Pedras</option>
              <option value="Passo do Adão">Passo do Adão</option>
              <option value="Passo do Goulart">Passo do Goulart</option>
              <option value="Passo do Sabão">Passo do Sabão</option>
              <option value="Passo do Sobrado">Passo do Sobrado</option>
              <option value="Passo do Verde">Passo do Verde</option>
              <option value="Passo Fundo">Passo Fundo</option>
              <option value="Passo Novo">Passo Novo</option>
              <option value="Passo Raso">Passo Raso</option>
              <option value="Paulo Bento">Paulo Bento</option>
              <option value="Pavão">Pavão</option>
              <option value="Paverama">Paverama</option>
              <option value="Pedras Altas">Pedras Altas</option>
              <option value="Pedreiras">Pedreiras</option>
              <option value="Pedro Garcia">Pedro Garcia</option>
              <option value="Pedro Osório">Pedro Osório</option>
              <option value="Pedro Paiva">Pedro Paiva</option>
              <option value="Pejuçara">Pejuçara</option>
              <option value="Pelotas">Pelotas</option>
              <option value="Picada Café">Picada Café</option>
              <option value="Pinhal Alto">Pinhal Alto</option>
              <option value="Pinhal da Serra">Pinhal da Serra</option>
              <option value="Pinhal Grande">Pinhal Grande</option>
              <option value="Pinhal">Pinhal</option>
              <option value="Pinhalzinho">Pinhalzinho</option>
              <option value="Pinheirinho do Vale">Pinheirinho do Vale</option>
              <option value="Pinheiro Machado">Pinheiro Machado</option>
              <option value="Pinheiro Marcado">Pinheiro Marcado</option>
              <option value="Pinto Bandeira">Pinto Bandeira</option>
              <option value="Piraí">Piraí</option>
              <option value="Pirapó">Pirapó</option>
              <option value="Piratini">Piratini</option>
              <option value="Pitanga">Pitanga</option>
              <option value="Planalto">Planalto</option>
              <option value="Plano Alto">Plano Alto</option>
              <option value="Poço das Antas">Poço das Antas</option>
              <option value="Polígono do Erval">Polígono do Erval</option>
              <option value="Pólo Petroquímico de Triunfo">Pólo Petroquímico de Triunfo</option>
              <option value="Pontão">Pontão</option>
              <option value="Ponte Preta">Ponte Preta</option>
              <option value="Portão">Portão</option>
              <option value="Porto Batista">Porto Batista</option>
              <option value="Porto Lucena">Porto Lucena</option>
              <option value="Porto Mauá">Porto Mauá</option>
              <option value="Porto Vera Cruz">Porto Vera Cruz</option>
              <option value="Porto Xavier">Porto Xavier</option>
              <option value="Pouso Novo">Pouso Novo</option>
              <option value="Povoado Tozzo">Povoado Tozzo</option>
              <option value="Povo Novo">Povo Novo</option>
              <option value="Prado Novo">Prado Novo</option>
              <option value="Pranchada">Pranchada</option>
              <option value="Pratos">Pratos</option>
              <option value="Presidente Lucena">Presidente Lucena</option>
              <option value="Progresso">Progresso</option>
              <option value="Protásio Alves">Protásio Alves</option>
              <option value="Pulador">Pulador</option>
              <option value="Putinga">Putinga</option>
              <option value="Quaraim">Quaraim</option>
              <option value="Quaraí">Quaraí</option>
              <option value="Quatro Irmãos">Quatro Irmãos</option>
              <option value="Quevedos">Quevedos</option>
              <option value="Quilombo">Quilombo</option>
              <option value="Quintão">Quintão</option>
              <option value="Quinta">Quinta</option>
              <option value="Quinze de Novembro">Quinze de Novembro</option>
              <option value="Quitéria">Quitéria</option>
              <option value="Rancho Velho">Rancho Velho</option>
              <option value="Redentora">Redentora</option>
              <option value="Refugiado">Refugiado</option>
              <option value="Relvado">Relvado</option>
              <option value="Restinga Seca">Restinga Seca</option>
              <option value="Rincão del Rei">Rincão del Rei</option>
              <option value="Rincão de São Miguel">Rincão de São Miguel</option>
              <option value="Rincão de São Pedro">Rincão de São Pedro</option>
              <option value="Rincão Doce">Rincão Doce</option>
              <option value="Rincão do Cristóvão Pereira">Rincão do Cristóvão Pereira</option>
              <option value="Rincão do Meio">Rincão do Meio</option>
              <option value="Rincão do Segredo">Rincão do Segredo</option>
              <option value="Rincão dos Kroeff">Rincão dos Kroeff</option>
              <option value="Rincão dos Mendes">Rincão dos Mendes</option>
              <option value="Rincão Vermelho">Rincão Vermelho</option>
              <option value="Rio Azul">Rio Azul</option>
              <option value="Rio Branco">Rio Branco</option>
              <option value="Rio da Ilha">Rio da Ilha</option>
              <option value="Rio dos Índios">Rio dos Índios</option>
              <option value="Rio Grande">Rio Grande</option>
              <option value="Rio Pardinho">Rio Pardinho</option>
              <option value="Rio Pardo">Rio Pardo</option>
              <option value="Rio Telha">Rio Telha</option>
              <option value="Rio Tigre">Rio Tigre</option>
              <option value="Rio Toldo">Rio Toldo</option>
              <option value="Riozinho">Riozinho</option>
              <option value="Roca Sales">Roca Sales</option>
              <option value="Rodeio Bonito">Rodeio Bonito</option>
              <option value="Rolador">Rolador</option>
              <option value="Rolante">Rolante</option>
              <option value="Rolantinho da Figueira">Rolantinho da Figueira</option>
              <option value="Ronda Alta">Ronda Alta</option>
              <option value="Rondinha">Rondinha</option>
              <option value="Roque Gonzales">Roque Gonzales</option>
              <option value="Rosário do Sul">Rosário do Sul</option>
              <option value="Rosário">Rosário</option>
              <option value="Sagrada Família">Sagrada Família</option>
              <option value="Saicã">Saicã</option>
              <option value="Saldanha Marinho">Saldanha Marinho</option>
              <option value="Saltinho">Saltinho</option>
              <option value="Salto do Jacuí">Salto do Jacuí</option>
              <option value="Salto">Salto</option>
              <option value="Salvador das Missões">Salvador das Missões</option>
              <option value="Salvador do Sul">Salvador do Sul</option>
              <option value="Sananduva">Sananduva</option>
              <option value="Santa Bárbara do Sul">Santa Bárbara do Sul</option>
              <option value="Santa Bárbara">Santa Bárbara</option>
              <option value="Santa Catarina">Santa Catarina</option>
              <option value="Santa Cecília do Sul">Santa Cecília do Sul</option>
              <option value="Santa Clara do Ingaí">Santa Clara do Ingaí</option>
              <option value="Santa Clara do Sul">Santa Clara do Sul</option>
              <option value="Santa Cristina">Santa Cristina</option>
              <option value="Santa Cruz">Santa Cruz</option>
              <option value="Santa Cruz da Concórdia">Santa Cruz da Concórdia</option>
              <option value="Santa Cruz do Sul">Santa Cruz do Sul</option>
              <option value="Santa Flora">Santa Flora</option>
              <option value="Santa Inês">Santa Inês</option>
              <option value="Santa Izabel do Sul">Santa Izabel do Sul</option>
              <option value="Santa Lúcia do Piaí">Santa Lúcia do Piaí</option>
              <option value="Santa Lúcia">Santa Lúcia</option>
              <option value="Santa Luíza">Santa Luíza</option>
              <option value="Santa Luzia">Santa Luzia</option>
              <option value="Santa Margarida do Sul">Santa Margarida do Sul</option>
              <option value="Santa Maria do Herval">Santa Maria do Herval</option>
              <option value="Santa Maria">Santa Maria</option>
              <option value="Santana da Boa Vista">Santana da Boa Vista</option>
              <option value="Santana do Livramento">Santana do Livramento</option>
              <option value="Santana">Santana</option>
              <option value="Santa Rita do Sul">Santa Rita do Sul</option>
              <option value="Santa Rosa">Santa Rosa</option>
              <option value="Santa Silvana">Santa Silvana</option>
              <option value="Santa Teresinha">Santa Teresinha</option>
              <option value="Santa Tereza">Santa Tereza</option>
              <option value="Sant'auta">Sant'auta</option>
              <option value="Santa Vitória do Palmar">Santa Vitória do Palmar</option>
              <option value="Santiago">Santiago</option>
              <option value="Santo Amaro do Sul">Santo Amaro do Sul</option>
              <option value="Santo Ângelo">Santo Ângelo</option>
              <option value="Santo Antônio da Patrulha">Santo Antônio da Patrulha</option>
              <option value="Santo Antônio das Missões">Santo Antônio das Missões</option>
              <option value="Santo Antônio de Castro">Santo Antônio de Castro</option>
              <option value="Santo Antônio do Bom Retiro">Santo Antônio do Bom Retiro</option>
              <option value="Santo Antônio do Palma">Santo Antônio do Palma</option>
              <option value="Santo Antônio do Planalto">Santo Antônio do Planalto</option>
              <option value="Santo Antônio">Santo Antônio</option>
              <option value="Santo Augusto">Santo Augusto</option>
              <option value="Santo Cristo">Santo Cristo</option>
              <option value="Santo Expedito do Sul">Santo Expedito do Sul</option>
              <option value="Santo Inácio">Santo Inácio</option>
              <option value="São Bento">São Bento</option>
              <option value="São Bom Jesus">São Bom Jesus</option>
              <option value="São Borja">São Borja</option>
              <option value="São Carlos">São Carlos</option>
              <option value="São Domingos do Sul">São Domingos do Sul</option>
              <option value="São Francisco de Assis">São Francisco de Assis</option>
              <option value="São Francisco de Paula">São Francisco de Paula</option>
              <option value="São Francisco">São Francisco</option>
              <option value="São Gabriel">São Gabriel</option>
              <option value="São Jerônimo">São Jerônimo</option>
              <option value="São João Batista">São João Batista</option>
              <option value="São João Bosco">São João Bosco</option>
              <option value="São João da Urtiga">São João da Urtiga</option>
              <option value="São João do Polesine">São João do Polesine</option>
              <option value="São João">São João</option>
              <option value="São Jorge">São Jorge</option>
              <option value="São José da Glória">São José da Glória</option>
              <option value="São José das Missões">São José das Missões</option>
              <option value="São José de Castro">São José de Castro</option>
              <option value="São José do Centro">São José do Centro</option>
              <option value="São José do Herval">São José do Herval</option>
              <option value="São José do Hortêncio">São José do Hortêncio</option>
              <option value="São José do Inhacorá">São José do Inhacorá</option>
              <option value="São José do Norte">São José do Norte</option>
              <option value="São José do Ouro">São José do Ouro</option>
              <option value="São José dos Ausentes">São José dos Ausentes</option>
              <option value="São José do Sul">São José do Sul</option>
              <option value="São José">São José</option>
              <option value="São Leopoldo">São Leopoldo</option>
              <option value="São Lourenço das Missões">São Lourenço das Missões</option>
              <option value="São Lourenço do Sul">São Lourenço do Sul</option>
              <option value="São Luís Rei">São Luís Rei</option>
              <option value="São Luiz Gonzaga">São Luiz Gonzaga</option>
              <option value="São Luiz">São Luiz</option>
              <option value="São Manuel">São Manuel</option>
              <option value="São Marcos">São Marcos</option>
              <option value="São Martinho da Serra">São Martinho da Serra</option>
              <option value="São Martinho">São Martinho</option>
              <option value="São Miguel das Missões">São Miguel das Missões</option>
              <option value="São Miguel">São Miguel</option>
              <option value="São Nicolau">São Nicolau</option>
              <option value="São Paulo">São Paulo</option>
              <option value="São Paulo das Missões">São Paulo das Missões</option>
              <option value="São Pedro da Serra">São Pedro da Serra</option>
              <option value="São Pedro das Missões">São Pedro das Missões</option>
              <option value="São Pedro do Butiá">São Pedro do Butiá</option>
              <option value="São Pedro do Iraxim">São Pedro do Iraxim</option>
              <option value="São Pedro do Sul">São Pedro do Sul</option>
              <option value="São Pedro">São Pedro</option>
              <option value="São Roque">São Roque</option>
              <option value="São Sebastião do Caí">São Sebastião do Caí</option>
              <option value="São Sebastião">São Sebastião</option>
              <option value="São Sepé">São Sepé</option>
              <option value="São Simão">São Simão</option>
              <option value="São Valentim do Sul">São Valentim do Sul</option>
              <option value="São Valentim">São Valentim</option>
              <option value="São Valério do Sul">São Valério do Sul</option>
              <option value="São Vendelino">São Vendelino</option>
              <option value="São Vicente do Sul">São Vicente do Sul</option>
              <option value="Sapiranga">Sapiranga</option>
              <option value="Sapucaia do Sul">Sapucaia do Sul</option>
              <option value="Sarandi">Sarandi</option>
              <option value="Seberi">Seberi</option>
              <option value="Sede Aurora">Sede Aurora</option>
              <option value="Sede Nova">Sede Nova</option>
              <option value="Segredo">Segredo</option>
              <option value="Seival">Seival</option>
              <option value="Selbach">Selbach</option>
              <option value="Senador Salgado Filho">Senador Salgado Filho</option>
              <option value="Sentinela do Sul">Sentinela do Sul</option>
              <option value="Serafim Schmidt">Serafim Schmidt</option>
              <option value="Serafina Corrêa">Serafina Corrêa</option>
              <option value="Sério">Sério</option>
              <option value="Serra dos Gregórios">Serra dos Gregórios</option>
              <option value="Serrinha">Serrinha</option>
              <option value="Serrinha Velha">Serrinha Velha</option>
              <option value="Sertão">Sertão</option>
              <option value="Sertão Santana">Sertão Santana</option>
              <option value="Sertãozinho">Sertãozinho</option>
              <option value="Sete de Setembro">Sete de Setembro</option>
              <option value="Sete Lagoas">Sete Lagoas</option>
              <option value="Severiano de Almeida">Severiano de Almeida</option>
              <option value="Silva Jardim">Silva Jardim</option>
              <option value="Silveira">Silveira</option>
              <option value="Silveira Martins">Silveira Martins</option>
              <option value="Sinimbu">Sinimbu</option>
              <option value="Sírio">Sírio</option>
              <option value="Sítio Gabriel">Sítio Gabriel</option>
              <option value="Sobradinho">Sobradinho</option>
              <option value="Soledade">Soledade</option>
              <option value="Souza Ramos">Souza Ramos</option>
              <option value="Suspiro">Suspiro</option>
              <option value="Tabaí">Tabaí</option>
              <option value="Tabajara">Tabajara</option>
              <option value="Taim">Taim</option>
              <option value="Tainhas">Tainhas</option>
              <option value="Tamanduá">Tamanduá</option>
              <option value="Tanque">Tanque</option>
              <option value="Tapejara">Tapejara</option>
              <option value="Tapera">Tapera</option>
              <option value="Tapes">Tapes</option>
              <option value="Taquaral">Taquaral</option>
              <option value="Taquara">Taquara</option>
              <option value="Taquarichim">Taquarichim</option>
              <option value="Taquari">Taquari</option>
              <option value="Taquaruçu do Sul">Taquaruçu do Sul</option>
              <option value="Tavares">Tavares</option>
              <option value="Tenente Portela">Tenente Portela</option>
              <option value="Terra de Areia">Terra de Areia</option>
              <option value="Tesouras">Tesouras</option>
              <option value="Teutônia">Teutônia</option>
              <option value="Tiaraju">Tiaraju</option>
              <option value="Timbaúva">Timbaúva</option>
              <option value="Tio Hugo">Tio Hugo</option>
              <option value="Tiradentes do Sul">Tiradentes do Sul</option>
              <option value="Toropi">Toropi</option>
              <option value="Toroquá">Toroquá</option>
              <option value="Torquato Severo">Torquato Severo</option>
              <option value="Torres">Torres</option>
              <option value="Torrinhas">Torrinhas</option>
              <option value="Touro Passo">Touro Passo</option>
              <option value="Tramandaí">Tramandaí</option>
              <option value="Travesseiro">Travesseiro</option>
              <option value="Trentin">Trentin</option>
              <option value="Três Arroios">Três Arroios</option>
              <option value="Três Barras">Três Barras</option>
              <option value="Três Cachoeiras">Três Cachoeiras</option>
              <option value="Três Coroas">Três Coroas</option>
              <option value="Três de Maio">Três de Maio</option>
              <option value="Três Forquilhas">Três Forquilhas</option>
              <option value="Três Palmeiras">Três Palmeiras</option>
              <option value="Três Passos">Três Passos</option>
              <option value="Três Vendas">Três Vendas</option>
              <option value="Trindade do Sul">Trindade do Sul</option>
              <option value="Triunfo">Triunfo</option>
              <option value="Tronqueiras">Tronqueiras</option>
              <option value="Tucunduva">Tucunduva</option>
              <option value="Tuiuti">Tuiuti</option>
              <option value="Tunas">Tunas</option>
              <option value="Túnel Verde">Túnel Verde</option>
              <option value="Tupanci do Sul">Tupanci do Sul</option>
              <option value="Tupanciretã">Tupanciretã</option>
              <option value="Tupancy ou Vila Block">Tupancy ou Vila Block</option>
              <option value="Tupandi">Tupandi</option>
              <option value="Tupantuba">Tupantuba</option>
              <option value="Tuparendi">Tuparendi</option>
              <option value="Tupinambá">Tupinambá</option>
              <option value="Tupi Silveira">Tupi Silveira</option>
              <option value="Turuçu">Turuçu</option>
              <option value="Turvinho">Turvinho</option>
              <option value="Ubiretama">Ubiretama</option>
              <option value="Umbu">Umbu</option>
              <option value="União da Serra">União da Serra</option>
              <option value="Unistalda">Unistalda</option>
              <option value="Uruguaiana">Uruguaiana</option>
              <option value="Vacacai">Vacacai</option>
              <option value="Vacaria">Vacaria</option>
              <option value="Valdástico">Valdástico</option>
              <option value="Vale do Rio Cai">Vale do Rio Cai</option>
              <option value="Vale do Sol">Vale do Sol</option>
              <option value="Vale dos Vinhedos">Vale dos Vinhedos</option>
              <option value="Vale Real">Vale Real</option>
              <option value="Vale Veneto">Vale Veneto</option>
              <option value="Vale Verde">Vale Verde</option>
              <option value="Vanini">Vanini</option>
              <option value="Vasco Alves">Vasco Alves</option>
              <option value="Venâncio Aires">Venâncio Aires</option>
              <option value="Vera Cruz">Vera Cruz</option>
              <option value="Veranópolis">Veranópolis</option>
              <option value="Vertentes">Vertentes</option>
              <option value="Vespasiano Correa">Vespasiano Correa</option>
              <option value="Viadutos">Viadutos</option>
              <option value="Viamão">Viamão</option>
              <option value="Vicente Dutra">Vicente Dutra</option>
              <option value="Victor Graeff">Victor Graeff</option>
              <option value="Vila Bender">Vila Bender</option>
              <option value="Vila Boqueirão">Vila Boqueirão</option>
              <option value="Vila Cruz">Vila Cruz</option>
              <option value="Vila Fernando Ferrari">Vila Fernando Ferrari</option>
              <option value="Vila Flores">Vila Flores</option>
              <option value="Vila Langaro">Vila Langaro</option>
              <option value="Vila Laranjeira">Vila Laranjeira</option>
              <option value="Vila Maria">Vila Maria</option>
              <option value="Vila Nova do Sul">Vila Nova do Sul</option>
              <option value="Vila Oliva">Vila Oliva</option>
              <option value="Vila Rica">Vila Rica</option>
              <option value="Vila Seca">Vila Seca</option>
              <option value="Vila Turvo">Vila Turvo</option>
              <option value="Vista Alegre do Prata">Vista Alegre do Prata</option>
              <option value="Vista Alegre">Vista Alegre</option>
              <option value="Vista Gaúcha">Vista Gaúcha</option>
              <option value="Vitória das Missões">Vitória das Missões</option>
              <option value="Vitória">Vitória</option>
              <option value="Volta Alegre">Volta Alegre</option>
              <option value="Volta Fechada">Volta Fechada</option>
              <option value="Volta Grande">Volta Grande</option>
              <option value="Westfália">Westfália</option>
              <option value="Xadrez">Xadrez</option>
              <option value="Xangri-lá">Xangri-lá</option>
            </select>
          </div>
        </div>
        <div class="row half">
          <div class="6u">
            <input type="text" class="text" name="txtBairro" placeholder="Bairro" onBlur='cadastroRapido("Controller/Empresa.controller.php?op=1","alerta","info_candidato")' value="<?php 			
			if(count($infoEmpresa) > 0){
				echo $infoEmpresa[0]->bairro;
			}			
			?>"/>
          </div>
          <div class="6u">
            <input type="text" class="text" name="txtRua" placeholder="Rua / Número" onBlur='cadastroRapido("Controller/Empresa.controller.php?op=1","alerta","info_candidato")' value="<?php 			
			if(count($infoEmpresa) > 0){
				echo $infoEmpresa[0]->rua;
			}			
			?>"/>
          </div>
        </div>
      </header>
    </form>
  </div>
</section>

<section id="vaga" class="four">
  <div class="container"  id='gestor_vaga'> <br />
    <?php
	  if(!isset($_SESSION['form_filtro_vaga'])){
	?>
    <header>
      <h2>Minhas vagas</h2>
    </header>
    <form onSubmit="return false" id="cad_vaga">    
    
      <div class="row half">
      	
        <div class="12u">
          <input type="text" class="text" name="txtCod" placeholder="Codigo do atendimento" />
        </div>
      </div>
     
      <div class="row half">
        <div class="6u">
          <select name="txtNome">
            <option value=""> Selecione a vaga</option>
            <?php												   
              //$pFuncaoDAO = new PFuncaoDAO();
              
              //$pFuncao = $pFuncaoDAO->get();
              
              echo "<option value='outro'> OUTRO </option>";
              
			  /*
              foreach($pFuncao as $funcao){													
                echo "<option value='".$funcao->NOME."'> ".htmlentities($funcao->NOME)." </option>";
              }	
			  */
			  											  
              ?>
          </select>
        </div>
        <div class="6u">
          <input type="text" class="text" name="txtSalario" placeholder="Remuneração" size="11" onKeyUp="moeda(this);" />
        </div>
      </div>
      <div class="row half">
        <div class="6u" id='input_full'> <b>Beneficios</b> <br />
          <input class='check' name='cbVT' type='checkbox' value='cbVT' />
          Vale transporte
          <input class='check' name='ckVR' type='checkbox' value='ckVR' />
          Vale refeição <br />
          <input class='check' name='ckPlanoSaude' type='checkbox' value='ckPlanoSaude' />
          Plano de saúde
          <input class='check' name='ckOutrosBeneficio' type='checkbox' value='ckOutrosBeneficio' />
          Outros </div>
        <div class="6u">
          <select name="cbAreaExperiencia">
            <option value=''> Candidatos com experiência em</option>
            <option value='Comercial, Vendas'>Comercial, Vendas</option>
            <option value='Administração'>Administração</option>
            <option value='Industrial, Produção, Fábrica'>Industrial, Produção, Fábrica</option>
            <option value='Logística'>Logística</option>
            <option value='Informática, TI, Telecomunicação'>Informática, TI, Telecomunicação</option>
            <option value='Construção, Manutenção'>Construção, Manutenção</option>
            <option value='Contabil, Finanças, Economia'>Contabil, Finanças, Economia</option>
            <option value='Alimentação, Gastronomia'>Alimentação, Gastronomia</option>
            <option value='Engenharia'>Engenharia</option>
            <option value='Telemarketing'>Telemarketing</option>
            <option value='Saúde'>Saúde</option>
            <option value='Recursos Humanos'>Recursos Humanos</option>
            <option value='Transportes'>Transportes</option>
            <option value='Educação, Ensino, Idiomas'>Educação, Ensino, Idiomas</option>
            <option value='Compras, Almoxarifado, Materiais, Suprimentos'>Compras, Almoxarifado, Materiais, Suprimentos</option>
            <option value='Jurídica'>Jurídica</option>
            <option value='Marketing'>Marketing</option>
            <option value='Hotelaria, Turismo'>Hotelaria, Turismo</option>
            <option value='Seguranca'>Segurança</option>
            <option value='Qualidade'>Qualidade</option>
            <option value='Arquitetura, Decoração, Designer'>Arquitetura, Decoração, Designer</option>
            <option value='Estética'>Estética</option>
            <option value='Comunicação, TV, Cinema'>Comunicação, TV, Cinema</option>
            <option value='Química, Petroquímica'>Química, Petroquímica</option>
            <option value='Comércio Exterior, Importação, Exportação'>Comércio Exterior, Importação, Exportação</option>
            <option value='Cultura, Lazer, Entretenimento'>Cultura, Lazer, Entretenimento</option>
            <option value='Agricultura, Pecuária, Veterinária'>Agricultura, Pecuária, Veterinária</option>
            <option value='Moda'>Moda</option>
            <option value='Auditoria'>Auditoria</option>
            <option value='Artes'>Artes</option>
            <option value='Serviços Sociais, Comunitários'>Serviços Sociais, Comunitários</option>
            <option value='Ciências, Pesquisa'>Ciências, Pesquisa</option>
            <option value='Meio Ambiente, Ecologia'>Meio Ambiente, Ecologia</option>
            <option value='Administracao'>Administração</option>
            <option value='Arquitetura'>Arquitetura</option>
            <option value='TI - Infra estrutura'>TI - Infra estrutura</option>
            <option value='TI - Desenvolvimento'>TI - Desenvolvimento</option>
            <option value='Financeiro'>Financeiro</option>
            <option value='Departamento pessoal'>Departamento pessoal</option>
            <option value='Recrutamento e Selecao'>Recrutamento e Seleção</option>
            <option value='Compras'>Compras</option>
            <option value='Manutencao predial'>Manutenção predial</option>
            <option value='Juridico'>Jurídico</option>
            <option value='Suprimentos e compras'>Suprimentos e compras</option>
            <option value='Marketing - comunicacao'>Marketing - comunicação</option>
            <option value='Logistica academica'>Logística acadêmica</option>
            <option value='Relacionamento com cliente'>Relacionamento com cliente</option>
            <option value='Secretaria escolar'>Secretaria escolar</option>
            <option value='Professor - Tec em informatica'>Professor - Tec em informática</option>
            <option value='Professor - Tec em administracao'>Professor - Tec em administração</option>
            <option value='Instrutor de lingua inglesa'>Instrutor de lingua inglesa</option>
            <option value='Instrutor profissional'>Instrutor profissional</option>
            <option value='Servicos Gerais (Aux limpeza)'>Serviços Gerais (Aux limpeza)</option>
            <option value='Telemarketing'>Telemarketing</option>
          </select>
        </div>
      </div>
      <div class="row half">
        <div class="6u">
          <select name="cbCargo" onChange='cadastroRapido("Controller/AreaInteresse.controller.php?op=3","return_funcao","cad_vaga")'>
            <option value=''> Área da vaga</option>
            <option value='Arquitetura'>Arquitetura</option>
            <option value='Cobrança'>Cobrança</option>
            <option value='Comercial'>Comercial</option>
            <option value='CPD'>CPD</option>
            <option value='Departamento pessoal'>Departamento pessoal</option>
            <option value='Educação'>Educação</option>
            <option value='Financeiro'>Financeiro</option>
            <option value='Juridico'>Jurídico</option>
            <option value='Legislação academica'>Legislação acadêmica</option>
            <option value='Manutenção'>Manutenção</option>
            <option value='Marketing'>Marketing</option>
            <option value='Recrutamento e seleção'>Recrutamento e seleção</option>
            <option value='Serviços gerais'>Serviços gerais</option>
            <option value='Suprimentos'>Suprimentos</option>
            <option value='Juridico'>Jurídico</option>
            <option value='TI - Desenvolvimento'>TI - Desenvolvimento</option>
            <option value='TI - Infraestrutura'>TI - Infraestrutura</option>
            <option value='Treinamento'>Treinamento</option>
          </select>
        </div>
        <div class="6u" id='return_funcao'>
          <select name="cbFuncao" disabled>
            <option value=''> Selecione a área </option>
          </select>
        </div>
      </div>
      <div class="row half">
        <div class="6u" id='input_full'> <b>Vaga para candidatos de nível</b> <br />
          <input class='check' name='ckMedio'  type='checkbox' value='ckMedio' />
          Ensino Fundamental / Médio
          <input class='check' name='ckTecnico'  type='checkbox' value='ckTecnico' />
          Técnico <br />
          <input class='check' name='ckSuperior'  type='checkbox' value='ckSuperior' />
          Superior
          <input class='check' name='ckPos'  type='checkbox' value='ckPos' />
          Pós-Graduação </div>
        <div class="6u" id='input_full'> <b>Somente candidatos com idioma</b> <br />
          <input class='check' name='ckIngles' type='checkbox' value='ckIngles' />
          Inglês
          <input class='check' name='ckItaliano' type='checkbox' value='ckItaliano' />
          Italiano
          <input class='check' name='ckFrances' type='checkbox' value='ckFrances' />
          Francês<br />
          <input class='check' name='ckEspanhol' type='checkbox' value='ckEspanhol' />
          Espanhol
          <input class='check' name='ckOutrosIdioma' type='checkbox' value='ckOutrosIdioma' />
          Outros </div>
      </div>
      <div class="row half">
        <div class="6u">
          <select name="txtCidade">
            <option value="">Cidade</option>
            <option value="Porto Alegre">Porto Alegre</option>
            <option value="Aceguá">Aceguá</option>
            <option value="Afonso Rodrigues">Afonso Rodrigues</option>
            <option value="Aguapés">Aguapés</option>
            <option value="Água Santa">Água Santa</option>
            <option value="Águas Claras">Águas Claras</option>
            <option value="Agudo">Agudo</option>
            <option value="Ajuricaba">Ajuricaba</option>
            <option value="Albardão">Albardão</option>
            <option value="Alecrim">Alecrim</option>
            <option value="Alegrete">Alegrete</option>
            <option value="Alegria">Alegria</option>
            <option value="Alfredo Brenner">Alfredo Brenner</option>
            <option value="Almirante Tamandaré do Sul">Almirante Tamandaré do Sul</option>
            <option value="Alpestre">Alpestre</option>
            <option value="Alto Alegre">Alto Alegre</option>
            <option value="Alto da União">Alto da União</option>
            <option value="Alto Feliz">Alto Feliz</option>
            <option value="Alto Paredão">Alto Paredão</option>
            <option value="Alto Recreio">Alto Recreio</option>
            <option value="Alto Uruguai">Alto Uruguai</option>
            <option value="Alvorada">Alvorada</option>
            <option value="Amaral Ferrador">Amaral Ferrador</option>
            <option value="Ametista do Sul">Ametista do Sul</option>
            <option value="André da Rocha">André da Rocha</option>
            <option value="Anta Gorda">Anta Gorda</option>
            <option value="Antônio Kerpel">Antônio Kerpel</option>
            <option value="Antônio Prado">Antônio Prado</option>
            <option value="Arambaré">Arambaré</option>
            <option value="Araricá">Araricá</option>
            <option value="Aratiba">Aratiba</option>
            <option value="Arco-Íris">Arco-Íris</option>
            <option value="Arco Verde">Arco Verde</option>
            <option value="Arroio Canoas">Arroio Canoas</option>
            <option value="Arroio do Meio">Arroio do Meio</option>
            <option value="Arroio do Padre">Arroio do Padre</option>
            <option value="Arroio do Sal">Arroio do Sal</option>
            <option value="Arroio do Só">Arroio do Só</option>
            <option value="Arroio dos Ratos">Arroio dos Ratos</option>
            <option value="Arroio do Tigre">Arroio do Tigre</option>
            <option value="Arroio Grande">Arroio Grande</option>
            <option value="Árvore Só">Árvore Só</option>
            <option value="Arvorezinha">Arvorezinha</option>
            <option value="Atafona">Atafona</option>
            <option value="Atiaçu">Atiaçu</option>
            <option value="Augusto Pestana">Augusto Pestana</option>
            <option value="Áurea">Áurea</option>
            <option value="Avelino Paranhos">Avelino Paranhos</option>
            <option value="Azevedo Sodré">Azevedo Sodré</option>
            <option value="Bacupari">Bacupari</option>
            <option value="Bagé">Bagé</option>
            <option value="Baliza">Baliza</option>
            <option value="Balneário Pinhal">Balneário Pinhal</option>
            <option value="Banhado do Colégio">Banhado do Colégio</option>
            <option value="Barão de Cotegipe">Barão de Cotegipe</option>
            <option value="Barão do Triunfo">Barão do Triunfo</option>
            <option value="Barão">Barão</option>
            <option value="Barracão">Barracão</option>
            <option value="Barra do Guarita">Barra do Guarita</option>
            <option value="Barra do Ouro">Barra do Ouro</option>
            <option value="Barra do Quaraí">Barra do Quaraí</option>
            <option value="Barra do Ribeiro">Barra do Ribeiro</option>
            <option value="Barra do Rio Azul">Barra do Rio Azul</option>
            <option value="Barra Funda">Barra Funda</option>
            <option value="Barreirinho">Barreirinho</option>
            <option value="Barreiro">Barreiro</option>
            <option value="Barro Preto">Barro Preto</option>
            <option value="Barros Cassal">Barros Cassal</option>
            <option value="Barro Vermelho">Barro Vermelho</option>
            <option value="Basílio">Basílio</option>
            <option value="Bela Vista">Bela Vista</option>
            <option value="Beluno">Beluno</option>
            <option value="Benjamin Constant do Sul">Benjamin Constant do Sul</option>
            <option value="Bento Gonçalves">Bento Gonçalves</option>
            <option value="Bexiga">Bexiga</option>
            <option value="Boa Esperança">Boa Esperança</option>
            <option value="Boa Vista das Missões">Boa Vista das Missões</option>
            <option value="Boa Vista do Buricá">Boa Vista do Buricá</option>
            <option value="Boa Vista do Cadeado">Boa Vista do Cadeado</option>
            <option value="Boa Vista do Incra">Boa Vista do Incra</option>
            <option value="Boa Vista do Sul">Boa Vista do Sul</option>
            <option value="Boa Vista">Boa Vista</option>
            <option value="Boca do Monte">Boca do Monte</option>
            <option value="Boi Preto">Boi Preto</option>
            <option value="Bojuru">Bojuru</option>
            <option value="Bom Jardim">Bom Jardim</option>
            <option value="Bom Jesus">Bom Jesus</option>
            <option value="Bom Princípio">Bom Princípio</option>
            <option value="Bom Progresso">Bom Progresso</option>
            <option value="Bom Retiro do Guaíba">Bom Retiro do Guaíba</option>
            <option value="Bom Retiro do Sul">Bom Retiro do Sul</option>
            <option value="Bom Retiro">Bom Retiro</option>
            <option value="Bonito">Bonito</option>
            <option value="Boqueirão do Leão">Boqueirão do Leão</option>
            <option value="Boqueirão">Boqueirão</option>
            <option value="Bororé">Bororé</option>
            <option value="Borussia">Borussia</option>
            <option value="Bossoroca">Bossoroca</option>
            <option value="Botucaraí">Botucaraí</option>
            <option value="Bozano">Bozano</option>
            <option value="Braga">Braga</option>
            <option value="Brochier">Brochier</option>
            <option value="Buriti">Buriti</option>
            <option value="Butiá">Butiá</option>
            <option value="Butiás">Butiás</option>
            <option value="Caçapava do Sul">Caçapava do Sul</option>
            <option value="Cacequi">Cacequi</option>
            <option value="Cachoeira do Sul">Cachoeira do Sul</option>
            <option value="Cachoeirinha">Cachoeirinha</option>
            <option value="Cacique Doble">Cacique Doble</option>
            <option value="Cadorna">Cadorna</option>
            <option value="Caibaté">Caibaté</option>
            <option value="Caiçara">Caiçara</option>
            <option value="Camaquã">Camaquã</option>
            <option value="Camargo">Camargo</option>
            <option value="Cambará do Sul">Cambará do Sul</option>
            <option value="Campestre Baixo">Campestre Baixo</option>
            <option value="Campestre da Serra">Campestre da Serra</option>
            <option value="Campina das Missões">Campina das Missões</option>
            <option value="Campina Redonda">Campina Redonda</option>
            <option value="Campinas do Sul">Campinas do Sul</option>
            <option value="Campinas">Campinas</option>
            <option value="Campo Bom">Campo Bom</option>
            <option value="Campo Branco">Campo Branco</option>
            <option value="Campo do Meio">Campo do Meio</option>
            <option value="Campo Novo">Campo Novo</option>
            <option value="Campo Santo">Campo Santo</option>
            <option value="Campos Borges">Campos Borges</option>
            <option value="Campo Seco">Campo Seco</option>
            <option value="Campo Vicente">Campo Vicente</option>
            <option value="Candelária">Candelária</option>
            <option value="Cândido Freire">Cândido Freire</option>
            <option value="Cândido Godói">Cândido Godói</option>
            <option value="Candiota">Candiota</option>
            <option value="Canela">Canela</option>
            <option value="Canguçu">Canguçu</option>
            <option value="Canhembora">Canhembora</option>
            <option value="Canoas">Canoas</option>
            <option value="Canudos do Vale">Canudos do Vale</option>
            <option value="Capané">Capané</option>
            <option value="Capão Bonito do Sul">Capão Bonito do Sul</option>
            <option value="Capão Bonito">Capão Bonito</option>
            <option value="Capão Comprido">Capão Comprido</option>
            <option value="Capão da Canoa">Capão da Canoa</option>
            <option value="Capão da Porteira">Capão da Porteira</option>
            <option value="Capão do Cedro">Capão do Cedro</option>
            <option value="Capão do Cipó">Capão do Cipó</option>
            <option value="Capão do Leão">Capão do Leão</option>
            <option value="Capela de Santana">Capela de Santana</option>
            <option value="Capela Velha">Capela Velha</option>
            <option value="Capinzal">Capinzal</option>
            <option value="Capitão">Capitão</option>
            <option value="Capivari do Sul">Capivari do Sul</option>
            <option value="Capivarita">Capivarita</option>
            <option value="Capoeira Grande">Capoeira Grande</option>
            <option value="Capo-Erê">Capo-Erê</option>
            <option value="Caraá">Caraá</option>
            <option value="Carajá Seival">Carajá Seival</option>
            <option value="Carazinho">Carazinho</option>
            <option value="Carlos Barbosa">Carlos Barbosa</option>
            <option value="Carlos Gomes">Carlos Gomes</option>
            <option value="Carovi">Carovi</option>
            <option value="Casca">Casca</option>
            <option value="Cascata">Cascata</option>
            <option value="Caseiros">Caseiros</option>
            <option value="Castelinho">Castelinho</option>
            <option value="Catimbau">Catimbau</option>
            <option value="Catuípe">Catuípe</option>
            <option value="Cavajureta">Cavajureta</option>
            <option value="Caverá">Caverá</option>
            <option value="Caxias do Sul">Caxias do Sul</option>
            <option value="Cazuza Ferreira">Cazuza Ferreira</option>
            <option value="Cedro Marcado">Cedro Marcado</option>
            <option value="Centenário">Centenário</option>
            <option value="Centro Linha Brasil">Centro Linha Brasil</option>
            <option value="Cerrito Alegre">Cerrito Alegre</option>
            <option value="Cerrito do Ouro ou Vila do Cerrito">Cerrito do Ouro ou Vila do Cerrito</option>
            <option value="Cerrito">Cerrito</option>
            <option value="Cerro Alto">Cerro Alto</option>
            <option value="Cerro Branco">Cerro Branco</option>
            <option value="Cerro Claro">Cerro Claro</option>
            <option value="Cerro do Martins">Cerro do Martins</option>
            <option value="Cerro do Roque">Cerro do Roque</option>
            <option value="Cerro Grande do Sul">Cerro Grande do Sul</option>
            <option value="Cerro Grande">Cerro Grande</option>
            <option value="Cerro Largo">Cerro Largo</option>
            <option value="Chapada">Chapada</option>
            <option value="Charqueadas">Charqueadas</option>
            <option value="Charrua">Charrua</option>
            <option value="Chiapetta">Chiapetta</option>
            <option value="Chicolomã">Chicolomã</option>
            <option value="Chimarrão">Chimarrão</option>
            <option value="Chorão">Chorão</option>
            <option value="Chuí">Chuí</option>
            <option value="Chuvisca">Chuvisca</option>
            <option value="Cidreira">Cidreira</option>
            <option value="Cinqüentenário">Cinqüentenário</option>
            <option value="Ciríaco">Ciríaco</option>
            <option value="Clara">Clara</option>
            <option value="Clemente Argolo">Clemente Argolo</option>
            <option value="Coimbra">Coimbra</option>
            <option value="Colinas">Colinas</option>
            <option value="Colônia das Almas">Colônia das Almas</option>
            <option value="Colônia Medeiros">Colônia Medeiros</option>
            <option value="Colônia Municipal">Colônia Municipal</option>
            <option value="Colônia Nova">Colônia Nova</option>
            <option value="Colônia São João">Colônia São João</option>
            <option value="Colônia Z-3">Colônia Z-3</option>
            <option value="Coloninha">Coloninha</option>
            <option value="Colorado">Colorado</option>
            <option value="Comandai">Comandai</option>
            <option value="Condor">Condor</option>
            <option value="Consolata">Consolata</option>
            <option value="Constantina">Constantina</option>
            <option value="Coqueiro Baixo">Coqueiro Baixo</option>
            <option value="Coqueiros do Sul">Coqueiros do Sul</option>
            <option value="Cordilheira">Cordilheira</option>
            <option value="Coroados">Coroados</option>
            <option value="Coronel Barros">Coronel Barros</option>
            <option value="Coronel Bicaco">Coronel Bicaco</option>
            <option value="Coronel Finzito">Coronel Finzito</option>
            <option value="Coronel Pilar">Coronel Pilar</option>
            <option value="Coronel Teixeira">Coronel Teixeira</option>
            <option value="Cortado">Cortado</option>
            <option value="Costa da Cadeia">Costa da Cadeia</option>
            <option value="Costão">Costão</option>
            <option value="Cotiporã">Cotiporã</option>
            <option value="Coxilha Grande">Coxilha Grande</option>
            <option value="Coxilha">Coxilha</option>
            <option value="Cr-1">Cr-1</option>
            <option value="Crissiumal">Crissiumal</option>
            <option value="Cristal do Sul">Cristal do Sul</option>
            <option value="Cristal">Cristal</option>
            <option value="Criúva">Criúva</option>
            <option value="Cruz Alta">Cruz Alta</option>
            <option value="Cruzaltense">Cruzaltense</option>
            <option value="Cruzeiro">Cruzeiro</option>
            <option value="Cruzeiro do Sul">Cruzeiro do Sul</option>
            <option value="Curral Alto">Curral Alto</option>
            <option value="Curumim">Curumim</option>
            <option value="Daltro Filho">Daltro Filho</option>
            <option value="Dário Lassance">Dário Lassance</option>
            <option value="David Canabarro">David Canabarro</option>
            <option value="Delfina">Delfina</option>
            <option value="Deodoro">Deodoro</option>
            <option value="Depósito">Depósito</option>
            <option value="Derrubadas">Derrubadas</option>
            <option value="Dezesseis de Novembro">Dezesseis de Novembro</option>
            <option value="Dilermando de Aguiar">Dilermando de Aguiar</option>
            <option value="Divino">Divino</option>
            <option value="Dois Irmãos das Missões">Dois Irmãos das Missões</option>
            <option value="Dois Irmãos">Dois Irmãos</option>
            <option value="Dois Lajeados">Dois Lajeados</option>
            <option value="Dom Feliciano">Dom Feliciano</option>
            <option value="Dom Pedrito">Dom Pedrito</option>
            <option value="Dom Pedro de Alcântara">Dom Pedro de Alcântara</option>
            <option value="Dona Francisca">Dona Francisca</option>
            <option value="Dona Otília">Dona Otília</option>
            <option value="Dourado">Dourado</option>
            <option value="Doutor Edgardo Pereira Velho">Doutor Edgardo Pereira Velho</option>
            <option value="Doutor Maurício Cardoso">Doutor Maurício Cardoso</option>
            <option value="Doutor Ricardo">Doutor Ricardo</option>
            <option value="Durasnal">Durasnal</option>
            <option value="Eldorado do Sul">Eldorado do Sul</option>
            <option value="Eletra">Eletra</option>
            <option value="Encantado">Encantado</option>
            <option value="Encruzilhada do Sul">Encruzilhada do Sul</option>
            <option value="Encruzilhada">Encruzilhada</option>
            <option value="Engenho Velho">Engenho Velho</option>
            <option value="Entre-Ijuís">Entre-Ijuís</option>
            <option value="Entrepelado">Entrepelado</option>
            <option value="Entre Rios do Sul">Entre Rios do Sul</option>
            <option value="Erebango">Erebango</option>
            <option value="Erechim">Erechim</option>
            <option value="Ernestina">Ernestina</option>
            <option value="Ernesto Alves">Ernesto Alves</option>
            <option value="Erval Grande">Erval Grande</option>
            <option value="Erval Seco">Erval Seco</option>
            <option value="Erveiras">Erveiras</option>
            <option value="Esmeralda">Esmeralda</option>
            <option value="Esperança do Sul">Esperança do Sul</option>
            <option value="Esperança">Esperança</option>
            <option value="Espigão Alto">Espigão Alto</option>
            <option value="Espigão">Espigão</option>
            <option value="Espinilho Grande">Espinilho Grande</option>
            <option value="Espírito Santo">Espírito Santo</option>
            <option value="Espumoso">Espumoso</option>
            <option value="Esquina Araújo">Esquina Araújo</option>
            <option value="Esquina Bom Sucesso">Esquina Bom Sucesso</option>
            <option value="Esquina Gaúcha">Esquina Gaúcha</option>
            <option value="Esquina Ipiranga">Esquina Ipiranga</option>
            <option value="Esquina Piratini">Esquina Piratini</option>
            <option value="Estação">Estação</option>
            <option value="Estância Grande">Estância Grande</option>
            <option value="Estância Velha">Estância Velha</option>
            <option value="Esteio">Esteio</option>
            <option value="Esteira">Esteira</option>
            <option value="Estreito">Estreito</option>
            <option value="Estrela">Estrela</option>
            <option value="Estrela Velha">Estrela Velha</option>
            <option value="Eugênio de Castro">Eugênio de Castro</option>
            <option value="Evangelista">Evangelista</option>
            <option value="Fagundes Varela">Fagundes Varela</option>
            <option value="Fão">Fão</option>
            <option value="Faria Lemos">Faria Lemos</option>
            <option value="Farinhas">Farinhas</option>
            <option value="Farrapos">Farrapos</option>
            <option value="Farroupilha">Farroupilha</option>
            <option value="Faxinal do Soturno">Faxinal do Soturno</option>
            <option value="Faxinal">Faxinal</option>
            <option value="Faxinalzinho">Faxinalzinho</option>
            <option value="Fazenda Fialho">Fazenda Fialho</option>
            <option value="Fazenda Souza">Fazenda Souza</option>
            <option value="Fazenda Vilanova">Fazenda Vilanova</option>
            <option value="Feliz">Feliz</option>
            <option value="Ferreira">Ferreira</option>
            <option value="Flores da Cunha">Flores da Cunha</option>
            <option value="Floresta">Floresta</option>
            <option value="Floriano Peixoto">Floriano Peixoto</option>
            <option value="Flórida">Flórida</option>
            <option value="Fontoura Xavier">Fontoura Xavier</option>
            <option value="Formigueiro">Formigueiro</option>
            <option value="Formosa">Formosa</option>
            <option value="Forninho">Forninho</option>
            <option value="Forquetinha">Forquetinha</option>
            <option value="Fortaleza dos Valos">Fortaleza dos Valos</option>
            <option value="Frederico Westphalen">Frederico Westphalen</option>
            <option value="Freire">Freire</option>
            <option value="Frei Sebastião">Frei Sebastião</option>
            <option value="Garibaldina">Garibaldina</option>
            <option value="Garibaldi">Garibaldi</option>
            <option value="Garruchos">Garruchos</option>
            <option value="Gaurama">Gaurama</option>
            <option value="General Câmara">General Câmara</option>
            <option value="Gentil">Gentil</option>
            <option value="Getúlio Vargas">Getúlio Vargas</option>
            <option value="Giruá">Giruá</option>
            <option value="Glória">Glória</option>
            <option value="Glorinha">Glorinha</option>
            <option value="Goio-En">Goio-En</option>
            <option value="Gramado dos Loureiros">Gramado dos Loureiros</option>
            <option value="Gramado">Gramado</option>
            <option value="Gramado São Pedro">Gramado São Pedro</option>
            <option value="Gramado Xavier">Gramado Xavier</option>
            <option value="Gravataí">Gravataí</option>
            <option value="Guabiju">Guabiju</option>
            <option value="Guaíba">Guaíba</option>
            <option value="Guajuviras">Guajuviras</option>
            <option value="Guaporé">Guaporé</option>
            <option value="Guarani das Missões">Guarani das Missões</option>
            <option value="Guassu Boi">Guassu Boi</option>
            <option value="Guassupi">Guassupi</option>
            <option value="Harmonia">Harmonia</option>
            <option value="Herval">Herval</option>
            <option value="Herveiras">Herveiras</option>
            <option value="Hidráulica">Hidráulica</option>
            <option value="Horizontina">Horizontina</option>
            <option value="Hulha Negra">Hulha Negra</option>
            <option value="Humaitá">Humaitá</option>
            <option value="Ibarama">Ibarama</option>
            <option value="Ibaré">Ibaré</option>
            <option value="Ibiaçá">Ibiaçá</option>
            <option value="Ibiraiaras">Ibiraiaras</option>
            <option value="Ibirapuitã">Ibirapuitã</option>
            <option value="Ibirubá">Ibirubá</option>
            <option value="Igrejinha">Igrejinha</option>
            <option value="Ijucapirama">Ijucapirama</option>
            <option value="Ijuí">Ijuí</option>
            <option value="Ilha dos Marinheiros">Ilha dos Marinheiros</option>
            <option value="Ilópolis">Ilópolis</option>
            <option value="Imbé">Imbé</option>
            <option value="Imigrante">Imigrante</option>
            <option value="Independência">Independência</option>
            <option value="Inhacorá">Inhacorá</option>
            <option value="Inhanduí">Inhanduí</option>
            <option value="Ipê">Ipê</option>
            <option value="Ipiranga do Sul">Ipiranga do Sul</option>
            <option value="Ipiranga">Ipiranga</option>
            <option value="Ipuaçu">Ipuaçu</option>
            <option value="Iraí">Iraí</option>
            <option value="Iruí">Iruí</option>
            <option value="Itaara">Itaara</option>
            <option value="Itacolomi">Itacolomi</option>
            <option value="Itacurubi">Itacurubi</option>
            <option value="Itaimbezinho">Itaimbezinho</option>
            <option value="Itaí">Itaí</option>
            <option value="Itão">Itão</option>
            <option value="Itapororó">Itapororó</option>
            <option value="Itapuã">Itapuã</option>
            <option value="Itapucá">Itapucá</option>
            <option value="Itaqui">Itaqui</option>
            <option value="Itatiba do Sul">Itatiba do Sul</option>
            <option value="Itati">Itati</option>
            <option value="Itaúba">Itaúba</option>
            <option value="Ituim">Ituim</option>
            <option value="Ivaí">Ivaí</option>
            <option value="Ivorá">Ivorá</option>
            <option value="Ivoti">Ivoti</option>
            <option value="Jaboticaba">Jaboticaba</option>
            <option value="Jacuizinho">Jacuizinho</option>
            <option value="Jacutinga">Jacutinga</option>
            <option value="Jaguarão">Jaguarão</option>
            <option value="Jaguarete">Jaguarete</option>
            <option value="Jaguari">Jaguari</option>
            <option value="Jansen">Jansen</option>
            <option value="Jaquirana">Jaquirana</option>
            <option value="Jari">Jari</option>
            <option value="Jazidas ou Capela São Vicente">Jazidas ou Capela São Vicente</option>
            <option value="João Arregui">João Arregui</option>
            <option value="João Rodrigues">João Rodrigues</option>
            <option value="Joça Tavares">Joça Tavares</option>
            <option value="Jóia">Jóia</option>
            <option value="José Otávio">José Otávio</option>
            <option value="Juá">Juá</option>
            <option value="Júlio de Castilhos">Júlio de Castilhos</option>
            <option value="Lagoa Bonita do Sul">Lagoa Bonita do Sul</option>
            <option value="Lagoa dos Patos">Lagoa dos Patos</option>
            <option value="Lagoa dos Três Cantos">Lagoa dos Três Cantos</option>
            <option value="Lagoão">Lagoão</option>
            <option value="Lagoa Vermelha">Lagoa Vermelha</option>
            <option value="Lajeado Bonito">Lajeado Bonito</option>
            <option value="Lajeado Cerne">Lajeado Cerne</option>
            <option value="Lajeado do Bugre">Lajeado do Bugre</option>
            <option value="Lajeado Grande">Lajeado Grande</option>
            <option value="Lajeado">Lajeado</option>
            <option value="Lara">Lara</option>
            <option value="Laranjeira">Laranjeira</option>
            <option value="Lava-Pés">Lava-Pés</option>
            <option value="Lavras do Sul">Lavras do Sul</option>
            <option value="Leonel Rocha">Leonel Rocha</option>
            <option value="Liberato Salzano">Liberato Salzano</option>
            <option value="Lindolfo Collor">Lindolfo Collor</option>
            <option value="Linha Comprida">Linha Comprida</option>
            <option value="Linha Nova">Linha Nova</option>
            <option value="Linha Vitória">Linha Vitória</option>
            <option value="Loreto">Loreto</option>
            <option value="Maçambará">Maçambará</option>
            <option value="Machadinho">Machadinho</option>
            <option value="Magistério">Magistério</option>
            <option value="Mampituba">Mampituba</option>
            <option value="Manchinha">Manchinha</option>
            <option value="Mangueiras">Mangueiras</option>
            <option value="Manoel Viana">Manoel Viana</option>
            <option value="Maquiné">Maquiné</option>
            <option value="Maratá">Maratá</option>
            <option value="Marau">Marau</option>
            <option value="Marcelino Ramos">Marcelino Ramos</option>
            <option value="Marcorama">Marcorama</option>
            <option value="Mariana Pimentel">Mariana Pimentel</option>
            <option value="Mariano Moro">Mariano Moro</option>
            <option value="Mariante">Mariante</option>
            <option value="Mariápolis">Mariápolis</option>
            <option value="Marques de Souza">Marques de Souza</option>
            <option value="Matarazzo">Matarazzo</option>
            <option value="Mata">Mata</option>
            <option value="Mato Castelhano">Mato Castelhano</option>
            <option value="Mato Grande">Mato Grande</option>
            <option value="Mato Leitão">Mato Leitão</option>
            <option value="Mato Perso">Mato Perso</option>
            <option value="Mato Queimado">Mato Queimado</option>
            <option value="Mauá">Mauá</option>
            <option value="Maximiliano de Almeida">Maximiliano de Almeida</option>
            <option value="Medianeira">Medianeira</option>
            <option value="Minas do Leão">Minas do Leão</option>
            <option value="Miraguaia">Miraguaia</option>
            <option value="Miraguaí">Miraguaí</option>
            <option value="Mirim">Mirim</option>
            <option value="Montauri">Montauri</option>
            <option value="Monte Alegre">Monte Alegre</option>
            <option value="Monte Alegre dos Campos">Monte Alegre dos Campos</option>
            <option value="Monte Alverne">Monte Alverne</option>
            <option value="Monte Belo do Sul">Monte Belo do Sul</option>
            <option value="Monte Bonito">Monte Bonito</option>
            <option value="Montenegro">Montenegro</option>
            <option value="Mormaço">Mormaço</option>
            <option value="Morrinhos do Sul">Morrinhos do Sul</option>
            <option value="Morrinhos">Morrinhos</option>
            <option value="Morro Alto">Morro Alto</option>
            <option value="Morro Azul">Morro Azul</option>
            <option value="Morro Redondo">Morro Redondo</option>
            <option value="Morro Reuter">Morro Reuter</option>
            <option value="Morungava">Morungava</option>
            <option value="Mostardas">Mostardas</option>
            <option value="Muçum">Muçum</option>
            <option value="Muitos Capões">Muitos Capões</option>
            <option value="Muliterno">Muliterno</option>
            <option value="Não-Me-Toque">Não-Me-Toque</option>
            <option value="Nazaré">Nazaré</option>
            <option value="Nicolau Vergueiro">Nicolau Vergueiro</option>
            <option value="Nonoai">Nonoai</option>
            <option value="Nossa Senhora Aparecida">Nossa Senhora Aparecida</option>
            <option value="Nossa Senhora da Conceição">Nossa Senhora da Conceição</option>
            <option value="Nova Alvorada">Nova Alvorada</option>
            <option value="Nova Araçá">Nova Araçá</option>
            <option value="Nova Bassano">Nova Bassano</option>
            <option value="Nova Boa Vista">Nova Boa Vista</option>
            <option value="Nova Bréscia">Nova Bréscia</option>
            <option value="Nova Candelária">Nova Candelária</option>
            <option value="Nova Esperança do Sul">Nova Esperança do Sul</option>
            <option value="Nova Hartz">Nova Hartz</option>
            <option value="Nova Milano">Nova Milano</option>
            <option value="Nova Pádua">Nova Pádua</option>
            <option value="Nova Palma">Nova Palma</option>
            <option value="Nova Petrópolis">Nova Petrópolis</option>
            <option value="Nova Prata">Nova Prata</option>
            <option value="Nova Ramada">Nova Ramada</option>
            <option value="Nova Roma do Sul">Nova Roma do Sul</option>
            <option value="Nova Santa Rita">Nova Santa Rita</option>
            <option value="Nova Sardenha">Nova Sardenha</option>
            <option value="Novo Barreiro">Novo Barreiro</option>
            <option value="Novo Cabrais">Novo Cabrais</option>
            <option value="Novo Hamburgo">Novo Hamburgo</option>
            <option value="Novo Horizonte">Novo Horizonte</option>
            <option value="Novo Machado">Novo Machado</option>
            <option value="Novo Planalto">Novo Planalto</option>
            <option value="Novo Tiradentes">Novo Tiradentes</option>
            <option value="Novo Xingu">Novo Xingu</option>
            <option value="Oralina">Oralina</option>
            <option value="Osório">Osório</option>
            <option value="Osvaldo Cruz">Osvaldo Cruz</option>
            <option value="Osvaldo Kroeff">Osvaldo Kroeff</option>
            <option value="Otávio Rocha">Otávio Rocha</option>
            <option value="Pacheca">Pacheca</option>
            <option value="Padilha">Padilha</option>
            <option value="Padre Gonzales">Padre Gonzales</option>
            <option value="Paim Filho">Paim Filho</option>
            <option value="Palmares do Sul">Palmares do Sul</option>
            <option value="Palmas">Palmas</option>
            <option value="Palmeira das Missões">Palmeira das Missões</option>
            <option value="Palmitinho">Palmitinho</option>
            <option value="Pampeiro">Pampeiro</option>
            <option value="Panambi">Panambi</option>
            <option value="Pântano Grande">Pântano Grande</option>
            <option value="Paraí">Paraí</option>
            <option value="Paraíso do Sul">Paraíso do Sul</option>
            <option value="Pareci Novo">Pareci Novo</option>
            <option value="Parobé">Parobé</option>
            <option value="Passa Sete">Passa Sete</option>
            <option value="Passinhos">Passinhos</option>
            <option value="Passo Burmann">Passo Burmann</option>
            <option value="Passo da Areia">Passo da Areia</option>
            <option value="Passo da Caveira">Passo da Caveira</option>
            <option value="Passo das Pedras">Passo das Pedras</option>
            <option value="Passo do Adão">Passo do Adão</option>
            <option value="Passo do Goulart">Passo do Goulart</option>
            <option value="Passo do Sabão">Passo do Sabão</option>
            <option value="Passo do Sobrado">Passo do Sobrado</option>
            <option value="Passo do Verde">Passo do Verde</option>
            <option value="Passo Fundo">Passo Fundo</option>
            <option value="Passo Novo">Passo Novo</option>
            <option value="Passo Raso">Passo Raso</option>
            <option value="Paulo Bento">Paulo Bento</option>
            <option value="Pavão">Pavão</option>
            <option value="Paverama">Paverama</option>
            <option value="Pedras Altas">Pedras Altas</option>
            <option value="Pedreiras">Pedreiras</option>
            <option value="Pedro Garcia">Pedro Garcia</option>
            <option value="Pedro Osório">Pedro Osório</option>
            <option value="Pedro Paiva">Pedro Paiva</option>
            <option value="Pejuçara">Pejuçara</option>
            <option value="Pelotas">Pelotas</option>
            <option value="Picada Café">Picada Café</option>
            <option value="Pinhal Alto">Pinhal Alto</option>
            <option value="Pinhal da Serra">Pinhal da Serra</option>
            <option value="Pinhal Grande">Pinhal Grande</option>
            <option value="Pinhal">Pinhal</option>
            <option value="Pinhalzinho">Pinhalzinho</option>
            <option value="Pinheirinho do Vale">Pinheirinho do Vale</option>
            <option value="Pinheiro Machado">Pinheiro Machado</option>
            <option value="Pinheiro Marcado">Pinheiro Marcado</option>
            <option value="Pinto Bandeira">Pinto Bandeira</option>
            <option value="Piraí">Piraí</option>
            <option value="Pirapó">Pirapó</option>
            <option value="Piratini">Piratini</option>
            <option value="Pitanga">Pitanga</option>
            <option value="Planalto">Planalto</option>
            <option value="Plano Alto">Plano Alto</option>
            <option value="Poço das Antas">Poço das Antas</option>
            <option value="Polígono do Erval">Polígono do Erval</option>
            <option value="Pólo Petroquímico de Triunfo">Pólo Petroquímico de Triunfo</option>
            <option value="Pontão">Pontão</option>
            <option value="Ponte Preta">Ponte Preta</option>
            <option value="Portão">Portão</option>
            <option value="Porto Batista">Porto Batista</option>
            <option value="Porto Lucena">Porto Lucena</option>
            <option value="Porto Mauá">Porto Mauá</option>
            <option value="Porto Vera Cruz">Porto Vera Cruz</option>
            <option value="Porto Xavier">Porto Xavier</option>
            <option value="Pouso Novo">Pouso Novo</option>
            <option value="Povoado Tozzo">Povoado Tozzo</option>
            <option value="Povo Novo">Povo Novo</option>
            <option value="Prado Novo">Prado Novo</option>
            <option value="Pranchada">Pranchada</option>
            <option value="Pratos">Pratos</option>
            <option value="Presidente Lucena">Presidente Lucena</option>
            <option value="Progresso">Progresso</option>
            <option value="Protásio Alves">Protásio Alves</option>
            <option value="Pulador">Pulador</option>
            <option value="Putinga">Putinga</option>
            <option value="Quaraim">Quaraim</option>
            <option value="Quaraí">Quaraí</option>
            <option value="Quatro Irmãos">Quatro Irmãos</option>
            <option value="Quevedos">Quevedos</option>
            <option value="Quilombo">Quilombo</option>
            <option value="Quintão">Quintão</option>
            <option value="Quinta">Quinta</option>
            <option value="Quinze de Novembro">Quinze de Novembro</option>
            <option value="Quitéria">Quitéria</option>
            <option value="Rancho Velho">Rancho Velho</option>
            <option value="Redentora">Redentora</option>
            <option value="Refugiado">Refugiado</option>
            <option value="Relvado">Relvado</option>
            <option value="Restinga Seca">Restinga Seca</option>
            <option value="Rincão del Rei">Rincão del Rei</option>
            <option value="Rincão de São Miguel">Rincão de São Miguel</option>
            <option value="Rincão de São Pedro">Rincão de São Pedro</option>
            <option value="Rincão Doce">Rincão Doce</option>
            <option value="Rincão do Cristóvão Pereira">Rincão do Cristóvão Pereira</option>
            <option value="Rincão do Meio">Rincão do Meio</option>
            <option value="Rincão do Segredo">Rincão do Segredo</option>
            <option value="Rincão dos Kroeff">Rincão dos Kroeff</option>
            <option value="Rincão dos Mendes">Rincão dos Mendes</option>
            <option value="Rincão Vermelho">Rincão Vermelho</option>
            <option value="Rio Azul">Rio Azul</option>
            <option value="Rio Branco">Rio Branco</option>
            <option value="Rio da Ilha">Rio da Ilha</option>
            <option value="Rio dos Índios">Rio dos Índios</option>
            <option value="Rio Grande">Rio Grande</option>
            <option value="Rio Pardinho">Rio Pardinho</option>
            <option value="Rio Pardo">Rio Pardo</option>
            <option value="Rio Telha">Rio Telha</option>
            <option value="Rio Tigre">Rio Tigre</option>
            <option value="Rio Toldo">Rio Toldo</option>
            <option value="Riozinho">Riozinho</option>
            <option value="Roca Sales">Roca Sales</option>
            <option value="Rodeio Bonito">Rodeio Bonito</option>
            <option value="Rolador">Rolador</option>
            <option value="Rolante">Rolante</option>
            <option value="Rolantinho da Figueira">Rolantinho da Figueira</option>
            <option value="Ronda Alta">Ronda Alta</option>
            <option value="Rondinha">Rondinha</option>
            <option value="Roque Gonzales">Roque Gonzales</option>
            <option value="Rosário do Sul">Rosário do Sul</option>
            <option value="Rosário">Rosário</option>
            <option value="Sagrada Família">Sagrada Família</option>
            <option value="Saicã">Saicã</option>
            <option value="Saldanha Marinho">Saldanha Marinho</option>
            <option value="Saltinho">Saltinho</option>
            <option value="Salto do Jacuí">Salto do Jacuí</option>
            <option value="Salto">Salto</option>
            <option value="Salvador das Missões">Salvador das Missões</option>
            <option value="Salvador do Sul">Salvador do Sul</option>
            <option value="Sananduva">Sananduva</option>
            <option value="Santa Bárbara do Sul">Santa Bárbara do Sul</option>
            <option value="Santa Bárbara">Santa Bárbara</option>
            <option value="Santa Catarina">Santa Catarina</option>
            <option value="Santa Cecília do Sul">Santa Cecília do Sul</option>
            <option value="Santa Clara do Ingaí">Santa Clara do Ingaí</option>
            <option value="Santa Clara do Sul">Santa Clara do Sul</option>
            <option value="Santa Cristina">Santa Cristina</option>
            <option value="Santa Cruz">Santa Cruz</option>
            <option value="Santa Cruz da Concórdia">Santa Cruz da Concórdia</option>
            <option value="Santa Cruz do Sul">Santa Cruz do Sul</option>
            <option value="Santa Flora">Santa Flora</option>
            <option value="Santa Inês">Santa Inês</option>
            <option value="Santa Izabel do Sul">Santa Izabel do Sul</option>
            <option value="Santa Lúcia do Piaí">Santa Lúcia do Piaí</option>
            <option value="Santa Lúcia">Santa Lúcia</option>
            <option value="Santa Luíza">Santa Luíza</option>
            <option value="Santa Luzia">Santa Luzia</option>
            <option value="Santa Margarida do Sul">Santa Margarida do Sul</option>
            <option value="Santa Maria do Herval">Santa Maria do Herval</option>
            <option value="Santa Maria">Santa Maria</option>
            <option value="Santana da Boa Vista">Santana da Boa Vista</option>
            <option value="Santana do Livramento">Santana do Livramento</option>
            <option value="Santana">Santana</option>
            <option value="Santa Rita do Sul">Santa Rita do Sul</option>
            <option value="Santa Rosa">Santa Rosa</option>
            <option value="Santa Silvana">Santa Silvana</option>
            <option value="Santa Teresinha">Santa Teresinha</option>
            <option value="Santa Tereza">Santa Tereza</option>
            <option value="Sant'auta">Sant'auta</option>
            <option value="Santa Vitória do Palmar">Santa Vitória do Palmar</option>
            <option value="Santiago">Santiago</option>
            <option value="Santo Amaro do Sul">Santo Amaro do Sul</option>
            <option value="Santo Ângelo">Santo Ângelo</option>
            <option value="Santo Antônio da Patrulha">Santo Antônio da Patrulha</option>
            <option value="Santo Antônio das Missões">Santo Antônio das Missões</option>
            <option value="Santo Antônio de Castro">Santo Antônio de Castro</option>
            <option value="Santo Antônio do Bom Retiro">Santo Antônio do Bom Retiro</option>
            <option value="Santo Antônio do Palma">Santo Antônio do Palma</option>
            <option value="Santo Antônio do Planalto">Santo Antônio do Planalto</option>
            <option value="Santo Antônio">Santo Antônio</option>
            <option value="Santo Augusto">Santo Augusto</option>
            <option value="Santo Cristo">Santo Cristo</option>
            <option value="Santo Expedito do Sul">Santo Expedito do Sul</option>
            <option value="Santo Inácio">Santo Inácio</option>
            <option value="São Bento">São Bento</option>
            <option value="São Bom Jesus">São Bom Jesus</option>
            <option value="São Borja">São Borja</option>
            <option value="São Carlos">São Carlos</option>
            <option value="São Domingos do Sul">São Domingos do Sul</option>
            <option value="São Francisco de Assis">São Francisco de Assis</option>
            <option value="São Francisco de Paula">São Francisco de Paula</option>
            <option value="São Francisco">São Francisco</option>
            <option value="São Gabriel">São Gabriel</option>
            <option value="São Jerônimo">São Jerônimo</option>
            <option value="São João Batista">São João Batista</option>
            <option value="São João Bosco">São João Bosco</option>
            <option value="São João da Urtiga">São João da Urtiga</option>
            <option value="São João do Polesine">São João do Polesine</option>
            <option value="São João">São João</option>
            <option value="São Jorge">São Jorge</option>
            <option value="São José da Glória">São José da Glória</option>
            <option value="São José das Missões">São José das Missões</option>
            <option value="São José de Castro">São José de Castro</option>
            <option value="São José do Centro">São José do Centro</option>
            <option value="São José do Herval">São José do Herval</option>
            <option value="São José do Hortêncio">São José do Hortêncio</option>
            <option value="São José do Inhacorá">São José do Inhacorá</option>
            <option value="São José do Norte">São José do Norte</option>
            <option value="São José do Ouro">São José do Ouro</option>
            <option value="São José dos Ausentes">São José dos Ausentes</option>
            <option value="São José do Sul">São José do Sul</option>
            <option value="São José">São José</option>
            <option value="São Leopoldo">São Leopoldo</option>
            <option value="São Lourenço das Missões">São Lourenço das Missões</option>
            <option value="São Lourenço do Sul">São Lourenço do Sul</option>
            <option value="São Luís Rei">São Luís Rei</option>
            <option value="São Luiz Gonzaga">São Luiz Gonzaga</option>
            <option value="São Luiz">São Luiz</option>
            <option value="São Manuel">São Manuel</option>
            <option value="São Marcos">São Marcos</option>
            <option value="São Martinho da Serra">São Martinho da Serra</option>
            <option value="São Martinho">São Martinho</option>
            <option value="São Miguel das Missões">São Miguel das Missões</option>
            <option value="São Miguel">São Miguel</option>
            <option value="São Nicolau">São Nicolau</option>
            <option value="São Paulo">São Paulo</option>
            <option value="São Paulo das Missões">São Paulo das Missões</option>
            <option value="São Pedro da Serra">São Pedro da Serra</option>
            <option value="São Pedro das Missões">São Pedro das Missões</option>
            <option value="São Pedro do Butiá">São Pedro do Butiá</option>
            <option value="São Pedro do Iraxim">São Pedro do Iraxim</option>
            <option value="São Pedro do Sul">São Pedro do Sul</option>
            <option value="São Pedro">São Pedro</option>
            <option value="São Roque">São Roque</option>
            <option value="São Sebastião do Caí">São Sebastião do Caí</option>
            <option value="São Sebastião">São Sebastião</option>
            <option value="São Sepé">São Sepé</option>
            <option value="São Simão">São Simão</option>
            <option value="São Valentim do Sul">São Valentim do Sul</option>
            <option value="São Valentim">São Valentim</option>
            <option value="São Valério do Sul">São Valério do Sul</option>
            <option value="São Vendelino">São Vendelino</option>
            <option value="São Vicente do Sul">São Vicente do Sul</option>
            <option value="Sapiranga">Sapiranga</option>
            <option value="Sapucaia do Sul">Sapucaia do Sul</option>
            <option value="Sarandi">Sarandi</option>
            <option value="Seberi">Seberi</option>
            <option value="Sede Aurora">Sede Aurora</option>
            <option value="Sede Nova">Sede Nova</option>
            <option value="Segredo">Segredo</option>
            <option value="Seival">Seival</option>
            <option value="Selbach">Selbach</option>
            <option value="Senador Salgado Filho">Senador Salgado Filho</option>
            <option value="Sentinela do Sul">Sentinela do Sul</option>
            <option value="Serafim Schmidt">Serafim Schmidt</option>
            <option value="Serafina Corrêa">Serafina Corrêa</option>
            <option value="Sério">Sério</option>
            <option value="Serra dos Gregórios">Serra dos Gregórios</option>
            <option value="Serrinha">Serrinha</option>
            <option value="Serrinha Velha">Serrinha Velha</option>
            <option value="Sertão">Sertão</option>
            <option value="Sertão Santana">Sertão Santana</option>
            <option value="Sertãozinho">Sertãozinho</option>
            <option value="Sete de Setembro">Sete de Setembro</option>
            <option value="Sete Lagoas">Sete Lagoas</option>
            <option value="Severiano de Almeida">Severiano de Almeida</option>
            <option value="Silva Jardim">Silva Jardim</option>
            <option value="Silveira">Silveira</option>
            <option value="Silveira Martins">Silveira Martins</option>
            <option value="Sinimbu">Sinimbu</option>
            <option value="Sírio">Sírio</option>
            <option value="Sítio Gabriel">Sítio Gabriel</option>
            <option value="Sobradinho">Sobradinho</option>
            <option value="Soledade">Soledade</option>
            <option value="Souza Ramos">Souza Ramos</option>
            <option value="Suspiro">Suspiro</option>
            <option value="Tabaí">Tabaí</option>
            <option value="Tabajara">Tabajara</option>
            <option value="Taim">Taim</option>
            <option value="Tainhas">Tainhas</option>
            <option value="Tamanduá">Tamanduá</option>
            <option value="Tanque">Tanque</option>
            <option value="Tapejara">Tapejara</option>
            <option value="Tapera">Tapera</option>
            <option value="Tapes">Tapes</option>
            <option value="Taquaral">Taquaral</option>
            <option value="Taquara">Taquara</option>
            <option value="Taquarichim">Taquarichim</option>
            <option value="Taquari">Taquari</option>
            <option value="Taquaruçu do Sul">Taquaruçu do Sul</option>
            <option value="Tavares">Tavares</option>
            <option value="Tenente Portela">Tenente Portela</option>
            <option value="Terra de Areia">Terra de Areia</option>
            <option value="Tesouras">Tesouras</option>
            <option value="Teutônia">Teutônia</option>
            <option value="Tiaraju">Tiaraju</option>
            <option value="Timbaúva">Timbaúva</option>
            <option value="Tio Hugo">Tio Hugo</option>
            <option value="Tiradentes do Sul">Tiradentes do Sul</option>
            <option value="Toropi">Toropi</option>
            <option value="Toroquá">Toroquá</option>
            <option value="Torquato Severo">Torquato Severo</option>
            <option value="Torres">Torres</option>
            <option value="Torrinhas">Torrinhas</option>
            <option value="Touro Passo">Touro Passo</option>
            <option value="Tramandaí">Tramandaí</option>
            <option value="Travesseiro">Travesseiro</option>
            <option value="Trentin">Trentin</option>
            <option value="Três Arroios">Três Arroios</option>
            <option value="Três Barras">Três Barras</option>
            <option value="Três Cachoeiras">Três Cachoeiras</option>
            <option value="Três Coroas">Três Coroas</option>
            <option value="Três de Maio">Três de Maio</option>
            <option value="Três Forquilhas">Três Forquilhas</option>
            <option value="Três Palmeiras">Três Palmeiras</option>
            <option value="Três Passos">Três Passos</option>
            <option value="Três Vendas">Três Vendas</option>
            <option value="Trindade do Sul">Trindade do Sul</option>
            <option value="Triunfo">Triunfo</option>
            <option value="Tronqueiras">Tronqueiras</option>
            <option value="Tucunduva">Tucunduva</option>
            <option value="Tuiuti">Tuiuti</option>
            <option value="Tunas">Tunas</option>
            <option value="Túnel Verde">Túnel Verde</option>
            <option value="Tupanci do Sul">Tupanci do Sul</option>
            <option value="Tupanciretã">Tupanciretã</option>
            <option value="Tupancy ou Vila Block">Tupancy ou Vila Block</option>
            <option value="Tupandi">Tupandi</option>
            <option value="Tupantuba">Tupantuba</option>
            <option value="Tuparendi">Tuparendi</option>
            <option value="Tupinambá">Tupinambá</option>
            <option value="Tupi Silveira">Tupi Silveira</option>
            <option value="Turuçu">Turuçu</option>
            <option value="Turvinho">Turvinho</option>
            <option value="Ubiretama">Ubiretama</option>
            <option value="Umbu">Umbu</option>
            <option value="União da Serra">União da Serra</option>
            <option value="Unistalda">Unistalda</option>
            <option value="Uruguaiana">Uruguaiana</option>
            <option value="Vacacai">Vacacai</option>
            <option value="Vacaria">Vacaria</option>
            <option value="Valdástico">Valdástico</option>
            <option value="Vale do Rio Cai">Vale do Rio Cai</option>
            <option value="Vale do Sol">Vale do Sol</option>
            <option value="Vale dos Vinhedos">Vale dos Vinhedos</option>
            <option value="Vale Real">Vale Real</option>
            <option value="Vale Veneto">Vale Veneto</option>
            <option value="Vale Verde">Vale Verde</option>
            <option value="Vanini">Vanini</option>
            <option value="Vasco Alves">Vasco Alves</option>
            <option value="Venâncio Aires">Venâncio Aires</option>
            <option value="Vera Cruz">Vera Cruz</option>
            <option value="Veranópolis">Veranópolis</option>
            <option value="Vertentes">Vertentes</option>
            <option value="Vespasiano Correa">Vespasiano Correa</option>
            <option value="Viadutos">Viadutos</option>
            <option value="Viamão">Viamão</option>
            <option value="Vicente Dutra">Vicente Dutra</option>
            <option value="Victor Graeff">Victor Graeff</option>
            <option value="Vila Bender">Vila Bender</option>
            <option value="Vila Boqueirão">Vila Boqueirão</option>
            <option value="Vila Cruz">Vila Cruz</option>
            <option value="Vila Fernando Ferrari">Vila Fernando Ferrari</option>
            <option value="Vila Flores">Vila Flores</option>
            <option value="Vila Langaro">Vila Langaro</option>
            <option value="Vila Laranjeira">Vila Laranjeira</option>
            <option value="Vila Maria">Vila Maria</option>
            <option value="Vila Nova do Sul">Vila Nova do Sul</option>
            <option value="Vila Oliva">Vila Oliva</option>
            <option value="Vila Rica">Vila Rica</option>
            <option value="Vila Seca">Vila Seca</option>
            <option value="Vila Turvo">Vila Turvo</option>
            <option value="Vista Alegre do Prata">Vista Alegre do Prata</option>
            <option value="Vista Alegre">Vista Alegre</option>
            <option value="Vista Gaúcha">Vista Gaúcha</option>
            <option value="Vitória das Missões">Vitória das Missões</option>
            <option value="Vitória">Vitória</option>
            <option value="Volta Alegre">Volta Alegre</option>
            <option value="Volta Fechada">Volta Fechada</option>
            <option value="Volta Grande">Volta Grande</option>
            <option value="Westfália">Westfália</option>
            <option value="Xadrez">Xadrez</option>
            <option value="Xangri-lá">Xangri-lá</option>
          </select>
        </div>
        <div class="6u">
          <input type="text" class="text" name="txtHorario" placeholder="Horário" />
        </div>
      </div>
      <div class="row half">
        <div class="12u">
          <textarea name="txtMsg" placeholder="Informações adicionais"></textarea>
        </div>
      </div>
      <div class="row">
      	<div class="12u" id='btn_load_1'>
        	Processando...
        </div>
      
        <div class="12u" id="btn_1"> 
        	<a class="button submit" onclick='post("Controller/Vaga.controller.php?op=1","btn_1","btn_load_1","retorno_1","cad_vaga")'>Cadastrar vaga</a> 
            
            <div id='retorno_1' style="margin-top:15px;"></div>
            
        </div>
        
        <div class="12u"> 
        	<a onClick='getId("Controller/Vaga.controller.php?op=<?php echo sha1(1) ?>","gestor_vaga")'>Filtrar as vagas já cadastradas</a> 
        </div>

      </div>            
    </form>
    <?php
      }else{
		include 'Include/FiltroVagaEmpresa.view.php';
	  }
   0 ?>
    <!-- InstanceEndEditable --> </div>
</section>
<section id="candidato" class="two">
<div class="container" id='gestor_candidato'>
<!-- InstanceBeginEditable name="EditRegion10" -->

<?php
if(!isset($_SESSION['form_list_candidato'])){
?>

<header>
  <h2>Encontre novos candidatos</h2>
  <p>O candidato que você procura está aqui!!!</p>
</header>
<form onSubmit="return false" id="get_candidato">
  <div class="row half">
    <div class="12u" id='input_full2'> <b>Auto imagem</b> <br />
      <?php
		   $infoCandidatoDAO = new InfoCandidatoDAO();
		   
		   $list_imagem = $infoCandidatoDAO->getListCod();
		   
		   $cont = 1;
		   
		   foreach($list_imagem as $list){		  
		   	   echo "<input class='check' name='auto_".$list->auto_imagem."' type='checkbox' value='auto_".$list->auto_imagem."' />".$list->auto_imagem." &nbsp;";
			   
			   if($cont == 7){
			   		echo "<br />";
					
					$cont = 0;
			   }
			   
			   $cont++;
		   }  

		   ?>
    </div>
  </div>
  <div class="row half">
    <div class="6u">
      <input type="text" class="text" name="txtEscola" placeholder="Escola" 
      value="<?php if(isset($_SESSION['FiltroCandidato'])){ echo $_SESSION['FiltroCandidato']['txtEscola']; }?>" />
    </div>
    <div class="6u">
      <select name="cbAreaExperiencia">
        <option value=''> Candidatos com experiência em</option>
        <option value='Comercial, Vendas'>Comercial, Vendas</option>
        <option value='Administração'>Administração</option>
        <option value='Industrial, Produção, Fábrica'>Industrial, Produção, Fábrica</option>
        <option value='Logística'>Logística</option>
        <option value='Informática, TI, Telecomunicação'>Informática, TI, Telecomunicação</option>
        <option value='Construção, Manutenção'>Construção, Manutenção</option>
        <option value='Contabil, Finanças, Economia'>Contabil, Finanças, Economia</option>
        <option value='Alimentação, Gastronomia'>Alimentação, Gastronomia</option>
        <option value='Engenharia'>Engenharia</option>
        <option value='Telemarketing'>Telemarketing</option>
        <option value='Saúde'>Saúde</option>
        <option value='Recursos Humanos'>Recursos Humanos</option>
        <option value='Transportes'>Transportes</option>
        <option value='Educação, Ensino, Idiomas'>Educação, Ensino, Idiomas</option>
        <option value='Compras, Almoxarifado, Materiais, Suprimentos'>Compras, Almoxarifado, Materiais, Suprimentos</option>
        <option value='Jurídica'>Jurídica</option>
        <option value='Marketing'>Marketing</option>
        <option value='Hotelaria, Turismo'>Hotelaria, Turismo</option>
        <option value='Seguranca'>Segurança</option>
        <option value='Qualidade'>Qualidade</option>
        <option value='Arquitetura, Decoração, Designer'>Arquitetura, Decoração, Designer</option>
        <option value='Estética'>Estética</option>
        <option value='Comunicação, TV, Cinema'>Comunicação, TV, Cinema</option>
        <option value='Química, Petroquímica'>Química, Petroquímica</option>
        <option value='Comércio Exterior, Importação, Exportação'>Comércio Exterior, Importação, Exportação</option>
        <option value='Cultura, Lazer, Entretenimento'>Cultura, Lazer, Entretenimento</option>
        <option value='Agricultura, Pecuária, Veterinária'>Agricultura, Pecuária, Veterinária</option>
        <option value='Moda'>Moda</option>
        <option value='Auditoria'>Auditoria</option>
        <option value='Artes'>Artes</option>
        <option value='Serviços Sociais, Comunitários'>Serviços Sociais, Comunitários</option>
        <option value='Ciências, Pesquisa'>Ciências, Pesquisa</option>
        <option value='Meio Ambiente, Ecologia'>Meio Ambiente, Ecologia</option>
        <option value='Administracao'>Administração</option>
        <option value='Arquitetura'>Arquitetura</option>
        <option value='TI - Infra estrutura'>TI - Infra estrutura</option>
        <option value='TI - Desenvolvimento'>TI - Desenvolvimento</option>
        <option value='Financeiro'>Financeiro</option>
        <option value='Departamento pessoal'>Departamento pessoal</option>
        <option value='Recrutamento e Selecao'>Recrutamento e Seleção</option>
        <option value='Compras'>Compras</option>
        <option value='Manutencao predial'>Manutenção predial</option>
        <option value='Juridico'>Jurídico</option>
        <option value='Suprimentos e compras'>Suprimentos e compras</option>
        <option value='Marketing - comunicacao'>Marketing - comunicação</option>
        <option value='Logistica academica'>Logística acadêmica</option>
        <option value='Relacionamento com cliente'>Relacionamento com cliente</option>
        <option value='Secretaria escolar'>Secretaria escolar</option>
        <option value='Professor - Tec em informatica'>Professor - Tec em informática</option>
        <option value='Professor - Tec em administracao'>Professor - Tec em administração</option>
        <option value='Instrutor de lingua inglesa'>Instrutor de lingua inglesa</option>
        <option value='Instrutor profissional'>Instrutor profissional</option>
      </select>
      
      <?php 
	  if(isset($_SESSION['FiltroCandidato'])){ ?>
		<script>	
		get_candidato.cbAreaExperiencia.value = "<?php echo $_SESSION['FiltroCandidato']['cbAreaExperiencia']  ?>";
		</script>
	  <?php 
	  }
	  ?>
    </div>
  </div>
  <div class="row half">
    <div class="6u">
      <select name="cbNivel" onChange='cadastroRapido("Controller/Curso.controller.php?op=3","curso_input","get_candidato")'>
        <option value=''> Nível do curso </option>
        <option value='Ensino médio'>Ensino médio</option>
        <option value='Profissionalizante'>Profissionalizante</option>
        <option value='Técnicos'>Técnicos</option>
        <option value='Superior'>Superior</option>
        <option value='Pós'>Pós</option>
      </select>
      
      <?php 
	  if(isset($_SESSION['FiltroCandidato'])){ ?>
		<script>	
		get_candidato.cbNivel.value = "<?php echo $_SESSION['FiltroCandidato']['cbNivel']  ?>";
		</script>
	  <?php 
	  }
	  ?>
      
    </div>
    <div class="6u" id='curso_input'>
      <?php
	  $nivel = $_SESSION['FiltroCandidato']['cbNivel'];
	  
	  include 'Include/setCurso.view.php';
	  ?>
    </div>
  </div>
  <div class="row half">
    <div class="6u">
      <select name="cbCargo" onChange='cadastroRapido("Controller/AreaInteresse.controller.php?op=3","return_funcao_2","get_candidato")'>
        <option value=''> Área da vaga</option>
        <option value='Arquitetura'>Arquitetura</option>
        <option value='Cobrança'>Cobrança</option>
        <option value='Comercial'>Comercial</option>
        <option value='CPD'>CPD</option>
        <option value='Departamento pessoal'>Departamento pessoal</option>
        <option value='Educação'>Educação</option>
        <option value='Financeiro'>Financeiro</option>
        <option value='Juridico'>Jurídico</option>
        <option value='Legislação academica'>Legislação acadêmica</option>
        <option value='Manutenção'>Manutenção</option>
        <option value='Marketing'>Marketing</option>
        <option value='Recrutamento e seleção'>Recrutamento e seleção</option>
        <option value='Serviços gerais'>Serviços gerais</option>
        <option value='Suprimentos'>Suprimentos</option>
        <option value='Juridico'>Jurídico</option>
        <option value='TI - Desenvolvimento'>TI - Desenvolvimento</option>
        <option value='TI - Infraestrutura'>TI - Infraestrutura</option>
        <option value='Treinamento'>Treinamento</option>
      </select>
      
      <?php 
	  if(isset($_SESSION['FiltroCandidato'])){ ?>
		<script>	
		get_candidato.cbCargo.value = "<?php echo $_SESSION['FiltroCandidato']['cbCargo']  ?>";
		</script>
	  <?php 
	  }
	  ?>
      
    </div>
    <div class="6u" id='return_funcao_2'>
      <?php
	  if(isset($_SESSION['FiltroCandidato'])){
		  $list_funcao = array();
		  
		  if($_SESSION['FiltroCandidato']['cbCargo'] == "Arquitetura"){
			  $list_funcao = 
			  array(
			  'Função',
			  'ARQUITETO',
			  'COORDENADOR / SUPERVISOR',
			  'ESTÁGIO'
			  );
		  }else if($_SESSION['FiltroCandidato']['cbCargo'] == "Cobrança"){
			  $list_funcao = 
			  array(
			  'COORDENADOR / SUPERVISOR',
			  'ASSIST. RECUP. DE CREDITO',
			  'ESTÁGIO'
			  );
		  }else if($_SESSION['FiltroCandidato']['cbCargo'] == "Comercial"){
			  $list_funcao = 
			  array(
			  'GERENTE COMERCIAL',
			  'GERENTE COMERCIAL DAS FALCULDADES',
			  'GERENTE DE EQUIPE COMERCIAL',
			  'ESPECIALISTA EM PLANEJAMENTO DE VENDAS',
			  'ANALISTA DE PLANEJAMENTO DE VENDAS',
			  'COORDENADOR ADMINISTRATIVO COMERCIAL',
			  'AGENDADOR(A)',
			  'ATENDENTE DE CHAT',
			  'DIVULGADOR (A)',
			  'AUXILIAR DE ATENDIMENTO',
			  'VENDEDOR(A)',
			  'ESTÁGIO'
			  );
		  }else if($_SESSION['FiltroCandidato']['cbCargo'] == "CPD"){
			  $list_funcao = 
			  array(
			  'ESTAGIÁRIO',
			  'AUXILIAR DE CPD'
			  );
		  }else if($_SESSION['FiltroCandidato']['cbCargo'] == "Departamento pessoal"){
			  $list_funcao = 
			  array(
			  'ANALISTA / AUXILIAR / ASSITENTE',
			  'COORDENADOR / SUPERVISOR',
			  'ESTÁGIO'
			  );
		  }else if($_SESSION['FiltroCandidato']['cbCargo'] == "Educação"){
		  
			  $list_funcao = 
			  array(
			  'DIRETOR DE FACULDADE',
			  'DIRETOR(A) DE ESCOLA',
			  'VICE DIRETOR(A) FACULDADE',
			  'DIRIGENTE REGIONAL',
			  'GERENTE DE FACULDADE',
			  'GERENTE DE FILIAL',
			  'GESTOR(A) AREA IDIOMA',
			  'COORDENADOR DE GRADUAÇÃO',
			  'COORDENADOR DE  PÓS GRADUAÇÃO',
			  'COORDENADOR EDUCAÇÃO TÉCNICA',
			  'COORDENADOR EDUCAÇÃO CURSOS LIVRES',
			  'COORDENADOR (A) NADD',
			  'COORDENADOR DE IDIOMAS',
			  'PROF. NIVEL SUPERIOR',
			  'PROFESSOR(A) DE NIVEL TECNICO ADM',
			  'PROFESSOR(A) DE NIVEL TECNICO INFO',
			  'PROF. TITULAR DE ADMINISTRAÇÃO',
			  'TUTOR',
			  'INSTRUTOR DE CURSOS LIVRES',
			  'INSTRUTOR DE IDIOMAS',
			  'GERENTE DE PLANEJAMENTO DE TURMAS',
			  'ASSISTENTE DE PLANEJAMENO DE TURMAS',
			  'SECRETARIA ACADEMICA',
			  'SECRETARIO(A)  GERAL',
			  'SECRETARIO(A) DE ESCOLA',
			  'INTERPRETE DE LIBRAS',
			  'ESTÁGIO'
			  );
		  
		  }else if($_SESSION['FiltroCandidato']['cbCargo'] == "Financeiro"){
		  
			  $list_funcao = 
			  array(
			  'GERENTE ADMINISTRATIVO FINANCEIRO',
			  'ANALISTA / AUXILIAR / ASSISTENTE',
			  'COORDENADOR / SUPERVISOR',
			  'ESTÁGIO',
			  'COORDENADOR / SUPERVISOR'
			  );
		  
		  }else if($_SESSION['FiltroCandidato']['cbCargo'] == "Juridico"){
		  
			  $list_funcao = 
			  array(
			  'ADVOGADO (A)',
			  'COORDENADOR JURIDICO',
			  'ESTÁGIO'
			  );
		  
		  }else if($_SESSION['FiltroCandidato']['cbCargo'] == "Legislação academica"){
		  
			  $list_funcao = 
			  array(
			  'GERENTE DE LEGISLAÇÃO ESCOLA',
			  'SUPERVISOR ESCOLAR',
			  'ASSIST. DE SUPERV. ESCOLAS',
			  'ASSISTENTE PEDAGOGICO',
			  'BIBLIOTECARIO',
			  'ESTÁGIO'
			  );
		  
		  }else if($_SESSION['FiltroCandidato']['cbCargo'] == "Manutenção"){
		  
			  $list_funcao = 
			  array(
			  'COORDENADOR DE MANUTENÇÃO',
			  'SUPERVISOR DE MANUTENÇÃO',
			  'AUXILIAR DE MANUTENÇÃO',
			  'ESTOFADOR',
			  'MECANICO DE REFRIGERACAO'
			  );
		  
		  }else if($_SESSION['FiltroCandidato']['cbCargo'] == "Marketing"){
		  
			  $list_funcao = 
			  array(
			  'GERENTE DE MARKETING',
			  'DIRETOR DE CRIAÇÃO',
			  'ASSITENTE DE CRIAÇÃO',
			  'ASSISTENTE DE MARKETING DIGITAL',
			  'ASSITENTE DE MARKETING DE EVENTOS',
			  'ESTAGIÁRIO'
			  );
		  
		  }else if($_SESSION['FiltroCandidato']['cbCargo'] == "Recrutamento e seleção"){
		  
			  $list_funcao = 
			  array(
			  'COORDENADOR / SUPERVISOR',
			  'ANALISTA / AUXILIAR / ASSISTENTE',
			  'ESTÁGIO'
			  );
		  
		  }else if($_SESSION['FiltroCandidato']['cbCargo'] == "Serviços gerais"){
		  
			  $list_funcao = 
			  array(
			  'ZELADOR',
			  'AUXILIAR DE LIMPEZA'
			  );
		  
		  }else if($_SESSION['FiltroCandidato']['cbCargo'] == "Suprimentos"){
		  
			  $list_funcao = 
			  array(
			  'COORDENADOR / SUPERVISOR',
			  'ANALISTA / AUXILIAR / ASSISTENTE',
			  'ARQUITETO',
			  'ESTÁGIO'
			  );
		  
		  }else if($_SESSION['FiltroCandidato']['cbCargo'] == "TI - Desenvolvimento"){
		  
			  $list_funcao = 
			  array(
			  'COORDENADOR / SUPERVISOR',
			  'ANALISTA / AUXILIAR / ASSISTENTE',
			  'ESTÁGIO'
			  );
		  
		  }else if($_SESSION['FiltroCandidato']['cbCargo'] == "TI - Infraestrutura"){
		  
			  $list_funcao = 
			  array(
			  'COORDENADOR DE T&D',
			  'ANALISTA DE T&D',
			  'ESTÁGIO'
			  );
		  
		  
		  }else if($_SESSION['FiltroCandidato']['cbCargo'] == "Treinamento"){
		  
			  $list_funcao = 
			  array(
			  'CORDENADOR DE T&D',
			  'ANALISTA DE T&D',
			  'ESTAGIÁRIO',
			  'SECRETARIA EXECUTIVA'
			  );
		  }
		  
		  echo '<select name="cbFuncao" >';
		   
		  echo "<option value=''> Selecione a área </option> ";
		   
		  foreach($list_funcao as $funcao){
			  echo "<option value='".$funcao."'> ".$funcao." </option>";
		  }
		  
		  echo '</select>';
	  }else{
		  echo '<select name="cbFuncao" disabled>
				  <option value=""> Selecione a área </option>
				</select>';
	  }
	  ?>
            
    </div>
  </div>
  <div class="row half">
    <div class="6u">
      <input type="text" class="text" name="txtNome" placeholder="Nome" 
      value="<?php if(isset($_SESSION['FiltroCandidato'])){ echo $_SESSION['FiltroCandidato']['txtNome']; }?>" />
    </div>
    <div class="6u">
      <select name="cbInformatica" >
        <option value=''> Conhecimento em informática?</option>
        <option value='Não conheço informatica'> Não conhece informática </option>
        <option value='Conhecimento básico em informatica'> Conhecimento básico em informática </option>
        <option value='Conhecimento avançado em informatica'> Conhecimento avançado em informática </option>
      </select>
      
      <?php 
	  if(isset($_SESSION['FiltroCandidato'])){ ?>
		<script>	
		get_candidato.cbInformatica.value = "<?php echo $_SESSION['FiltroCandidato']['cbInformatica']  ?>";
		</script>
	  <?php 
	  }
	  ?>
      
    </div>
  </div>
  <div class="row half">
    <div class="6u" id='input_full'> <b>CNH</b> <br />                       
    <input class='check' name='ckCnha' type='checkbox' value='ckCnha' <?php if(isset($_SESSION['FiltroCandidato']['ckCnha'])){ echo 'checked'; }?> /> A
    <input class='check' name='ckCnhb' type='checkbox' value='ckCnhb' <?php if(isset($_SESSION['FiltroCandidato']['ckCnhb'])){ echo 'checked'; }?> /> B
    <input class='check' name='ckCnhc' type='checkbox' value='ckCnhc' <?php if(isset($_SESSION['FiltroCandidato']['ckCnhc'])){ echo 'checked'; }?> /> C
    <input class='check' name='ckCnhd' type='checkbox' value='ckCnhd' <?php if(isset($_SESSION['FiltroCandidato']['ckCnhd'])){ echo 'checked'; }?> /> D
    <input class='check' name='ckCnhe' type='checkbox' value='ckCnhe' <?php if(isset($_SESSION['FiltroCandidato']['ckCnhe'])){ echo 'checked'; }?> /> E                             
    </div>                    
                        
    <div class="6u" id='input_full'> <b>Turnos disponível?</b> <br />          	
    <input class='check' name='ckManha' type='checkbox' value='ckManha' <?php if(isset($_SESSION['FiltroCandidato']['ckManha'])){ echo 'checked'; }?> /> Manhã
    <input class='check' name='ckTarde' type='checkbox' value='ckTarde' <?php if(isset($_SESSION['FiltroCandidato']['ckTarde'])){ echo 'checked'; }?> /> Tarde
    <input class='check' name='ckNoite' type='checkbox' value='ckNoite' <?php if(isset($_SESSION['FiltroCandidato']['ckNoite'])){ echo 'checked'; }?> /> Noite			
    </div>
        
    </div>
        
    <div class="row half">          
    <div class="6u" id='input_full'> <b>Dias disponível?</b> <br />            
      <input class='check' name='ckSeg'  type='checkbox' value='ckSeg' <?php if(isset($_SESSION['FiltroCandidato']['ckSeg'])){ echo 'checked'; }?> /> Seg
      
      <input class='check' name='ckTer'  type='checkbox' value='ckTer' <?php if(isset($_SESSION['FiltroCandidato']['ckTer'])){ echo 'checked'; }?> /> Ter
     
     <input class='check' name='ckQua'  type='checkbox' value='ckQua' <?php if(isset($_SESSION['FiltroCandidato']['ckQua'])){ echo 'checked'; }?> /> Qua
      
      <input class='check' name='ckQui'  type='checkbox' value='ckQui' <?php if(isset($_SESSION['FiltroCandidato']['ckQui'])){ echo 'checked'; }?> /> Qui 
      
      <input class='check' name='ckSex'  type='checkbox' value='ckSex' <?php if(isset($_SESSION['FiltroCandidato']['ckSex'])){ echo 'checked'; }?> /> Sex <br />
      
      <input input  name='ckSab'  type='checkbox' value='ckSab' <?php if(isset($_SESSION['FiltroCandidato']['ckSab'])){ echo 'checked'; }?> /> Sáb
      
      <input input  name='ckDomingo'  type='checkbox' value='ckDomingo' <?php if(isset($_SESSION['FiltroCandidato']['ckDomingo'])){ echo 'checked'; }?> /> Domingo				
    </div>
    
    <div class="6u" id='input_full'> <b>Idiomas</b> <br />          	          
    <input class='check' name='ckIngles' type='checkbox' value='ckIngles' <?php if(isset($_SESSION['FiltroCandidato']['ckIngles'])){ echo 'checked'; }?> /> Inglês
    <input class='check' name='ckItaliano' type='checkbox' value='ckItaliano' <?php if(isset($_SESSION['FiltroCandidato']['ckItaliano'])){ echo 'checked'; }?> /> Italiano
    <input class='check' name='ckFrances' type='checkbox' value='ckFrances' <?php if(isset($_SESSION['FiltroCandidato']['ckFrances'])){ echo 'checked'; }?> /> Francês<br/>
    <input class='check' name='ckEspanhol' type='checkbox' value='ckEspanhol' <?php if(isset($_SESSION['FiltroCandidato']['ckEspanhol'])){ echo 'checked'; }?> /> Espanhol
    <input class='check' name='ckOutros' type='checkbox' value='ckOutros' <?php if(isset($_SESSION['FiltroCandidato']['ckOutros'])){ echo 'checked'; }?> /> Outros				
    </div>  
  </div>
  <div class="row half">
    <div class="6u">
      <input type="text" class="text" name="txtEstado" placeholder="Estado" value="Rio Grande do Sul" disabled/>
    </div>
    <div class="6u">
      <select name="txtCidade">
        <option value="">Cidade</option>
        <option value="Porto Alegre">Porto Alegre</option>
        <option value="Aceguá">Aceguá</option>
        <option value="Afonso Rodrigues">Afonso Rodrigues</option>
        <option value="Aguapés">Aguapés</option>
        <option value="Água Santa">Água Santa</option>
        <option value="Águas Claras">Águas Claras</option>
        <option value="Agudo">Agudo</option>
        <option value="Ajuricaba">Ajuricaba</option>
        <option value="Albardão">Albardão</option>
        <option value="Alecrim">Alecrim</option>
        <option value="Alegrete">Alegrete</option>
        <option value="Alegria">Alegria</option>
        <option value="Alfredo Brenner">Alfredo Brenner</option>
        <option value="Almirante Tamandaré do Sul">Almirante Tamandaré do Sul</option>
        <option value="Alpestre">Alpestre</option>
        <option value="Alto Alegre">Alto Alegre</option>
        <option value="Alto da União">Alto da União</option>
        <option value="Alto Feliz">Alto Feliz</option>
        <option value="Alto Paredão">Alto Paredão</option>
        <option value="Alto Recreio">Alto Recreio</option>
        <option value="Alto Uruguai">Alto Uruguai</option>
        <option value="Alvorada">Alvorada</option>
        <option value="Amaral Ferrador">Amaral Ferrador</option>
        <option value="Ametista do Sul">Ametista do Sul</option>
        <option value="André da Rocha">André da Rocha</option>
        <option value="Anta Gorda">Anta Gorda</option>
        <option value="Antônio Kerpel">Antônio Kerpel</option>
        <option value="Antônio Prado">Antônio Prado</option>
        <option value="Arambaré">Arambaré</option>
        <option value="Araricá">Araricá</option>
        <option value="Aratiba">Aratiba</option>
        <option value="Arco-Íris">Arco-Íris</option>
        <option value="Arco Verde">Arco Verde</option>
        <option value="Arroio Canoas">Arroio Canoas</option>
        <option value="Arroio do Meio">Arroio do Meio</option>
        <option value="Arroio do Padre">Arroio do Padre</option>
        <option value="Arroio do Sal">Arroio do Sal</option>
        <option value="Arroio do Só">Arroio do Só</option>
        <option value="Arroio dos Ratos">Arroio dos Ratos</option>
        <option value="Arroio do Tigre">Arroio do Tigre</option>
        <option value="Arroio Grande">Arroio Grande</option>
        <option value="Árvore Só">Árvore Só</option>
        <option value="Arvorezinha">Arvorezinha</option>
        <option value="Atafona">Atafona</option>
        <option value="Atiaçu">Atiaçu</option>
        <option value="Augusto Pestana">Augusto Pestana</option>
        <option value="Áurea">Áurea</option>
        <option value="Avelino Paranhos">Avelino Paranhos</option>
        <option value="Azevedo Sodré">Azevedo Sodré</option>
        <option value="Bacupari">Bacupari</option>
        <option value="Bagé">Bagé</option>
        <option value="Baliza">Baliza</option>
        <option value="Balneário Pinhal">Balneário Pinhal</option>
        <option value="Banhado do Colégio">Banhado do Colégio</option>
        <option value="Barão de Cotegipe">Barão de Cotegipe</option>
        <option value="Barão do Triunfo">Barão do Triunfo</option>
        <option value="Barão">Barão</option>
        <option value="Barracão">Barracão</option>
        <option value="Barra do Guarita">Barra do Guarita</option>
        <option value="Barra do Ouro">Barra do Ouro</option>
        <option value="Barra do Quaraí">Barra do Quaraí</option>
        <option value="Barra do Ribeiro">Barra do Ribeiro</option>
        <option value="Barra do Rio Azul">Barra do Rio Azul</option>
        <option value="Barra Funda">Barra Funda</option>
        <option value="Barreirinho">Barreirinho</option>
        <option value="Barreiro">Barreiro</option>
        <option value="Barro Preto">Barro Preto</option>
        <option value="Barros Cassal">Barros Cassal</option>
        <option value="Barro Vermelho">Barro Vermelho</option>
        <option value="Basílio">Basílio</option>
        <option value="Bela Vista">Bela Vista</option>
        <option value="Beluno">Beluno</option>
        <option value="Benjamin Constant do Sul">Benjamin Constant do Sul</option>
        <option value="Bento Gonçalves">Bento Gonçalves</option>
        <option value="Bexiga">Bexiga</option>
        <option value="Boa Esperança">Boa Esperança</option>
        <option value="Boa Vista das Missões">Boa Vista das Missões</option>
        <option value="Boa Vista do Buricá">Boa Vista do Buricá</option>
        <option value="Boa Vista do Cadeado">Boa Vista do Cadeado</option>
        <option value="Boa Vista do Incra">Boa Vista do Incra</option>
        <option value="Boa Vista do Sul">Boa Vista do Sul</option>
        <option value="Boa Vista">Boa Vista</option>
        <option value="Boca do Monte">Boca do Monte</option>
        <option value="Boi Preto">Boi Preto</option>
        <option value="Bojuru">Bojuru</option>
        <option value="Bom Jardim">Bom Jardim</option>
        <option value="Bom Jesus">Bom Jesus</option>
        <option value="Bom Princípio">Bom Princípio</option>
        <option value="Bom Progresso">Bom Progresso</option>
        <option value="Bom Retiro do Guaíba">Bom Retiro do Guaíba</option>
        <option value="Bom Retiro do Sul">Bom Retiro do Sul</option>
        <option value="Bom Retiro">Bom Retiro</option>
        <option value="Bonito">Bonito</option>
        <option value="Boqueirão do Leão">Boqueirão do Leão</option>
        <option value="Boqueirão">Boqueirão</option>
        <option value="Bororé">Bororé</option>
        <option value="Borussia">Borussia</option>
        <option value="Bossoroca">Bossoroca</option>
        <option value="Botucaraí">Botucaraí</option>
        <option value="Bozano">Bozano</option>
        <option value="Braga">Braga</option>
        <option value="Brochier">Brochier</option>
        <option value="Buriti">Buriti</option>
        <option value="Butiá">Butiá</option>
        <option value="Butiás">Butiás</option>
        <option value="Caçapava do Sul">Caçapava do Sul</option>
        <option value="Cacequi">Cacequi</option>
        <option value="Cachoeira do Sul">Cachoeira do Sul</option>
        <option value="Cachoeirinha">Cachoeirinha</option>
        <option value="Cacique Doble">Cacique Doble</option>
        <option value="Cadorna">Cadorna</option>
        <option value="Caibaté">Caibaté</option>
        <option value="Caiçara">Caiçara</option>
        <option value="Camaquã">Camaquã</option>
        <option value="Camargo">Camargo</option>
        <option value="Cambará do Sul">Cambará do Sul</option>
        <option value="Campestre Baixo">Campestre Baixo</option>
        <option value="Campestre da Serra">Campestre da Serra</option>
        <option value="Campina das Missões">Campina das Missões</option>
        <option value="Campina Redonda">Campina Redonda</option>
        <option value="Campinas do Sul">Campinas do Sul</option>
        <option value="Campinas">Campinas</option>
        <option value="Campo Bom">Campo Bom</option>
        <option value="Campo Branco">Campo Branco</option>
        <option value="Campo do Meio">Campo do Meio</option>
        <option value="Campo Novo">Campo Novo</option>
        <option value="Campo Santo">Campo Santo</option>
        <option value="Campos Borges">Campos Borges</option>
        <option value="Campo Seco">Campo Seco</option>
        <option value="Campo Vicente">Campo Vicente</option>
        <option value="Candelária">Candelária</option>
        <option value="Cândido Freire">Cândido Freire</option>
        <option value="Cândido Godói">Cândido Godói</option>
        <option value="Candiota">Candiota</option>
        <option value="Canela">Canela</option>
        <option value="Canguçu">Canguçu</option>
        <option value="Canhembora">Canhembora</option>
        <option value="Canoas">Canoas</option>
        <option value="Canudos do Vale">Canudos do Vale</option>
        <option value="Capané">Capané</option>
        <option value="Capão Bonito do Sul">Capão Bonito do Sul</option>
        <option value="Capão Bonito">Capão Bonito</option>
        <option value="Capão Comprido">Capão Comprido</option>
        <option value="Capão da Canoa">Capão da Canoa</option>
        <option value="Capão da Porteira">Capão da Porteira</option>
        <option value="Capão do Cedro">Capão do Cedro</option>
        <option value="Capão do Cipó">Capão do Cipó</option>
        <option value="Capão do Leão">Capão do Leão</option>
        <option value="Capela de Santana">Capela de Santana</option>
        <option value="Capela Velha">Capela Velha</option>
        <option value="Capinzal">Capinzal</option>
        <option value="Capitão">Capitão</option>
        <option value="Capivari do Sul">Capivari do Sul</option>
        <option value="Capivarita">Capivarita</option>
        <option value="Capoeira Grande">Capoeira Grande</option>
        <option value="Capo-Erê">Capo-Erê</option>
        <option value="Caraá">Caraá</option>
        <option value="Carajá Seival">Carajá Seival</option>
        <option value="Carazinho">Carazinho</option>
        <option value="Carlos Barbosa">Carlos Barbosa</option>
        <option value="Carlos Gomes">Carlos Gomes</option>
        <option value="Carovi">Carovi</option>
        <option value="Casca">Casca</option>
        <option value="Cascata">Cascata</option>
        <option value="Caseiros">Caseiros</option>
        <option value="Castelinho">Castelinho</option>
        <option value="Catimbau">Catimbau</option>
        <option value="Catuípe">Catuípe</option>
        <option value="Cavajureta">Cavajureta</option>
        <option value="Caverá">Caverá</option>
        <option value="Caxias do Sul">Caxias do Sul</option>
        <option value="Cazuza Ferreira">Cazuza Ferreira</option>
        <option value="Cedro Marcado">Cedro Marcado</option>
        <option value="Centenário">Centenário</option>
        <option value="Centro Linha Brasil">Centro Linha Brasil</option>
        <option value="Cerrito Alegre">Cerrito Alegre</option>
        <option value="Cerrito do Ouro ou Vila do Cerrito">Cerrito do Ouro ou Vila do Cerrito</option>
        <option value="Cerrito">Cerrito</option>
        <option value="Cerro Alto">Cerro Alto</option>
        <option value="Cerro Branco">Cerro Branco</option>
        <option value="Cerro Claro">Cerro Claro</option>
        <option value="Cerro do Martins">Cerro do Martins</option>
        <option value="Cerro do Roque">Cerro do Roque</option>
        <option value="Cerro Grande do Sul">Cerro Grande do Sul</option>
        <option value="Cerro Grande">Cerro Grande</option>
        <option value="Cerro Largo">Cerro Largo</option>
        <option value="Chapada">Chapada</option>
        <option value="Charqueadas">Charqueadas</option>
        <option value="Charrua">Charrua</option>
        <option value="Chiapetta">Chiapetta</option>
        <option value="Chicolomã">Chicolomã</option>
        <option value="Chimarrão">Chimarrão</option>
        <option value="Chorão">Chorão</option>
        <option value="Chuí">Chuí</option>
        <option value="Chuvisca">Chuvisca</option>
        <option value="Cidreira">Cidreira</option>
        <option value="Cinqüentenário">Cinqüentenário</option>
        <option value="Ciríaco">Ciríaco</option>
        <option value="Clara">Clara</option>
        <option value="Clemente Argolo">Clemente Argolo</option>
        <option value="Coimbra">Coimbra</option>
        <option value="Colinas">Colinas</option>
        <option value="Colônia das Almas">Colônia das Almas</option>
        <option value="Colônia Medeiros">Colônia Medeiros</option>
        <option value="Colônia Municipal">Colônia Municipal</option>
        <option value="Colônia Nova">Colônia Nova</option>
        <option value="Colônia São João">Colônia São João</option>
        <option value="Colônia Z-3">Colônia Z-3</option>
        <option value="Coloninha">Coloninha</option>
        <option value="Colorado">Colorado</option>
        <option value="Comandai">Comandai</option>
        <option value="Condor">Condor</option>
        <option value="Consolata">Consolata</option>
        <option value="Constantina">Constantina</option>
        <option value="Coqueiro Baixo">Coqueiro Baixo</option>
        <option value="Coqueiros do Sul">Coqueiros do Sul</option>
        <option value="Cordilheira">Cordilheira</option>
        <option value="Coroados">Coroados</option>
        <option value="Coronel Barros">Coronel Barros</option>
        <option value="Coronel Bicaco">Coronel Bicaco</option>
        <option value="Coronel Finzito">Coronel Finzito</option>
        <option value="Coronel Pilar">Coronel Pilar</option>
        <option value="Coronel Teixeira">Coronel Teixeira</option>
        <option value="Cortado">Cortado</option>
        <option value="Costa da Cadeia">Costa da Cadeia</option>
        <option value="Costão">Costão</option>
        <option value="Cotiporã">Cotiporã</option>
        <option value="Coxilha Grande">Coxilha Grande</option>
        <option value="Coxilha">Coxilha</option>
        <option value="Cr-1">Cr-1</option>
        <option value="Crissiumal">Crissiumal</option>
        <option value="Cristal do Sul">Cristal do Sul</option>
        <option value="Cristal">Cristal</option>
        <option value="Criúva">Criúva</option>
        <option value="Cruz Alta">Cruz Alta</option>
        <option value="Cruzaltense">Cruzaltense</option>
        <option value="Cruzeiro">Cruzeiro</option>
        <option value="Cruzeiro do Sul">Cruzeiro do Sul</option>
        <option value="Curral Alto">Curral Alto</option>
        <option value="Curumim">Curumim</option>
        <option value="Daltro Filho">Daltro Filho</option>
        <option value="Dário Lassance">Dário Lassance</option>
        <option value="David Canabarro">David Canabarro</option>
        <option value="Delfina">Delfina</option>
        <option value="Deodoro">Deodoro</option>
        <option value="Depósito">Depósito</option>
        <option value="Derrubadas">Derrubadas</option>
        <option value="Dezesseis de Novembro">Dezesseis de Novembro</option>
        <option value="Dilermando de Aguiar">Dilermando de Aguiar</option>
        <option value="Divino">Divino</option>
        <option value="Dois Irmãos das Missões">Dois Irmãos das Missões</option>
        <option value="Dois Irmãos">Dois Irmãos</option>
        <option value="Dois Lajeados">Dois Lajeados</option>
        <option value="Dom Feliciano">Dom Feliciano</option>
        <option value="Dom Pedrito">Dom Pedrito</option>
        <option value="Dom Pedro de Alcântara">Dom Pedro de Alcântara</option>
        <option value="Dona Francisca">Dona Francisca</option>
        <option value="Dona Otília">Dona Otília</option>
        <option value="Dourado">Dourado</option>
        <option value="Doutor Edgardo Pereira Velho">Doutor Edgardo Pereira Velho</option>
        <option value="Doutor Maurício Cardoso">Doutor Maurício Cardoso</option>
        <option value="Doutor Ricardo">Doutor Ricardo</option>
        <option value="Durasnal">Durasnal</option>
        <option value="Eldorado do Sul">Eldorado do Sul</option>
        <option value="Eletra">Eletra</option>
        <option value="Encantado">Encantado</option>
        <option value="Encruzilhada do Sul">Encruzilhada do Sul</option>
        <option value="Encruzilhada">Encruzilhada</option>
        <option value="Engenho Velho">Engenho Velho</option>
        <option value="Entre-Ijuís">Entre-Ijuís</option>
        <option value="Entrepelado">Entrepelado</option>
        <option value="Entre Rios do Sul">Entre Rios do Sul</option>
        <option value="Erebango">Erebango</option>
        <option value="Erechim">Erechim</option>
        <option value="Ernestina">Ernestina</option>
        <option value="Ernesto Alves">Ernesto Alves</option>
        <option value="Erval Grande">Erval Grande</option>
        <option value="Erval Seco">Erval Seco</option>
        <option value="Erveiras">Erveiras</option>
        <option value="Esmeralda">Esmeralda</option>
        <option value="Esperança do Sul">Esperança do Sul</option>
        <option value="Esperança">Esperança</option>
        <option value="Espigão Alto">Espigão Alto</option>
        <option value="Espigão">Espigão</option>
        <option value="Espinilho Grande">Espinilho Grande</option>
        <option value="Espírito Santo">Espírito Santo</option>
        <option value="Espumoso">Espumoso</option>
        <option value="Esquina Araújo">Esquina Araújo</option>
        <option value="Esquina Bom Sucesso">Esquina Bom Sucesso</option>
        <option value="Esquina Gaúcha">Esquina Gaúcha</option>
        <option value="Esquina Ipiranga">Esquina Ipiranga</option>
        <option value="Esquina Piratini">Esquina Piratini</option>
        <option value="Estação">Estação</option>
        <option value="Estância Grande">Estância Grande</option>
        <option value="Estância Velha">Estância Velha</option>
        <option value="Esteio">Esteio</option>
        <option value="Esteira">Esteira</option>
        <option value="Estreito">Estreito</option>
        <option value="Estrela">Estrela</option>
        <option value="Estrela Velha">Estrela Velha</option>
        <option value="Eugênio de Castro">Eugênio de Castro</option>
        <option value="Evangelista">Evangelista</option>
        <option value="Fagundes Varela">Fagundes Varela</option>
        <option value="Fão">Fão</option>
        <option value="Faria Lemos">Faria Lemos</option>
        <option value="Farinhas">Farinhas</option>
        <option value="Farrapos">Farrapos</option>
        <option value="Farroupilha">Farroupilha</option>
        <option value="Faxinal do Soturno">Faxinal do Soturno</option>
        <option value="Faxinal">Faxinal</option>
        <option value="Faxinalzinho">Faxinalzinho</option>
        <option value="Fazenda Fialho">Fazenda Fialho</option>
        <option value="Fazenda Souza">Fazenda Souza</option>
        <option value="Fazenda Vilanova">Fazenda Vilanova</option>
        <option value="Feliz">Feliz</option>
        <option value="Ferreira">Ferreira</option>
        <option value="Flores da Cunha">Flores da Cunha</option>
        <option value="Floresta">Floresta</option>
        <option value="Floriano Peixoto">Floriano Peixoto</option>
        <option value="Flórida">Flórida</option>
        <option value="Fontoura Xavier">Fontoura Xavier</option>
        <option value="Formigueiro">Formigueiro</option>
        <option value="Formosa">Formosa</option>
        <option value="Forninho">Forninho</option>
        <option value="Forquetinha">Forquetinha</option>
        <option value="Fortaleza dos Valos">Fortaleza dos Valos</option>
        <option value="Frederico Westphalen">Frederico Westphalen</option>
        <option value="Freire">Freire</option>
        <option value="Frei Sebastião">Frei Sebastião</option>
        <option value="Garibaldina">Garibaldina</option>
        <option value="Garibaldi">Garibaldi</option>
        <option value="Garruchos">Garruchos</option>
        <option value="Gaurama">Gaurama</option>
        <option value="General Câmara">General Câmara</option>
        <option value="Gentil">Gentil</option>
        <option value="Getúlio Vargas">Getúlio Vargas</option>
        <option value="Giruá">Giruá</option>
        <option value="Glória">Glória</option>
        <option value="Glorinha">Glorinha</option>
        <option value="Goio-En">Goio-En</option>
        <option value="Gramado dos Loureiros">Gramado dos Loureiros</option>
        <option value="Gramado">Gramado</option>
        <option value="Gramado São Pedro">Gramado São Pedro</option>
        <option value="Gramado Xavier">Gramado Xavier</option>
        <option value="Gravataí">Gravataí</option>
        <option value="Guabiju">Guabiju</option>
        <option value="Guaíba">Guaíba</option>
        <option value="Guajuviras">Guajuviras</option>
        <option value="Guaporé">Guaporé</option>
        <option value="Guarani das Missões">Guarani das Missões</option>
        <option value="Guassu Boi">Guassu Boi</option>
        <option value="Guassupi">Guassupi</option>
        <option value="Harmonia">Harmonia</option>
        <option value="Herval">Herval</option>
        <option value="Herveiras">Herveiras</option>
        <option value="Hidráulica">Hidráulica</option>
        <option value="Horizontina">Horizontina</option>
        <option value="Hulha Negra">Hulha Negra</option>
        <option value="Humaitá">Humaitá</option>
        <option value="Ibarama">Ibarama</option>
        <option value="Ibaré">Ibaré</option>
        <option value="Ibiaçá">Ibiaçá</option>
        <option value="Ibiraiaras">Ibiraiaras</option>
        <option value="Ibirapuitã">Ibirapuitã</option>
        <option value="Ibirubá">Ibirubá</option>
        <option value="Igrejinha">Igrejinha</option>
        <option value="Ijucapirama">Ijucapirama</option>
        <option value="Ijuí">Ijuí</option>
        <option value="Ilha dos Marinheiros">Ilha dos Marinheiros</option>
        <option value="Ilópolis">Ilópolis</option>
        <option value="Imbé">Imbé</option>
        <option value="Imigrante">Imigrante</option>
        <option value="Independência">Independência</option>
        <option value="Inhacorá">Inhacorá</option>
        <option value="Inhanduí">Inhanduí</option>
        <option value="Ipê">Ipê</option>
        <option value="Ipiranga do Sul">Ipiranga do Sul</option>
        <option value="Ipiranga">Ipiranga</option>
        <option value="Ipuaçu">Ipuaçu</option>
        <option value="Iraí">Iraí</option>
        <option value="Iruí">Iruí</option>
        <option value="Itaara">Itaara</option>
        <option value="Itacolomi">Itacolomi</option>
        <option value="Itacurubi">Itacurubi</option>
        <option value="Itaimbezinho">Itaimbezinho</option>
        <option value="Itaí">Itaí</option>
        <option value="Itão">Itão</option>
        <option value="Itapororó">Itapororó</option>
        <option value="Itapuã">Itapuã</option>
        <option value="Itapucá">Itapucá</option>
        <option value="Itaqui">Itaqui</option>
        <option value="Itatiba do Sul">Itatiba do Sul</option>
        <option value="Itati">Itati</option>
        <option value="Itaúba">Itaúba</option>
        <option value="Ituim">Ituim</option>
        <option value="Ivaí">Ivaí</option>
        <option value="Ivorá">Ivorá</option>
        <option value="Ivoti">Ivoti</option>
        <option value="Jaboticaba">Jaboticaba</option>
        <option value="Jacuizinho">Jacuizinho</option>
        <option value="Jacutinga">Jacutinga</option>
        <option value="Jaguarão">Jaguarão</option>
        <option value="Jaguarete">Jaguarete</option>
        <option value="Jaguari">Jaguari</option>
        <option value="Jansen">Jansen</option>
        <option value="Jaquirana">Jaquirana</option>
        <option value="Jari">Jari</option>
        <option value="Jazidas ou Capela São Vicente">Jazidas ou Capela São Vicente</option>
        <option value="João Arregui">João Arregui</option>
        <option value="João Rodrigues">João Rodrigues</option>
        <option value="Joça Tavares">Joça Tavares</option>
        <option value="Jóia">Jóia</option>
        <option value="José Otávio">José Otávio</option>
        <option value="Juá">Juá</option>
        <option value="Júlio de Castilhos">Júlio de Castilhos</option>
        <option value="Lagoa Bonita do Sul">Lagoa Bonita do Sul</option>
        <option value="Lagoa dos Patos">Lagoa dos Patos</option>
        <option value="Lagoa dos Três Cantos">Lagoa dos Três Cantos</option>
        <option value="Lagoão">Lagoão</option>
        <option value="Lagoa Vermelha">Lagoa Vermelha</option>
        <option value="Lajeado Bonito">Lajeado Bonito</option>
        <option value="Lajeado Cerne">Lajeado Cerne</option>
        <option value="Lajeado do Bugre">Lajeado do Bugre</option>
        <option value="Lajeado Grande">Lajeado Grande</option>
        <option value="Lajeado">Lajeado</option>
        <option value="Lara">Lara</option>
        <option value="Laranjeira">Laranjeira</option>
        <option value="Lava-Pés">Lava-Pés</option>
        <option value="Lavras do Sul">Lavras do Sul</option>
        <option value="Leonel Rocha">Leonel Rocha</option>
        <option value="Liberato Salzano">Liberato Salzano</option>
        <option value="Lindolfo Collor">Lindolfo Collor</option>
        <option value="Linha Comprida">Linha Comprida</option>
        <option value="Linha Nova">Linha Nova</option>
        <option value="Linha Vitória">Linha Vitória</option>
        <option value="Loreto">Loreto</option>
        <option value="Maçambará">Maçambará</option>
        <option value="Machadinho">Machadinho</option>
        <option value="Magistério">Magistério</option>
        <option value="Mampituba">Mampituba</option>
        <option value="Manchinha">Manchinha</option>
        <option value="Mangueiras">Mangueiras</option>
        <option value="Manoel Viana">Manoel Viana</option>
        <option value="Maquiné">Maquiné</option>
        <option value="Maratá">Maratá</option>
        <option value="Marau">Marau</option>
        <option value="Marcelino Ramos">Marcelino Ramos</option>
        <option value="Marcorama">Marcorama</option>
        <option value="Mariana Pimentel">Mariana Pimentel</option>
        <option value="Mariano Moro">Mariano Moro</option>
        <option value="Mariante">Mariante</option>
        <option value="Mariápolis">Mariápolis</option>
        <option value="Marques de Souza">Marques de Souza</option>
        <option value="Matarazzo">Matarazzo</option>
        <option value="Mata">Mata</option>
        <option value="Mato Castelhano">Mato Castelhano</option>
        <option value="Mato Grande">Mato Grande</option>
        <option value="Mato Leitão">Mato Leitão</option>
        <option value="Mato Perso">Mato Perso</option>
        <option value="Mato Queimado">Mato Queimado</option>
        <option value="Mauá">Mauá</option>
        <option value="Maximiliano de Almeida">Maximiliano de Almeida</option>
        <option value="Medianeira">Medianeira</option>
        <option value="Minas do Leão">Minas do Leão</option>
        <option value="Miraguaia">Miraguaia</option>
        <option value="Miraguaí">Miraguaí</option>
        <option value="Mirim">Mirim</option>
        <option value="Montauri">Montauri</option>
        <option value="Monte Alegre">Monte Alegre</option>
        <option value="Monte Alegre dos Campos">Monte Alegre dos Campos</option>
        <option value="Monte Alverne">Monte Alverne</option>
        <option value="Monte Belo do Sul">Monte Belo do Sul</option>
        <option value="Monte Bonito">Monte Bonito</option>
        <option value="Montenegro">Montenegro</option>
        <option value="Mormaço">Mormaço</option>
        <option value="Morrinhos do Sul">Morrinhos do Sul</option>
        <option value="Morrinhos">Morrinhos</option>
        <option value="Morro Alto">Morro Alto</option>
        <option value="Morro Azul">Morro Azul</option>
        <option value="Morro Redondo">Morro Redondo</option>
        <option value="Morro Reuter">Morro Reuter</option>
        <option value="Morungava">Morungava</option>
        <option value="Mostardas">Mostardas</option>
        <option value="Muçum">Muçum</option>
        <option value="Muitos Capões">Muitos Capões</option>
        <option value="Muliterno">Muliterno</option>
        <option value="Não-Me-Toque">Não-Me-Toque</option>
        <option value="Nazaré">Nazaré</option>
        <option value="Nicolau Vergueiro">Nicolau Vergueiro</option>
        <option value="Nonoai">Nonoai</option>
        <option value="Nossa Senhora Aparecida">Nossa Senhora Aparecida</option>
        <option value="Nossa Senhora da Conceição">Nossa Senhora da Conceição</option>
        <option value="Nova Alvorada">Nova Alvorada</option>
        <option value="Nova Araçá">Nova Araçá</option>
        <option value="Nova Bassano">Nova Bassano</option>
        <option value="Nova Boa Vista">Nova Boa Vista</option>
        <option value="Nova Bréscia">Nova Bréscia</option>
        <option value="Nova Candelária">Nova Candelária</option>
        <option value="Nova Esperança do Sul">Nova Esperança do Sul</option>
        <option value="Nova Hartz">Nova Hartz</option>
        <option value="Nova Milano">Nova Milano</option>
        <option value="Nova Pádua">Nova Pádua</option>
        <option value="Nova Palma">Nova Palma</option>
        <option value="Nova Petrópolis">Nova Petrópolis</option>
        <option value="Nova Prata">Nova Prata</option>
        <option value="Nova Ramada">Nova Ramada</option>
        <option value="Nova Roma do Sul">Nova Roma do Sul</option>
        <option value="Nova Santa Rita">Nova Santa Rita</option>
        <option value="Nova Sardenha">Nova Sardenha</option>
        <option value="Novo Barreiro">Novo Barreiro</option>
        <option value="Novo Cabrais">Novo Cabrais</option>
        <option value="Novo Hamburgo">Novo Hamburgo</option>
        <option value="Novo Horizonte">Novo Horizonte</option>
        <option value="Novo Machado">Novo Machado</option>
        <option value="Novo Planalto">Novo Planalto</option>
        <option value="Novo Tiradentes">Novo Tiradentes</option>
        <option value="Novo Xingu">Novo Xingu</option>
        <option value="Oralina">Oralina</option>
        <option value="Osório">Osório</option>
        <option value="Osvaldo Cruz">Osvaldo Cruz</option>
        <option value="Osvaldo Kroeff">Osvaldo Kroeff</option>
        <option value="Otávio Rocha">Otávio Rocha</option>
        <option value="Pacheca">Pacheca</option>
        <option value="Padilha">Padilha</option>
        <option value="Padre Gonzales">Padre Gonzales</option>
        <option value="Paim Filho">Paim Filho</option>
        <option value="Palmares do Sul">Palmares do Sul</option>
        <option value="Palmas">Palmas</option>
        <option value="Palmeira das Missões">Palmeira das Missões</option>
        <option value="Palmitinho">Palmitinho</option>
        <option value="Pampeiro">Pampeiro</option>
        <option value="Panambi">Panambi</option>
        <option value="Pântano Grande">Pântano Grande</option>
        <option value="Paraí">Paraí</option>
        <option value="Paraíso do Sul">Paraíso do Sul</option>
        <option value="Pareci Novo">Pareci Novo</option>
        <option value="Parobé">Parobé</option>
        <option value="Passa Sete">Passa Sete</option>
        <option value="Passinhos">Passinhos</option>
        <option value="Passo Burmann">Passo Burmann</option>
        <option value="Passo da Areia">Passo da Areia</option>
        <option value="Passo da Caveira">Passo da Caveira</option>
        <option value="Passo das Pedras">Passo das Pedras</option>
        <option value="Passo do Adão">Passo do Adão</option>
        <option value="Passo do Goulart">Passo do Goulart</option>
        <option value="Passo do Sabão">Passo do Sabão</option>
        <option value="Passo do Sobrado">Passo do Sobrado</option>
        <option value="Passo do Verde">Passo do Verde</option>
        <option value="Passo Fundo">Passo Fundo</option>
        <option value="Passo Novo">Passo Novo</option>
        <option value="Passo Raso">Passo Raso</option>
        <option value="Paulo Bento">Paulo Bento</option>
        <option value="Pavão">Pavão</option>
        <option value="Paverama">Paverama</option>
        <option value="Pedras Altas">Pedras Altas</option>
        <option value="Pedreiras">Pedreiras</option>
        <option value="Pedro Garcia">Pedro Garcia</option>
        <option value="Pedro Osório">Pedro Osório</option>
        <option value="Pedro Paiva">Pedro Paiva</option>
        <option value="Pejuçara">Pejuçara</option>
        <option value="Pelotas">Pelotas</option>
        <option value="Picada Café">Picada Café</option>
        <option value="Pinhal Alto">Pinhal Alto</option>
        <option value="Pinhal da Serra">Pinhal da Serra</option>
        <option value="Pinhal Grande">Pinhal Grande</option>
        <option value="Pinhal">Pinhal</option>
        <option value="Pinhalzinho">Pinhalzinho</option>
        <option value="Pinheirinho do Vale">Pinheirinho do Vale</option>
        <option value="Pinheiro Machado">Pinheiro Machado</option>
        <option value="Pinheiro Marcado">Pinheiro Marcado</option>
        <option value="Pinto Bandeira">Pinto Bandeira</option>
        <option value="Piraí">Piraí</option>
        <option value="Pirapó">Pirapó</option>
        <option value="Piratini">Piratini</option>
        <option value="Pitanga">Pitanga</option>
        <option value="Planalto">Planalto</option>
        <option value="Plano Alto">Plano Alto</option>
        <option value="Poço das Antas">Poço das Antas</option>
        <option value="Polígono do Erval">Polígono do Erval</option>
        <option value="Pólo Petroquímico de Triunfo">Pólo Petroquímico de Triunfo</option>
        <option value="Pontão">Pontão</option>
        <option value="Ponte Preta">Ponte Preta</option>
        <option value="Portão">Portão</option>
        <option value="Porto Batista">Porto Batista</option>
        <option value="Porto Lucena">Porto Lucena</option>
        <option value="Porto Mauá">Porto Mauá</option>
        <option value="Porto Vera Cruz">Porto Vera Cruz</option>
        <option value="Porto Xavier">Porto Xavier</option>
        <option value="Pouso Novo">Pouso Novo</option>
        <option value="Povoado Tozzo">Povoado Tozzo</option>
        <option value="Povo Novo">Povo Novo</option>
        <option value="Prado Novo">Prado Novo</option>
        <option value="Pranchada">Pranchada</option>
        <option value="Pratos">Pratos</option>
        <option value="Presidente Lucena">Presidente Lucena</option>
        <option value="Progresso">Progresso</option>
        <option value="Protásio Alves">Protásio Alves</option>
        <option value="Pulador">Pulador</option>
        <option value="Putinga">Putinga</option>
        <option value="Quaraim">Quaraim</option>
        <option value="Quaraí">Quaraí</option>
        <option value="Quatro Irmãos">Quatro Irmãos</option>
        <option value="Quevedos">Quevedos</option>
        <option value="Quilombo">Quilombo</option>
        <option value="Quintão">Quintão</option>
        <option value="Quinta">Quinta</option>
        <option value="Quinze de Novembro">Quinze de Novembro</option>
        <option value="Quitéria">Quitéria</option>
        <option value="Rancho Velho">Rancho Velho</option>
        <option value="Redentora">Redentora</option>
        <option value="Refugiado">Refugiado</option>
        <option value="Relvado">Relvado</option>
        <option value="Restinga Seca">Restinga Seca</option>
        <option value="Rincão del Rei">Rincão del Rei</option>
        <option value="Rincão de São Miguel">Rincão de São Miguel</option>
        <option value="Rincão de São Pedro">Rincão de São Pedro</option>
        <option value="Rincão Doce">Rincão Doce</option>
        <option value="Rincão do Cristóvão Pereira">Rincão do Cristóvão Pereira</option>
        <option value="Rincão do Meio">Rincão do Meio</option>
        <option value="Rincão do Segredo">Rincão do Segredo</option>
        <option value="Rincão dos Kroeff">Rincão dos Kroeff</option>
        <option value="Rincão dos Mendes">Rincão dos Mendes</option>
        <option value="Rincão Vermelho">Rincão Vermelho</option>
        <option value="Rio Azul">Rio Azul</option>
        <option value="Rio Branco">Rio Branco</option>
        <option value="Rio da Ilha">Rio da Ilha</option>
        <option value="Rio dos Índios">Rio dos Índios</option>
        <option value="Rio Grande">Rio Grande</option>
        <option value="Rio Pardinho">Rio Pardinho</option>
        <option value="Rio Pardo">Rio Pardo</option>
        <option value="Rio Telha">Rio Telha</option>
        <option value="Rio Tigre">Rio Tigre</option>
        <option value="Rio Toldo">Rio Toldo</option>
        <option value="Riozinho">Riozinho</option>
        <option value="Roca Sales">Roca Sales</option>
        <option value="Rodeio Bonito">Rodeio Bonito</option>
        <option value="Rolador">Rolador</option>
        <option value="Rolante">Rolante</option>
        <option value="Rolantinho da Figueira">Rolantinho da Figueira</option>
        <option value="Ronda Alta">Ronda Alta</option>
        <option value="Rondinha">Rondinha</option>
        <option value="Roque Gonzales">Roque Gonzales</option>
        <option value="Rosário do Sul">Rosário do Sul</option>
        <option value="Rosário">Rosário</option>
        <option value="Sagrada Família">Sagrada Família</option>
        <option value="Saicã">Saicã</option>
        <option value="Saldanha Marinho">Saldanha Marinho</option>
        <option value="Saltinho">Saltinho</option>
        <option value="Salto do Jacuí">Salto do Jacuí</option>
        <option value="Salto">Salto</option>
        <option value="Salvador das Missões">Salvador das Missões</option>
        <option value="Salvador do Sul">Salvador do Sul</option>
        <option value="Sananduva">Sananduva</option>
        <option value="Santa Bárbara do Sul">Santa Bárbara do Sul</option>
        <option value="Santa Bárbara">Santa Bárbara</option>
        <option value="Santa Catarina">Santa Catarina</option>
        <option value="Santa Cecília do Sul">Santa Cecília do Sul</option>
        <option value="Santa Clara do Ingaí">Santa Clara do Ingaí</option>
        <option value="Santa Clara do Sul">Santa Clara do Sul</option>
        <option value="Santa Cristina">Santa Cristina</option>
        <option value="Santa Cruz">Santa Cruz</option>
        <option value="Santa Cruz da Concórdia">Santa Cruz da Concórdia</option>
        <option value="Santa Cruz do Sul">Santa Cruz do Sul</option>
        <option value="Santa Flora">Santa Flora</option>
        <option value="Santa Inês">Santa Inês</option>
        <option value="Santa Izabel do Sul">Santa Izabel do Sul</option>
        <option value="Santa Lúcia do Piaí">Santa Lúcia do Piaí</option>
        <option value="Santa Lúcia">Santa Lúcia</option>
        <option value="Santa Luíza">Santa Luíza</option>
        <option value="Santa Luzia">Santa Luzia</option>
        <option value="Santa Margarida do Sul">Santa Margarida do Sul</option>
        <option value="Santa Maria do Herval">Santa Maria do Herval</option>
        <option value="Santa Maria">Santa Maria</option>
        <option value="Santana da Boa Vista">Santana da Boa Vista</option>
        <option value="Santana do Livramento">Santana do Livramento</option>
        <option value="Santana">Santana</option>
        <option value="Santa Rita do Sul">Santa Rita do Sul</option>
        <option value="Santa Rosa">Santa Rosa</option>
        <option value="Santa Silvana">Santa Silvana</option>
        <option value="Santa Teresinha">Santa Teresinha</option>
        <option value="Santa Tereza">Santa Tereza</option>
        <option value="Sant'auta">Sant'auta</option>
        <option value="Santa Vitória do Palmar">Santa Vitória do Palmar</option>
        <option value="Santiago">Santiago</option>
        <option value="Santo Amaro do Sul">Santo Amaro do Sul</option>
        <option value="Santo Ângelo">Santo Ângelo</option>
        <option value="Santo Antônio da Patrulha">Santo Antônio da Patrulha</option>
        <option value="Santo Antônio das Missões">Santo Antônio das Missões</option>
        <option value="Santo Antônio de Castro">Santo Antônio de Castro</option>
        <option value="Santo Antônio do Bom Retiro">Santo Antônio do Bom Retiro</option>
        <option value="Santo Antônio do Palma">Santo Antônio do Palma</option>
        <option value="Santo Antônio do Planalto">Santo Antônio do Planalto</option>
        <option value="Santo Antônio">Santo Antônio</option>
        <option value="Santo Augusto">Santo Augusto</option>
        <option value="Santo Cristo">Santo Cristo</option>
        <option value="Santo Expedito do Sul">Santo Expedito do Sul</option>
        <option value="Santo Inácio">Santo Inácio</option>
        <option value="São Bento">São Bento</option>
        <option value="São Bom Jesus">São Bom Jesus</option>
        <option value="São Borja">São Borja</option>
        <option value="São Carlos">São Carlos</option>
        <option value="São Domingos do Sul">São Domingos do Sul</option>
        <option value="São Francisco de Assis">São Francisco de Assis</option>
        <option value="São Francisco de Paula">São Francisco de Paula</option>
        <option value="São Francisco">São Francisco</option>
        <option value="São Gabriel">São Gabriel</option>
        <option value="São Jerônimo">São Jerônimo</option>
        <option value="São João Batista">São João Batista</option>
        <option value="São João Bosco">São João Bosco</option>
        <option value="São João da Urtiga">São João da Urtiga</option>
        <option value="São João do Polesine">São João do Polesine</option>
        <option value="São João">São João</option>
        <option value="São Jorge">São Jorge</option>
        <option value="São José da Glória">São José da Glória</option>
        <option value="São José das Missões">São José das Missões</option>
        <option value="São José de Castro">São José de Castro</option>
        <option value="São José do Centro">São José do Centro</option>
        <option value="São José do Herval">São José do Herval</option>
        <option value="São José do Hortêncio">São José do Hortêncio</option>
        <option value="São José do Inhacorá">São José do Inhacorá</option>
        <option value="São José do Norte">São José do Norte</option>
        <option value="São José do Ouro">São José do Ouro</option>
        <option value="São José dos Ausentes">São José dos Ausentes</option>
        <option value="São José do Sul">São José do Sul</option>
        <option value="São José">São José</option>
        <option value="São Leopoldo">São Leopoldo</option>
        <option value="São Lourenço das Missões">São Lourenço das Missões</option>
        <option value="São Lourenço do Sul">São Lourenço do Sul</option>
        <option value="São Luís Rei">São Luís Rei</option>
        <option value="São Luiz Gonzaga">São Luiz Gonzaga</option>
        <option value="São Luiz">São Luiz</option>
        <option value="São Manuel">São Manuel</option>
        <option value="São Marcos">São Marcos</option>
        <option value="São Martinho da Serra">São Martinho da Serra</option>
        <option value="São Martinho">São Martinho</option>
        <option value="São Miguel das Missões">São Miguel das Missões</option>
        <option value="São Miguel">São Miguel</option>
        <option value="São Nicolau">São Nicolau</option>
        <option value="São Paulo">São Paulo</option>
        <option value="São Paulo das Missões">São Paulo das Missões</option>
        <option value="São Pedro da Serra">São Pedro da Serra</option>
        <option value="São Pedro das Missões">São Pedro das Missões</option>
        <option value="São Pedro do Butiá">São Pedro do Butiá</option>
        <option value="São Pedro do Iraxim">São Pedro do Iraxim</option>
        <option value="São Pedro do Sul">São Pedro do Sul</option>
        <option value="São Pedro">São Pedro</option>
        <option value="São Roque">São Roque</option>
        <option value="São Sebastião do Caí">São Sebastião do Caí</option>
        <option value="São Sebastião">São Sebastião</option>
        <option value="São Sepé">São Sepé</option>
        <option value="São Simão">São Simão</option>
        <option value="São Valentim do Sul">São Valentim do Sul</option>
        <option value="São Valentim">São Valentim</option>
        <option value="São Valério do Sul">São Valério do Sul</option>
        <option value="São Vendelino">São Vendelino</option>
        <option value="São Vicente do Sul">São Vicente do Sul</option>
        <option value="Sapiranga">Sapiranga</option>
        <option value="Sapucaia do Sul">Sapucaia do Sul</option>
        <option value="Sarandi">Sarandi</option>
        <option value="Seberi">Seberi</option>
        <option value="Sede Aurora">Sede Aurora</option>
        <option value="Sede Nova">Sede Nova</option>
        <option value="Segredo">Segredo</option>
        <option value="Seival">Seival</option>
        <option value="Selbach">Selbach</option>
        <option value="Senador Salgado Filho">Senador Salgado Filho</option>
        <option value="Sentinela do Sul">Sentinela do Sul</option>
        <option value="Serafim Schmidt">Serafim Schmidt</option>
        <option value="Serafina Corrêa">Serafina Corrêa</option>
        <option value="Sério">Sério</option>
        <option value="Serra dos Gregórios">Serra dos Gregórios</option>
        <option value="Serrinha">Serrinha</option>
        <option value="Serrinha Velha">Serrinha Velha</option>
        <option value="Sertão">Sertão</option>
        <option value="Sertão Santana">Sertão Santana</option>
        <option value="Sertãozinho">Sertãozinho</option>
        <option value="Sete de Setembro">Sete de Setembro</option>
        <option value="Sete Lagoas">Sete Lagoas</option>
        <option value="Severiano de Almeida">Severiano de Almeida</option>
        <option value="Silva Jardim">Silva Jardim</option>
        <option value="Silveira">Silveira</option>
        <option value="Silveira Martins">Silveira Martins</option>
        <option value="Sinimbu">Sinimbu</option>
        <option value="Sírio">Sírio</option>
        <option value="Sítio Gabriel">Sítio Gabriel</option>
        <option value="Sobradinho">Sobradinho</option>
        <option value="Soledade">Soledade</option>
        <option value="Souza Ramos">Souza Ramos</option>
        <option value="Suspiro">Suspiro</option>
        <option value="Tabaí">Tabaí</option>
        <option value="Tabajara">Tabajara</option>
        <option value="Taim">Taim</option>
        <option value="Tainhas">Tainhas</option>
        <option value="Tamanduá">Tamanduá</option>
        <option value="Tanque">Tanque</option>
        <option value="Tapejara">Tapejara</option>
        <option value="Tapera">Tapera</option>
        <option value="Tapes">Tapes</option>
        <option value="Taquaral">Taquaral</option>
        <option value="Taquara">Taquara</option>
        <option value="Taquarichim">Taquarichim</option>
        <option value="Taquari">Taquari</option>
        <option value="Taquaruçu do Sul">Taquaruçu do Sul</option>
        <option value="Tavares">Tavares</option>
        <option value="Tenente Portela">Tenente Portela</option>
        <option value="Terra de Areia">Terra de Areia</option>
        <option value="Tesouras">Tesouras</option>
        <option value="Teutônia">Teutônia</option>
        <option value="Tiaraju">Tiaraju</option>
        <option value="Timbaúva">Timbaúva</option>
        <option value="Tio Hugo">Tio Hugo</option>
        <option value="Tiradentes do Sul">Tiradentes do Sul</option>
        <option value="Toropi">Toropi</option>
        <option value="Toroquá">Toroquá</option>
        <option value="Torquato Severo">Torquato Severo</option>
        <option value="Torres">Torres</option>
        <option value="Torrinhas">Torrinhas</option>
        <option value="Touro Passo">Touro Passo</option>
        <option value="Tramandaí">Tramandaí</option>
        <option value="Travesseiro">Travesseiro</option>
        <option value="Trentin">Trentin</option>
        <option value="Três Arroios">Três Arroios</option>
        <option value="Três Barras">Três Barras</option>
        <option value="Três Cachoeiras">Três Cachoeiras</option>
        <option value="Três Coroas">Três Coroas</option>
        <option value="Três de Maio">Três de Maio</option>
        <option value="Três Forquilhas">Três Forquilhas</option>
        <option value="Três Palmeiras">Três Palmeiras</option>
        <option value="Três Passos">Três Passos</option>
        <option value="Três Vendas">Três Vendas</option>
        <option value="Trindade do Sul">Trindade do Sul</option>
        <option value="Triunfo">Triunfo</option>
        <option value="Tronqueiras">Tronqueiras</option>
        <option value="Tucunduva">Tucunduva</option>
        <option value="Tuiuti">Tuiuti</option>
        <option value="Tunas">Tunas</option>
        <option value="Túnel Verde">Túnel Verde</option>
        <option value="Tupanci do Sul">Tupanci do Sul</option>
        <option value="Tupanciretã">Tupanciretã</option>
        <option value="Tupancy ou Vila Block">Tupancy ou Vila Block</option>
        <option value="Tupandi">Tupandi</option>
        <option value="Tupantuba">Tupantuba</option>
        <option value="Tuparendi">Tuparendi</option>
        <option value="Tupinambá">Tupinambá</option>
        <option value="Tupi Silveira">Tupi Silveira</option>
        <option value="Turuçu">Turuçu</option>
        <option value="Turvinho">Turvinho</option>
        <option value="Ubiretama">Ubiretama</option>
        <option value="Umbu">Umbu</option>
        <option value="União da Serra">União da Serra</option>
        <option value="Unistalda">Unistalda</option>
        <option value="Uruguaiana">Uruguaiana</option>
        <option value="Vacacai">Vacacai</option>
        <option value="Vacaria">Vacaria</option>
        <option value="Valdástico">Valdástico</option>
        <option value="Vale do Rio Cai">Vale do Rio Cai</option>
        <option value="Vale do Sol">Vale do Sol</option>
        <option value="Vale dos Vinhedos">Vale dos Vinhedos</option>
        <option value="Vale Real">Vale Real</option>
        <option value="Vale Veneto">Vale Veneto</option>
        <option value="Vale Verde">Vale Verde</option>
        <option value="Vanini">Vanini</option>
        <option value="Vasco Alves">Vasco Alves</option>
        <option value="Venâncio Aires">Venâncio Aires</option>
        <option value="Vera Cruz">Vera Cruz</option>
        <option value="Veranópolis">Veranópolis</option>
        <option value="Vertentes">Vertentes</option>
        <option value="Vespasiano Correa">Vespasiano Correa</option>
        <option value="Viadutos">Viadutos</option>
        <option value="Viamão">Viamão</option>
        <option value="Vicente Dutra">Vicente Dutra</option>
        <option value="Victor Graeff">Victor Graeff</option>
        <option value="Vila Bender">Vila Bender</option>
        <option value="Vila Boqueirão">Vila Boqueirão</option>
        <option value="Vila Cruz">Vila Cruz</option>
        <option value="Vila Fernando Ferrari">Vila Fernando Ferrari</option>
        <option value="Vila Flores">Vila Flores</option>
        <option value="Vila Langaro">Vila Langaro</option>
        <option value="Vila Laranjeira">Vila Laranjeira</option>
        <option value="Vila Maria">Vila Maria</option>
        <option value="Vila Nova do Sul">Vila Nova do Sul</option>
        <option value="Vila Oliva">Vila Oliva</option>
        <option value="Vila Rica">Vila Rica</option>
        <option value="Vila Seca">Vila Seca</option>
        <option value="Vila Turvo">Vila Turvo</option>
        <option value="Vista Alegre do Prata">Vista Alegre do Prata</option>
        <option value="Vista Alegre">Vista Alegre</option>
        <option value="Vista Gaúcha">Vista Gaúcha</option>
        <option value="Vitória das Missões">Vitória das Missões</option>
        <option value="Vitória">Vitória</option>
        <option value="Volta Alegre">Volta Alegre</option>
        <option value="Volta Fechada">Volta Fechada</option>
        <option value="Volta Grande">Volta Grande</option>
        <option value="Westfália">Westfália</option>
        <option value="Xadrez">Xadrez</option>
        <option value="Xangri-lá">Xangri-lá</option>
      </select>
      
      <?php 
	  if(isset($_SESSION['FiltroCandidato'])){ ?>
		<script>	
		get_candidato.txtCidade.value = "<?php echo $_SESSION['FiltroCandidato']['txtCidade']  ?>";
		</script>
	  <?php 
	  }
	  ?>
      
    </div>
  </div>
  <div class="row">
    <div class="12u" id='btn_load_2'>
    Processando...
    </div>
      
    <div class="12u" id="btn_2"> 
    	<a class="button submit" onclick='post("Controller/Candidato.controller.php?op=2","btn_2","btn_load_2","gestor_candidato","get_candidato")'>Encontrar novos candidatos</a>         
    </div>
    
  </div>
</form>
<?php
}else{
  $list_candidato = unserialize($_SESSION['form_list_candidato']);

  include 'Include/getCandidato.view.php';
}
?>
</div>
</section>

<!-- Contact -->
<section id="vaga_pendente" class="three">
  <div class="container" style="width:95%;">
    <header>
      <h2>Vagas pendentes de aprovação</h2>
    </header>
    <?php	  
	  	$vagaDAO = new VagaDAO();
		
		$list_vaga = $vagaDAO->getPendente();
		
		echo "<table id='dataTables-example4'>";
						
		echo "	<thead>";
						
					if(count($list_vaga) > 0){
						echo "<tr>
								<th>Vaga</th>
								<th>Escola</th>
								<th>Bolsa-auxílio</th>
								<th>Data</th>
								<th>Atendimento</th>
								<th>&nbsp;</th>
								<th>&nbsp;</th>
							  </tr>";						
					}else{
						echo "<tr>
								<th>Aviso</th>
							  </tr>";
					}						
													
		echo "	</thead>
				<tbody>";
					if(count($list_vaga) > 0){
						foreach($list_vaga as $list){
							
							echo "<tr>
									<th>".$list->nome."</th>
									<th>".$list->escola."</th>
									<th> R$ ".number_format($list->salario, 2, ',', '.')."</th>
									<th>".date('d/m/Y',strtotime($list->data))."</th>
									<th>".$list->atendimento."</th>";
									?>
                                    <th> <a href="Controller/Vaga.controller.php?op=<?php echo sha1(2) ?>&id=<?php echo base64_encode($list->id) ?>">&nbsp;&nbsp;<u>Explorar</u>&nbsp;&nbsp;</a> </th>
                                    <th> 
                                    	<a onclick='getId("Controller/Vaga.controller.php?op=15&id=<?php echo base64_encode($list->id) ?>","vaga_pendente")'> &nbsp;&nbsp;<u>Cancelar</u>&nbsp;&nbsp;</a> 
                                    </th>
                                    <?php
							echo "</tr>";		
						}	
											
					}else{
						echo "<tr>
								<th>Não encontramos vagas pendentes.</th>
							 </tr>";
							
					}
		echo "	</tbody>
			 </table>";					 
		?>
  </div>
</section>
</div>
<!-- Footer -->
<div id="footer"> 
  <!-- Copyright -->
  <div class="copyright">
    <ul class="menu">
      <li>AST Facilities.</li>
    </ul>
  </div>
</div>

<!-- Page-Level Plugin Scripts - Tables --> 
<script src="js/dataTables/jquery.dataTables.js"></script> 
<script src="js/dataTables/dataTables.bootstrap.js"></script>
</body>
</html>