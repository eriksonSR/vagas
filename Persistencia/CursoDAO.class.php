<?php
	class CursoDAO{	
		// Declaração de atributos.
		private $conexao;
		
		// Declaração de metodos.
		public function __construct(){
			$this->conexao = Conexao::getInstancia();
		}
		
		public function cadastro($curso){
			$start = $this->conexao->prepare("
			insert into curso() values(null,?,?,?,?,?,?,?)
			");
			$start->bindValue(1,$curso->id_user);		
			$start->bindValue(2,$curso->nivel);		
			$start->bindValue(3,$curso->curso);		
			$start->bindValue(4,$curso->escola);			
			$start->bindValue(5,$curso->descricao);					
			$start->bindValue(6,$curso->data_inicio);														
			$start->bindValue(7,$curso->data_fim);		
			$start->execute();						
		}
		
		public function alt($curso){
			$start = $this->conexao->prepare("
			update curso set nivel = ?, curso = ?, escola = ?, descricao = ?, data_inicio = ?, data_fim = ? where id = ?
			");
			$start->bindValue(1,$curso->nivel);		
			$start->bindValue(2,$curso->curso);		
			$start->bindValue(3,$curso->escola);			
			$start->bindValue(4,$curso->descricao);					
			$start->bindValue(5,$curso->data_inicio);														
			$start->bindValue(6,$curso->data_fim);		
			$start->bindValue(7,$curso->id);		
			$start->execute();						
		}
		
		public function excluir($id){
			$start = $this->conexao->prepare("
			delete from curso where id = ?
			");
			$start->bindValue(1,$id);		
			$start->execute();						
		}	
		
		public function getAll($user){
			$start = $this->conexao->prepare("
			select * from curso where id_user = ? order by id desc
			");
			$start->bindValue(1,$user->id);
			$start->execute();
			return $start->fetchALL(PDO::FETCH_CLASS,'Curso');
		}
		
		public function getId($id){
			$start = $this->conexao->prepare("
			select * from curso where id = ?
			");
			$start->bindValue(1,$id);
			$start->execute();
			return $start->fetchALL(PDO::FETCH_CLASS,'Curso');
		}
		
		
		
	}
	
?>