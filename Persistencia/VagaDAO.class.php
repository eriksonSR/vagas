<?php
	class VagaDAO{	
		// Declaração de atributos.
		private $conexao;
		
		// Declaração de metodos.
		public function __construct(){
			$this->conexao = Conexao::getInstancia();
		}
		
		public function cadastro($vaga){
			$start = $this->conexao->prepare("
			insert into vaga() values(null,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,null,null,null,?)
			");
			$start->bindValue(1,$vaga->id_user);
			$start->bindValue(2,$vaga->id_recrutador);
			$start->bindValue(3,$vaga->codigo);				
			$start->bindValue(4,$vaga->nome);		
			$start->bindValue(5,$vaga->area_experiencia);		
			$start->bindValue(6,$vaga->area);		
			$start->bindValue(7,$vaga->atividade);		
			$start->bindValue(8,$vaga->beneficio);		
			$start->bindValue(9,$vaga->escolaridade);		
			$start->bindValue(10,$vaga->idioma);		
			$start->bindValue(11,$vaga->horario);		
			$start->bindValue(12,$vaga->salario);	
			$start->bindValue(13,$vaga->cidade);
			$start->bindValue(14,$vaga->informacao_adicional);									
			$start->bindValue(15,$vaga->nivel);
			$start->bindValue(16,$vaga->data);
			$start->bindValue(17,$vaga->hora);
			$start->bindValue(18,$vaga->atendimento);
			$start->bindValue(19,$vaga->escola);
		
			$start->execute();						
		}
		
		public function alt($vaga){
			$start = $this->conexao->prepare("
			update vaga set nome = ?, area_experiencia = ?, area = ?,  atividade = ?, beneficio = ?, escolaridade = ?, idioma = ?, horario = ?, salario = ?, cidade = ?, informacao_adicional = ?, nivel = ?, id_recrutador = ?, atendimento = ?, data_conclusao = ?, hora_conclusao = ? where id = ?
			");	
			$start->bindValue(1,$vaga->nome);	
			$start->bindValue(2,$vaga->area_experiencia);			
			$start->bindValue(3,$vaga->area);		
			$start->bindValue(4,$vaga->atividade);		
			$start->bindValue(5,$vaga->beneficio);		
			$start->bindValue(6,$vaga->escolaridade);		
			$start->bindValue(7,$vaga->idioma);					
			$start->bindValue(8,$vaga->horario);		
			$start->bindValue(9,$vaga->salario);	
			$start->bindValue(10,$vaga->cidade);	
			$start->bindValue(11,$vaga->informacao_adicional);						
			$start->bindValue(12,$vaga->nivel);		
			$start->bindValue(13,$vaga->id_recrutador);	
			$start->bindValue(14,$vaga->atendimento);	
			$start->bindValue(15,$vaga->data_conclusao);	
			$start->bindValue(16,$vaga->hora_conclusao);					
			$start->bindValue(17,$vaga->id);
			$start->execute();						
		}
		
		public function altNivel($nivel,$id_vaga,$id_user){
			$start = $this->conexao->prepare("
			update vaga set nivel = ? where id = ? and id_user = ?
			");	
			$start->bindValue(1,$nivel);	
			$start->bindValue(2,$id_vaga);			
			$start->bindValue(3,$id_user);		
			$start->execute();						
		}
		
		public function excluir($id){
			$start = $this->conexao->prepare("
			delete from vaga where id = ?
			");
			$start->bindValue(1,$id);		
			$start->execute();						
		}	
		
		public function getAll($user){
			$start = $this->conexao->prepare("
			select * from vaga where id_user = ? order by id desc
			");
			$start->bindValue(1,$user->id);
			$start->execute();
			return $start->fetchALL(PDO::FETCH_CLASS,'Vaga');
		}
		
		public function getPendente(){
			$start = $this->conexao->prepare("
			select * from vaga where nivel = 4 order by id desc
			");
			$start->execute();
			return $start->fetchALL(PDO::FETCH_CLASS,'Vaga');
		}
		
		public function getDispIndicacao($user){
			$start = $this->conexao->prepare("
			select * from vaga where id_user = ? and nivel = 1 order by id desc
			");
			$start->bindValue(1,$user->id);
			$start->execute();
			return $start->fetchALL(PDO::FETCH_CLASS,'Vaga');
		}
		
		public function getId($id){
			$start = $this->conexao->prepare("
			select vaga.*, info_empresa.nome as empresa from vaga 
			
			left outer join user on user.id = vaga.id_user
			
			left outer join info_empresa on info_empresa.id_user = user.id
			
			where vaga.id = ? and user.id is not null
			");
			$start->bindValue(1,$id);
			$start->execute();
			return $start->fetchALL(PDO::FETCH_CLASS,'Vaga');
		}
		
		public function getContCod($cod){
			$start = $this->conexao->prepare("
			select * from vaga where codigo = ?		
			");
			$start->bindValue(1,$cod);
			$start->execute();
			return $start->rowCount();
		}
		
		public function getFiltro($vaga,$salario,$beneficio,$escolaridade,$idioma){
			
			$sql = "select distinct vaga.id, vaga.*, info_empresa.nome as empresa, user.email as empresa_user from vaga ";
			
			//if(strlen($vaga->empresa) > 0){
				$sql .= " left outer join user on user.id = vaga.id_user 
						  
						  left outer join info_empresa on info_empresa.id_user = user.id";
			//}
						
			$sql .= " where vaga.id is not null and vaga.nivel = 1";
			
			if(strlen($vaga->codigo) > 0){
				$sql .= " and vaga.codigo = '".$vaga->codigo."'";
			}
			
			if(strlen($vaga->empresa) > 0){
				$sql .= " and info_empresa.nome like '%$vaga->empresa%' and info_empresa.id is not null and user.id is not null";
			}
			
			if(strlen($vaga->area_experiencia) > 0){
				$sql .= " and vaga.area_experiencia = '".$vaga->area_experiencia."'";
			}
			
			if(strlen($vaga->area) > 0){
				$sql .= " and vaga.area = '".$vaga->area."'";
			}
			
			if(strlen($vaga->atividade) > 0){
				$sql .= " and vaga.atividade = '".$vaga->atividade."'";
			}
			
			if(strlen($vaga->cidade) > 0){
				$sql .= " and vaga.cidade = '".$vaga->cidade."'";
			}
			
			if(strlen($vaga->horario) > 0){
				$sql .= " and vaga.horario like '%$vaga->area%'";
			}
			
			if(isset($salario[0])){
				$sql .= " and vaga.salario >= ".$salario[0];
			}
			
			if(isset($salario[1])){
				$sql .= " and vaga.salario <= ".$salario[1];
			}
			
			if(count($beneficio) > 0){
				foreach($beneficio as $bnf){
					$sql .= " and vaga.beneficio like '%$bnf%'";
				}
			}
			
			if(count($escolaridade) > 0){
				foreach($escolaridade as $esc){
					$sql .= " and vaga.escolaridade like '%$esc%'";
				}
			}
			
			if(count($idioma) > 0){
				foreach($idioma as $idm){
					$sql .= " and vaga.idioma like '%$idm%'";
				}
			}
			
			$sql .= " order by vaga.id desc limit 0,200";
			
			$start = $this->conexao->prepare($sql);
			$start->execute();
			return $start->fetchALL(PDO::FETCH_CLASS,'Vaga');
		
		}
		
		
		public function getFiltroEmpresa($vaga,$salario,$beneficio,$escolaridade,$idioma){
			
			$sql = "select distinct vaga.id, vaga.*, info_empresa.nome as empresa, user.email as empresa_user from vaga ";
			
			//if(strlen($vaga->empresa) > 0){
				$sql .= " left outer join user on user.id = vaga.id_user 
						  
						  left outer join info_empresa on info_empresa.id_user = user.id";
			//}
						
			$sql .= " where vaga.id is not null ";
			
				$sql .= " and vaga.id_user = '".$vaga->id_user."'";
			
			if(strlen($vaga->id_recrutador) > 0){
				$sql .= " and vaga.id_recrutador = '".$vaga->id_recrutador."'";
			}
			
			if(strlen($vaga->atendimento) > 0){
				$sql .= " and vaga.atendimento = '".$vaga->atendimento."'";
			}
			
			if(strlen($vaga->nivel) > 0){
				$sql .= " and vaga.nivel = ".$vaga->nivel."";
			}else{
				$sql .= " and vaga.nivel = 1";
			}
			
			if(strlen($vaga->codigo) > 0){
				$sql .= " and vaga.codigo = '".$vaga->codigo."'";
			}
			
			if(strlen($vaga->area_experiencia) > 0){
				$sql .= " and vaga.area_experiencia = '".$vaga->area_experiencia."'";
			}
			
			if(strlen($vaga->area) > 0){
				$sql .= " and vaga.area = '".$vaga->area."'";
			}
			
			if(strlen($vaga->atividade) > 0){
				$sql .= " and vaga.atividade = '".$vaga->atividade."'";
			}
			
			if(strlen($vaga->cidade) > 0){
				$sql .= " and vaga.cidade = '".$vaga->cidade."'";
			}
			
			if(strlen($vaga->horario) > 0){
				$sql .= " and vaga.horario like '%$vaga->area%'";
			}
			
			if(strlen($vaga->nome) > 0){
				$sql .= " and vaga.nome like '%$vaga->nome%'";
			}
			
			if(isset($salario[0])){
				$sql .= " and vaga.salario >= ".$salario[0];
			}
			
			if(isset($salario[1])){
				$sql .= " and vaga.salario <= ".$salario[1];
			}
			
			if(count($beneficio) > 0){
				foreach($beneficio as $bnf){
					$sql .= " and vaga.beneficio like '%$bnf%'";
				}
			}
			
			if(count($escolaridade) > 0){
				foreach($escolaridade as $esc){
					$sql .= " and vaga.escolaridade like '%$esc%'";
				}
			}
			
			if(count($idioma) > 0){
				foreach($idioma as $idm){
					$sql .= " and vaga.idioma like '%$idm%'";
				}
			}		
			
			$sql .= " order by vaga.id desc limit 0,200";
			$start = $this->conexao->prepare($sql);
			$start->execute();
			return $start->fetchALL(PDO::FETCH_CLASS,'Vaga');
			
		}
		
		public function getTotal($data){
		
			$sql = "select * from vaga where id is not null ";
		
			if(isset($data[0])){
				$sql .= " and data >= '".$data[0]."' ";
			}
			
			if(isset($data[1])){
				$sql .= " and data <= '".$data[1]."' ";
			}
			
			$start = $this->conexao->prepare($sql);
			$start->execute();
			return $start->fetchALL(PDO::FETCH_CLASS,'Vaga');
		}
		
		public function getCaptacao($data){
			$sql = "select * from vaga where nivel = 1 ";
			
			if(isset($data[0])){
				$sql .= " and data >= '".$data[0]."' ";
			}
			
			if(isset($data[1])){
				$sql .= " and data <= '".$data[1]."' ";
			}
			
			$start = $this->conexao->prepare($sql);
			$start->execute();
			return $start->fetchALL(PDO::FETCH_CLASS,'Vaga');
		}
		
		public function getConcluido($data){
			$sql = "select * from vaga where nivel = 2 ";
			
			if(isset($data[0])){
				$sql .= " and data >= '".$data[0]."' ";
			}
			
			if(isset($data[1])){
				$sql .= " and data <= '".$data[1]."' ";
			}
			
			if(isset($data[2])){
				$sql .= " and data_conclusao >= '".$data[2]."' ";
			}
			
			if(isset($data[3])){
				$sql .= " and data_conclusao <= '".$data[3]."' ";
			}
			
			
			$start = $this->conexao->prepare($sql);
			$start->execute();
			return $start->fetchALL(PDO::FETCH_CLASS,'Vaga');
		}
		
		public function ajuste_nome($texto,$quantos){
			$ate=$quantos;
			$mensagem=$texto;
			$str = substr($mensagem,$ate,1);
			if($str!=" "){
				while($str!=" "){
					$i=1;
					$ate=$ate+$i;
					$mensagem=$texto;
					$str = substr($mensagem,$ate,1);
				}		
			}
			
			$str = substr($mensagem,0,$ate);
			return $str." ...";
		}	
		
		public function getContTotal($data,$id_recrutador){
		
			$sql = "select * from vaga where id is not null ";
		
			if(isset($data[0])){
				$sql .= " and data >= '".$data[0]."' ";
			}
			
			if(isset($data[1])){
				$sql .= " and data <= '".$data[1]."' ";
			}
			
			if(strlen($id_recrutador) > 0){
				$sql .= " and id_recrutador = ".$id_recrutador." ";
			}
			
			$start = $this->conexao->prepare($sql);
			$start->execute();
			return $start->rowCount();
		}
		
		public function getContCaptacao($data,$id_recrutador){
			$sql = "select * from vaga where nivel = 1 ";
			
			if(isset($data[0])){
				$sql .= " and data >= '".$data[0]."' ";
			}
			
			if(isset($data[1])){
				$sql .= " and data <= '".$data[1]."' ";
			}
			
			if(strlen($id_recrutador) > 0){
				$sql .= " and id_recrutador = ".$id_recrutador." ";
			}
			
			$start = $this->conexao->prepare($sql);
			$start->execute();
			return $start->rowCount();
		}
		
		public function getContConcluido($data,$id_recrutador){
			$sql = "select * from vaga where nivel = 2 ";
			
			if(isset($data[0])){
				$sql .= " and data >= '".$data[0]."' ";
			}
			
			if(isset($data[1])){
				$sql .= " and data <= '".$data[1]."' ";
			}
			
			if(isset($data[2])){
				$sql .= " and data_conclusao >= '".$data[2]."' ";
			}
			
			if(isset($data[3])){
				$sql .= " and data_conclusao <= '".$data[3]."' ";
			}
						
			if(strlen($id_recrutador) > 0){
				$sql .= " and id_recrutador = ".$id_recrutador." ";
			}
			
			$start = $this->conexao->prepare($sql);
			$start->execute();
			return $start->rowCount();
		}
	
		public function getContCancelado($data,$id_recrutador){
			$sql = "select * from vaga where nivel = 3 ";
			
			if(isset($data[0])){
				$sql .= " and data >= '".$data[0]."' ";
			}
			
			if(isset($data[1])){
				$sql .= " and data <= '".$data[1]."' ";
			}
			
			if(strlen($id_recrutador) > 0){
				$sql .= " and id_recrutador = ".$id_recrutador." ";
			}
			
			$start = $this->conexao->prepare($sql);
			$start->execute();
			return $start->rowCount();
		}
		
		public function getContPendente($data,$id_recrutador){
			$sql = "select * from vaga where nivel = 4 ";
			
			if(isset($data[0])){
				$sql .= " and data >= '".$data[0]."' ";
			}
			
			if(isset($data[1])){
				$sql .= " and data <= '".$data[1]."' ";
			}
			
			if(strlen($id_recrutador) > 0){
				$sql .= " and id_recrutador = ".$id_recrutador." ";
			}
			
			$start = $this->conexao->prepare($sql);
			$start->execute();
			return $start->rowCount();
		}
		
		public function getCancelado($data){
			$sql = "select * from vaga where nivel = 3 ";
			
			if(isset($data[0])){
				$sql .= " and data >= '".$data[0]."' ";
			}
			
			if(isset($data[1])){
				$sql .= " and data <= '".$data[1]."' ";
			}
			
			$start = $this->conexao->prepare($sql);
			$start->execute();
			return $start->fetchALL(PDO::FETCH_CLASS,'Vaga');
		}
		
		public function getPendenteFoco($data){
			$sql = "select * from vaga where nivel = 4 ";
			
			if(isset($data[0])){
				$sql .= " and data >= '".$data[0]."' ";
			}
			
			if(isset($data[1])){
				$sql .= " and data <= '".$data[1]."' ";
			}
			
			$start = $this->conexao->prepare($sql);
			$start->execute();
			return $start->fetchALL(PDO::FETCH_CLASS,'Vaga');
		}
	
	}
	
?>