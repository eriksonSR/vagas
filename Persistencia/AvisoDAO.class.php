<?php
	class AvisoDAO{	
		// Declaração de atributos.
		private $conexao;		
		
		// Declaração de metodos.
		public function __construct(){
			$this->conexao = Conexao::getInstancia();
		}
		
		public function cadastro($aviso){
			$start = $this->conexao->prepare("
			insert into aviso() values(null,?,?,?,?,?,?,?)
			");			
			$start->bindValue(1,$aviso->id_autor); 
			$start->bindValue(2,$aviso->id_user); 
			$start->bindValue(3,$aviso->id_vaga);
			$start->bindValue(4,$aviso->texto);
			$start->bindValue(5,$aviso->tipo);
			$start->bindValue(6,$aviso->data);
			$start->bindValue(7,$aviso->hora);	
			$start->execute();						
		}
		
		public function getUser($id,$data){
			$start = $this->conexao->prepare("
			 select * from aviso where id_user = ? and data > ?
			");
			$start->bindValue(1,$id);
			$start->bindValue(2,$data);
			$start->execute();
			return $start->fetchALL(PDO::FETCH_CLASS,'Aviso');
		}
		
		public function excluirVaga($aviso){
			$start = $this->conexao->prepare("
			delete from aviso where id_autor = ? and id_user = ? and id_vaga = ?
			");			
			$start->bindValue(1,$aviso->id_autor); 
			$start->bindValue(2,$aviso->id_user); 
			$start->bindValue(3,$aviso->id_vaga);
			$start->execute();						
		}
	}
?>