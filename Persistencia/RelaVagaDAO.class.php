<?php
	class RelaVagaDAO{	
		// Declaração de atributos.
		private $conexao;
		
		// Declaração de metodos.
		public function __construct(){
			$this->conexao = Conexao::getInstancia();
		}
		
		public function cadastro($relaVaga){
			$start = $this->conexao->prepare("
			insert into rela_vaga() values(null,?,?,?,?,?)
			");
			$start->bindValue(1,$relaVaga->id_vaga);
			$start->bindValue(2,$relaVaga->id_user);
			$start->bindValue(3,$relaVaga->nivel);
			$start->bindValue(4,$relaVaga->data);
			$start->bindValue(5,$relaVaga->hora);			
			$start->execute();						
		}
		
		public function alt($relaVaga){
			$start = $this->conexao->prepare("
			update rela_vaga set nivel = ? where id = ?
			");
			$start->bindValue(1,$relaVaga->nivel);
			$start->bindValue(2,$relaVaga->id);
			$start->execute();						
		}
		
		public function altRela($relaVaga){
			$start = $this->conexao->prepare("
			update rela_vaga set nivel = ? where id_user = ? and id_vaga = ?
			");
			$start->bindValue(1,$relaVaga->nivel);
			$start->bindValue(2,$relaVaga->id_user);
			$start->bindValue(3,$relaVaga->id_vaga);
			$start->execute();						
		}
		
		public function excluir($relaVaga){
			$start = $this->conexao->prepare("
			delete from rela_vaga where id_user = ? and id_vaga = ?
			");
			$start->bindValue(1,$relaVaga->id_user);
			$start->bindValue(2,$relaVaga->id_vaga);
			$start->execute();						
		}
		
		public function getUser($id){
			$start = $this->conexao->prepare("
			select distinct info_candidato.id, info_candidato.*, rela_vaga.nivel as nivel_candidatura from info_candidato
			
			left outer join rela_vaga on rela_vaga.id_user = info_candidato.id_user
			
			where rela_vaga.id_vaga = ? and rela_vaga.id is not null order by rela_vaga.nivel 	
			");
			$start->bindValue(1,$id);
			$start->execute();
			return $start->fetchALL(PDO::FETCH_CLASS,'InfoCandidato');
		}	
		
		public function getUserRecrutado($id){
			$start = $this->conexao->prepare("
			select distinct info_candidato.id, info_candidato.*, rela_vaga.nivel as nivel_candidatura from info_candidato
			
			left outer join rela_vaga on rela_vaga.id_user = info_candidato.id_user
			
			where rela_vaga.id_vaga = ? and rela_vaga.id is not null and rela_vaga.nivel = 	2 	
			");
			$start->bindValue(1,$id);
			$start->execute();
			return $start->fetchALL(PDO::FETCH_CLASS,'InfoCandidato');
		}	
		
		
		public function getNivelUser($id,$nivel){
			$start = $this->conexao->prepare("
			select distinct info_candidato.id, info_candidato.*, rela_vaga.nivel as nivel_candidatura from info_candidato
			
			left outer join rela_vaga on rela_vaga.id_user = info_candidato.id_user 
			
			where rela_vaga.id_vaga = ? and rela_vaga.id is not null and rela_vaga.nivel = ?			
			");
			$start->bindValue(1,$id);
			$start->bindValue(2,$nivel);
			$start->execute();
			return $start->fetchALL(PDO::FETCH_CLASS,'InfoCandidato');
		}
		
		public function getVaga($id){
			$start = $this->conexao->prepare("
			select distinct vaga.id, vaga.*, info_empresa.nome as empresa, user.email as empresa_user from vaga
			
			left outer join user on user.id = vaga.id_user 
						  
			left outer join info_empresa on info_empresa.id_user = user.id
						  
			left outer join rela_vaga on rela_vaga.id_vaga = vaga.id
			
			where rela_vaga.id_user = ? and rela_vaga.id is not null			
			");
			$start->bindValue(1,$id);
			$start->execute();
			return $start->fetchALL(PDO::FETCH_CLASS,'Vaga');
		}
		
		public function getRela($relaVaga){
			$start = $this->conexao->prepare("
			select * from rela_vaga where id_user = ? and id_vaga = ?
			");
			$start->bindValue(1,$relaVaga->id_user);
			$start->bindValue(2,$relaVaga->id_vaga);
			$start->execute();	
			return $start->fetchALL(PDO::FETCH_CLASS,'RelaVaga');
		}
		
		public function getTotal($data){
			$sql = " select rela_vaga.*, vaga.codigo codigo_vaga, vaga.nome nome_vaga, info_candidato.nome nome_candidato, vaga.salario salario_vaga from rela_vaga 
			
					left outer join vaga on vaga.id = rela_vaga.id_vaga 
					
					left outer join info_candidato on info_candidato.id_user = rela_vaga.id_user
					
					where rela_vaga.id is not null 
					
					";
			
			if(isset($data[0])){
				$sql .= " and rela_vaga.data >= '".$data[0]."' ";
			}
			
			if(isset($data[1])){
				$sql .= " and rela_vaga.data <= '".$data[1]."' ";
			}
			
			$start = $this->conexao->prepare($sql);
			$start->execute();
			return $start->fetchALL(PDO::FETCH_CLASS,'RelaVaga');
		}
		
		public function getCandidato($data){
			$sql = " select rela_vaga.*, vaga.codigo codigo_vaga, vaga.nome nome_vaga, info_candidato.nome nome_candidato, vaga.salario salario_vaga from rela_vaga 
			
					left outer join vaga on vaga.id = rela_vaga.id_vaga 
					
					left outer join info_candidato on info_candidato.id_user = rela_vaga.id_user
			
					where rela_vaga.nivel = 1 ";
			
			if(isset($data[0])){
				$sql .= " and rela_vaga.data >= '".$data[0]."' ";
			}
			
			if(isset($data[1])){
				$sql .= " and rela_vaga.data <= '".$data[1]."' ";
			}
			
			$start = $this->conexao->prepare($sql);
			$start->execute();
			return $start->fetchALL(PDO::FETCH_CLASS,'RelaVaga');
		}
		
		public function getRecrutado($data){
			$sql = " select rela_vaga.*, vaga.codigo codigo_vaga, vaga.nome nome_vaga, info_candidato.nome nome_candidato, vaga.salario salario_vaga from rela_vaga 
			
					left outer join vaga on vaga.id = rela_vaga.id_vaga 
					
					left outer join info_candidato on info_candidato.id_user = rela_vaga.id_user
									
					where rela_vaga.nivel = 2 ";
			
			if(isset($data[0])){
				$sql .= " and rela_vaga.data >= '".$data[0]."' ";
			}
			
			if(isset($data[1])){
				$sql .= " and rela_vaga.data <= '".$data[1]."' ";
			}
			
			$start = $this->conexao->prepare($sql);
			$start->execute();
			return $start->fetchALL(PDO::FETCH_CLASS,'RelaVaga');
		}
		
		public function getContRela($relaVaga){
			$start = $this->conexao->prepare("
			select * from rela_vaga where id_user = ? and id_vaga = ?
			");
			$start->bindValue(1,$relaVaga->id_user);
			$start->bindValue(2,$relaVaga->id_vaga);
			$start->execute();	
			return $start->rowCount();
		}
		
		public function getContCandidatoVaga($id){
			$start = $this->conexao->prepare("
			select * from rela_vaga where id_vaga = ?
			");
			$start->bindValue(1,$id);
			$start->execute();	
			return $start->rowCount();
		}
		
		public function getContTotal($data,$id_recrutador){
			$sql = " select * from rela_vaga ";
			
			$sql .= " left outer join vaga on vaga.id = rela_vaga.id_vaga ";
			
			$sql .= " where rela_vaga.id is not null ";
			
			if(isset($data[0])){
				$sql .= " and vaga.data >= '".$data[0]."' ";
			}
			
			if(isset($data[1])){
				$sql .= " and vaga.data <= '".$data[1]."' ";
			}
			
			if(strlen($id_recrutador) > 0){
				$sql .= " and vaga.id_recrutador = ".$id_recrutador." ";
			}
			
			$start = $this->conexao->prepare($sql);
			$start->execute();
			return $start->rowCount();			
		}
		
		public function getContCandidato($data,$id_recrutador){
			
			$sql = " select * from rela_vaga ";
			
			$sql .= " left outer join vaga on vaga.id = rela_vaga.id_vaga ";
			
			$sql .= " where rela_vaga.id is not null and rela_vaga.nivel = 1 ";
			
			if(isset($data[0])){
				$sql .= " and rela_vaga.data >= '".$data[0]."' ";
			}
			
			if(isset($data[1])){
				$sql .= " and rela_vaga.data <= '".$data[1]."' ";
			}
			
			if(strlen($id_recrutador) > 0){
				$sql .= " and vaga.id_recrutador = ".$id_recrutador." ";
			}
												
			$start = $this->conexao->prepare($sql);
			$start->execute();
			return $start->rowCount();
		}
		
		public function getContRecrutado($data,$id_recrutador){
			$sql = " select * from rela_vaga ";
			
			$sql .= " left outer join vaga on vaga.id = rela_vaga.id_vaga ";
			
			$sql .= " where rela_vaga.id is not null and rela_vaga.nivel = 2 ";
			
			if(isset($data[0])){
				$sql .= " and rela_vaga.data >= '".$data[0]."' ";
			}
			
			if(isset($data[1])){
				$sql .= " and rela_vaga.data <= '".$data[1]."' ";
			}
			
			if(strlen($id_recrutador) > 0){
				$sql .= " and vaga.id_recrutador = ".$id_recrutador." ";
			}
			
			$start = $this->conexao->prepare($sql);
			$start->execute();
			return $start->rowCount();
		}
		
		public function getContSelecionado($data,$id_recrutador){
			$sql = " select * from rela_vaga ";
			
			$sql .= " left outer join vaga on vaga.id = rela_vaga.id_vaga ";
			
			$sql .= " where rela_vaga.id is not null and rela_vaga.nivel = 3 ";
			
			if(isset($data[0])){
				$sql .= " and rela_vaga.data >= '".$data[0]."' ";
			}
			
			if(isset($data[1])){
				$sql .= " and rela_vaga.data <= '".$data[1]."' ";
			}
			
			if(isset($data[2])){
				$sql .= " and vaga.data_conclusao >= '".$data[2]."' ";
			}
			
			if(isset($data[3])){
				$sql .= " and vaga.data_conclusao <= '".$data[3]."' ";
			}
			
			if(strlen($id_recrutador) > 0){
				$sql .= " and vaga.id_recrutador = ".$id_recrutador." ";
			}
			
			$start = $this->conexao->prepare($sql);
			$start->execute();
			return $start->rowCount();
		}
		
		
		public function getContPendente($data,$id_recrutador){
			$sql = " select * from rela_vaga ";
			
			$sql .= " left outer join vaga on vaga.id = rela_vaga.id_vaga ";
			
			$sql .= " where rela_vaga.id is not null and rela_vaga.nivel = 4 ";
			
			if(isset($data[0])){
				$sql .= " and rela_vaga.data >= '".$data[0]."' ";
			}
			
			if(isset($data[1])){
				$sql .= " and rela_vaga.data <= '".$data[1]."' ";
			}
			
			if(strlen($id_recrutador) > 0){
				$sql .= " and vaga.id_recrutador = ".$id_recrutador." ";
			}
			
			$start = $this->conexao->prepare($sql);
			$start->execute();
			return $start->rowCount();
		}
		
		public function getSelecionado($data){
			$sql = "select rela_vaga.*, vaga.codigo codigo_vaga, vaga.nome nome_vaga, info_candidato.nome nome_candidato, vaga.salario salario_vaga from rela_vaga 
			
					left outer join vaga on vaga.id = rela_vaga.id_vaga 
					
					left outer join info_candidato on info_candidato.id_user = rela_vaga.id_user
			
					where rela_vaga.nivel = 3 ";
			
			if(isset($data[0])){
				$sql .= " and rela_vaga.data >= '".$data[0]."' ";
			}
			
			if(isset($data[1])){
				$sql .= " and rela_vaga.data <= '".$data[1]."' ";
			}
			
			$start = $this->conexao->prepare($sql);
			$start->execute();
			return $start->fetchALL(PDO::FETCH_CLASS,'RelaVaga');
		}
		
	}
?>