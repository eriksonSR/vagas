<?php
	class RecrutadorDAO{	
		// Declaração de atributos.
		private $conexao;
		
		// Declaração de metodos.
		public function __construct(){
			$this->conexao = Conexao::getInstancia();
		}
		
		public function cadastro($recrutador){
			$start = $this->conexao->prepare("
			insert into recrutador() values(null,?,?)
			");
			$start->bindValue(1,$recrutador->id_user);		
			$start->bindValue(2,$recrutador->nome);			
			$start->execute();			
		}
		
		public function alt($recrutador){
			$start = $this->conexao->prepare("
			update recrutador set nome = ? where id = ?
			");	
			$start->bindValue(1,$recrutador->nome);			
			$start->bindValue(2,$recrutador->id);	
			$start->execute();			
		}
		
		public function getEmpresa($id){
			$start = $this->conexao->prepare("
			select * from recrutador where id_user = ?
			");
			$start->bindValue(1,$id);
			$start->execute();
			
			return $start->fetchALL(PDO::FETCH_CLASS,'Recrutador');
			
		}
		
		public function getId($id){
			$start = $this->conexao->prepare("
			select * from recrutador where id = ?
			");
			$start->bindValue(1,$id);
			$start->execute();
			return $start->fetchALL(PDO::FETCH_CLASS,'Recrutador');
		}
		
		public function get(){
			$start = $this->conexao->prepare("
			select * from recrutador 
			");
			$start->execute();
			return $start->fetchALL(PDO::FETCH_CLASS,'Recrutador');
		}
		
	}
	
?>