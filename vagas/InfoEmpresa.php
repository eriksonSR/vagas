<?php
session_start();

date_default_timezone_set('America/Sao_Paulo');	
include 'Banco/Conexao.class.php';
include 'Model/User.class.php';
include 'Persistencia/UserDAO.class.php';
include 'Model/InfoEmpresa.class.php';
include 'Persistencia/InfoEmpresaDAO.class.php';

if(isset($_SESSION['foco_empresa']) && isset($_SESSION['ct_admin'])){
	$infoEmpresa = unserialize($_SESSION['foco_empresa']);
}else{
	header('location:index.php');
}
?>
<!DOCTYPE HTML>
<html><!-- InstanceBegin template="/Templates/InfoEmpresaTemplate.dwt" codeOutsideHTMLIsLocked="false" --> 
<head>

<title>Bem-vindo</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600" rel="stylesheet" type="text/css" />

<link rel="stylesheet" type="text/css" href="css/table/table.css">

<link href="css/table/style.css" rel="stylesheet" type="text/css" />
<link href="css/table/form/screen.css" rel="stylesheet" type="text/css" />

<!--[if lte IE 8]><script src="js/html5shiv.js"></script><![endif]-->
<script src="js/request/AjaxRequest.js"></script> 

<script src="js/jquery.min.js"></script>
<script src="js/skel.min.js"></script>
<script src="js/skel-panels.min.js"></script>
<script src="js/init.js"></script>

<!-- Fim da popup -->

<noscript>
<link rel="stylesheet" href="css/skel-noscript.css" />
<link rel="stylesheet" href="css/style.css" />
<link rel="stylesheet" href="css/style-wide.css" />
</noscript>
<!--[if lte IE 9]><link rel="stylesheet" href="css/ie9.css" /><![endif]-->
<!--[if lte IE 8]><link rel="stylesheet" href="css/ie8.css" /><![endif]-->
</head>
<body>

<div id="pop"> <img src="images/ajax-loader.gif" > </div>

<div id='alerta'> </div>
<!-- Header -->
<div id="header" class="skel-panels-fixed">
  <div class="top">
    <div id="logo"> 
      <h1 id="title"><!-- InstanceBeginEditable name="EditRegion1" -->	  
  	  <?php
	  echo $infoEmpresa[0]->nome;	  	  
	  ?>
	  <!-- InstanceEndEditable --></h1>     
    </div>
    <!-- Nav -->
    <nav id="nav">
      <ul>
        <li><a href="#info" id="info-link" class="skel-panels-ignoreHref"><span class="fa fa-info-circle">Informações pessoais</span></a></li>        
                
        <!-- InstanceBeginEditable name="EditRegion6" -->
		<li><a href="Controller/Admin.controller.php?op=<?php echo sha1(7) ?>" class="skel-panels-ignoreHref"><span class="fa fa-folder-open-o">Acessar como</span></a></li>  		
		<!-- InstanceEndEditable -->
        
        <li style="border-bottom:1px #999999 groove; margin-top:7px;"> </li>
        
        <!-- InstanceBeginEditable name="EditRegion10" -->
         <?php
        	if(isset($_SESSION['ct_empresa'])){
				echo '<li><a href="PerfilEmpresa.php"><span class="fa fa-home">Voltar ao perfil inicial</span></a></li>';
			}else if(isset($_SESSION['ct_user'])){
				echo '<li><a href="PerfilCandidato.php"><span class="fa fa-home">Voltar ao perfil inicial</span></a></li>';
			}else if(isset($_SESSION['ct_admin'])){
				echo '<li><a href="PerfilAdmin.php"><span class="fa fa-home">Voltar ao perfil inicial</span></a></li>';
			}
		 ?>
        <!-- InstanceEndEditable -->
        
        <li><a href="Controller/Logoff.controller.php"><span class="fa fa-lock">Sair do sistema</span></a></li>
      </ul>
    </nav>
  </div>
  <div class="bottom">
    <!-- Social Icons -->
    <ul class="icons">
      <li><a href="#" class="fa fa-facebook solo"><span>Facebook</span></a></li>
      <li><a href="#" class="fa fa-envelope solo"><span>Email</span></a></li>
    </ul>
  </div>
</div>
<!-- Main -->
<div id="main">
	
  <!-- Intro -->
  <section id="info" class="one">
    <div class="container">
        <!--  <a  class="image featured"><img src="../banner.jpg" alt="" /></a> -->
        <!-- InstanceBeginEditable name="EditRegion4" -->
        <header id='nome_vaga'>
          <h2 class="alt"><strong><?php echo $infoEmpresa[0]->nome; ?></strong></h2>
        </header>
        <p># Estágios –Portal de Currículos, onde as oportunidades <br />
          encontram os estagiários certos.</p>
        <!-- InstanceEndEditable -->
    </div>
  </section> 
  
  <section id="info" class="two">
    <div class="container">
    
      <header>
        <h2>Informações da empresa</h2>           
      </header> 
      
	  <div id="dados_vaga"> 
		  <!-- InstanceBeginEditable name="EditRegion5" -->
          <header>
            <div class="row half" style="margin-top:-15px;">
              <div class="6u" id='input_full'> <b style="color:#000000;">Razão social</b> <br />
               <?php
               echo $infoEmpresa[0]->razao_social;
               ?>
              </div>          
            
              <div class="6u" id='input_full'> <b style="color:#000000;">Nome fantasia</b> <br />
               <?php
               echo $infoEmpresa[0]->nome;
               ?>
              </div>
            </div>
            
            <div class="row half" style="margin-top:-15px;">
              <div class="6u" id='input_full'> <b style="color:#000000;">Ramu de atuação</b> <br />
               <?php
               echo $infoEmpresa[0]->ramu_atuacao;
               ?>
              </div>          
            
              <div class="6u" id='input_full'> <b style="color:#000000;">Porte da empresa</b> <br />
               <?php
               echo $infoEmpresa[0]->porte;
               ?>
              </div>
            </div>
            
            <div class="row half" style="margin-top:-15px;">
              <div class="6u" id='input_full'> <b style="color:#000000;">Website</b> <br />
               <?php
               echo $infoEmpresa[0]->site;
               ?>
              </div>          
            
              <div class="6u" id='input_full'> <b style="color:#000000;">E-mail</b> <br />
               <?php
               echo $infoEmpresa[0]->email;
               ?>
              </div>
            </div>
            
            <div class="row half" style="margin-top:-15px;">
              <div class="6u" id='input_full'> <b style="color:#000000;">Telefone</b> <br />
               <?php
               echo $infoEmpresa[0]->tel;
               ?>
              </div>          
            
              <div class="6u" id='input_full'> <b style="color:#000000;">Celular</b> <br />
               <?php
               echo $infoEmpresa[0]->cel;
               ?>
              </div>
            </div>
            
            <div class="row half" style="margin-top:-15px;">
              <div class="6u" id='input_full'> <b style="color:#000000;">Facebook</b> <br />
               <?php
               echo $infoEmpresa[0]->facebook;
               ?>
              </div>          
            
              <div class="6u" id='input_full'> <b style="color:#000000;">Twitter</b> <br />
               <?php
               echo $infoEmpresa[0]->twitter;
               ?>
              </div>
            </div>
           </header> 
            
            <header>
         	  <h2>Representante do RH</h2>             
          	</header>
             
             <header>	
             
             <div class="row half" style="margin-top:-15px;">
              <div class="6u" id='input_full'> <b style="color:#000000;">Representante do RH</b> <br />
               <?php
               echo $infoEmpresa[0]->nome_rh;
               ?>
              </div>          
            
              <div class="6u" id='input_full'> <b style="color:#000000;">Telefone</b> <br />
               <?php
               echo $infoEmpresa[0]->tel_rh;
               ?>
              </div>
             </div>
             
             <div class="row half" style="margin-top:-15px;">
              <div class="6u" id='input_full'> <b style="color:#000000;">Celular</b> <br />
               <?php
               echo $infoEmpresa[0]->cel_rh;
               ?>
              </div>          
            
              <div class="6u" id='input_full'> <b style="color:#000000;">E-mail</b> <br />
               <?php
               echo $infoEmpresa[0]->email_rh;
               ?>
              </div>
             </div>
                
        	</header>
            
             <header>
         	   <h2>Endereço cadastrado</h2>            
          	 </header>
                         
              <div class="row half" style="margin-top:-15px;">
              <div class="6u" id='input_full'> <b style="color:#000000;">Estado</b> <br />
               <?php
               echo $infoEmpresa[0]->estado;
               ?>
              </div>          
            
              <div class="6u" id='input_full'> <b style="color:#000000;">Cidade</b> <br />
               <?php
               echo $infoEmpresa[0]->cidade;
               ?>
              </div>
             </div>
             
             <div class="row half" style="margin-top:-15px;">
              <div class="6u" id='input_full'> <b style="color:#000000;">Bairro</b> <br />
               <?php
               echo $infoEmpresa[0]->bairro;
               ?>
              </div>          
            
              <div class="6u" id='input_full'> <b style="color:#000000;">Rua / Número</b> <br />
               <?php
               echo $infoEmpresa[0]->rua;
               ?>
              </div>
             </div>

		  <!-- InstanceEndEditable -->   
          
          <div class="row">
		  <!-- InstanceBeginEditable name="EditRegion7" -->          
              	<?php
				if(isset($_SESSION['ct_admin'])){
					
					echo "<div class='12u' id='botao_bloqueio'> ";
				
					$userDAO = new UserDAO();
															
					$user = $userDAO->getId($infoEmpresa[0]->id_user);
					
					if($user[0]->status == 0){
						?>                	
    	            	<a class="button submit" onClick='getId("Controller/Admin.controller.php?op=<?php echo sha1(4) ?>&id=<?php echo base64_encode($infoEmpresa[0]->id_user) ?>&status=<?php echo base64_encode(1) ?>&tp=empresa","botao_bloqueio")'>Bloquear empresa</a> 		                
						<?php
					}else{
						?>                	
    	            	<a class="button submit" onClick='getId("Controller/Admin.controller.php?op=<?php echo sha1(4) ?>&id=<?php echo base64_encode($infoEmpresa[0]->id_user) ?>&status=<?php echo base64_encode(0) ?>&tp=empresa","botao_bloqueio")'>Liberar empresa</a> 		                
						<?php
					}
					
					echo "</div>";										
				}
				?>
              
          <!-- InstanceEndEditable -->
          </div>
        
      </div> 
      
    </div>
  </section>    
 
</div>
<!-- Footer -->
<div id="footer">
  <!-- Copyright -->
  <div class="copyright">
    <ul class="menu">
      <li>AST Facilities.</li>
    </ul>
  </div>
</div>
</body>
<!-- InstanceEnd --></html>
