<?php
	class Experiencia{
		// Declaração de atributos.
		private $id;
		private $id_user;
		private $empresa;
		private $cargo;
		private $descricao;
		private $data_inicio;
		private $data_fim;		
		
		// Declaração de metodos.
		public function __set($atributo,$valor){
			$this->$atributo = $valor;
		}
		
		public function __get($atributo){
			return $this->$atributo;
		}
	}

?>