<?php
	class InfoEmpresa{
		private $id;
		private $id_user;		
		private $razao_social = NULL;					
		private $nome = NULL;			
		private $site = NULL;				
		private $porte = NULL;
		private $ramu_atuacao = NULL;		
		private $nome_rh = NULL;
		private $tel_rh = NULL;		
		private $cel_rh = NULL;		
		private $email_rh = NULL;		
		private $tel = NULL;	
		private $cel = NULL;
		private $facebook = NULL;
		private $twitter = NULL;							
		private $estado = NULL;
		private $cidade = NULL;
		private $bairro = NULL;
		private $rua = NULL;							
		private $data;
		private $hora;
		
		// Utilizado em filtros
		private $cnpj;
		private $email;
		
		public function __set($atributo,$valor){
			$this->$atributo=$valor;
		}
		public function __get($atributo){
			return $this->$atributo;
		}			
	}
?>