<?php
	class Vaga{
		private $id;
		private $id_user;
		private $id_recrutador;
		private $codigo;
		private $nome;
		private $area_experiencia;
		private $area;
		private $atividade;
		private $beneficio;
		private $escolaridade;
		private $idioma;
		private $horario;	
		private $salario;
		private $cidade;
		private $nivel;		
		private $informacao_adicional;
		private $data;
		private $hora;
		private $escola;
		
		private $empresa;
		private $empresa_user;
		
		private $atendimento;
		private $gerente;
		
		private $data_conclusao;
		private $hora_conclusao;
		
		/* 
		Nivel:
		
		1 - Capta��o de candidatos.
		
		*/	
		
		public function __set($atributo,$valor){
			$this->$atributo=$valor;
		}
		public function __get($atributo){
			return $this->$atributo;
		}			
	}
?>