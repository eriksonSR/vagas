<?php
	class Validacao{
		
		public static function email($email){
			return filter_var($email,FILTER_VALIDATE_EMAIL);
		}						
		
		public static function numero($numero){
			if(is_numeric($numero)){
				return is_numeric($numero) && $numero>0;
			}else{
				return FALSE;
			}
		}		
		public static function tel($tel){
			$format = '/^[0-9]{10,10}$/'; 
			return preg_match($format,$tel);
		}
		
		public static function data($mes,$dia,$ano){
			return checkdate($mes,$dia,$ano);
		}
		
		public static function login($login){
			$format = '/^[a-z0-9@#$%&*(){}\[\]\-\+_\;\:\|\.\,]{4,20}$/'; 
			// minimo 4 digitos maximo 20 somente letra e numero
			return preg_match($format,$login);
		}
		
		public static function senha($senha){
			$format = '/^[a-zA-Z0-9@#$%&*(){}\[\]\-\+_\;\:\|\.\,]{6,255}$/'; 
			return preg_match($format,$senha);
		}
		
		public static function nome($senha){
			$format = '/^[a-zA-Z0-9\\@\\#\\$\\%\\&\\*\\(\\)\\[\\]\\{\\}\\<\\>\\.\\,\\:\\;\\?\\+\\-\\"\\!]{1,255}$/';
			return preg_match($format,$senha);
		}
	}
?> 