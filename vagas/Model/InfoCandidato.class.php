<?php
	class InfoCandidato{
		private $id;
		private $id_user;		
		private $nome = NULL;	
		private $rg = NULL;					
		private $tel = NULL;
		private $cel = NULL;		
		private $facebook = NULL;
		private $linkedin = NULL;		
		private $nascimento = NULL;
		private $informatica = NULL;		
		private $cnh = "";
		private $idioma = "";		
		private $estado_civil = NULL;				
		private $escolaridade = NULL;								
		private $filho_pequeno = NULL;
		private $sexo = NULL;				
		private $dia_disponivel = "";
		private $turno_disponivel = "";		
		private $estado = NULL;
		private $cidade = NULL;
		private $bairro = NULL;
		private $rua = NULL;
		private $auto_imagem = "";
		private $anotacao = "";
							
		private $data;
		private $hora;
		
		// Utilizado no filtro pela empresa
		private $email;
		private $cpf;
		
		// Utilizado na listagem de candidatos na pagina da vaga
		private $nivel_candidatura;
		
		
		public function __set($atributo,$valor){
			$this->$atributo=$valor;
		}
		public function __get($atributo){
			return $this->$atributo;
		}			
	}
?>