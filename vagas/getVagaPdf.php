<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<style>
</style>

<body>
<?php
session_start();

include 'Banco/Conexao.class.php';
include 'Model/Vaga.class.php';

if(isset($_SESSION['foco_vaga'])){
	
	$vaga = unserialize($_SESSION['foco_vaga']);
	
	$html = '
		<html>
		<head>
		<style>
		body {font-family: sans-serif;
			font-size: 9pt;
			background: transparent url(\'geraPdf/examples/bgbarcode.png\') repeat-y scroll left top;					
		}
		
		h5, p {	margin: 0pt;
		}
		table.items {
			font-size: 9pt; 
			border-collapse: collapse;
			border: 3px solid #880000; 
		}
		td { vertical-align: top; 
		}
		table thead td { background-color: #EEEEEE;
			text-align: center;
		}
		table tfoot td { background-color: #AAFFEE;
			text-align: center;
		}
		.barcode {
			padding: 1.5mm;
			margin: 0;
			vertical-align: top;
			color: #000000;
		}
		.barcodecell {
			text-align: center;
			vertical-align: middle;
			padding: 0;
		}
		h2{
			text-align: center;
			font-size:30px;
			color:#7087A9;
		}
		h3{
			color:#53698A;
		}
		#corpo_left{
			margin-left:6%;
		}
		
		#corpo_right{
			right:15%; 
			position:absolute;
			top:14.5%;
		}
		
		#corpo_centro{
			text-align: center;
		}
		
		</style>
		</head>
		<body>
						
		<h2>'.$vaga[0]->nome.'</h2>
		<p style="text-align: center; padding-top:-10px;">'.substr($vaga[0]->escolaridade,0,strlen($vaga[0]->escolaridade) - 1).' </p>
		
		<div id="corpo_left">
				
			<h3>Empresa</h3>		
			<p>'.$vaga[0]->empresa.'</p>			
			
			<h3>Área de atuação </h3>		
			
			<p>'.$vaga[0]->area.'</p>
					
			<h3>Atividades relacionadas</h3>		
			<p>'.$vaga[0]->atividade.'</p>
			
			<h3>Idiomas desejados</h3>		
			<p>'.substr($vaga[0]->idioma,0,strlen($vaga[0]->idioma) - 1).'</p>
			
			<h3>Remuneração   </h3>		
	
			<p>R$ '.number_format($vaga[0]->salario, 2, ',', '.').'</p>			
			
			
			<p style="margin-top:5%;">
				Deseja se candidatar a esta vaga? acesse <b><u>www.qi.edu.br/recrutamento</u></b> <br /><br />Código da vaga: <b>'.$vaga[0]->codigo.'</b>
			</p>
			
		</div>
		
		<div id="corpo_right">
		
			<h3>Candidatos com experiência </h3>		
			
			<p style="padding-top:12.5px;">'.$vaga[0]->area_experiencia.'</p>
			
			<h3 >Cidade </h3>
			
			<p style="padding-top:12.5px;">'.$vaga[0]->cidade.'</p>
			
			<h3>Benefícios  </h3>		

			<p style="padding-top:12.5px;">'.substr($vaga[0]->beneficio,0,strlen($vaga[0]->beneficio) - 1).'</p>
			
			<h3>Horário  </h3>		

			<p style="padding-top:12px;">'.$vaga[0]->horario.'</p>
			
		</div>

		<p style="position:absolute; right:10%; top:2%; right:5%; color:#999999">
			AST Facilities.
		</p>

		</body>
		</html>
		';
		//==============================================================
		//==============================================================
		
		include("geraPdf/mpdf.php");
		
		$mpdf=new mPDF('','','','',20,15,25,25,10,10); 
		$mpdf->WriteHTML($html);
		$mpdf->Output(); 
		
		exit;
}else{
	session_unset();
	header('location:index.php');
}

?>
</body>
</html>
