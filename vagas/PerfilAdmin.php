<?php
session_start();

date_default_timezone_set('America/Sao_Paulo');	
include 'Banco/Conexao.class.php';
include 'Model/User.class.php';

include 'Persistencia/VagaDAO.class.php';
include 'Persistencia/UserDAO.class.php';
include 'Persistencia/RelaVagaDAO.class.php';
include 'Model/Recrutador.class.php';
include 'Persistencia/RecrutadorDAO.class.php';

include 'Model/InfoCandidato.class.php';
include 'Persistencia/InfoCandidatoDAO.class.php';

if(isset($_SESSION['ct_admin'])){
	
	$user = unserialize($_SESSION['ct_admin']);
		
}else{
	session_unset();
	header('location:index.php');
}
?>
<!DOCTYPE HTML>
<html><!-- InstanceBegin template="/Templates/PerfilAdminTemplate.dwt" codeOutsideHTMLIsLocked="false" --> 
<head>

<title>Bem-vindo</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600" rel="stylesheet" type="text/css" />

<link rel="stylesheet" type="text/css" href="css/table/table.css">

<link href="css/table/style.css" rel="stylesheet" type="text/css" />
<link href="css/table/form/screen.css" rel="stylesheet" type="text/css" />

<link rel="stylesheet" type="text/css" href="css/table/tabulacao.css">   

<!--[if lte IE 8]><script src="js/html5shiv.js"></script><![endif]-->
<script src="js/request/AjaxRequest.js"></script> 

<script src="js/jquery.min.js"></script>
<script src="js/skel.min.js"></script> 
<script src="js/skel-panels.min.js"></script>
<script src="js/init.js"></script>

<script>
function mascara(o,f){
	v_obj=o 
	v_fun=f
	setTimeout("execmascara()",1)
}

function execmascara(){
	v_obj.value=v_fun(v_obj.value)
}

function leech(v){
	v=v.replace(/o/gi,"0")
	v=v.replace(/i/gi,"1")
	v=v.replace(/z/gi,"2")
	v=v.replace(/e/gi,"3")
	v=v.replace(/a/gi,"4")
	v=v.replace(/s/gi,"5")
	v=v.replace(/t/gi,"7")
	return v
}

///////////////////////////////////////////////////

function cpf(v){ 
	v=v.replace(/\D/g,"")                    //Remove tudo o que não é dígito 
	v=v.replace(/(\d{3})(\d)/,"$1.$2")       //Coloca um ponto entre o terceiro e o quarto dígitos 
	v=v.replace(/(\d{3})(\d)/,"$1.$2")       //Coloca um ponto entre o terceiro e o quarto dígitos 
											 //de novo (para o segundo bloco de números) 
	v=v.replace(/(\d{3})(\d{1,2})$/,"$1-$2") //Coloca um hífen entre o terceiro e o quarto dígitos 
	return v 
} 

function cnpj(v){
    v=v.replace(/\D/g,"")                           //Remove tudo o que não é dígito
    v=v.replace(/^(\d{2})(\d)/,"$1.$2")             //Coloca ponto entre o segundo e o terceiro dígitos
    v=v.replace(/^(\d{2})\.(\d{3})(\d)/,"$1.$2.$3") //Coloca ponto entre o quinto e o sexto dígitos
    v=v.replace(/\.(\d{3})(\d)/,".$1/$2")           //Coloca uma barra entre o oitavo e o nono dígitos
    v=v.replace(/(\d{4})(\d)/,"$1-$2")              //Coloca um hífen depois do bloco de quatro dígitos
    return v
}

function data(v){
	v=v.replace(/\D/g,"")                    //Remove tudo o que não é dígito
	v=v.replace(/(\d{2})(\d)/,"$1/$2")       //Coloca um ponto entre o terceiro e o quarto dígitos
	v=v.replace(/(\d{2})(\d)/,"$1/$2")       //Coloca um ponto entre o terceiro e o quarto dígitos
											 //de novo (para o segundo bloco de números)
	return v
}
</script>

<!-- Popup utilizada no primeiro login depois do cadastro -->
<style>
#popup_pf { 	 	 
	display:none; /* Hide the DIV */
	position: absolute;
 	left: 47%;
	top:4%;
	width:350px; 
	background:#FFFFFF;  
	z-index:100; /* Layering ( on-top of others), if you have lots of layers: I just maximized, you can change it yourself */
	/* additional features, can be omitted */
	border:2px solid #CCCCCC;  	
	padding:15px;  
	font-size:15px;  
	-moz-box-shadow: 0 0 5px #CCCCCC;
	-webkit-box-shadow: 0 0 5px #CCCCCC;
	box-shadow: 0 0 2px #999999;
}
</style>

<script type="text/javascript"> 
	
$(document).ready( function() {
	
	// When site loaded, load the Popupbox First
	loadPopupBox();

	$('#popupBoxClose').click( function() {			
		unloadPopupBox();									  			 
	});
	
	function unloadPopupBox() {	// TO Unload the Popupbox
		$('#popup_pf').fadeOut("slow");											 		
	}	
	
	function loadPopupBox() {	// To Load the Popupbox				
		$('#popup_pf').fadeIn("slow");
	}
	/**********************************************************/

});
</script>




<style>
#popup_box2 { 	 	
	display: none;
	position: absolute; 	
	top:2%;
	
	width:350px; 
	background:#FFFFFF;  
	z-index:100; /* Layering ( on-top of others), if you have lots of layers: I just maximized, you can change it yourself */
	/* additional features, can be omitted */
	border:2px solid #CCCCCC;  	
	padding:15px;  
	font-size:15px;  
	-moz-box-shadow: 0 0 5px #CCCCCC;
	-webkit-box-shadow: 0 0 5px #CCCCCC;
	box-shadow: 0 0 2px #999999;
}

@media screen and (min-width:975px){
	#popup_box2 { 	
		left: 45%;	
	}
}

@media screen and (max-width:975px){
	#popup_box2 { 	 	
		left: 30%;		
	}
}

</style>

<script type="text/javascript">
	
	$(document).ready( function() {
		
		// When site loaded, load the Popupbox First
		loadPopupBox();
	
		$('#popupBoxClose').click( function() {			
			unloadPopupBox();									  			 
		});
		
		function unloadPopupBox() {	// TO Unload the Popupbox
			$('#popup_box2').fadeOut("slow");	
			$("#popup_box2").remove(); 									 		
		}	
		
		function loadPopupBox() {	// To Load the Popupbox				
			$('#popup_box2').fadeIn("slow");
		}
		/**********************************************************/

	});
</script>

<script>
// Centraliza a div de poup no centro da tela independente do scroll
function id( el ){
	return document.getElementById( el );
}

var marginTopoInicial;//criando variavel de escopo global

calcMarginTop();

//levando em conta o scroll
id('popup_box2').style.marginTop = marginTopoInicial+window.pageYOffset+'px';

function calcMarginTop()
{
	var pop = id('popup_box2');
	var marginTopo = parseInt( getMarginTop( pop ) );
	var marginBaixo = parseInt( getMarginBottom( pop ) );

	if( pop.offsetTop < -1 )
		pop.style.marginTop = '-'+( parseInt( pop.offsetTop ) - marginTopo )+'px';
	else if( marginTopo!=marginBaixo )
	pop.style.marginTop = '-248px';
}

function getMarginTop( el )
{
	if( window.getComputedStyle )
		return document.defaultView.getComputedStyle(el, null).marginTop;
	else if( el.currentStyle )
		return el.currentStyle['marginTop'];
}
function getMarginBottom( el )
{
	if( window.getComputedStyle )
		return document.defaultView.getComputedStyle(el, null).marginBottom;
	else if( el.currentStyle )
		return el.currentStyle['marginBottom'];
}
//
</script>

<!-- Fim da popup -->

<noscript>
<link rel="stylesheet" href="css/skel-noscript.css" />
<link rel="stylesheet" href="css/style.css" />
<link rel="stylesheet" href="css/style-wide.css" />
</noscript>
<!--[if lte IE 9]><link rel="stylesheet" href="css/ie9.css" /><![endif]-->
<!--[if lte IE 8]><link rel="stylesheet" href="css/ie8.css" /><![endif]-->

</head>
<body>

<div id='box_view'> 
<!-- InstanceBeginEditable name="EditRegion4" -->
<?php
if(isset($_SESSION['aviso'])){
	echo "<div id='popup_box2' style='text-align:center;'>
			 <a id='popupBoxClose'>Fechar</a>".$_SESSION['aviso']."
		  </div>";
	
	unset($_SESSION['aviso']);
}
?>
<!-- InstanceEndEditable -->  
</div>
<div id="pop"> <img src="images/ajax-loader.gif" > </div>

<!-- Header -->
<div id="header" class="skel-panels-fixed">
  <div class="top">
    <div id="logo"> 
      <!--<span class="image avatar48"><img src="images/avatar.jpg" alt="" /></span>-->
      <h1 id="title"><!-- InstanceBeginEditable name="EditRegion1" -->
	  <?php
	  $posicao = strpos($user[0]->email, '@');								
      echo substr($user[0]->email, 0, $posicao);
	  ?>
	  <!-- InstanceEndEditable --></h1>
      <span class="byline">
   		<a onClick='getId("Controller/Cadastro.controller.php?op=1","alerta")' style="font-weight:bold;" href="#">Alterar senha</a>
      </span> 
   </div>
    <!-- Nav -->
    <nav id="nav">
      <ul>
        <li><a href="#top" id="top-link" class="skel-panels-ignoreHref"><span class="fa fa-home">Meu perfil</span></a></li>        
        
        <li><a href="#info" id="info-link" class="skel-panels-ignoreHref"><span class="fa fa-info">Informações do sistema</span></a></li>  
        
        <li><a href="#candidato" id="candidato-link" class="skel-panels-ignoreHref"><span class="fa fa-home">Gestor de candidatos</span></a></li>        
        
        <li><a href="#empresa" id="empresa-link" class="skel-panels-ignoreHref"><span class="fa fa-home">Gestor de empresa</span></a></li>        
         <li><a href="#auto_imagem" id="auto_imagem-link" class="skel-panels-ignoreHref"><span class="fa fa-upload">Auto imagem</span></a></li>        
        
        <li style="border-bottom:1px #999999 groove; margin-top:7px;"> </li>
        <li><a href="Controller/Logoff.controller.php"><span class="fa fa-lock">Sair do sistema</span></a></li>
      </ul>
    </nav>
  </div>
  <!--
  <div class="bottom">    
    <ul class="icons">
      <li><a href="#" class="fa fa-facebook solo"><span>Facebook</span></a></li>
      <li><a href="#" class="fa fa-envelope solo"><span>Email</span></a></li>
    </ul>
  </div>
  -->
</div>
<!-- Main -->
<div id="main">
	
  <!-- Intro -->
  <section id="top" class="one">
    <div class="container"> <br />
      <header>
        <h2 class="alt">Você acessou como <strong>Administrador</strong> </h2>
      </header>
      <p>Com este acesso você pode gerenciar os candidatos e empresas do sistema.</p>
    </div>
  </section>
  
  <!-- Contact -->
 	
  <section id="info" class="two">
    <div class="container"> 
    
      <header>
        <h2>Informações do sistema</h2>           
      </header> 
      
      <form onSubmit="return false" id="form_info_sistema">
                                 
	   <div id="info_sistema" style="margin-bottom:50px;"> 
		   <!-- InstanceBeginEditable name="EditRegion2" -->
       <?php
	   	$userDAO = new UserDAO();
		$vagaDAO = new VagaDAO();
		$relaVagaDAO = new RelaVagaDAO();
		
		$data = array(NULL,NULL);
		
		$_SESSION['filtro_master_data'] = serialize($data);
				
	   ?>
              
        <div class="row half">
                  
          <div class="12u">             
            <h3>Candidatos cadastrados</h3>                                         
          </div>        
        
          <div class="6u" id='input_full'> <b style="color:#000000;">Cadastro completo - liberados</b> <br />
           <a onClick='getId("Controller/Admin.controller.php?op=8&cmd=getUserCompAtivo&tp=user","box_view")'>
           <?php
            echo $userDAO->getContUserCompAtivo($data);
           ?>
           </a>
          </div>
            
          <div class="6u" id='input_full'> <b style="color:#000000;">Cadastro incompleto - liberados</b> <br />
           <a onClick='getId("Controller/Admin.controller.php?op=8&cmd=getUserIncompAtivo&tp=user","box_view")'>
           <?php
            echo $userDAO->getContUserIncompAtivo($data);
           ?>
           </a>   
          </div>    
                
          <div class="6u" id='input_full'> <b style="color:#000000;">Cadastro completo - Desativados</b> <br />
           <a onClick='getId("Controller/Admin.controller.php?op=8&cmd=getUserCompDesativo&tp=user","box_view")'>  
           <?php
            echo $userDAO->getContUserCompDesativo($data);
           ?>
           </a>
          </div>
        
          <div class="6u" id='input_full'> <b style="color:#000000;">Cadastro incompleto - Desativados</b> <br />
           <a onClick='getId("Controller/Admin.controller.php?op=8&cmd=getUserIncompDesativo&tp=user","box_view")'>    
           <?php
            echo $userDAO->getContUserIncompDesativo($data);
           ?>
           </a>
          </div>
          
        </div>
        
        <div class="row half" style="margin-top:-15px;">  
          <div class="12u">             
            <h3>Vagas cadastradas</h3>                                         
          </div>
        </div>
        
        <div class="row half">   
          
          <div class="6u" id='input_full'> <b style="color:#000000;">Total cadastrado</b> <br />
           <a onClick='getId("Controller/Admin.controller.php?op=8&cmd=getTotal&tp=vaga","box_view")'>
           <?php
            echo $vagaDAO->getContTotal($data,NULL);
           ?>
           </a>
          </div>          
          
          <div class="6u" id='input_full'> <b style="color:#000000;">Em captação</b> <br />
           <a onClick='getId("Controller/Admin.controller.php?op=8&cmd=getCaptacao&tp=vaga","box_view")'>  
           <?php
           echo $vagaDAO->getContCaptacao($data,NULL);
           ?>
           </a>
          </div>          
        
        </div>
        
        <div class="row half">   
          
          <div class="6u" id='input_full'> <b style="color:#000000;">Concluida</b> <br />
           <a onClick='getId("Controller/Admin.controller.php?op=8&cmd=getConcluido&tp=vaga","box_view")'>    
           <?php
           echo $vagaDAO->getContConcluido($data,NULL);
           ?>
           </a>
          </div>          
        
          <div class="6u" id='input_full'> <b style="color:#000000;">Desativada</b> <br />
           <a onClick='getId("Controller/Admin.controller.php?op=8&cmd=getCancelado&tp=vaga","box_view")'>      
           <?php
           echo $vagaDAO->getContCancelado($data,NULL);
           ?>
           </a>
          </div>
        </div>
		
        <div class="row half">   
          
          <div class="12u" id='input_full2'> <b style="color:#000000;">Pendente para aprovação</b> <br />
           <a onClick='getId("Controller/Admin.controller.php?op=8&cmd=getPendente&tp=vaga","box_view")'>    
           <?php
           echo $vagaDAO->getContPendente($data,NULL);
           ?>
           </a>
          </div>          

		</div>
                
        <div class="row half">          
          <div class="12u">             
            <h3>Candidatos relacionados a vagas</h3>                                         
          </div>        
        </div>
        
        <div class="row half">   
         
          <div class="6u" id='input_full'> <b style="color:#000000;">Total cadastrado</b> <br />
           <a onClick='getId("Controller/Admin.controller.php?op=8&cmd=getTotal&tp=relaVaga","box_view")'>        
           <?php
             echo $relaVagaDAO->getContTotal($data,NULL);
           ?>
           </a>
          </div>          

          <div class="6u" id='input_full'> <b style="color:#000000;">Em candidatura</b> <br />
           <a onClick='getId("Controller/Admin.controller.php?op=8&cmd=getCandidato&tp=relaVaga","box_view")'>          
           <?php
             echo $relaVagaDAO->getContCandidato($data,NULL);  
           ?>
           </a>
          </div>
        </div>
        
        <div class="row half">   
         
          <div class="6u" id='input_full'> <b style="color:#000000;">Recrutados</b> <br />
           <a onClick='getId("Controller/Admin.controller.php?op=8&cmd=getRecrutado&tp=relaVaga","box_view")'>            
           <?php
           echo $relaVagaDAO->getContRecrutado($data,NULL);          
           ?>
           </a>
          </div>          
          <div class="6u" id='input_full'> <b style="color:#000000;">Selecionados</b> <br />
           <a onClick='getId("Controller/Admin.controller.php?op=8&cmd=getSelecionado&tp=relaVaga","box_view")'>            
           <?php
           echo $relaVagaDAO->getContSelecionado($data,NULL);          
           ?>
           </a>
          </div>
        </div>
        
		<!-- InstanceEndEditable -->   
       </div> 
       
       <div id='margin_bottom'> </div>  
         
       <div class="row half"> 
       
        
          <div class="12u">             
             <h4>Filtrar informações por período</h4>                                         
          </div>        
          
                
          <div class="12u" style="margin-bottom:30px; margin-top:10px;">
          <!-- InstanceBeginEditable name="EditRegion3" -->
              <select name="cbRecrutador">
                <option value=''> Selecione um recrutador </option>
                <?php
			  	
				$recrutadorDAO = new RecrutadorDAO();
				
				$list_recrutador = $recrutadorDAO->get();
				
				foreach($list_recrutador as $recrutador){
					echo "<option value='".$recrutador->id."'> ".$recrutador->nome." </option>";
				}
				
			   ?>
              </select>
              <!-- InstanceEndEditable -->
         </div>
        	                    
          <div class="6u">
            <input type="text" class="text" name="txtDataInicio" placeholder="Data cadastro - início" onkeypress='mascara(this,data)' maxlength='10'/>
          </div>
          <div class="6u">
            <input type="text" class="text" name="txtDataFim" placeholder="Data cadastro - término" onkeypress='mascara(this,data)' maxlength='10'/>
          </div>  
		
          <div class="12u">
          	<p style="margin-bottom:10px; margin-top:10px;"> Filtro para vagas concluidas </p>
          </div>
          
          <div class="6u">
            <input type="text" class="text" name="txtDataInicioConc" placeholder="Data conclusão - início" onkeypress='mascara(this,data)' maxlength='10'/>
          </div>
          <div class="6u">
            <input type="text" class="text" name="txtDataFimConc" placeholder="Data conclusão - término" onkeypress='mascara(this,data)' maxlength='10'/>
          </div> 
          
        </div>	
      	                       
      	<div class="row">
          <div class="12u" id='btn_load_1'>
        	Processando...
	      </div>
            
    	  <div class="12u" id="btn_1">  
           <a class="button submit" onClick='post("Controller/Admin.controller.php?op=3","btn_1","btn_load_1","info_sistema","form_info_sistema")'>Filtrar informações</a> 
          </div>
                    
      	</div>
        
      </form>    
                   
    </div>
  </section>     
  
   <section id="candidato" class="three">
    <div class="container" id='gestor_candidato'>
      <header>
        <h2>Gestor de candidatos</h2>
        <p>Procure o candidato que deseja acessar</p>
      </header>
      <form onSubmit="return false" id="get_candidato">
       
       <div class="row half">   
		 <div class="12u" id='input_full2'> <b>Auto imagem</b> <br />
           <!-- InstanceBeginEditable name="EditRegion5" -->
		   <?php
		   $infoCandidatoDAO = new InfoCandidatoDAO();
		   
		   $list_imagem = $infoCandidatoDAO->getListCod();
		   
		   $cont = 1;
		   
		   foreach($list_imagem as $list){		  
		   	   echo "<input class='check' name='auto_".$list->auto_imagem."' type='checkbox' value='auto_".$list->auto_imagem."' />".$list->auto_imagem." &nbsp;";
			   
			   if($cont == 7){
			   		echo "<br />";
					
					$cont = 0;
			   }
			   
			   $cont++;
		   }  

		   ?><!-- InstanceEndEditable -->         
         </div>           
       </div>
       
       <div class="row half">   
           <div class="6u">
            <input type="text" class="text" name="txtCpf" placeholder="CPF" onkeypress='mascara(this,cpf)' maxlength='14'/>
          </div> 
               
          <div class="6u">
			<input type="text" class="text" name="txtEscola" placeholder="Escola" />
          </div>
       </div>
        
       <div class="row half">

          <div class="6u">
       		<select name="cbNivel" onChange='cadastro("Controller/Curso.controller.php?op=3","curso_input","get_candidato")'>
                <option value=''> Nível do curso </option>                
                <option value='Ensino médio'>Ensino médio</option>
                <option value='Profissionalizante'>Profissionalizante</option>
                <option value='Técnicos'>Técnicos</option>
                <option value='Superior'>Superior</option>
                <option value='Pós'>Pós</option>                
       		</select>
       	  </div>
          
          <div class="6u" id='curso_input'>
            <input type="text" class="text" placeholder="Selecione o nível do curso" disabled/>
          </div>

       </div>

        <div class="row half">        
          <div class="6u">
            <input type="text" class="text" name="txtNome" placeholder="Nome" />
          </div>

		  <div class="6u">
            <input type="text" class="text" name="txtEmail" placeholder="E-mail" />
          </div>       
        </div>		
        
        <div class="row half">        
          <div class="6u">
            <input onkeypress='mascara(this,telefone)' maxlength='14' type="text" class="text" name="txtTel" placeholder="Telefone" />
          </div>

		  <div class="6u">
            <input onkeypress='mascara(this,telefone)' maxlength='14' type="text" class="text" name="txtCel" placeholder="Celular" />
          </div>       
        </div>		

        <div class="row">
          <div class="12u" id='btn_load_2'>
        	Processando...
	      </div>
            
    	  <div class="12u" id="btn_2">  
                <a class="button submit" onclick='post("Controller/Admin.controller.php?op=1","btn_2","btn_load_2","gestor_candidato","get_candidato")'>Encontrar candidato</a>
            </div>           
        </div>

      </form> 
        
    </div>
  </section> 
  
  <section id="empresa" class="four">
    <div class="container" id='gestor_empresa'>
      <header>
        <h2>Gestor de empresas</h2>
        <p>Procure a empresa que deseja acessar</p>
      </header>
      <form onSubmit="return false" id="get_empresa">
        
        <div class="row half">        
          <div class="12u">
            <input type="text" class="text" name="txtCnpj" placeholder="CNPJ" onkeypress='mascara(this,cnpj)' maxlength='18'/>
          </div> 
        </div>	
        
        <div class="row half">        
          <div class="6u">
            <input type="text" class="text" name="txtNome" placeholder="Empresa" />
          </div>

		  <div class="6u">
            <input type="text" class="text" name="txtEmail" placeholder="E-mail" />
          </div>       
        </div>		
               
        <div class="row">
          <div class="12u" id='btn_load_3'>
        	Processando...
	      </div>
            
    	  <div class="12u" id="btn_3"> 
                <a class="button submit" onclick='post("Controller/Admin.controller.php?op=2","btn_3","btn_load_3","gestor_empresa","get_empresa")'>Encontrar empresa</a>
          </div>           
        </div>

      </form> 
        
    </div> 
  </section> 

  <section id="auto_imagem" class="four">
    <div class="container">
      <header>
        <h2>Auto imagem</h2>
        <p>Carregue o excel</p>
      </header>
      <form enctype="multipart/form-data" method="post"  action="Controller/Admin.controller.php?op=4">
                                                
        <div class="row half">        
          <div class="12u">
            <input class="text" name='ffFoto' type='file' />
          </div> 
        </div>	
               
        <div class="row">
            <div class="12u">                
                <input name="upload" type="submit" value="Atualizar dados" class="button submit" />
            </div>           
        </div>

      </form> 
        
    </div>
  </section> 
  
</div>
<!-- Footer -->
<div id="footer">
  <!-- Copyright -->
  <div class="copyright">
    <ul class="menu">
      <li>AST Facilities.</li>
    </ul>
  </div>
</div>

<!-- Page-Level Plugin Scripts - Tables -->
<script src="js/dataTables/jquery.dataTables.js"></script>
<script src="js/dataTables/dataTables.bootstrap.js"></script>
     
</body>
<!-- InstanceEnd --></html>
