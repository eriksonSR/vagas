

<?php
session_start();

date_default_timezone_set('America/Sao_Paulo');	
include 'Banco/Conexao.class.php';
include 'Model/Vaga.class.php';
include 'Persistencia/VagaDAO.class.php';
include 'Model/InfoCandidato.class.php';
include 'Model/RelaVaga.class.php';
include 'Persistencia/RelaVagaDAO.class.php';
include 'Model/User.class.php';
include 'Model/Recrutador.class.php';
include 'Persistencia/RecrutadorDAO.class.php';

include 'RM/Banco/Conexao2.class.php';
include 'RM/Model/PFuncao.class.php';
include 'RM/Persistencia/PFuncaoDAO.class.php';

if(isset($_SESSION['foco_vaga']) && (isset($_SESSION['ct_user']) || isset($_SESSION['ct_empresa']) || isset($_SESSION['ct_admin']))){
	$vaga = unserialize($_SESSION['foco_vaga']);
}else{
	header('location:index.php');
}
?>
<!DOCTYPE HTML>
<html><!-- InstanceBegin template="/Templates/VagaTemplate.dwt" codeOutsideHTMLIsLocked="false" -->
<head>

<title>Bem-vindo</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="css/table/table.css">
<link href="css/table/style.css" rel="stylesheet" type="text/css" />
<link href="css/table/form/screen.css" rel="stylesheet" type="text/css" />
<!--[if lte IE 8]><script src="js/html5shiv.js"></script><![endif]-->
<script src="js/request/AjaxRequest.js"></script>
<script src="js/jquery.min.js"></script>
<script src="js/skel.min.js"></script>
<script src="js/skel-panels.min.js"></script>
<script src="js/init.js"></script>
<script>
function mascara(o,f){
	v_obj=o 
	v_fun=f
	setTimeout("execmascara()",1)
}

function execmascara(){
	v_obj.value=v_fun(v_obj.value)
}

function leech(v){
	v=v.replace(/o/gi,"0")
	v=v.replace(/i/gi,"1")
	v=v.replace(/z/gi,"2")
	v=v.replace(/e/gi,"3")
	v=v.replace(/a/gi,"4")
	v=v.replace(/s/gi,"5")
	v=v.replace(/t/gi,"7")
	return v
}

///////////////////////////////////////////////////

function numeros(v){
   return v.replace(/\D/g,"")
}

function telefone(v){
	v=v.replace(/\D/g,"")                 //Remove tudo o que não é dígito
	v=v.replace(/^(\d\d)(\d)/g,"($1) $2") //Coloca parênteses em volta dos dois primeiros dígitos
	v=v.replace(/(\d{4})(\d)/,"$1-$2")    //Coloca hífen entre o quarto e o quinto dígitos
	return v
}       

function data(v){
	v=v.replace(/\D/g,"")                    //Remove tudo o que não é dígito
	v=v.replace(/(\d{2})(\d)/,"$1/$2")       //Coloca um ponto entre o terceiro e o quarto dígitos
	v=v.replace(/(\d{2})(\d)/,"$1/$2")       //Coloca um ponto entre o terceiro e o quarto dígitos
											 //de novo (para o segundo bloco de números)
	return v
}

function cpf(v){ 
	v=v.replace(/\D/g,"")                    //Remove tudo o que não é dígito 
	v=v.replace(/(\d{3})(\d)/,"$1.$2")       //Coloca um ponto entre o terceiro e o quarto dígitos 
	v=v.replace(/(\d{3})(\d)/,"$1.$2")       //Coloca um ponto entre o terceiro e o quarto dígitos 
											 //de novo (para o segundo bloco de números) 
	v=v.replace(/(\d{3})(\d{1,2})$/,"$1-$2") //Coloca um hífen entre o terceiro e o quarto dígitos 
	return v 
} 

function moeda(z){  
	v = z.value;
	v=v.replace(/\D/g,"")  //permite digitar apenas números
	v=v.replace(/[0-9]{12}/,"inválido")   //limita pra máximo 999.999.999,99
	v=v.replace(/(\d{1})(\d{8})$/,"$1.$2")  //coloca ponto antes dos últimos 8 digitos
	v=v.replace(/(\d{1})(\d{5})$/,"$1.$2")  //coloca ponto antes dos últimos 5 digitos
	v=v.replace(/(\d{1})(\d{1,2})$/,"$1,$2")	//coloca virgula antes dos últimos 2 digitos
		z.value = v;
}


</script>
<!-- Popup utilizada no primeiro login depois do cadastro -->
<style>
#popup_pf {
	display:none; /* Hide the DIV */
	position: absolute;
	left: 47%;
	top:4%;
	width:350px;
	background:#FFFFFF;
	z-index:100; /* Layering ( on-top of others), if you have lots of layers: I just maximized, you can change it yourself */
	/* additional features, can be omitted */
	border:2px solid #CCCCCC;
	padding:15px;
	font-size:15px;
	-moz-box-shadow: 0 0 5px #CCCCCC;
	-webkit-box-shadow: 0 0 5px #CCCCCC;
	box-shadow: 0 0 2px #999999;
}
</style>
<script type="text/javascript"> 
	
$(document).ready( function() {
	
	// When site loaded, load the Popupbox First
	loadPopupBox();

	$('#popupBoxClose').click( function() {			
		unloadPopupBox();									  			 
	});
	
	function unloadPopupBox() {	// TO Unload the Popupbox
		$('#popup_pf').fadeOut("slow");											 		
	}	
	
	function loadPopupBox() {	// To Load the Popupbox				
		$('#popup_pf').fadeIn("slow");
	}
	/**********************************************************/

});
</script>
<!-- Fim da popup -->
<noscript>
<link rel="stylesheet" href="css/skel-noscript.css" />
<link rel="stylesheet" href="css/style.css" />
<link rel="stylesheet" href="css/style-wide.css" />
</noscript>
<!--[if lte IE 9]><link rel="stylesheet" href="css/ie9.css" /><![endif]-->
<!--[if lte IE 8]><link rel="stylesheet" href="css/ie8.css" /><![endif]-->
</head><body>
<div id="pop"> <img src="images/ajax-loader.gif" > </div>
<div id='alerta'></div>
<!-- Header -->
<div id="header" class="skel-panels-fixed">
  <div class="top">
    <div id="logo">
      <h1 id="title"><!-- InstanceBeginEditable name="EditRegion1" -->
	  <?php
	  
	 


	  ?><!-- InstanceEndEditable --></h1>
      <span class="byline"><!-- InstanceBeginEditable name="EditRegion8" -->
	  <?php
	  $vagaDAO = new VagaDAO();
	  
	  echo "Codigo da vaga: <br />".$vaga[0]->codigo;
	  ?>
	  <!-- InstanceEndEditable --></span> </div>
    <!-- Nav -->
    <nav id="nav">
      <ul>
        <li><a href="#info" id="info-link" class="skel-panels-ignoreHref"><span class="fa fa-info-circle">Descrição da vaga</span></a></li>
        
        <!-- InstanceBeginEditable name="EditRegion9" -->
        <?php
		if(isset($_SESSION['ct_empresa'])){
		?>
        	<li><a href="#candidato" id="candidato-link" class="skel-panels-ignoreHref"><span class="fa fa-user">Candidatos</span></a></li>
        <?php
		}
		?>
        <!-- InstanceEndEditable -->
        <li style="border-bottom:1px #999999 groove; margin-top:7px;"> </li>
        <li><a href="getVagaPdf.php" target="_blank"><span class="fa fa-file-o">Gerar vaga em PDF</span></a></li>
        <!-- InstanceBeginEditable name="EditRegion10" -->
        <?php
        	if(isset($_SESSION['ct_empresa'])){
				echo '<li><a href="PerfilEmpresa.php"><span class="fa fa-home">Voltar ao perfil inicial</span></a></li>';
			}else if(isset($_SESSION['ct_user'])){
				echo '<li><a href="PerfilCandidato.php"><span class="fa fa-home">Voltar ao perfil inicial</span></a></li>';
			}else{
				echo '<li><a href="PerfilAdmin.php"><span class="fa fa-home">Voltar ao perfil inicial</span></a></li>';
			}			
		?>
        <!-- InstanceEndEditable -->
        <li><a href="Controller/Logoff.controller.php"><span class="fa fa-lock">Sair do sistema</span></a></li>
      </ul>
    </nav>
  </div>
  <!--
  <div class="bottom">
    <ul class="icons">
      <li><a href="#" class="fa fa-facebook solo"><span>Facebook</span></a></li>
      <li><a href="#" class="fa fa-envelope solo"><span>Email</span></a></li>
    </ul>
  </div>
  -->
</div>
<!-- Main -->
<div id="main">
  <!-- Intro -->
  <section id="top" class="one">
    <div class="container">
      <!--  <a  class="image featured"><img src="../banner.jpg" alt="" /></a> -->
      <!-- InstanceBeginEditable name="EditRegion4" -->
        <header id='nome_vaga'>
          <h2 class="alt"><strong><?php echo $vaga[0]->nome; ?></strong></h2>
        </header>       
        <?php 
		echo $vaga[0]->empresa;
		?>  
        <!-- InstanceEndEditable --> </div>
  </section>
  <section id="info" class="two">
    <div class="container">
      <header>
        <h2>Descrição da vaga</h2>
        <div id='nivel_vaga'> <!-- InstanceBeginEditable name="EditRegion6" -->       
        <?php
		if($vaga[0]->nivel == 1){
			echo " <b style='color:green; font-size:28px;'>Vaga em captação de candidatos </b>";
		}else if($vaga[0]->nivel == 2){
			echo " <b style='color:blue; font-size:28px;'>Vaga concluida </b>";
		}else if($vaga[0]->nivel == 3){
			echo " <b style='color:red; font-size:28px;'>Vaga cancelada </b>";
		}else if($vaga[0]->nivel == 4){
			echo " <b style='color:#CCCC33; font-size:28px;'>Vaga para aprovação </b>";
		}
		?>        
		<!-- InstanceEndEditable --> </div>
      </header>
      <div id="dados_vaga"> <!-- InstanceBeginEditable name="EditRegion5" -->	   
      <?php
	  if($vaga[0]->nivel != 4){
	  ?> 
      	  <div class="row half" style="margin-top:-15px;">
              <div class="12u" id='input_full_2'> <b style="color:#000000;">Gerente</b> <br />
			<?php
               echo $vaga[0]->gerente;
               ?>
              </div>
          </div>
         
          <div class="row half" style="margin-top:-15px;">                  
    
              <div class="6u" id='input_full'> <b style="color:#000000;">Beneficios</b> <br />
               <?php
               echo substr($vaga[0]->beneficio,0,strlen($vaga[0]->beneficio) - 1);
               ?>
              </div>  
              
              <div class="6u" id='input_full'> <b style="color:#000000;">Remuneração</b> <br />
               <?php
               echo "R$ ".number_format($vaga[0]->salario, 2, ',', '.');
               ?>
              </div>  
          </div>
        
          <div class="row half" style="margin-top:-15px;">
              <div class="12u" id='input_full_2'> <b style="color:#000000;">Candidatos com experiência em</b> <br />
               <?php
                echo $vaga[0]->area_experiencia;
               ?>
              </div>  
          </div> 
          
          <div class="row half" style="margin-top:-15px;">
              <div class="6u" id='input_full'> <b style="color:#000000;">Área da vaga</b> <br />
               <?php
               echo $vaga[0]->area;
               ?>
              </div>          
              
              <div class="6u" id='input_full'> <b style="color:#000000;">Atividade da área</b> <br />
               <?php
               echo $vaga[0]->atividade;
               ?>
              </div>
          </div>
          
          <div class="row half">
              
              <div class="6u" id='input_full'> <b style="color:#000000;">Nível da vaga</b> <br />
               <?php
               echo substr($vaga[0]->escolaridade,0,strlen($vaga[0]->escolaridade) - 1);
               ?>
              </div>   
              
              <div class="6u" id='input_full'> <b style="color:#000000;">Idiomas desejados</b> <br />
               <?php
               echo substr($vaga[0]->idioma,0,strlen($vaga[0]->idioma) - 1);
               ?>
              </div>      
          </div>
          
          <div class="row half">
              <div class="6u" id='input_full'> <b style="color:#000000;">Cidade</b> <br />
               <?php
               echo $vaga[0]->cidade;
               ?>
              </div>
              
              <div class="6u" id='input_full'> <b style="color:#000000;">Horário</b> <br />
               <?php
               echo $vaga[0]->horario;
               ?>
              </div>          
          </div>
          
          <div class="row half">
              <div class="6u" id='input_full'> <b style="color:#000000;">Data de cadastro</b> <br />
               <?php
               echo date('d/m/Y',strtotime($vaga[0]->data));
               ?>
              </div>
              
              <div class="6u" id='input_full'> <b style="color:#000000;">Hora de cadastro</b> <br />
               <?php
               echo $vaga[0]->hora;
               ?>
              </div>          
          </div>
          
           <div class="row half">
              <div class="12u" id='input_full_2'> <b style="color:#000000;">Informações adicionais</b> <br />
               <?php
               echo $vaga[0]->informacao_adicional;
               ?>
              </div>                    
          </div>
      <?php
	  }else{
		 include 'Include/EditVaga.view.php';
	  }
	  ?>
	  <!-- InstanceEndEditable -->
        <div class="row" id='botao_vaga_all'> <!-- InstanceBeginEditable name="EditRegion12" --> 
		<?php				
         if($vaga[0]->nivel != 2 && $vaga[0]->nivel != 4){									
            
            if(isset($_SESSION['ct_empresa'])){
                echo "<div class='12u' id='botao_vaga'>";
                				
				?>
				<a class="button submit" onClick='getId("Controller/Vaga.controller.php?op=12","alerta")'>Concluir vaga</a> &nbsp		              		
				<?php
                       
                if($vaga[0]->nivel == 1){
                    ?>
                    <a class="button submit" onClick='getId2("Controller/Vaga.controller.php?op=11&nv=3","botao_vaga","nivel_vaga")'>Cancelar vaga</a> 
                    <?php
                }else if($vaga[0]->nivel == 3){
                    ?>
                    <a class="button submit" onClick='getId2("Controller/Vaga.controller.php?op=11&nv=1","botao_vaga","nivel_vaga")'>Ativar vaga</a> 
                    <?php
                }
                                
                echo "</div>";						
            }
            
            echo "<div class='12u' id='botao_candidato'>";
            
            if(isset($_SESSION['ct_empresa'])){
            ?>
              <a onClick='getId("Controller/Vaga.controller.php?op=3","dados_vaga")'> Desejo editar esta vaga</a>	
            <?php
            }else if(isset($_SESSION['ct_user'])){
                $relaVagaDAO = new RelaVagaDAO();
                $relaVaga = new RelaVaga();
                
                $vaga = unserialize($_SESSION['foco_vaga']);
                $user = unserialize($_SESSION['ct_user']);

                $relaVaga->id_vaga = $vaga[0]->id;
                $relaVaga->id_user = $user[0]->id;
                                    
                $cont = $relaVagaDAO->getContRela($relaVaga);
                
                if($cont == 0){					
                ?>
                    <a class="button submit" onclick='getId2("Controller/Vaga.controller.php?op=5&tipo=1","botao_candidato","retorno_candidato")'>Quero me candidatar</a>
                <?php
                }else{
                ?>
                    <a class="button submit" onclick='getId2("Controller/Vaga.controller.php?op=5&tipo=2","botao_candidato","retorno_candidato")'>Retirar  minha candidatura</a>
                <?php
                }
            }
            
            echo "</div>";
			
        }
        ?>
                
			  <!-- InstanceEndEditable --> </div>
      </div>
    </div>
  </section>
  <!-- InstanceBeginEditable name="EditRegion11" -->
<?php
if(isset($_SESSION['ct_empresa'])){
?>
  <section id="candidato" class="three">
    <div class="container" id='info_candidato'>
      <header>
        <h2>Candidatos</h2>
        <p>Candidatos relacionados a vaga</p>
      </header>
      <div style="margin-bottom:20px; margin-top:-20px;">
        <form onSubmit="return false" id="filtro_candidato">
          <select name="cbNivel" onChange='cadastro("Controller/Vaga.controller.php?op=4","retorno_candidato","filtro_candidato")'>
            <option value=""> Desejo visualizar somente </option>
            <option value="1">Candidatos</option>
            <option value="2">Selecionados</option>
            <option value="3">Recrutados</option>
          </select>
        </form>
      </div>
      <div id='retorno_candidato'>
        <?php
      	$relaVagaDAO = new RelaVagaDAO();
		
		$list_info_candidato = $relaVagaDAO->getUser($vaga[0]->id);
		
		if(count($list_info_candidato) > 0){
			
			echo "<table>
					<thead>
					  <tr>
						<th>Nome</th>
						<th>Idade</th>
						<th>Cidade</th>
						<th>Nível</th>
						<th>&nbsp;</th>
					  </tr>
					</thead>
					<tbody>";
					foreach($list_info_candidato as $list){
						echo "<tr>
								<th>".$list->nome."</th>";
								
								if(strlen($list->nascimento) > 0){
									$hoje = date('Y-d-m');
								
									$idade =  $hoje - $list->nascimento;
									
									echo "<th>".$idade."</th>";
								}else{
									echo "<th> --- </th>";
								}
								
								if(strlen($list->cidade) > 0){
									echo "<th>".$list->cidade."</th>";
								}else{
									echo "<th> --- </th>";
								}
																										
								if($list->nivel_candidatura == 1){
									echo "<th style='color:red;'> Candidato </th>";
								}else if($list->nivel_candidatura == 2){
									echo "<th style='color:green;'> Selecionado </th>";
								}else if($list->nivel_candidatura == 3){
									echo "<th style='color:blue;'> Recrutado </th>";
								}
								
								?>
	<th> <a onclick='getId("Controller/Vaga.controller.php?op=<?php echo sha1(7) ?>&id=<?php echo base64_encode($list->id_user) ?>","info_candidato")' >&nbsp;&nbsp;<u>Explorar</u>&nbsp;&nbsp;</a> </th>
	<?php
							
						echo "</tr>";
					}					  
			echo "	</tbody>
				  </table>";			
		}else{
			echo "<table>
					<thead>
					  <tr>
						<th>Aviso</th>
					  </tr>
					</thead>
					<tbody>
					  <tr>
						<th>Não há candidatos relacionados a vaga.</th>
					  </tr>
					</tbody>
				  </table>";
		}
      	
		 ?>
      </div>
    </div>
  </section>
<?php
}
?>
      <!-- InstanceEndEditable --> </div>
<!-- Footer -->
<div id="footer">
  <!-- Copyright -->
  <div class="copyright">
    <ul class="menu">
      <li>AST Facilities.</li>
    </ul>
  </div>
</div>
</body>
<!-- InstanceEnd --></html>
