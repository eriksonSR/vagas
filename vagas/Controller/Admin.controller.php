<?php
session_start();

date_default_timezone_set('America/Sao_Paulo');	
include '../Banco/Conexao.class.php';
include '../Model/User.class.php';
include '../Model/InfoCandidato.class.php';
include '../Persistencia/InfoCandidatoDAO.class.php';
include '../Model/InfoEmpresa.class.php';
include '../Persistencia/InfoEmpresaDAO.class.php';
include '../Persistencia/UserDAO.class.php';
include '../Model/Validacao.class.php';

include '../Persistencia/VagaDAO.class.php';
include '../Persistencia/RelaVagaDAO.class.php';
include '../Model/RelaVaga.class.php';
include '../Model/Vaga.class.php';
include '../Model/Curso.class.php';
include '../Persistencia/CursoDAO.class.php';

include '../Model/Upload.class.php';

include '../PHPExcel/Classes/PHPExcel/IOFactory.php';

if($_POST){
	if($_GET['op'] == 1){
		$infoCandidatoDAO = new InfoCandidatoDAO();
		
		$infoCandidato = new InfoCandidato();
		
		if(strlen($_POST['txtCpf']) > 0){
			$infoCandidato->cpf = $_POST['txtCpf'];
		}
		
		if(strlen($_POST['txtNome']) > 0){
			$infoCandidato->nome = $_POST['txtNome'];
		}
		
		if(strlen($_POST['txtEmail']) > 0){
			$infoCandidato->email = $_POST['txtEmail'];
		}
					
		if(strlen($_POST['txtCel']) > 0){
			$infoCandidato->cel = $_POST['txtCel'];
		}
		
		if(strlen($_POST['txtTel']) > 0){
			$infoCandidato->tel = $_POST['txtTel'];
		}
		
		$curso = new Curso();

		if(strlen($_POST['txtEscola']) > 0){
			$curso->escola = $_POST['txtEscola'];
		}
						
		if(strlen($_POST['cbNivel']) > 0){								
			$curso->nivel = $_POST['cbNivel'];
			$curso->curso = $_POST['cbCurso'];				
		}
								   
		$list_imagem = $infoCandidatoDAO->getListCod();
		
		$check_imagem = array();

		foreach($list_imagem as $list){	
		 	if(isset($_POST["auto_".$list->auto_imagem.""])){
				$check_imagem[] = $list->auto_imagem;
			}
		}
		
		$list_candidato = $infoCandidatoDAO->getFiltroAdmin($infoCandidato,$curso,$check_imagem);
		
		include '../Include/getCandidato.view.php';

	}else if($_GET['op'] == 2){
		$infoEmpresaDAO = new InfoEmpresaDAO();
			
		$infoEmpresa = new InfoEmpresa();
		
		if(strlen($_POST['txtCnpj']) > 0){
			$infoEmpresa->cnpj = $_POST['txtCnpj'];						
		}
		
		if(strlen($_POST['txtEmail']) > 0){
			$infoEmpresa->email = $_POST['txtEmail'];
		}
		
		if(strlen($_POST['txtNome']) > 0){
			$infoEmpresa->nome = $_POST['txtNome'];
		}
		
		$list_empresa = $infoEmpresaDAO->getFiltroAdmin($infoEmpresa);
		
		include '../Include/getEmpresa.view.php';
	
	}else if($_GET['op'] == 3){
	
		$cont_erro = 0;
		$data = array(NULL,NULL,NULL,NULL);
	
		// Filtro data de cadastro
		if(strlen($_POST['txtDataInicio']) > 0){
			
			$aux = str_replace("/","-",$_POST['txtDataInicio']);
			$aux = date("d/m/Y",strtotime($aux));				
			$aux = str_replace("-","/",$aux);	
						
			$aux = explode('/',$aux);
			
			$dia = $aux[0];
			$mes = $aux[1];
			$ano = $aux[2];
																	
			if(Validacao::data($mes,$dia,$ano)){
				$data[0] = date($ano."-".$mes."-".$dia);
			}
			
		}
	
		if(strlen($_POST['txtDataFim']) > 0){
		
			$aux = str_replace("/","-",$_POST['txtDataFim']);
			$aux = date("d/m/Y",strtotime($aux));				
			$aux = str_replace("-","/",$aux);	
						
			$aux = explode('/',$aux);
			
			$dia = $aux[0];
			$mes = $aux[1];
			$ano = $aux[2];
														
			if(Validacao::data($mes,$dia,$ano)){			
				$data[1] = date($ano."-".$mes."-".$dia);
				
				if(strtotime($data[0]) > strtotime($data[1])){
					$data[1] = NULL;
				}			
			}	
		}
		// />
		
		// Filtro data de conclus�o
		if(strlen($_POST['txtDataInicioConc']) > 0){
			
			$aux = str_replace("/","-",$_POST['txtDataInicioConc']);
			$aux = date("d/m/Y",strtotime($aux));				
			$aux = str_replace("-","/",$aux);	
						
			$aux = explode('/',$aux);
			
			$dia = $aux[0];
			$mes = $aux[1];
			$ano = $aux[2];
																	
			if(Validacao::data($mes,$dia,$ano)){
				$data[2] = date($ano."-".$mes."-".$dia);
			}
			
		}
	
		if(strlen($_POST['txtDataFimConc']) > 0){
		
			$aux = str_replace("/","-",$_POST['txtDataFimConc']);
			$aux = date("d/m/Y",strtotime($aux));				
			$aux = str_replace("-","/",$aux);	
						
			$aux = explode('/',$aux);
			
			$dia = $aux[0];
			$mes = $aux[1];
			$ano = $aux[2];
														
			if(Validacao::data($mes,$dia,$ano)){			
				$data[3] = date($ano."-".$mes."-".$dia);
				
				if(strtotime($data[2]) > strtotime($data[3])){
					$data[3] = NULL;
				}			
			}	
		}
		// />
		
		if(strlen($_POST['cbRecrutador']) > 0){
			$id_recrutador = $_POST['cbRecrutador'];
		}else{
			$id_recrutador = NULL;
		}
		
		
		$_SESSION['filtro_master_data'] = serialize($data);
		
		include '../Include/InfoSistema.view.php';
		
	}else if($_GET['op'] == 4){		
								
		$erros = 0;
		
		if(!empty($_FILES['ffFoto'])){ // testa se o arquivo esta cheio pelo !.
			$up = new Upload($_FILES['ffFoto']);
			$up->diretorio = '../PHPExcel/excel_list/';
			$aceitos = array('application/vnd.ms-excel','application/vnd.openxmlformats-officedocument.spreadsheetml.sheet','application/vnd.ms-excel.sheet.macroEnabled.12','application/vnd.ms-excel.template.macroEnabled.12','application/vnd.openxmlformats-officedocument.spreadsheetml.template');
			$up->formatos = $aceitos;
			
			if($up->erro != 0){
				$_SESSION['aviso'] = "Arquivo n�o reconhecido, teste novamente";
				
				$erros++;
			}

			if(!$up->testeTipo()){
				$_SESSION['aviso'] = 'Formato de arquivo invalido';
				
				$erros++;
			}	
			
							
		}else{		
			$_SESSION['aviso'] = 'Nenhum arquivo selecionado';
			
			$erros++;
		}	

		if($erros == 0){
			
			$infoCandidatoDAO = new InfoCandidatoDAO();
			
			$url = $up->upload("auto_imagem");
					
			chmod('../PHPExcel/excel_list/'.$url.'',0777);	
								
			$inputFileName = '../PHPExcel/excel_list/'.$url.'';

			try {
				$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
				$objReader = PHPExcel_IOFactory::createReader($inputFileType);
				$objPHPExcel = $objReader->load($inputFileName);
			} catch (Exception $e) {
			   // erro
			}
			
			$sheet = $objPHPExcel->getSheet(0);
			$highestRow = $sheet->getHighestRow();
			$highestColumn = $sheet->getHighestColumn();		
			
						
			for ($row = 1; $row <= $highestRow; $row++) {
				//  Read a row of data into an array
				$rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, 
				NULL, TRUE, FALSE);
				
				if(strlen($rowData[0][2]) == 11){
					// Passando o cpf e o cod do auto imagem para substuir oos candidatos com este cpf
					$cpf = $infoCandidatoDAO->mask("###.###.###-##",$rowData[0][2]); 				
									
					$infoCandidatoDAO->autoImagem($cpf,$rowData[0][4]);					
				}
								
			}												
			
			$_SESSION['aviso'] = htmlentities("Atualiza��o de dados concluida.");
			
			@unlink('../PHPExcel/excel_list/'.$url.'');
			
		}

		header('location:../PerfilAdmin.php');
	}
	
}else if($_GET['op'] == '1'){
	include '../Include/FormFiltroCandidatoAdmin.view.php';
}else if($_GET['op'] == sha1(2) && isset($_GET['id'])){	
	$infoEmpresaDAO = new InfoEmpresaDAO();
	
	$_SESSION['foco_empresa'] = serialize($infoEmpresaDAO->getUser(base64_decode($_GET['id'])));
	
	header('location:../InfoEmpresa.php');	
}else if($_GET['op'] == 3){
	include '../Include/FormFiltroEmpresaAdmin.view.php';
	
}else if($_GET['op'] == sha1(4) && isset($_GET['id']) && isset($_GET['status']) && isset($_GET['tp'])){
	$userDAO = new UserDAO();
	
	$userx->id = base64_decode($_GET['id']);
	$userx->status = base64_decode($_GET['status']);
	
	$userDAO->altStatus($userx);
	
	if($userx->status == 0){
		if($_GET['tp'] == "user"){
			$_SESSION['aviso'] = htmlentities("Usu�rio liberado com sucesso");
		}else{
			$_SESSION['aviso'] = htmlentities("Empresa liberada com sucesso");		
		}
	}else{
		if($_GET['tp'] == "user"){
			$_SESSION['aviso'] = htmlentities("Usu�rio bloqueado com sucesso");
		}else{
			$_SESSION['aviso'] = htmlentities("Empresa bloqueada com sucesso");			
		}
	}
	
	if($_GET['tp'] == "user"){
		include '../Include/BotaoBloqueioCandidato.view.php';
	}else{
		include '../Include/BotaoBloqueioEmpresa.view.php';
	}
	
	include '../Include/Aviso.view.php';
	
}else if($_GET['op'] == sha1(5) && isset($_SESSION['foco_candidato']) && isset($_SESSION['ct_admin'])){
	$userDAO = new UserDAO();

	$infoCandidato = unserialize($_SESSION['foco_candidato']);

	$_SESSION['ct_user'] = serialize($userDAO->getId($infoCandidato[0]->id_user));
	unset($_SESSION['ct_empresa']);
	
	header('location:../PerfilCandidato.php');		
}else if($_GET['op'] == sha1(6) && isset($_SESSION['ct_admin'])){

	unset($_SESSION['ct_user']);
	unset($_SESSION['ct_empresa']);
	
	header('location:../PerfilAdmin.php');
	
}else if($_GET['op'] == sha1(7) && isset($_SESSION['foco_empresa']) && isset($_SESSION['ct_admin'])){
	$userDAO = new UserDAO();

	$infoEmpresa = unserialize($_SESSION['foco_empresa']);

	$_SESSION['ct_empresa'] = serialize($userDAO->getId($infoEmpresa[0]->id_user));
	unset($_SESSION['ct_user']);
	
	header('location:../PerfilEmpresa.php');	
	
}else if($_GET['op'] == 8 && isset($_GET['cmd'])){
	
	$userDAO = new UserDAO();
	$vagaDAO = new VagaDAO();
	$relaVagaDAO = new RelaVagaDAO();
	
	$data = unserialize($_SESSION['filtro_master_data']);
	
	if($_GET['tp'] == 'user'){
		$tipo = "";
		
		if($_GET['cmd'] == 'getEmpresaCompAtivo'){
			$list_user = $userDAO->getEmpresaCompAtivo($data);		
			
			$explorar = 0;		
			$tipo = "empresa";
			
		}else if($_GET['cmd'] == 'getEmpresaIncomAtivo'){
			$list_user = $userDAO->getEmpresaIncomAtivo($data);	
			
			$explorar = 1;
			
		}else if($_GET['cmd'] == 'getEmpresaCompDesativo'){
			$list_user = $userDAO->getEmpresaCompDesativo($data);		
			
			$explorar = 0;		
			$tipo = "empresa";	
					
		}else if($_GET['cmd'] == 'getEmpresaIncomDesativo'){
			$list_user = $userDAO->getEmpresaIncomDesativo($data);	
			
			$explorar = 1;
			
		}else if($_GET['cmd'] == 'getUserCompAtivo'){
			$list_user = $userDAO->getUserCompAtivo($data);		
			
			$explorar = 0;
			$tipo = "user";
			
		}else if($_GET['cmd'] == 'getUserIncompAtivo'){
			$list_user = $userDAO->getUserIncompAtivo($data);		
			
			$explorar = 1;
			
		}else if($_GET['cmd'] == 'getUserCompDesativo'){
			$list_user = $userDAO->getUserCompDesativo($data);		

			$explorar = 0;
			$tipo = "user";		
				
		}else if($_GET['cmd'] == 'getUserIncompDesativo'){
			$list_user = $userDAO->getUserIncompDesativo($data);		

			$explorar = 1;			
		}
						
		include '../Include/FocoRelatUse.view.php';
		
	}else if($_GET['tp'] == 'vaga'){
	
		if($_GET['cmd'] == 'getTotal'){
			$list_vaga = $vagaDAO->getTotal($data);
		}else if($_GET['cmd'] == 'getCaptacao'){
			$list_vaga = $vagaDAO->getCaptacao($data);
		}else if($_GET['cmd'] == 'getConcluido'){
			$list_vaga = $vagaDAO->getConcluido($data);
		}else if($_GET['cmd'] == 'getCancelado'){
			$list_vaga = $vagaDAO->getCancelado($data);
		}else if($_GET['cmd'] == 'getPendente'){
			$list_vaga = $vagaDAO->getPendenteFoco($data);
		}
							
		include '../Include/FocoRelatVaga.view.php';
		
	}else{
		if($_GET['cmd'] == 'getTotal'){		
			$list_rela_vaga = $relaVagaDAO->getTotal($data);	
		}else if($_GET['cmd'] == 'getCandidato'){		
			$list_rela_vaga = $relaVagaDAO->getCandidato($data);	
		}else if($_GET['cmd'] == 'getRecrutado'){		
			$list_rela_vaga = $relaVagaDAO->getRecrutado($data);	
		}else if($_GET['cmd'] == 'getSelecionado'){		
			$list_rela_vaga = $relaVagaDAO->getSelecionado($data);	
		}
		
		include '../Include/FocoRelatRelaVaga.view.php';
	
	}
										
}
?>