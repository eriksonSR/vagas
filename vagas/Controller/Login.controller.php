<?php
	session_start();
	
	unset($_SESSION['ct_user']);
	unset($_SESSION['ct_empresa']);
	
	include '../Banco/Conexao.class.php';
	include '../Model/User.class.php';
	include '../Persistencia/UserDAO.class.php';
	
	include '../phpmailer/class.phpmailer.php';	
	include '../Model/Validacao.class.php';
	
	if($_POST){
		
		if($_GET['op'] == 1){
		
			$userDAO = new UserDAO();
			
			// Pode ser tanto cpf quanto cnpj depende do usuário
													
			$user->cpf = $_POST['txtLogin'];
			
			// Limpando cpf para validar no banco tmb junto com o com a mascara
			$cpf_limpo = str_replace(".","",$_POST['txtLogin']);
			$cpf_limpo = str_replace("-","",$cpf_limpo);
			
			$user->senha = sha1($_POST['txtSenha']);
									
			if($userDAO->logar($user,$cpf_limpo)){	
										
				$user_aux = $userDAO->getLogin($user,$cpf_limpo);														
													
				if($user_aux[0]->status == 0){
									
					if($user_aux[0]->tipo == 1){
						$url = "PerfilCandidato.php";
						
						$_SESSION['ct_user'] = serialize($user_aux);
					}else if($user_aux[0]->tipo == 2){
						$url = "PerfilEmpresa.php";
						
						$_SESSION['ct_empresa'] = serialize($user_aux);
					}
					
					include '../Include/Direcionamento.view.php';				
					
				}else{
					$_SESSION['aviso'] = "Seu usuário está desativado";
					include '../Include/Aviso.view.php';
				}
				
			}else{	
				echo "Usuário ou senha invalido";
			}
			
		}else if($_GET['op'] == 2){
			
			$userDAO = new UserDAO();
			
			// Pode ser tanto cpf quanto cnpj depende do usuário
			$user->email = $_POST['txtLogin'];
			$user->senha = sha1($_POST['txtSenha']);
		
			if($userDAO->logarAdmin($user)){	
										
				$user_aux = $userDAO->getLoginAdmin($user);														
													
				if($user_aux[0]->status == 0){
														
					$url = "PerfilAdmin.php";
					
					$_SESSION['ct_admin'] = serialize($user_aux);
					
					include '../Include/Direcionamento.view.php';				
																
				}else{
					echo "Seu usuário está desativado";					
				}
			}else{	
				echo "Usuário ou senha invalido";
			}
			
		}else if($_GET['op'] == 3){
			
			$userDAO = new UserDAO();			
			
			$user_reset = $userDAO->getEmail($_POST['txtEmail']);
			
			if(Validacao::email($_POST['txtEmail'])){
			
				if(count($user_reset) > 0){
					// Começando script para e-mail.
					
					$_SESSION['user_reset_senha'] = serialize($user_reset);
					
					$para = $_POST['txtEmail']; // E-MAIL DO DESTINATÁRIO

					$codigo = substr(md5($_POST['txtEmail'].time()),0,6);

					$_SESSION['codigo_reset_senha'] = $codigo;

					$mensagem = "<h3>Para alterar sua senha, informe o seguinte codigo em nosso sistema: </h3> Código de alteração: <b>".$codigo."</b> <br />";
										
					$assunto = iconv('utf8', 'ISO-8859-1', 'Alteração de senha'); //PEGA O ASSUNTO DO E-MAIL
		
					$mail = new PHPMailer(); //Instanciamos a classe PHPMailer
					
					$mail->IsSMTP(true); //Caso queira utilizar o programa de e-mail do seu servidor unix/linux para o envio de e-mail. Caso seja servidor windows o e-mail precisa ser enviado via STMP, então ai invéz de utilizar a função $mail->IsMail() utilizamos $mail->IsSMTP()
					
					$mail->IsHTML(true); //Ativa o envio de e-mail no formato html, se false desativa
										
					## Envio autenticado pelo gmail
					$mail->SMTPAuth = true;     // Autenticação ativada
					$mail->SMTPSecure = 'ssl';  // SSL REQUERIDO pelo GMail
					$mail->Host = 'smtp.gmail.com'; // SMTP utilizado
					$mail->Port = 465; 
					$mail->Username = 'portal@qi.edu.br';
					$mail->Password = 'portalqi1990';
					$mail->SetFrom('recrutamento@qi.edu.br', 'Recrutamento QI');
					$mail->CharSet="UTF-8";
					
					$mail->AddAddress("$para"); //E-mail que a mensagem será enviada
					
					$mail->Subject = $assunto; //Assunto da mensagem
															
					$mail->Body = $mensagem; //Corpo da mensagem										
										  
					$mail->Send();	
			
					$_SESSION['reset_senha'] = "Informe o código enviado para seu e-mail.";
			
					include '../Include/InputCodResetSenha.view.php';
				}else{
					$_SESSION['reset_senha'] = "E-mail não encontrado no sistema.";
					
					include '../Include/EmailResetSenha.view.php';
				}
			}else{
				$_SESSION['reset_senha'] = "E-mail invalido.";
				
				include '../Include/EmailResetSenha.view.php';
			}
													
		}else if($_GET['op'] == 4 && isset($_SESSION['codigo_reset_senha'])){
			if($_POST['txtCodigo'] == $_SESSION['codigo_reset_senha']){
								
				$_SESSION['aviso'] = "Informe sua nova senha";
				
				include '../Include/Aviso.view.php';	
				include '../Include/InputNovaSenha.view.php';
				
			}else{
				$_SESSION['aviso'] = "Código invalido";
				
				include '../Include/Aviso.view.php';	
				include '../Include/InputCodResetSenha.view.php';
			}
		}else if($_GET['op'] == 5){
			
			if(strlen($_POST['txtSenha']) == 0){
				$_SESSION['aviso'] = "Preencha uma senha.";
				
				include '../Include/Aviso.view.php';
				include '../Include/InputNovaSenha.view.php';
			}else if($_POST['txtSenha'] != $_POST['txtSenha2']){
				$_SESSION['aviso'] = "Confirme a senha corretamente.";			
				
				include '../Include/Aviso.view.php';
				include '../Include/InputNovaSenha.view.php';
			}else{
				$userDAO = new UserDAO();
				
				$user = unserialize($_SESSION['user_reset_senha']);
				
				$user[0]->senha = sha1($_POST['txtSenha']);
				
				$userDAO->alt($user[0]);
				
				$_SESSION['aviso'] = "Senha alterada com sucesso";			
				
				unset($_SESSION['user_reset_senha']);
				unset($_SESSION['codigo_reset_senha']);
				
				include '../Include/Aviso.view.php';
				include '../Include/InputAllLogin.view.php';
			}
								
		}
		
	}else if($_GET['op'] == 1 && isset($_GET['tipo'])){
		
		include '../Include/InputLogin.view.php';
	}else if($_GET['op'] == 2){
		
		include '../Include/EmailResetSenha.view.php';
	}else if($_GET['op'] == 3){
		
		include '../Include/InputAllLogin.view.php';
	}else{
		session_unset();	
		header('location:http://www.qi.com.br');
	}	
?>