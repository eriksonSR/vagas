<?php
session_start();

date_default_timezone_set('America/Sao_Paulo');	
include '../Banco/Conexao.class.php';
include '../Model/User.class.php';
include '../Persistencia/UserDAO.class.php';
include '../Model/Validacao.class.php';
include '../Model/InfoEmpresa.class.php';
include '../Persistencia/InfoEmpresaDAO.class.php';
include '../Model/Recrutador.class.php';
include '../Persistencia/RecrutadorDAO.class.php';

if($_POST){			
	if($_GET['op'] == 1){
		$infoEmpresaDAO = new InfoEmpresaDAO();
		$infoEmpresa = new InfoEmpresa();
				
		$user = unserialize($_SESSION['ct_empresa']);
		
		$infoEmpresa->id_user = $user[0]->id;
		
		$infoEmpresa->razao_social = $_POST['txtRazaoSocial'];
		
		if(strlen($_POST['txtNome']) > 0){
			$infoEmpresa->nome = $_POST['txtNome'];
		}
		
		$infoEmpresa->ramu_atuacao = $_POST['txtRamu'];
		
		$infoEmpresa->porte = $_POST['cbPorte'];
		$infoEmpresa->site = $_POST['txtSite'];				
		
		$infoEmpresa->tel = $_POST['txtTel'];
		$infoEmpresa->cel = $_POST['txtCel'];
		
		$infoEmpresa->facebook = $_POST['txtFace'];
		$infoEmpresa->twitter = $_POST['txtTwitter'];
			
		$infoEmpresa->nome_rh = $_POST['txtNomeRh'];
		$infoEmpresa->tel_rh = $_POST['txtTelRh'];
		$infoEmpresa->cel_rh = $_POST['txtCelRh'];
		$infoEmpresa->email_rh = $_POST['txtEmailRh'];
		
		$infoEmpresa->estado = $_POST['txtEstado'];
		$infoEmpresa->cidade = $_POST['txtCidade'];
		$infoEmpresa->bairro = $_POST['txtBairro'];
		$infoEmpresa->rua = $_POST['txtRua'];
		
		$infoEmpresa->data = date('Y-m-d'); 
		$infoEmpresa->hora = date('G:i:s');
		
		$infoEmpresaDAO->excluir($user[0]->id);
		$infoEmpresaDAO->cadastro($infoEmpresa);
		
		if(Validacao::email($_POST['txtEmail'])){
			$userDAO = new UserDAO();
			
			$user[0]->email = $_POST['txtEmail'];
						
			$userDAO->alt($user[0]);
			
			$_SESSION['ct_empresa'] = serialize($user);
		}
	
	}else if($_GET['op'] == 2){
		
		$recrutadorDAO = new RecrutadorDAO();
		
		$user = unserialize($_SESSION['ct_empresa']);
		
		if(strlen($_POST['txtNome']) > 0){
			$recrutador->id_user = $user[0]->id;		
			$recrutador->nome = $_POST['txtNome'];
			
			$recrutadorDAO->cadastro($recrutador);
			
			$_SESSION['aviso'] = "O recrutador foi cadastrada com sucesso.";
		}else{
			
			$_SESSION['aviso'] = "Preencha o nome do recrutador.";			
		}
		
		$id_form = 'cad_recrutador';
		
		include '../Include/ResetForm/FormAll.view.php';
		
		include '../Include/Aviso.view.php';
	
	}else if($_GET['op'] == 3){
		
		$recrutadorDAO = new RecrutadorDAO();
		
		if(strlen($_POST['txtNome']) > 0){

			$recrutador = $recrutadorDAO->getId(base64_decode($_GET['id']));

			$recrutador[0]->nome = $_POST['txtNome'];
			
			$recrutadorDAO->alt($recrutador[0]);	
					
			
			include '../Include/ListRecrutador.view.php';
			echo "<list>";
			include '../Include/CadRecrutador.view.php';
			echo "<list>";	
			
		}else{
			
			$_SESSION['aviso'] = "Preencha o nome do recrutador.";			
			
			include '../Include/Aviso.view.php';	
			echo "<list>";
			echo "<list>";	
		}
			
	}else{
		session_unset();
		header('location:../index.php');
	}

}else if($_GET['op'] == sha1(1)){

	include '../Include/ListRecrutador.view.php';

}else if($_GET['op'] == sha1(2)){
	// Exclus�o
	include '../Include/ListRecrutador.view.php';
	
}else if($_GET['op'] == sha1(3)){
	
	$recrutadorDAO = new RecrutadorDAO();
		
	$recrutador = $recrutadorDAO->getId(base64_decode($_GET['id']));
	
	include '../Include/EditRecrutador.view.php';
	
	$div = "popup_experiencia";
	
	include '../Include/ResetForm/ClosedAllPopup.view.php';
	
}else if($_GET['op'] == sha1(4)){

	include '../Include/CadRecrutador.view.php';
				
}else{
	session_unset();
	header('location:../index.php');
}
	
?>