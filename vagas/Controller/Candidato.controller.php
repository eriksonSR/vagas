<?php
session_start();

date_default_timezone_set('America/Sao_Paulo');	
include '../Banco/Conexao.class.php';
include '../Model/User.class.php';
include '../Persistencia/UserDAO.class.php';
include '../Model/Validacao.class.php';
include '../Model/InfoCandidato.class.php';
include '../Persistencia/InfoCandidatoDAO.class.php';
include '../Persistencia/InfoEmpresaDAO.class.php';
include '../Model/Curso.class.php';
include '../Persistencia/CursoDAO.class.php';

include '../Model/Upload.class.php';

if($_POST){			
	if($_GET['op'] == 1){
		$infoCandidatoDAO = new InfoCandidatoDAO();
		$infoCandidato = new InfoCandidato();
		
		
		$user = unserialize($_SESSION['ct_user']);
		
		$infoCandidato->id_user = $user[0]->id;
		
		if(strlen($_POST['txtNome']) > 0){
			$nome = ucfirst($_POST['txtNome']);
		
			$infoCandidato->nome = $nome;
		}else{
			$infoCandidato->nome = NULL;
		}
		
		$infoCandidato->tel = $_POST['txtTel'];
		$infoCandidato->cel = $_POST['txtCel'];
		
		$infoCandidato->facebook = $_POST['txtFace'];
		$infoCandidato->linkedin = $_POST['txtLinkedin'];
		
		
		if(strlen($_POST['txtNascimento']) > 0){
		
			$aux = str_replace("/","-",$_POST['txtNascimento']);
			$aux = date("d/m/Y",strtotime($aux));				
			$aux = str_replace("-","/",$aux);	
						
			$aux = explode('/',$aux);
			
			$dia = $aux[0];
			$mes = $aux[1];
			$ano = $aux[2];
																	
			if(Validacao::data($mes,$dia,$ano)){
				$infoCandidato->nascimento = date($ano."-".$mes."-".$dia);
			}
			
		}

		$infoCandidato->informatica = $_POST['cbInformatica'];
		
		// <Cnhs		
		if(isset($_POST['ckCnha'])){
			$infoCandidato->cnh .= "A,";
		}
		if(isset($_POST['ckCnhb'])){
			$infoCandidato->cnh .= "B,";
		}
		if(isset($_POST['ckCnhc'])){
			$infoCandidato->cnh .= "C,";
		}
		if(isset($_POST['ckCnhd'])){
			$infoCandidato->cnh .= "D,";
		}
		if(isset($_POST['ckCnhe'])){
			$infoCandidato->cnh .= "E,";
		}	
		// />
		
		// <Idiomas		
		if(isset($_POST['ckIngles'])){
			$infoCandidato->idioma .= "ingles,";
		}
		if(isset($_POST['ckItaliano'])){
			$infoCandidato->idioma .= "italiano,";
		}
		if(isset($_POST['ckFrances'])){
			$infoCandidato->idioma .= "frances,";		
		}		
		if(isset($_POST['ckEspanhol'])){
			$infoCandidato->idioma .= "espanhol,";		
		}
		if(isset($_POST['ckOutros'])){
			$infoCandidato->idioma .= "outros,";		
		}		
		// />
		
		$infoCandidato->estado_civil = $_POST['cbEstadoCivil'];				
										
		if(isset($_POST['rbFilho'])){
			$infoCandidato->filho_pequeno = $_POST['rbFilho'];
		}
		
		if(isset($_POST['rbSexo'])){
			$infoCandidato->sexo = $_POST['rbSexo'];
		}
												
		// <Disponibilidade dias						
		if(isset($_POST['ckSeg'])){
			$infoCandidato->dia_disponivel  .= "Segunda, ";				
		}
		
		if(isset($_POST['ckTer'])){
			$infoCandidato->dia_disponivel .= "Ter�a, ";			
		}
		
		if(isset($_POST['ckQua'])){
			$infoCandidato->dia_disponivel .= "Quarta, ";		
		}
		
		if(isset($_POST['ckQui'])){
			$infoCandidato->dia_disponivel .= "Quinta, ";		
		}
		
		if(isset($_POST['ckSex'])){
			$infoCandidato->dia_disponivel .= "Sexta, ";		
		}
		
		if(isset($_POST['ckSab'])){				
			$infoCandidato->dia_disponivel .= "Sabado, ";
			
		}
		
		if(isset($_POST['ckDomingo'])){
			$infoCandidato->dia_disponivel .= "Domingo, ";
		}			
		// />
		
		// <Disponibilidade de turno			
		if(isset($_POST['ckManha'])){
			$infoCandidato->turno_disponivel .= "Manha,";
		}
		if(isset($_POST['ckTarde'])){
			$infoCandidato->turno_disponivel .= "Tarde,";
		}
		if(isset($_POST['ckNoite'])){
			$infoCandidato->turno_disponivel .= "Noite,";		
		}			
		// />						
		
		$infoCandidato->estado = $_POST['txtEstado'];
		$infoCandidato->cidade = $_POST['txtCidade'];
		$infoCandidato->bairro = $_POST['txtBairro'];
		$infoCandidato->rua = $_POST['txtRua'];
		
		$infoCandidato->data = date('Y-m-d'); 
		$infoCandidato->hora = date('G:i:s');
						
		$infoCandidatoDAO->excluir($user[0]->id);
		$infoCandidatoDAO->cadastro($infoCandidato);
		
		if(Validacao::email($_POST['txtEmail'])){
			$userDAO = new UserDAO();
			
			$user[0]->email = $_POST['txtEmail'];
						
			$userDAO->alt($user[0]);
			
			$_SESSION['ct_user'] = serialize($user);
		}
		
	}else if($_GET['op'] == 2){
	
		$infoCandidatoDAO = new InfoCandidatoDAO();
		$infoEmpresaDAO = new InfoEmpresaDAO();
		
		$infoCandidato = new InfoCandidato();
		
		$user = unserialize($_SESSION['ct_empresa']);
		
		if($infoEmpresaDAO->getContValidaUser($user[0]->id) > 0){
		
			if(strlen($_POST['txtNome']) > 0){
				$infoCandidato->nome = $_POST['txtNome'];
			}
			
			if(strlen($_POST['cbInformatica']) > 0){
				$infoCandidato->informatica = $_POST['cbInformatica'];
			}
			
			// <Cnhs	
			$cnh = array();
				
			if(isset($_POST['ckCnha'])){
				$cnh[] = "A,";
			}
			if(isset($_POST['ckCnhb'])){
				$cnh[] = "B,";
			}
			if(isset($_POST['ckCnhc'])){
				$cnh[] = "C,";
			}
			if(isset($_POST['ckCnhd'])){
				$cnh[] = "D,";
			}
			if(isset($_POST['ckCnhe'])){
				$cnh[] = "E,";
			}	
			// />
	
			// <Idiomas		
			$idioma = array();
			
			if(isset($_POST['ckIngles'])){
				$idioma[] = "Ingl�s,";
			}
			if(isset($_POST['ckItaliano'])){
				$idioma[] = "Italiano,";
			}
			if(isset($_POST['ckFrances'])){
				$idioma[] = "Franc�s,";		
			}		
			if(isset($_POST['ckEspanhol'])){
				$idioma[] = "Espanhol,";		
			}
			if(isset($_POST['ckOutros'])){
				$idioma[] = "Outros,";		
			}		
			// />	
			
			// <Disponibilidade dias		
			$dia = array();
							
			if(isset($_POST['ckSeg'])){
				$dia[] = "Segunda,";				
			}
			
			if(isset($_POST['ckTer'])){
				$dia[] = "Ter�a,";			
			}
			
			if(isset($_POST['ckQua'])){
				$dia[] = "Quarta,";		
			}
			
			if(isset($_POST['ckQui'])){
				$dia[] = "Quinta,";		
			}
			
			if(isset($_POST['ckSex'])){
				$dia[] = "Sexta,";		
			}
			
			if(isset($_POST['ckSab'])){				
				$dia[] = "Sabado,";
				
			}
			
			if(isset($_POST['ckDomingo'])){
				$dia[] = "Domingo,";
			}			
			// />
			
			// <Disponibilidade de turno
			$turno = array();
						
			if(isset($_POST['ckManha'])){
				$turno[] = "Manha,";
			}
			if(isset($_POST['ckTarde'])){
				$turno[] = "Tarde,";
			}
			if(isset($_POST['ckNoite'])){
				$turno[] = "Noite,";		
			}			
			// />
			
			if(strlen($_POST['txtCidade']) > 0){
				$infoCandidato->cidade = $_POST['txtCidade'];
			}
			
			$curso = new Curso();

			if(strlen($_POST['txtEscola']) > 0){
				$curso->escola = $_POST['txtEscola'];
			}
							
			if(strlen($_POST['cbNivel']) > 0){								
				$curso->nivel = $_POST['cbNivel'];
				$curso->curso = $_POST['cbCurso'];				
			}
			
			$area = array();
			
			if(strlen($_POST['cbAreaExperiencia']) > 0){
				$area[0] = $_POST['cbAreaExperiencia'];
			}else{
				$area[0] = NULL;			
			}
			
			if(strlen($_POST['cbCargo']) > 0){
				$area[1] = $_POST['cbCargo'];
			}else{
				$area[1] = NULL;
			}
			
			if(strlen($_POST['cbFuncao']) > 0){
				$area[2] = $_POST['cbFuncao'];
			}else{
				$area[2] = NULL;
			}
			
			$list_imagem = $infoCandidatoDAO->getListCod();
		
			$check_imagem = array();
	
			foreach($list_imagem as $list){	
				if(isset($_POST["auto_".$list->auto_imagem.""])){
					$check_imagem[] = $list->auto_imagem;
				}
			}
		
								
			$list_candidato = $infoCandidatoDAO->getFiltro($infoCandidato,$cnh,$idioma,$dia,$turno,$curso,$area,$check_imagem);			
			
			$_SESSION['form_list_candidato'] = serialize($list_candidato);
			
			$_SESSION['FiltroCandidato'] = $_POST;
			
			include '../Include/getCandidato.view.php';						
			
		}else{		
			$_SESSION['aviso'] = "Para encontrar novos candidatos, <br />preencha o nome da empresa.";
			
		//	include '../Include/Aviso.view.php';
			include '../Include/FiltroCandidato.view.php';
		}	
		
	}else if($_GET['op'] == 3){
	
		$infoCandidatoDAO = new InfoCandidatoDAO();
		
		$infoCandidato = unserialize($_SESSION['foco_candidato']);
		
		$infoCandidato[0]->anotacao = $_POST['txtNota'];
		
		$infoCandidatoDAO->setNota($infoCandidato[0]);
		
		$_SESSION['foco_candidato'] = serialize($infoCandidato);
	
	}else if($_GET['op'] == 4){
		/*
		$user = unserialize($_SESSION['ct_user']);
		
		$up = new Upload($_FILES['cv_file']);
		$up->diretorio = '../PHPExcel/excel_list/';		
		
		if($up->erro != 0){
			
			$url = $up->upload("cv_".$user[0]->id);
					
			chmod('../PHPExcel/curriculo/'.$url.'',0777);	
								
			$inputFileName = '../PHPExcel/curriculo/'.$url.'';

			try {
				$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
				$objReader = PHPExcel_IOFactory::createReader($inputFileType);
				$objPHPExcel = $objReader->load($inputFileName);
			} catch (Exception $e) {
			   // erro
			}
			
			
			
		}else{
			echo "Arquivo n�o reconhecido, teste novamente";				
		}
		*/
		
		
	}else{
		session_unset();
		header('location:../index.php');
	}
}else if($_GET['op'] == 1){
	
	unset($_SESSION['form_list_candidato']);	
	include '../Include/FiltroCandidato.view.php';

}else if($_GET['op'] == sha1(2)){
	$infoCandidatoDAO = new InfoCandidatoDAO();
	
	$_SESSION['foco_candidato'] = serialize($infoCandidatoDAO->getUser(base64_decode($_GET['id'])));
			
	header('location:../InfoCandidato.php');
}else{
//	session_unset();
//	header('location:../index.php');
}
	
?>