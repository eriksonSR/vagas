<?php
session_start();

date_default_timezone_set('America/Sao_Paulo');	
include '../Banco/Conexao.class.php';
include '../Model/User.class.php';
include '../Model/Vaga.class.php';
include '../Persistencia/VagaDAO.class.php';
include '../Model/RelaVaga.class.php';
include '../Persistencia/RelaVagaDAO.class.php';
include '../Model/InfoCandidato.class.php';
include '../Model/Validacao.class.php';
include '../Persistencia/InfoCandidatoDAO.class.php';
include '../Model/Curso.class.php';
include '../Persistencia/CursoDAO.class.php';
include '../Model/Experiencia.class.php';
include '../Persistencia/ExperienciaDAO.class.php';
include '../Model/Aviso.class.php';
include '../Persistencia/AvisoDAO.class.php';
include '../Persistencia/UserDAO.class.php';

include '../phpmailer/class.phpmailer.php';	

include '../Model/Recrutador.class.php';
include '../Persistencia/RecrutadorDAO.class.php';

if($_POST){
	if($_GET['op'] == 1){
		
		$vagaDAO = new VagaDAO();
		
		$cont_erro = 0;
			
		if(strlen($_POST['txtHorario']) == 0){
			$erro = 'Preencha o horário.';
			
			$cont_erro++;
		}
		
		if(strlen($_POST['txtCidade']) == 0){
			$erro = 'Preencha a cidade.';
			
			$cont_erro++;
		}
		
		if($_POST['txtSalario'] == 0){
			$erro = 'Preencha a remuneração desejada.';
			
			$cont_erro++;
		}	

		if(strlen($_POST['cbCargo']) == 0){
			$erro = 'Preencha a área da vaga.';
			
			$cont_erro++;
		}
		
		if(strlen($_POST['cbFuncao']) == 0){
			$erro = 'Preencha a função da vaga.';
			
			$cont_erro++;
		}
		
		/*
		if(strlen($_POST['txtNome']) == 0){
			$erro = 'Preencha o nome da vaga.';
			
			$cont_erro++;
		}
		*/
							
		if($cont_erro == 0){
			
			$user = unserialize($_SESSION['ct_empresa']);
			
			$cont = 1;
										
			while($cont > 0){
				$cod_vaga = "";
				$valor = "ABCDEFGHJKLMNPQRSTUVWYZ123456789";
				srand((double)microtime()*1000000);
				
				for ($i=0; $i<8; $i++){
					$cod_vaga.= $valor[rand()%strlen($valor)];	
				}
				
				$cont = $vagaDAO->getContCod($cod_vaga);
			}
			
			$vaga->atendimento = $_POST['txtCod'];
			$vaga->codigo = $cod_vaga;								
			$vaga->id_user = $user[0]->id;
			$vaga->id_recrutador = $_POST['cbRecrutador'];
			$vaga->nome = $_POST['txtNome'];
			$vaga->area_experiencia = $_POST['cbAreaExperiencia'];
			$vaga->area = $_POST['cbCargo'];
			$vaga->atividade = $_POST['cbFuncao'];
			
			// Bolsa-auxilio
			$salario =  str_replace(".","",$_POST['txtSalario']); 
			$salario =  str_replace(",",".",$salario); 										
						
			$vaga->salario = $salario;
			// />
			
			// <Beneficios		
			if(isset($_POST['cbVT'])){
				$vaga->beneficio .= "Vale transporte,";
			}	
			
			if(isset($_POST['ckVR'])){
				$vaga->beneficio .= "Vale refeição,";
			}	
			if(isset($_POST['ckPlanoSaude'])){
				$vaga->beneficio .= "Plano de saúde,";
			}	
			if(isset($_POST['ckOutrosBeneficio'])){
				$vaga->beneficio .= "Outros,";
			}			
			// />
			
			// <Nivel de candidatos da vaga
			if(isset($_POST['ckMedio'])){
				$vaga->escolaridade .= "Ensino Fundamental / Médio,";
			}	
			if(isset($_POST['ckTecnico'])){
				$vaga->escolaridade .= "Técnico,";
			}	
			if(isset($_POST['ckSuperior'])){
				$vaga->escolaridade .= "Superior,";
			}	
			if(isset($_POST['ckPos'])){
				$vaga->escolaridade .= "Pós-Graduação,";
			}							
			// />
			
			// <Idiomas		
			if(isset($_POST['ckIngles'])){
				$vaga->idioma .= "Inglês,";
			}
			if(isset($_POST['ckItaliano'])){
				$vaga->idioma .= "Italiano,";
			}
			if(isset($_POST['ckFrances'])){
				$vaga->idioma .= "Francês,";		
			}		
			if(isset($_POST['ckEspanhol'])){
				$vaga->idioma .= "Espanhol,";		
			}
			if(isset($_POST['ckOutros'])){
				$vaga->idioma .= "Outros,";		
			}		
			// />						
			
			$vaga->cidade = $_POST['txtCidade'];
			$vaga->horario = $_POST['txtHorario'];
			$vaga->informacao_adicional = $_POST['txtMsg'];
			
			$vaga->data = date('Y-m-d'); 
			$vaga->hora = date('G:i:s');
				
			$vaga->nivel = 1;
						
			$vagaDAO->cadastro($vaga);

			// $aviso = "Vaga cadastrada com sucesso";
			
			// Listando os interesses ja cadastrados	
			/*		
			$list_vaga = $vagaDAO->getAll($user[0]);
			include '../Include/ListVaga.view.php';
			*/
			// />
			
			$id_form = 'cad_vaga';
			
			include '../Include/ResetForm/FormAll.view.php'; 
			
			echo "Vaga cadastrada com sucesso";
			
		}else{		
			echo $erro;
		}
		
	}else if($_GET['op'] == 2){
		
		$vagaDAO = new VagaDAO();
		
		$cont_erro = 0;
		
		if(strlen($_POST['txtHorario']) == 0){
			$_SESSION['aviso'] = 'Preencha o horário.';
			
			$cont_erro++;
		}
		
		if(strlen($_POST['txtCidade']) == 0){
			$_SESSION['aviso'] = 'Preencha a cidade.';
			
			$cont_erro++;
		}
		
		if($_POST['txtSalario'] == 0){
			$_SESSION['aviso'] = 'Preencha a remuneração desejada.';
			
			$cont_erro++;
		}	

		
		if(strlen($_POST['cbCargo']) == 0){
			$_SESSION['aviso'] = 'Preencha a área da vaga.';
			
			$cont_erro++;
		}
		
		if(strlen($_POST['cbFuncao']) == 0){
			$_SESSION['aviso'] = 'Preencha a função da vaga.';
			
			$cont_erro++;
		}
		/*
		if(strlen($_POST['txtNome']) == 0){
			$_SESSION['aviso'] = 'Preencha o nome da vaga.';
			
			$cont_erro++;
		}
		*/
		
		$vaga = unserialize($_SESSION['foco_vaga']);
																	
		if($cont_erro == 0){
			
			$vaga[0]->atendimento = $_POST['txtCod'];			
			$vaga[0]->nome = $_POST['txtNome'];			
			$vaga[0]->area_experiencia = $_POST['cbAreaExperiencia'];
			$vaga[0]->area = $_POST['cbCargo'];
			$vaga[0]->atividade = $_POST['cbFuncao'];
			
			// Ajustando o salario
			$salario =  str_replace(".","",$_POST['txtSalario']); 
			$salario =  str_replace(",",".",$salario); 										
						
			$vaga[0]->salario = $salario;
			// />
									
			// <Beneficios		
			$vaga[0]->beneficio = "";
			
			if(isset($_POST['cbVT'])){
				$vaga[0]->beneficio .= "Vale transporte,";
			}	
			
			if(isset($_POST['ckVR'])){
				$vaga[0]->beneficio .= "Vale refeição,";
			}	
			if(isset($_POST['ckPlanoSaude'])){
				$vaga[0]->beneficio .= "Plano de saúde,";
			}	
			if(isset($_POST['ckOutrosBeneficio'])){
				$vaga[0]->beneficio .= "Outros,";
			}			
			// />
			
			// <Nivel de candidatos da vaga
			$vaga[0]->escolaridade = "";
			
			if(isset($_POST['ckMedio'])){
				$vaga[0]->escolaridade .= "Ensino Fundamental / Médio,";
			}	
			if(isset($_POST['ckTecnico'])){
				$vaga[0]->escolaridade .= "Técnico,";
			}	
			if(isset($_POST['ckSuperior'])){
				$vaga[0]->escolaridade .= "Superior,";
			}	
			if(isset($_POST['ckPos'])){
				$vaga[0]->escolaridade .= "Pós-Graduação,";
			}							
			// />
			
			// <Idiomas		
			$vaga[0]->idioma = "";
			
			if(isset($_POST['ckIngles'])){
				$vaga[0]->idioma .= "Inglês,";
			}
			if(isset($_POST['ckItaliano'])){
				$vaga[0]->idioma .= "Italiano,";
			}
			if(isset($_POST['ckFrances'])){
				$vaga[0]->idioma .= "Francês,";		
			}		
			if(isset($_POST['ckEspanhol'])){
				$vaga[0]->idioma .= "Espanhol,";		
			}
			if(isset($_POST['ckOutros'])){
				$vaga[0]->idioma .= "Outros,";		
			}		
			// />						
									
			$vaga[0]->cidade = $_POST['txtCidade'];
			$vaga[0]->horario = $_POST['txtHorario'];
			
			$vaga[0]->informacao_adicional = $_POST['txtMsg'];
			
			
			if($vaga[0]->nivel == 4){
				$vaga[0]->nivel = 1;
			}
			
			$vagaDAO->alt($vaga[0]);
			
			if($vaga[0]->nivel != 4){
				$_SESSION['aviso'] = "Vaga alterada com sucesso";
			}else{
				$_SESSION['aviso'] = "Vaga aprovada com sucesso";
			}
			
			$_SESSION['foco_vaga'] = serialize($vaga);
			
			include '../Include/InfoVaga.view.php';
		}
		
		echo "<list>";
		include '../Include/Aviso.view.php';				
		echo $vaga[0]->nome;
		echo "<list>";
		
	}else if($_GET['op'] == 3){
		$vagaDAO = new VagaDAO();
		$infoCandidatoDAO = new InfoCandidatoDAO();
		
		$vaga = new Vaga();
		
		$user = unserialize($_SESSION['ct_user']);
		
		if($infoCandidatoDAO->getContValidaUser($user[0]->id) > 0){
		
			if(strlen($_POST['txtCodigo']) > 0){
				$vaga->codigo = $_POST['txtCodigo'];			
			}else{
				$vaga->codigo = NULL;	
			}
			
			if(strlen($_POST['cbAreaExperiencia']) > 0){
				$vaga->area_experiencia = $_POST['txtArea'];
			}else{
				$vaga->area_experiencia = NULL;
			}

			if(strlen($_POST['cbCargo']) > 0){
				$vaga->area = $_POST['cbCargo'];
			}else{
				$vaga->area = NULL;
			}
			
			if(strlen($_POST['cbFuncao']) > 0){
				$vaga->atividade = $_POST['cbFuncao'];
			}else{
				$vaga->atividade = NULL;
			}
			
			$salario = array();
			
			if(strlen($_POST['txtSalario_1']) > 0){
				// Ajustando o salario
				$salario_inicio =  str_replace(".","",$_POST['txtSalario_1']); 
				$salario_inicio =  str_replace(",",".",$salario_inicio);
							
				$salario[0] = $salario_inicio;
				// />
			}else{
				$salario[0] = NULL;
			}
			
			if(strlen($_POST['txtSalario_2']) > 0){
				// Ajustando o salario
				$salario_fim =  str_replace(".","",$_POST['txtSalario_2']); 
				$salario_fim =  str_replace(",",".",$salario_fim);
							
				$salario[1] = $salario_fim;
				// />
			}else{
				$salario[1] = NULL;
			}
			
			if(strlen($_POST['txtEmpresa']) > 0){
				$vaga->empresa = $_POST['txtEmpresa'];
			}else{
				$vaga->empresa = NULL;		
			}
			
			
			// <Beneficios		
			$beneficio = array();
			
			if(isset($_POST['cbVT'])){
				$beneficio[] = "Vale transporte,";
			}	
			
			if(isset($_POST['ckVR'])){
				$beneficio[] = "Vale refeição,";
			}	
			if(isset($_POST['ckPlanoSaude'])){
				$beneficio[] = "Plano de saúde,";
			}	
			if(isset($_POST['ckOutrosBeneficio'])){
				$beneficio[] = "Outros,";
			}			
			// />
			
			// <Nivel de candidatos da vaga
			$escolaridade = array();
			
			if(isset($_POST['ckMedio'])){
				$escolaridade[] = "Ensino Fundamental / Médio,";
			}	
			if(isset($_POST['ckTecnico'])){
				$escolaridade[] = "Técnico,";
			}	
			if(isset($_POST['ckSuperior'])){
				$escolaridade[] = "Superior,";
			}	
			if(isset($_POST['ckPos'])){
				$escolaridade[] = "Pós-Graduação,";
			}							
			// />
			
			// <Idiomas		
			$idioma = array();
			
			if(isset($_POST['ckIngles'])){
				$idioma[] = "Inglês,";
			}
			if(isset($_POST['ckItaliano'])){
				$idioma[] = "Italiano,";
			}
			if(isset($_POST['ckFrances'])){
				$idioma[] = "Francês,";		
			}		
			if(isset($_POST['ckEspanhol'])){
				$idioma[] = "Espanhol,";		
			}
			if(isset($_POST['ckOutros'])){
				$idioma[] = "Outros,";		
			}		
			// />	
			
			if(strlen($_POST['txtCidade']) > 0){
				$vaga->cidade = $_POST['txtCidade'];
			}else{
				$vaga->cidade = NULL;
			}
			
			if(strlen($_POST['txtHorario']) > 0){
				$vaga->horario = $_POST['txtHorario'];
			}else{
				$vaga->horario = NULL;
			}
			
			$vaga->nivel = 1;
			
			$list_vaga = $vagaDAO->getFiltro($vaga,$salario,$beneficio,$escolaridade,$idioma);
			
			$_SESSION['getVagaCandidato'] = $_POST; // salvando para sempre manter o ultimo formulario preenchido
								
			include '../Include/getVagaCandidato.view.php';
				
		}else{
			$_SESSION['aviso_vaga'] = "Para encontrar uma vaga, preencha seu nome nas informações pessoais.";
			
		//	include '../Include/Aviso.view.php';
			include '../Include/FiltroVagaCandidato.view.php';
		}
		
	}else if($_GET['op'] == 4){
		$relaVagaDAO = new RelaVagaDAO();
		
		$vaga = unserialize($_SESSION['foco_vaga']);
		
		if(strlen($_POST['cbNivel']) > 0){
			$list_info_candidato = $relaVagaDAO->getNivelUser($vaga[0]->id,$_POST['cbNivel']);		
		}else{
			$list_info_candidato = $relaVagaDAO->getUser($vaga[0]->id);		
		}
				
		// Para não utilizar uma nova busca no include
		$filtro_candidato = "ativo";
		
		include '../Include/CandidatosVaga.view.php';
	
	}else if($_GET['op'] == 5){
		$vagaDAO = new VagaDAO();
		$infoCandidatoDAO = new InfoCandidatoDAO();
		
		$vaga = new Vaga();
		
		$user = unserialize($_SESSION['ct_empresa']);		
		
		$vaga->id_user = $user[0]->id;
		
		if(strlen($_POST['txtCod']) > 0){
			$vaga->atendimento = $_POST['txtCod'];
		}else{
			$vaga->atendimento = NULL;
		}
		
		
		if(strlen($_POST['cbStatus']) > 0){
			$vaga->nivel = $_POST['cbStatus'];
		}else{
			$vaga->nivel = NULL;
		}
		
		if(strlen($_POST['cbRecrutador']) > 0){
			$vaga->id_recrutador = $_POST['cbRecrutador'];
		}else{
			$vaga->id_recrutador = NULL;
		}
		
		if(strlen($_POST['txtCodigo']) > 0){
			$vaga->codigo = $_POST['txtCodigo'];			
		}else{
			$vaga->codigo = NULL;	
		}
		
		if(strlen($_POST['cbAreaExperiencia']) > 0){
			$vaga->area_experiencia = $_POST['txtArea'];
		}else{
			$vaga->area_experiencia = NULL;
		}

		if(strlen($_POST['cbCargo']) > 0){
			$vaga->area = $_POST['cbCargo'];
		}else{
			$vaga->area = NULL;
		}
		
		if(strlen($_POST['cbFuncao']) > 0){
			$vaga->atividade = $_POST['cbFuncao'];
		}else{
			$vaga->atividade = NULL;
		}						
		
		$salario = array();
		
		if(strlen($_POST['txtSalario_1']) > 0){
			// Ajustando o salario
			$salario_inicio =  str_replace(".","",$_POST['txtSalario_1']); 
			$salario_inicio =  str_replace(",",".",$salario_inicio);
						
			$salario[0] = $salario_inicio;
			// />
		}else{
			$salario[0] = NULL;
		}
		
		if(strlen($_POST['txtSalario_2']) > 0){
			// Ajustando o salario
			$salario_fim =  str_replace(".","",$_POST['txtSalario_2']); 
			$salario_fim =  str_replace(",",".",$salario_fim);
						
			$salario[1] = $salario_fim;
			// />
		}else{
			$salario[1] = NULL;
		}		
		
		// <Beneficios		
		$beneficio = array();
		
		if(isset($_POST['cbVT'])){
			$beneficio[] = "Vale transporte,";
		}	
		
		if(isset($_POST['ckVR'])){
			$beneficio[] = "Vale refeição,";
		}	
		if(isset($_POST['ckPlanoSaude'])){
			$beneficio[] = "Plano de saúde,";
		}	
		if(isset($_POST['ckOutrosBeneficio'])){
			$beneficio[] = "Outros,";
		}			
		// />
		
		// <Nivel de candidatos da vaga
		$escolaridade = array();
		
		if(isset($_POST['ckMedio'])){
			$escolaridade[] = "Ensino Fundamental / Médio,";
		}	
		if(isset($_POST['ckTecnico'])){
			$escolaridade[] = "Técnico,";
		}	
		if(isset($_POST['ckSuperior'])){
			$escolaridade[] = "Superior,";
		}	
		if(isset($_POST['ckPos'])){
			$escolaridade[] = "Pós-Graduação,";
		}							
		// />
		
		// <Idiomas		
		$idioma = array();
		
		if(isset($_POST['ckIngles'])){
			$idioma[] = "Inglês,";
		}
		if(isset($_POST['ckItaliano'])){
			$idioma[] = "Italiano,";
		}
		if(isset($_POST['ckFrances'])){
			$idioma[] = "Francês,";		
		}		
		if(isset($_POST['ckEspanhol'])){
			$idioma[] = "Espanhol,";		
		}
		if(isset($_POST['ckOutros'])){
			$idioma[] = "Outros,";		
		}		
		// />	
		
		if(strlen($_POST['txtCidade']) > 0){
			$vaga->cidade = $_POST['txtCidade'];
		}else{
			$vaga->cidade = NULL;
		}
		
		if(strlen($_POST['txtHorario']) > 0){
			$vaga->horario = $_POST['txtHorario'];
		}else{
			$vaga->horario = NULL;
		}
				
		$list_vaga = $vagaDAO->getFiltroEmpresa($vaga,$salario,$beneficio,$escolaridade,$idioma);
		
		$_SESSION['form_filtro_vaga'] = $_POST;
					
		include '../Include/ListVaga.view.php';	
			
	}else{
		session_unset();
		header('location:../index.php');		
	}
	
}else if($_GET['op'] == sha1(1)){
	/*
	$vagaDAO = new VagaDAO();
		
	$user = unserialize($_SESSION['ct_empresa']);
	
	$list_vaga = $vagaDAO->getAll($user[0]);
	
	include '../Include/ListVaga.view.php';	
	*/
		
	include '../Include/FiltroVagaEmpresa.view.php';
	
}else if($_GET['op'] == sha1(2) && isset($_GET['id'])){
	$vagaDAO = new VagaDAO();
	
	$vaga = $vagaDAO->getId(base64_decode($_GET['id']));
	
	$_SESSION['foco_vaga'] = serialize($vaga);
	
	header('location:../Vaga.php');
	
}else if($_GET['op'] == 3 && isset($_SESSION['foco_vaga'])){
	$vaga = unserialize($_SESSION['foco_vaga']);
	
	include '../Include/EditVaga.view.php';
}else if($_GET['op'] == 4){
	include '../Include/FiltroVagaCandidato.view.php';
}else if($_GET['op'] == 5){	
	$relaVagaDAO = new RelaVagaDAO();
	$relaVaga = new RelaVaga();
	$userDAO = new UserDAO();
	$vagaDAO = new VagaDAO();
	
	$vaga = unserialize($_SESSION['foco_vaga']);
	$user = unserialize($_SESSION['ct_user']);
		
	$relaVaga->id_vaga = $vaga[0]->id;
	$relaVaga->id_user = $user[0]->id;
	
	$relaVagaDAO->excluir($relaVaga);
	
	if($_GET['tipo'] == 1){
		$relaVaga->nivel = 1;
		$relaVaga->data = date('Y-m-d'); 
		$relaVaga->hora = date('G:i:s');
	
		$relaVagaDAO->cadastro($relaVaga);
		
		$vaga = $vagaDAO->getId($relaVaga->id_vaga);
		
		$user_empresa = $userDAO->getId($vaga[0]->id_user);

		// Começando script para e-mail. 
						
		$nome = iconv('utf8', 'ISO-8859-1', "Recrutamento QI"); // E-MAIL DO REMETENTE
		
		$de = "recrutamento@qi.edu.br"; // E-MAIL DO REMETENTE
		
		$para = $user_empresa[0]->email; // E-MAIL DO DESTINATÁRIO
		
		$msg = "<html><body> ".iconv('utf8', 'ISO-8859-1', " <h2>Vaga: ".$vaga[0]->nome."</h2> - Um currículo foi anexado a sua vaga <br /> Código da vaga:".$vaga[0]->codigo."  <br /><br />Acesse http://qi.edu.br/recrutamento </body></html>");  // Mensagem
		
		$assunto = iconv('utf8', 'ISO-8859-1', "Novo candidato"); //PEGA O ASSUNTO DO E-MAIL

		$mail = new PHPMailer(); //Instanciamos a classe PHPMailer
		
		$mail->IsSMTP(true); //Caso queira utilizar o programa de e-mail do seu servidor unix/linux para o envio de e-mail. Caso seja servidor windows o e-mail precisa ser enviado via STMP, então ai invéz de utilizar a função $mail->IsMail() utilizamos $mail->IsSMTP()
		
		$mail->IsHTML(true); //Ativa o envio de e-mail no formato html, se false desativa
		
		$mail->From = $de; //E-mail do remente da mensagem
		
		$mail->FromName = $nome; //Nome do remente da mensagem
		
		$mail->AddAddress("$para"); //E-mail que a mensagem será enviada
		
		$mail->Subject = $assunto; //Assunto da mensagem
		
		$mail->Body = $msg; //Corpo da mensagem										
		
		$mail->Send();																							
				
	}
	
	$tipo = $_GET['tipo'];
	
	include '../Include/BotaoCandidatoVaga.view.php';	
	echo "<list>";	
	include '../Include/CandidatosVaga.view.php';	
	echo "<list>";
	
}else if($_GET['op'] == 6){	
	$relaVagaDAO = new RelaVagaDAO();
	
	$user = unserialize($_SESSION['ct_user']);	
	
	$list_vaga = $relaVagaDAO->getVaga($user[0]->id);
	
	include '../Include/getVagaCandidato.view.php';
}else if($_GET['op'] == sha1(7)){	
	$infoCandidatoDAO = new InfoCandidatoDAO();
	
	$infoCandidato = $infoCandidatoDAO->getUser(base64_decode($_GET['id']));
	
	// Para gerar em pdf o curriculo
	$_SESSION['foco_candidato'] = serialize($infoCandidato);
	
	include '../Include/InfoCandidato.view.php';
}else if($_GET['op'] == 8){	
	echo "<header>
	        <h2>Candidatos</h2>
    	    <p>A vaga não permite novos candidatos.</p>
      	  </header>";
		  
	include '../Include/FormFiltroCandidato.view.php';
		  
    echo "<div id='retorno_candidato'>";
			
	include '../Include/CandidatosVaga.view.php';
	
	echo "</div>";

}else if($_GET['op'] == 9 && isset($_GET['id'])){	
	$relaVagaDAO = new RelaVagaDAO();
	$relaVaga = new RelaVaga();
	
	$vaga = unserialize($_SESSION['foco_vaga']);
		
	$relaVaga->id_vaga = $vaga[0]->id;
	$relaVaga->id_user = base64_decode($_GET['id']);
	
	$relaVagaDAO->excluir($relaVaga);	
	
	echo "<header>
	        <h2>Candidatos</h2>
    	    <p>A vaga não permite novos candidatos.</p>
      	  </header>
      	  <div id='retorno_candidato'>";
		  
	include '../Include/CandidatosVaga.view.php';	
	
	echo "</div>";
	
}else if($_GET['op'] == 10 && isset($_GET['id_vaga']) && isset($_GET['id'])){	
	$infoCandidatoDAO = new InfoCandidatoDAO();
	$relaVagaDAO = new RelaVagaDAO();	
	
	$relaVaga->nivel = 2;
	$relaVaga->id = base64_decode($_GET['id_vaga']);
	
	$relaVagaDAO->alt($relaVaga);
	
	$infoCandidato = $infoCandidatoDAO->getUser(base64_decode($_GET['id']));
	
	include '../Include/InfoCandidato.view.php';

}else if($_GET['op'] == 10){	
		
	$vagaDAO = new VagaDAO();
		
	$user = unserialize($_SESSION['ct_empresa']);
	
	$list_vaga = $vagaDAO->getDispIndicacao($user[0]);
	
	include '../Include/ListVagaIndicacao.view.php';
		
}else if($_GET['op'] == sha1(11) && isset($_GET['id'])){
		
	$relaVagaDAO = new RelaVagaDAO();
	$avisoDAO = new AvisoDAO();
	$aviso = new Aviso();
	$userDAO = new UserDAO();
	
	$user = unserialize($_SESSION['ct_empresa']);		
	$infoCandidato = unserialize($_SESSION['foco_candidato']);
		
	$aviso->id_autor = $user[0]->id;
	$aviso->id_user = $infoCandidato[0]->id_user;
	$aviso->id_vaga = base64_decode($_GET['id']);
	$aviso->texto = "Olá ".$infoCandidato[0]->nome.", gostariamos de convidá-lo para participar de uma de nossas vagas.";
	$aviso->tipo = "convite para vaga";
	$aviso->data = date('Y-m-d'); 
	$aviso->hora = date('G:i:s');
	
	// Excluindo caso já exista um convite desta empresa para este candidato nesta vaga
	$avisoDAO->excluirVaga($aviso);
	$avisoDAO->cadastro($aviso);
	
	$user_candidato = $userDAO->getId($infoCandidato[0]->id_user);
	
	// Cadastrando o usuario na vaga
	$relaVaga->id_vaga = base64_decode($_GET['id']);
	$relaVaga->id_user = $user_candidato[0]->id;
	$relaVaga->nivel = 1;
	$relaVaga->data = date('Y-m-d'); 
	$relaVaga->hora = date('G:i:s');
	
	$relaVagaDAO->excluir($relaVaga);	
	$relaVagaDAO->cadastro($relaVaga);
	
	
	// Enviando e-mail para o candidato						
	$nome = iconv('utf8', 'ISO-8859-1', "Recrutamento QI"); // E-MAIL DO REMETENTE
	
	$de = "recrutamento@qi.edu.br"; // E-MAIL DO REMETENTE
	
	$para = $user_candidato[0]->email; // E-MAIL DO DESTINATÁRIO
	
	$msg = "<html><body> ".iconv('utf8', 'ISO-8859-1', " <h2>Você foi convidado para uma vaga </h2> - Visualize em http://qi.edu.br/recrutamento </body></html>");  // Mensagem
	
	$assunto = iconv('utf8', 'ISO-8859-1', "Convite para vaga"); //PEGA O ASSUNTO DO E-MAIL

	$mail = new PHPMailer(); //Instanciamos a classe PHPMailer
	
	$mail->IsSMTP(true); //Caso queira utilizar o programa de e-mail do seu servidor unix/linux para o envio de e-mail. Caso seja servidor windows o e-mail precisa ser enviado via STMP, então ai invéz de utilizar a função $mail->IsMail() utilizamos $mail->IsSMTP()
	
	$mail->IsHTML(true); //Ativa o envio de e-mail no formato html, se false desativa
	
	$mail->From = $de; //E-mail do remente da mensagem
	
	$mail->FromName = $nome; //Nome do remente da mensagem
	
	$mail->AddAddress("$para"); //E-mail que a mensagem será enviada
	
	$mail->Subject = $assunto; //Assunto da mensagem
	
	$mail->Body = $msg; //Corpo da mensagem										
	
	$mail->Send();	
	
	// />
	
	$_SESSION['aviso'] = "Convite enviado com sucesso";
	
	include '../Include/Aviso.view.php';

}else if($_GET['op'] == 11){	
	$vagaDAO = new VagaDAO();
	
	$vaga = unserialize($_SESSION['foco_vaga']);
	
	$vaga[0]->nivel = $_GET['nv'];
	
	$vagaDAO->alt($vaga[0]);
	
	$_SESSION['foco_vaga'] = serialize($vaga);
	
	if($_GET['nv'] == 3){
		$_SESSION['aviso'] = "Vaga cancelada com sucesso";
	}else{
		$_SESSION['aviso'] = "Vaga ativada com sucesso";
	}
	
	include '../Include/BotaoVaga.view.php';
	include '../Include/Aviso.view.php';
	
	echo "<list>";
	
	if($vaga[0]->nivel == 1){
		echo " <b style='color:green; font-size:28px;'>Vaga em captação de candidatos </b>";
	}else if($vaga[0]->nivel == 2){
		echo " <b style='color:blue; font-size:28px;'>Vaga concluida </b>";
	}else if($vaga[0]->nivel == 3){
		echo " <b style='color:red; font-size:28px;'>Vaga cancelada </b>";
	}
		
	echo "<list>";
}else if($_GET['op'] == 12){		
	$vaga = unserialize($_SESSION['foco_vaga']);
	
	include '../Include/ListSetCandidatoVaga.view.php';
	
}else if($_GET['op'] == sha1(13) && isset($_GET['id'])){	
	$relaVagaDAO = new RelaVagaDAO();
	$vagaDAO = new VagaDAO();
	
	$relaVaga = new RelaVaga();
	
	$vaga = unserialize($_SESSION['foco_vaga']);

	$vaga[0]->nivel = 2;
	$vaga[0]->data_conclusao = date('Y-m-d'); 
	$vaga[0]->hora_conclusao = date('G:i:s');

	$vagaDAO->alt($vaga[0]);
			
	$relaVaga->id_vaga = $vaga[0]->id;
	$relaVaga->id_user = base64_decode($_GET['id']);
	$relaVaga->nivel = 3;
	
	$relaVagaDAO->altRela($relaVaga);
	
	$_SESSION['foco_vaga'] = serialize($vaga);
		
	$_SESSION['aviso'] = "Vaga concluida com sucesso";
	
	// Fechando a popup dos candidatos
	include '../Include/ResetForm/ClosedPopup.view.php';
			
	include '../Include/Aviso.view.php';
	
	echo "<list>";
		
	echo " <b style='color:blue; font-size:28px;'>Vaga concluida </b>";				
			
	echo "<list>";
	
	include '../Include/CandidatosVaga.view.php';
	
	echo "<list>";
	
}else if($_GET['op'] == 14){
	unset($_SESSION['form_filtro_vaga']);
	
	include '../Include/CadVagaEmpresa.view.php';

}else if($_GET['op'] == 15){	
	
	$vagaDAO = new VagaDAO();
	
	$user = unserialize($_SESSION['ct_empresa']);
	
	$vagaDAO->altNivel(3,base64_decode($_GET['id']),$user[0]->id);		
	
	include '../Include/ListVagaPendente.view.php';
}else{
	session_unset();
	header('location:../index.php');
}

?>