-- phpMyAdmin SQL Dump
-- version 4.4.15.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Tempo de geração: 25/10/2017 às 08:51
-- Versão do servidor: 5.5.52
-- Versão do PHP: 5.6.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `recrutamento`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `area_interesse`
--

CREATE TABLE IF NOT EXISTS `area_interesse` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `cargo` varchar(100) NOT NULL,
  `funcao` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura para tabela `aviso`
--

CREATE TABLE IF NOT EXISTS `aviso` (
  `id` int(11) NOT NULL,
  `id_autor` int(11) DEFAULT NULL,
  `id_user` int(11) NOT NULL,
  `id_vaga` int(11) DEFAULT NULL,
  `texto` longtext NOT NULL,
  `tipo` varchar(100) NOT NULL,
  `data` date DEFAULT NULL,
  `hora` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura para tabela `curso`
--

CREATE TABLE IF NOT EXISTS `curso` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `nivel` varchar(50) NOT NULL,
  `curso` varchar(100) NOT NULL,
  `escola` varchar(100) NOT NULL,
  `descricao` longtext,
  `data_inicio` date DEFAULT NULL,
  `data_fim` date DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura para tabela `experiencia`
--

CREATE TABLE IF NOT EXISTS `experiencia` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `empresa` varchar(50) NOT NULL,
  `cargo` varchar(50) NOT NULL,
  `email` varchar(30) DEFAULT NULL,
  `tel` varchar(30) NOT NULL,
  `descricao` longtext,
  `data_inicio` date NOT NULL,
  `data_fim` date DEFAULT NULL,
  `atual` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura para tabela `info_candidato`
--

CREATE TABLE IF NOT EXISTS `info_candidato` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `nome` varchar(100) DEFAULT NULL,
  `rg` varchar(20) DEFAULT NULL,
  `tel` varchar(15) DEFAULT NULL,
  `cel` varchar(15) DEFAULT NULL,
  `facebook` varchar(50) DEFAULT NULL,
  `linkedin` varchar(50) DEFAULT NULL,
  `nascimento` varchar(10) DEFAULT NULL,
  `informatica` varchar(100) DEFAULT NULL,
  `cnh` varchar(20) DEFAULT NULL,
  `idioma` varchar(100) DEFAULT NULL,
  `estado_civil` varchar(50) DEFAULT NULL,
  `escolaridade` varchar(100) DEFAULT NULL,
  `filho_pequeno` varchar(30) DEFAULT NULL,
  `sexo` varchar(10) DEFAULT NULL,
  `dia_disponivel` varchar(100) DEFAULT NULL,
  `turno_disponivel` varchar(50) DEFAULT NULL,
  `estado` varchar(50) DEFAULT NULL,
  `cidade` varchar(50) DEFAULT NULL,
  `bairro` varchar(50) DEFAULT NULL,
  `rua` varchar(50) DEFAULT NULL,
  `data` date DEFAULT NULL,
  `hora` time DEFAULT NULL,
  `auto_imagem` varchar(50) DEFAULT NULL,
  `anotacao` longtext,
  `cidade_disponivel` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura para tabela `info_empresa`
--

CREATE TABLE IF NOT EXISTS `info_empresa` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `razao_social` varchar(100) DEFAULT NULL,
  `nome` varchar(100) DEFAULT NULL,
  `ramu_atuacao` varchar(100) DEFAULT NULL,
  `porte` varchar(50) DEFAULT NULL,
  `tel` varchar(15) DEFAULT NULL,
  `cel` varchar(15) DEFAULT NULL,
  `facebook` varchar(50) DEFAULT NULL,
  `twitter` varchar(50) DEFAULT NULL,
  `site` varchar(50) DEFAULT NULL,
  `nome_rh` varchar(100) DEFAULT NULL,
  `tel_rh` varchar(20) DEFAULT NULL,
  `cel_rh` varchar(20) DEFAULT NULL,
  `email_rh` varchar(50) DEFAULT NULL,
  `estado` varchar(50) DEFAULT NULL,
  `cidade` varchar(50) DEFAULT NULL,
  `bairro` varchar(50) DEFAULT NULL,
  `rua` varchar(50) DEFAULT NULL,
  `data` date DEFAULT NULL,
  `hora` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura para tabela `recrutador`
--

CREATE TABLE IF NOT EXISTS `recrutador` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `nome` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura para tabela `rela_vaga`
--

CREATE TABLE IF NOT EXISTS `rela_vaga` (
  `id` int(11) NOT NULL,
  `id_vaga` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `nivel` int(11) NOT NULL,
  `data` date DEFAULT NULL,
  `hora` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura para tabela `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `cpf` varchar(50) DEFAULT NULL,
  `cnpj` varchar(50) DEFAULT NULL,
  `senha` varchar(100) NOT NULL,
  `tipo` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `data` date DEFAULT NULL,
  `hora` time DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `user`
--

INSERT INTO `user` (`id`, `email`, `cpf`, `cnpj`, `senha`, `tipo`, `status`, `data`, `hora`) VALUES
(1, 'admin', NULL, NULL, 'da6f255a385667e87b61a9e2256844fbf33ed842', 0, 0, '2017-10-25', '00:00:00');

-- --------------------------------------------------------

--
-- Estrutura para tabela `vaga`
--

CREATE TABLE IF NOT EXISTS `vaga` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_recrutador` int(11) DEFAULT NULL,
  `codigo` varchar(20) NOT NULL,
  `nome` longtext NOT NULL,
  `area_experiencia` varchar(100) DEFAULT NULL,
  `area` varchar(100) DEFAULT NULL,
  `atividade` varchar(100) DEFAULT NULL,
  `beneficio` varchar(100) DEFAULT NULL,
  `escolaridade` varchar(100) DEFAULT NULL,
  `idioma` varchar(100) DEFAULT NULL,
  `horario` varchar(300) NOT NULL,
  `salario` double NOT NULL,
  `cidade` varchar(100) DEFAULT NULL,
  `informacao_adicional` longtext,
  `nivel` int(11) NOT NULL,
  `data` date DEFAULT NULL,
  `hora` time DEFAULT NULL,
  `atendimento` varchar(50) DEFAULT NULL,
  `gerente` varchar(100) DEFAULT NULL,
  `data_conclusao` date DEFAULT NULL,
  `hora_conclusao` time DEFAULT NULL,
  `escola` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Índices de tabelas apagadas
--

--
-- Índices de tabela `area_interesse`
--
ALTER TABLE `area_interesse`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`id_user`);

--
-- Índices de tabela `aviso`
--
ALTER TABLE `aviso`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_autor` (`id_autor`),
  ADD KEY `id_user` (`id_user`),
  ADD KEY `id_vaga` (`id_vaga`);

--
-- Índices de tabela `curso`
--
ALTER TABLE `curso`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`id_user`);

--
-- Índices de tabela `experiencia`
--
ALTER TABLE `experiencia`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`id_user`);

--
-- Índices de tabela `info_candidato`
--
ALTER TABLE `info_candidato`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`id_user`);

--
-- Índices de tabela `info_empresa`
--
ALTER TABLE `info_empresa`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`id_user`);

--
-- Índices de tabela `recrutador`
--
ALTER TABLE `recrutador`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`id_user`);

--
-- Índices de tabela `rela_vaga`
--
ALTER TABLE `rela_vaga`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_vaga` (`id_vaga`),
  ADD KEY `id_user` (`id_user`);

--
-- Índices de tabela `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `vaga`
--
ALTER TABLE `vaga`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`id_user`),
  ADD KEY `id_recrutador` (`id_recrutador`);

--
-- AUTO_INCREMENT de tabelas apagadas
--

--
-- AUTO_INCREMENT de tabela `area_interesse`
--
ALTER TABLE `area_interesse`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de tabela `aviso`
--
ALTER TABLE `aviso`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de tabela `curso`
--
ALTER TABLE `curso`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de tabela `experiencia`
--
ALTER TABLE `experiencia`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de tabela `info_candidato`
--
ALTER TABLE `info_candidato`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de tabela `info_empresa`
--
ALTER TABLE `info_empresa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de tabela `recrutador`
--
ALTER TABLE `recrutador`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de tabela `rela_vaga`
--
ALTER TABLE `rela_vaga`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de tabela `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de tabela `vaga`
--
ALTER TABLE `vaga`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Restrições para dumps de tabelas
--

--
-- Restrições para tabelas `area_interesse`
--
ALTER TABLE `area_interesse`
  ADD CONSTRAINT `area_interesse_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`);

--
-- Restrições para tabelas `aviso`
--
ALTER TABLE `aviso`
  ADD CONSTRAINT `aviso_ibfk_1` FOREIGN KEY (`id_autor`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `aviso_ibfk_2` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `aviso_ibfk_3` FOREIGN KEY (`id_vaga`) REFERENCES `vaga` (`id`);

--
-- Restrições para tabelas `curso`
--
ALTER TABLE `curso`
  ADD CONSTRAINT `curso_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`);

--
-- Restrições para tabelas `experiencia`
--
ALTER TABLE `experiencia`
  ADD CONSTRAINT `experiencia_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`);

--
-- Restrições para tabelas `info_candidato`
--
ALTER TABLE `info_candidato`
  ADD CONSTRAINT `info_candidato_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`);

--
-- Restrições para tabelas `info_empresa`
--
ALTER TABLE `info_empresa`
  ADD CONSTRAINT `info_empresa_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`);

--
-- Restrições para tabelas `recrutador`
--
ALTER TABLE `recrutador`
  ADD CONSTRAINT `recrutador_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`);

--
-- Restrições para tabelas `rela_vaga`
--
ALTER TABLE `rela_vaga`
  ADD CONSTRAINT `rela_vaga_ibfk_1` FOREIGN KEY (`id_vaga`) REFERENCES `vaga` (`id`),
  ADD CONSTRAINT `rela_vaga_ibfk_2` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`);

--
-- Restrições para tabelas `vaga`
--
ALTER TABLE `vaga`
  ADD CONSTRAINT `vaga_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `vaga_ibfk_2` FOREIGN KEY (`id_recrutador`) REFERENCES `recrutador` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
