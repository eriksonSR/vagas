<?php
	class AreaInteresseDAO{	
		// Declaração de atributos.
		private $conexao;
		
		// Declaração de metodos.
		public function __construct(){
			$this->conexao = Conexao::getInstancia();
		}
		
		public function cadastro($areaInteresse){
			$start = $this->conexao->prepare("
			insert into area_interesse() values(null,?,?,?)
			");
			$start->bindValue(1,$areaInteresse->id_user);		
			$start->bindValue(2,$areaInteresse->cargo);		
			$start->bindValue(3,$areaInteresse->funcao);			
			$start->execute();						
		}
		
		public function alt($areaInteresse){
			$start = $this->conexao->prepare("
			update area_interesse set cargo = ?, funcao = ? where id = ?
			");
			$start->bindValue(1,$areaInteresse->cargo);	
			$start->bindValue(2,$areaInteresse->funcao);			
			$start->bindValue(3,$areaInteresse->id);
			$start->execute();
		}
		
		public function excluir($id){
			$start = $this->conexao->prepare("
			delete from area_interesse where id = ?
			");
			$start->bindValue(1,$id);		
			$start->execute();						
		}	
		
		public function getAll($user){
			$start = $this->conexao->prepare("
			select * from area_interesse where id_user = ? order by id desc
			");
			$start->bindValue(1,$user->id);
			$start->execute();
			return $start->fetchALL(PDO::FETCH_CLASS,'AreaInteresse');
		}
		
		public function getId($id){
			$start = $this->conexao->prepare("
			select * from area_interesse where id = ?
			");
			$start->bindValue(1,$id);
			$start->execute();
			return $start->fetchALL(PDO::FETCH_CLASS,'AreaInteresse');
		}
		
	}
	
?>