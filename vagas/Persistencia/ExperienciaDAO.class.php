<?php
	class ExperienciaDAO{
		private $conexao;
		
		public function __construct(){	
			$this->conexao = Conexao::getInstancia();
		}
				
		public function cadastro($experiencia){
			$start = $this->conexao->prepare("
			insert into experiencia() values(null,?,?,?,?,?,?,?,?,?)
			");
			$start->bindValue(1,$experiencia->id_user);
			$start->bindValue(2,$experiencia->empresa);
			$start->bindValue(3,$experiencia->cargo);
			$start->bindValue(4,$experiencia->email);
			$start->bindValue(5,$experiencia->tel);
			$start->bindValue(6,$experiencia->descricao);
			$start->bindValue(7,$experiencia->data_inicio);
			$start->bindValue(8,$experiencia->data_fim);
			$start->bindValue(9,$experiencia->atual);
			$start->execute();			
		}
		
		public function alt($experiencia){
			$start = $this->conexao->prepare("
			update experiencia set empresa = ?, cargo = ?, email = ?, tel = ?, descricao = ?, data_inicio = ?, data_fim = ?, atual = ? where id = ?
			");
			$start->bindValue(1,$experiencia->empresa);
			$start->bindValue(2,$experiencia->cargo);
			$start->bindValue(3,$experiencia->email);
			$start->bindValue(4,$experiencia->tel);
			$start->bindValue(5,$experiencia->descricao);
			$start->bindValue(6,$experiencia->data_inicio);
			$start->bindValue(7,$experiencia->data_fim);
			$start->bindValue(8,$experiencia->atual);
			$start->bindValue(9,$experiencia->id);
			$start->execute();			
		}
		
		public function getAll($user){
			$start = $this->conexao->prepare("
			select * from experiencia where id_user = ? order by id desc
			");
			$start->bindValue(1,$user->id);
			$start->execute();
			return $start->fetchALL(PDO::FETCH_CLASS,'Experiencia');
		}
		
		public function excluir($id){
			$start = $this->conexao->prepare("
			delete from experiencia where id = ?
			");
			$start->bindValue(1,$id);		
			$start->execute();						
		}	
		
		
		public function getId($id){
			$start = $this->conexao->prepare("
			select * from experiencia where id = ?
			");
			$start->bindValue(1,$id);
			$start->execute();
			return $start->fetchALL(PDO::FETCH_CLASS,'Experiencia');
		}	
	}			
?>