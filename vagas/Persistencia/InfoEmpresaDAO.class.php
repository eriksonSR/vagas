<?php
	class infoEmpresaDAO{	
		// Declaração de atributos.
		private $conexao;
		
		// Declaração de metodos.
		public function __construct(){
			$this->conexao = Conexao::getInstancia();
		}
		
		public function cadastro($infoEmpresa){
			$start = $this->conexao->prepare("
			insert into info_empresa() values(null,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)
			");
			$start->bindValue(1,$infoEmpresa->id_user);			
			$start->bindValue(2,$infoEmpresa->razao_social);
			$start->bindValue(3,$infoEmpresa->nome);						
			$start->bindValue(4,$infoEmpresa->ramu_atuacao);
			$start->bindValue(5,$infoEmpresa->porte);			
			$start->bindValue(6,$infoEmpresa->tel);
			$start->bindValue(7,$infoEmpresa->cel);		
			$start->bindValue(8,$infoEmpresa->facebook);
			$start->bindValue(9,$infoEmpresa->twitter);			
			$start->bindValue(10,$infoEmpresa->site);			
			$start->bindValue(11,$infoEmpresa->nome_rh);			
			$start->bindValue(12,$infoEmpresa->tel_rh);
			$start->bindValue(13,$infoEmpresa->cel_rh);
			$start->bindValue(14,$infoEmpresa->email_rh);			
			$start->bindValue(15,$infoEmpresa->estado);
			$start->bindValue(16,$infoEmpresa->cidade);		
			$start->bindValue(17,$infoEmpresa->bairro);
			$start->bindValue(18,$infoEmpresa->rua);			
			$start->bindValue(19,$infoEmpresa->data);
			$start->bindValue(20,$infoEmpresa->hora);			
			$start->execute();
			return $start->errorCode();
		}
		
		public function getUser($id){
			$start = $this->conexao->prepare("
			 select info_empresa.*, user.email as email from info_empresa 
			 
			 left outer join user on user.id = info_empresa.id_user 
			 
			 where info_empresa.id_user = ? 
			");
			$start->bindValue(1,$id);
			$start->execute();
			return $start->fetchALL(PDO::FETCH_CLASS,'InfoEmpresa');
		}
		
		public function excluir($id){
			$start = $this->conexao->prepare("
			delete from info_empresa where id_user = ?
			");
			$start->bindValue(1,$id);
			$start->execute();			
		}	
		
		public function getContValidaUser($id){
			$start = $this->conexao->prepare("
			 select * from info_empresa where id_user = ? and nome is not null 
			");
			$start->bindValue(1,$id);
			$start->execute();
			return $start->rowCount();
		}
						
		public function getFiltroAdmin($infoEmpresa){
			
			$sql = "select distinct info_empresa.id, info_empresa.*, user.cnpj as cnpj from info_empresa 
			
					left outer join user on user.id = info_empresa.id_user 
			
					where info_empresa.id is not null and info_empresa.nome is not null and user.id is not null ";
			
			if(strlen($infoEmpresa->email) > 0){
				$sql .= " and user.email = '".$infoEmpresa->email."'";
			}
			
			if(strlen($infoEmpresa->cnpj) > 0){
				$sql .= " and user.cnpj = '".$infoEmpresa->cnpj."'";
			}
			
			if(strlen($infoEmpresa->nome) > 0){
				$sql .= " and (info_empresa.nome like '%$infoEmpresa->nome%' or info_empresa.razao_social like '%$infoEmpresa->nome%')";
			}
															
			$sql .= " order by info_empresa.id desc ";
			
			$start = $this->conexao->prepare($sql);
			$start->execute();
			return $start->fetchALL(PDO::FETCH_CLASS,'InfoEmpresa');		
		}		
	}
	
?>