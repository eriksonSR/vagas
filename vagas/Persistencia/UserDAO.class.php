<?php
	class UserDAO{	
		// Declaração de atributos.
		private $conexao;
		
		// Declaração de metodos.
		public function __construct(){
			$this->conexao = Conexao::getInstancia();
		}
		
		public function cadastro($user){
			$start = $this->conexao->prepare("
			insert into user() values(null,?,?,?,?,?,0,?,?)
			");
			$start->bindValue(1,$user->email);
			$start->bindValue(2,$user->cpf);
			$start->bindValue(3,$user->cnpj);	
			$start->bindValue(4,$user->senha);
			$start->bindValue(5,$user->tipo);
			$start->bindValue(6,$user->data);
			$start->bindValue(7,$user->hora);			
			$start->execute();						
		}	
		
		public function getContEmail($email){
			$start = $this->conexao->prepare("
			select * from user where email = ?
			");	
			$start->bindValue(1,$email); 	
			$start->execute();				
			return $start->rowCount();
		}
		
		public function getContCpf($cpf){
			$start = $this->conexao->prepare("
			select * from user where cpf = ?
			");	
			$start->bindValue(1,$cpf); 	
			$start->execute();				
			return $start->rowCount();
		}
		
		public function getContCnpj($cnpj){
			$start = $this->conexao->prepare("
			select * from user where cnpj = ?
			");	
			$start->bindValue(1,$cnpj); 	
			$start->execute();				
			return $start->rowCount();
		}

		public function getLogin($user,$cpf_limpo){
			$start = $this->conexao->prepare("
			 select * from user where ((cpf = ? or cpf = ?) and cpf is not null) or ((cnpj = ? or cnpj = ?) and cnpj is not null)
			");
			$start->bindValue(1,$user->cpf);
			$start->bindValue(2,$cpf_limpo);
			$start->bindValue(3,$user->cpf);
			$start->bindValue(4,$cpf_limpo);
			$start->execute();
			return $start->fetchALL(PDO::FETCH_CLASS,'User');
		}
		
		public function getLoginAdmin($user){
			$start = $this->conexao->prepare("
			 select * from user where email = ?
			");
			$start->bindValue(1,$user->email);
			$start->execute();
			return $start->fetchALL(PDO::FETCH_CLASS,'User');
		}
		
		public function getEmail($email){
			$start = $this->conexao->prepare("
			 select * from user where email = ?
			");
			$start->bindValue(1,$email);
			$start->execute();
			return $start->fetchALL(PDO::FETCH_CLASS,'User');
		}
		
		public function logar($user,$cpf_limpo){
								
			$start = $this->conexao->prepare("
			select * from user where ((cpf = ? or cpf = ?) or (cnpj = ? or cnpj = ?)) and senha = ? 
			");
			$start->bindValue(1,$user->cpf);
			$start->bindValue(2,$cpf_limpo);
			$start->bindValue(3,$user->cpf);
			$start->bindValue(4,$cpf_limpo);			
			$start->bindValue(5,$user->senha);
			$start->execute();
			
			if($start->rowCount() > 0){
				return TRUE;
			}else{
				return FALSE;
			}				
		}	
		
		public function logarAdmin($user){
			$start = $this->conexao->prepare("
			select * from user where email = ? and senha = ? and tipo = 0 and cnpj is null and cpf is null
			");
			$start->bindValue(1,$user->email);
			$start->bindValue(2,$user->senha);
			$start->execute();
			
			if($start->rowCount() > 0){
				return TRUE;
			}else{
				return FALSE;
			}				
		}
		
		public function alt($user){
			$start = $this->conexao->prepare("
			update user set email = ?, senha = ?, status = ? where id = ?
			");
			$start->bindValue(1,$user->email);
			$start->bindValue(2,$user->senha);
			$start->bindValue(3,$user->status);
			$start->bindValue(4,$user->id);
			$start->execute();						
		}
		
		public function altStatus($user){
			$start = $this->conexao->prepare("
			update user set status = ? where id = ?
			");
			$start->bindValue(1,$user->status);
			$start->bindValue(2,$user->id);
			$start->execute();						
		}
		
		public function getId($id){
			$start = $this->conexao->prepare("
			 select * from user where id = ?
			");
			$start->bindValue(1,$id);
			$start->execute();
			return $start->fetchALL(PDO::FETCH_CLASS,'User');
		}	

		public function getEmpresaAtivo($data){
			$sql = "select * from user where tipo = 2 and status = 0 ";
		
			if(isset($data[0])){
				$sql .= " and data >= '".$data[0]."' ";
			}
			
			if(isset($data[1])){
				$sql .= " and data <= '".$data[1]."' ";
			}
		
			$start = $this->conexao->prepare($sql);
			$start->execute();
			return $start->fetchALL(PDO::FETCH_CLASS,'User');
		}
		
		public function geEmpresaAtivo($data){
			$sql = "select * from user where tipo = 2 and status = 0 ";
		
			if(isset($data[0])){
				$sql .= " and data >= '".$data[0]."' ";
			}
			
			if(isset($data[1])){
				$sql .= " and data <= '".$data[1]."' ";
			}
		
			$start = $this->conexao->prepare($sql);
			$start->execute();
			return $start->fetchALL(PDO::FETCH_CLASS,'User');
		}				
				
		public function getEmpresaDesativo($data){
			$sql = "select * from user where tipo = 2 and status = 1 ";
		
			if(isset($data[0])){
				$sql .= " and data >= '".$data[0]."' ";
			}
			
			if(isset($data[1])){
				$sql .= " and data <= '".$data[1]."' ";
			}
		
			$start = $this->conexao->prepare($sql);
			$start->execute();
			return $start->fetchALL(PDO::FETCH_CLASS,'User');
		}
		
		public function getContEmpresaCompAtivo($data){
			$sql = "select * from user 
			
					left outer join info_empresa on info_empresa.id_user = user.id
					
					where user.tipo = 2 and user.status = 0 and info_empresa.nome is not null
			";
		
			if(isset($data[0])){
				$sql .= " and user.data >= '".$data[0]."' ";
			}
			
			if(isset($data[1])){
				$sql .= " and user.data <= '".$data[1]."' ";
			}
		
			$start = $this->conexao->prepare($sql);
			$start->execute();
			return $start->rowCount();
		}
		
		public function getEmpresaCompAtivo($data){
			$sql = "select * from user 
			
					left outer join info_empresa on info_empresa.id_user = user.id
					
					where user.tipo = 2 and user.status = 0 and info_empresa.nome is not null
			";
		
			if(isset($data[0])){
				$sql .= " and user.data >= '".$data[0]."' ";
			}
			
			if(isset($data[1])){
				$sql .= " and user.data <= '".$data[1]."' ";
			}
		
			$start = $this->conexao->prepare($sql);
			$start->execute();
			return $start->fetchALL(PDO::FETCH_CLASS,'User');
		}
		
		public function getEmpresaIncomAtivo($data){
			$sql = "select * from user 
			
					left outer join info_empresa on info_empresa.id_user = user.id
					
					where user.tipo = 2 and user.status = 0 and info_empresa.nome is null
			";
		
			if(isset($data[0])){
				$sql .= " and user.data >= '".$data[0]."' ";
			}
			
			if(isset($data[1])){
				$sql .= " and user.data <= '".$data[1]."' ";
			}
		
			$start = $this->conexao->prepare($sql);
			$start->execute();
			return $start->fetchALL(PDO::FETCH_CLASS,'User');
		}
		
		public function getContEmpresaIncomAtivo($data){
			$sql = "select * from user 
			
					left outer join info_empresa on info_empresa.id_user = user.id
					
					where user.tipo = 2 and user.status = 0 and info_empresa.nome is null
			";
		
			if(isset($data[0])){
				$sql .= " and user.data >= '".$data[0]."' ";
			}
			
			if(isset($data[1])){
				$sql .= " and user.data <= '".$data[1]."' ";
			}
		
			$start = $this->conexao->prepare($sql);
			$start->execute();
			return $start->rowCount();
		}
		
		
		public function getContEmpresaCompDesativo($data){
			$sql = "select * from user 
			
					left outer join info_empresa on info_empresa.id_user = user.id
					
					where user.tipo = 2 and user.status = 1 and info_empresa.nome is not null
			";
		
			if(isset($data[0])){
				$sql .= " and user.data >= '".$data[0]."' ";
			}
			
			if(isset($data[1])){
				$sql .= " and user.data <= '".$data[1]."' ";
			}
		
			$start = $this->conexao->prepare($sql);
			$start->execute();
			return $start->rowCount();
		}
		
		public function getContEmpresaIncomDesativo($data){
			$sql = "select * from user 
			
					left outer join info_empresa on info_empresa.id_user = user.id
					
					where user.tipo = 2 and user.status = 1 and info_empresa.nome is null
			";
		
			if(isset($data[0])){
				$sql .= " and user.data >= '".$data[0]."' ";
			}
			
			if(isset($data[1])){
				$sql .= " and user.data <= '".$data[1]."' ";
			}
		
			$start = $this->conexao->prepare($sql);
			$start->execute();
			return $start->rowCount();
		}
		
		public function getEmpresaIncomDesativo($data){
			$sql = "select * from user 
			
					left outer join info_empresa on info_empresa.id_user = user.id
					
					where user.tipo = 2 and user.status = 1 and info_empresa.nome is null
			";
		
			if(isset($data[0])){
				$sql .= " and user.data >= '".$data[0]."' ";
			}
			
			if(isset($data[1])){
				$sql .= " and user.data <= '".$data[1]."' ";
			}
		
			$start = $this->conexao->prepare($sql);
			$start->execute();
			return $start->fetchALL(PDO::FETCH_CLASS,'User');
		}
		
		public function getEmpresaCompDesativo($data){
			$sql = "select * from user 
			
					left outer join info_empresa on info_empresa.id_user = user.id
					
					where user.tipo = 2 and user.status = 1 and info_empresa.nome is not null
			";
		
			if(isset($data[0])){
				$sql .= " and user.data >= '".$data[0]."' ";
			}
			
			if(isset($data[1])){
				$sql .= " and user.data <= '".$data[1]."' ";
			}
		
			$start = $this->conexao->prepare($sql);
			$start->execute();
			return $start->fetchALL(PDO::FETCH_CLASS,'User');
		}
		
		public function getContUserCompAtivo($data){
			$sql = "select user.* from user 
						
			left outer join info_candidato on info_candidato.id_user = user.id
			
			where user.tipo = 1 and user.status = 0 and info_candidato.id is not null and info_candidato.nome is not null
			";
		
			if(isset($data[0])){
				$sql .= " and user.data >= '".$data[0]."' ";
			}
			
			if(isset($data[1])){
				$sql .= " and user.data <= '".$data[1]."' ";
			}
		
			$start = $this->conexao->prepare($sql);
			$start->execute();
			return $start->rowCount();
		}
		
		public function getUserCompAtivo($data){
			$sql = "select user.* from user 
						
			left outer join info_candidato on info_candidato.id_user = user.id
			
			where user.tipo = 1 and user.status = 0 and info_candidato.id is not null and info_candidato.nome is not null
			";
		
			if(isset($data[0])){
				$sql .= " and user.data >= '".$data[0]."' ";
			}
			
			if(isset($data[1])){
				$sql .= " and user.data <= '".$data[1]."' ";
			}
		
			$start = $this->conexao->prepare($sql);
			$start->execute();
			return $start->fetchALL(PDO::FETCH_CLASS,'User');
		}
		
		public function getUserIncompAtivo($data){
			$sql = "select user.* from user 
						
			left outer join info_candidato on info_candidato.id_user = user.id
			
			where user.tipo = 1 and user.status = 0 and info_candidato.id is null and info_candidato.nome is null
			";
		
			if(isset($data[0])){
				$sql .= " and user.data >= '".$data[0]."' ";
			}
			
			if(isset($data[1])){
				$sql .= " and user.data <= '".$data[1]."' ";
			}
		
			$start = $this->conexao->prepare($sql);
			$start->execute();
			return $start->fetchALL(PDO::FETCH_CLASS,'User');
		}
		
		public function getContUserIncompAtivo($data){
			$sql = "select user.* from user 
						
			left outer join info_candidato on info_candidato.id_user = user.id
			
			where user.tipo = 1 and user.status = 0 and info_candidato.id is null and info_candidato.nome is null
			";
		
			if(isset($data[0])){
				$sql .= " and user.data >= '".$data[0]."' ";
			}
			
			if(isset($data[1])){
				$sql .= " and user.data <= '".$data[1]."' ";
			}
		
			$start = $this->conexao->prepare($sql);
			$start->execute();
			return $start->rowCount();
		}
		
		public function getUserCompDesativo($data){
			
			$sql = "select user.* from user 
						
			left outer join info_candidato on info_candidato.id_user = user.id
			
			where user.tipo = 1 and user.status = 1 and info_candidato.id is not null and info_candidato.nome is not null
			";
		
			if(isset($data[0])){
				$sql .= " and user.data >= '".$data[0]."' ";
			}
			
			if(isset($data[1])){
				$sql .= " and user.data <= '".$data[1]."' ";
			}
		
			$start = $this->conexao->prepare($sql);
			$start->execute();
			return $start->fetchALL(PDO::FETCH_CLASS,'User');					
		}
		
		public function getContUserCompDesativo($data){
			
			$sql = "select user.* from user 
						
			left outer join info_candidato on info_candidato.id_user = user.id
			
			where user.tipo = 1 and user.status = 1 and info_candidato.id is not null and info_candidato.nome is not null
			";
		
			if(isset($data[0])){
				$sql .= " and user.data >= '".$data[0]."' ";
			}
			
			if(isset($data[1])){
				$sql .= " and user.data <= '".$data[1]."' ";
			}
		
			$start = $this->conexao->prepare($sql);
			$start->execute();
			return $start->rowCount();						
		}
		
		public function getContUserIncompDesativo($data){
			$sql = "select user.* from user 
						
			left outer join info_candidato on info_candidato.id_user = user.id
			
			where user.tipo = 1 and user.status = 1 and info_candidato.id is null and info_candidato.nome is null
			";
		
			if(isset($data[0])){
				$sql .= " and user.data >= '".$data[0]."' ";
			}
			
			if(isset($data[1])){
				$sql .= " and user.data <= '".$data[1]."' ";
			}
		
			$start = $this->conexao->prepare($sql);
			$start->execute();
			return $start->rowCount();
		}
		
		public function getUserIncompDesativo($data){
			$sql = "select user.* from user 
						
			left outer join info_candidato on info_candidato.id_user = user.id
			
			where user.tipo = 1 and user.status = 1 and info_candidato.id is null and info_candidato.nome is null
			";
		
			if(isset($data[0])){
				$sql .= " and user.data >= '".$data[0]."' ";
			}
			
			if(isset($data[1])){
				$sql .= " and user.data <= '".$data[1]."' ";
			}
		
			$start = $this->conexao->prepare($sql);
			$start->execute();
			return $start->fetchALL(PDO::FETCH_CLASS,'User');		
		}				
	}
	
?>