<?php
	class InfoCandidatoDAO{	
		// Declaração de atributos. 
		private $conexao;
		
		// Declaração de metodos.
		public function __construct(){
			$this->conexao = Conexao::getInstancia();
		}
		
		public function cadastro($infoCandidato){
			$start = $this->conexao->prepare("
			insert into info_candidato() values(null,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,null,null,null)		
			");
			$start->bindValue(1,$infoCandidato->id_user);
			$start->bindValue(2,$infoCandidato->nome);
			$start->bindValue(3,$infoCandidato->rg);
			$start->bindValue(4,$infoCandidato->tel);
			$start->bindValue(5,$infoCandidato->cel);
			$start->bindValue(6,$infoCandidato->facebook);
			$start->bindValue(7,$infoCandidato->linkedin);
			$start->bindValue(8,$infoCandidato->nascimento);
			$start->bindValue(9,$infoCandidato->informatica);
			$start->bindValue(10,$infoCandidato->cnh);
			$start->bindValue(11,$infoCandidato->idioma);
			$start->bindValue(12,$infoCandidato->estado_civil);
			$start->bindValue(13,$infoCandidato->escolaridade);
			$start->bindValue(14,$infoCandidato->filho_pequeno);
			$start->bindValue(15,$infoCandidato->sexo);
			$start->bindValue(16,$infoCandidato->dia_disponivel);
			$start->bindValue(17,$infoCandidato->turno_disponivel);
			$start->bindValue(18,$infoCandidato->estado);
			$start->bindValue(19,$infoCandidato->cidade);
			$start->bindValue(20,$infoCandidato->bairro);
			$start->bindValue(21,$infoCandidato->rua);
			$start->bindValue(22,$infoCandidato->data);
			$start->bindValue(23,$infoCandidato->hora);
			$start->execute();
			
		}
				
		public function setNota($infoCandidato){
			$start = $this->conexao->prepare("
			update info_candidato set anotacao = ? where id = ?
			");
			$start->bindValue(1,$infoCandidato->anotacao);
			$start->bindValue(2,$infoCandidato->id);
			$start->execute();			
		}
		
		public function autoImagem($cpf,$cod){
			$start = $this->conexao->prepare("
			 select id from user where cpf = ?
			");
			$start->bindValue(1,$cpf);
			$start->execute();
						
			$list_user = $start->fetchALL(PDO::FETCH_CLASS,'User');
			
			if(count($list_user) > 0){
				$start2 = $this->conexao->prepare("
				 update info_candidato set auto_imagem = ?  where id_user = ?
				");
				$start2->bindValue(1,$cod);
				$start2->bindValue(2,$list_user[0]->id);
				$start2->execute();
			}
			
		}
		
		
		public function getUser($id){
			$start = $this->conexao->prepare("
			 select info_candidato.*, user.email from info_candidato 
			 
			 left outer join user on user.id = info_candidato.id_user
			 
			 where info_candidato.id_user = ? 
			");
			$start->bindValue(1,$id);
			$start->execute();
			return $start->fetchALL(PDO::FETCH_CLASS,'InfoCandidato');
		}
		
		public function getListCod(){
			$start = $this->conexao->prepare("
			select distinct auto_imagem from info_candidato where auto_imagem is not null
			");
			$start->execute();
			return $start->fetchALL(PDO::FETCH_CLASS,'InfoCandidato');
		}				
		
		public function getContValidaUser($id){
			$start = $this->conexao->prepare("
			 select * from info_candidato where id_user = ? and nome is not null and (tel is not null or cel is not null) 
			");
			$start->bindValue(1,$id);
			$start->execute();
			return $start->rowCount();
		}
		
		public function excluir($id){
			$start = $this->conexao->prepare("
			delete from info_candidato where id_user = ?
			");
			$start->bindValue(1,$id);
			$start->execute();			
		}	
		
		
		public function getFiltro($infoCandidato,$cnh,$idioma,$dia,$turno,$curso,$area,$check_imagem){
			
			$sql = "select distinct info_candidato.id, info_candidato.*, user.email from info_candidato 
			
			left outer join user on user.id = info_candidato.id_user ";
									
			if(strlen($curso->nivel) > 0 || strlen($curso->escola) > 0){
				$sql .= " left outer join curso on curso.id_user = info_candidato.id_user ";
			}
			
			if(strlen($area[0]) > 0){
				$sql .= " left outer join experiencia on experiencia.id_user = info_candidato.id_user ";
			}
			
			if(strlen($area[1]) > 0){
				$sql .= " left outer join area_interesse on area_interesse.id_user = info_candidato.id_user ";
			}
			
			
			$sql .= " where info_candidato.id is not null and info_candidato.nome is not null ";
			
			if(strlen($infoCandidato->nome) > 0){
				$sql .= " and info_candidato.nome like '%$infoCandidato->nome%'";
			}
			
			if(strlen($infoCandidato->informatica) > 0){
				$sql .= " and info_candidato.informatica = '".$infoCandidato->informatica."'";
			}
			
			if(strlen($infoCandidato->cidade) > 0){
				$sql .= " and info_candidato.cidade = '".$infoCandidato->cidade."'";
			}
									
			if(count($cnh) > 0){
				foreach($cnh as $c){
					$sql .= " and info_candidato.cnh like '%$c%'";
				}
			}
			
			if(count($idioma) > 0){
				foreach($idioma as $i){
					$sql .= " and info_candidato.idioma like '%$i%'";
				}
			}
			
			if(count($dia) > 0){
				foreach($dia as $d){
					$sql .= " and info_candidato.dia_disponivel like '%$d%'";
				}
			}
			
			if(count($turno) > 0){
				foreach($turno as $t){
					$sql .= " and info_candidato.turno_disponivel like '%$t%'";
				}
			}
			
			if(strlen($curso->escola) > 0){
				$sql .= " and curso.id is not null and curso.escola like '%$curso->escola%'  "; 			
			}
			
			if(strlen($curso->nivel) > 0){
				$sql .= " and curso.id is not null and curso.nivel = '".$curso->nivel."' "; 
				
				if(strlen($curso->curso) > 0){
					$sql .= " and curso.curso = '".$curso->curso."' ";
				}				
			}
			
			if(strlen($area[0]) > 0){
				$sql .= " and experiencia.cargo = '".$area[0]."' and experiencia.id is not null";
			}
			
			if(strlen($area[1]) > 0){
				$sql .= " and area_interesse.cargo = '".$area[1]."' ";
			}

			if(strlen($area[2]) > 0){
				$sql .= " and area_interesse.funcao = '".$area[2]."' ";
			}
			
			if(count($check_imagem) > 0){
				
				$sql .= " and info_candidato.auto_imagem in (";
				
				$in = "";
				
				foreach($check_imagem as $imagem){
					$in .= "'".$imagem."'".",";
				}
				
				// retirando a ultima virgula
				$in = substr($in,0,strlen($in)-1);
				
				$sql .= $in;
				
				$sql .=") ";
			}
					
			$sql .= " order by info_candidato.id desc limit 0,300";
			
			$start = $this->conexao->prepare($sql);
			$start->execute();
			return $start->fetchALL(PDO::FETCH_CLASS,'InfoCandidato');		
			
		}
		
		public function getFiltroAdmin($infoCandidato,$curso,$check_imagem){
			
			$sql = "select distinct info_candidato.id, info_candidato.*, user.email from info_candidato 
			
					left outer join user on user.id = info_candidato.id_user ";
			
			if(strlen($curso->nivel) > 0 || strlen($curso->escola) > 0){
				$sql .= " left outer join curso on curso.id_user = info_candidato.id_user ";
			}
			
			$sql .= "where info_candidato.id is not null and info_candidato.nome is not null ";
			
			if(strlen($infoCandidato->email) > 0){
				$sql .= " and user.email = '".$infoCandidato->email."'";
			}
			
			if(strlen($infoCandidato->cpf) > 0){
				$sql .= " and user.cpf = '".$infoCandidato->cpf."'";
			}
			
			if(strlen($infoCandidato->nome) > 0){
				$sql .= " and info_candidato.nome like '%$infoCandidato->nome%'";
			}
									
			if(strlen($infoCandidato->cel) > 0){
				$sql .= " and info_candidato.cel = '".$infoCandidato->cel."'";
			}
			
			if(strlen($infoCandidato->tel) > 0){
				$sql .= " and info_candidato.tel = '".$infoCandidato->tel."'";
			}
			
			if(strlen($curso->escola) > 0){
				$sql .= " and curso.id is not null and curso.escola like '%$curso->escola%'  "; 			
			}
			
			if(strlen($curso->nivel) > 0){
				$sql .= " and curso.id is not null and curso.nivel = '".$curso->nivel."' "; 
				
				if(strlen($curso->curso) > 0){
					$sql .= " and curso.curso = '".$curso->curso."' ";
				}				
			}
			
			if(count($check_imagem) > 0){
				
				$sql .= " and auto_imagem in (";
				
				$in = "";
				
				foreach($check_imagem as $imagem){
					$in .= "'".$imagem."'".",";
				}
				
				// retirando a ultima virgula
				$in = substr($in,0,strlen($in)-1);
				
				$sql .= $in;
				
				$sql .=") ";
			}
						
			$sql .= " order by info_candidato.id desc limit 0,500";
			
			$start = $this->conexao->prepare($sql);
			$start->execute();
			return $start->fetchALL(PDO::FETCH_CLASS,'InfoCandidato');		
			
		}
		
		
		
		public function mask($mask,$str){

			$str = str_replace(" ","",$str);
		
			for($i=0;$i<strlen($str);$i++){
				$mask[strpos($mask,"#")] = $str[$i];
			}
		
			return $mask;
		
		}
	}
	
?>