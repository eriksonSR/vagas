<!DOCTYPE HTML>
<?php
	session_start();

	unset($_SESSION['user_reset_senha']);
	unset($_SESSION['codigo_reset_senha']);
?>

<!--
	Prologue 1.2 by HTML5 UP
	html5up.net | @n33co
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html><!-- #BeginTemplate "/Templates/IndexTemplate.dwt" --><!-- DW6 -->
<head>

<title>Bem-vindo</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600" rel="stylesheet" type="text/css" />
<!--[if lte IE 8]><script src="js/html5shiv.js"></script><![endif]-->
<script src="js/request/AjaxRequest.js"></script>
<script src="js/jquery.min.js"></script>
<script src="js/skel.min.js"></script>
<script src="js/skel-panels.min.js"></script>
<script src="js/init.js"></script>
<!-- Mascaras para formulario" -->
<script>
function mascara(o,f){
	v_obj=o
	v_fun=f
	setTimeout("execmascara()",1)
}

function execmascara(){
	v_obj.value=v_fun(v_obj.value)
}

function leech(v){
	v=v.replace(/o/gi,"0")
	v=v.replace(/i/gi,"1")
	v=v.replace(/z/gi,"2")
	v=v.replace(/e/gi,"3")
	v=v.replace(/a/gi,"4")
	v=v.replace(/s/gi,"5")
	v=v.replace(/t/gi,"7")
	return v
}

///////////////////////////////////////////////////


function cpf(v){
	v=v.replace(/\D/g,"")                    //Remove tudo o que não é dígito
	v=v.replace(/(\d{3})(\d)/,"$1.$2")       //Coloca um ponto entre o terceiro e o quarto dígitos
	v=v.replace(/(\d{3})(\d)/,"$1.$2")       //Coloca um ponto entre o terceiro e o quarto dígitos
											 //de novo (para o segundo bloco de números)
	v=v.replace(/(\d{3})(\d{1,2})$/,"$1-$2") //Coloca um hífen entre o terceiro e o quarto dígitos
	return v
}

function cnpj(v){
    v=v.replace(/\D/g,"")                           //Remove tudo o que não é dígito
    v=v.replace(/^(\d{2})(\d)/,"$1.$2")             //Coloca ponto entre o segundo e o terceiro dígitos
    v=v.replace(/^(\d{2})\.(\d{3})(\d)/,"$1.$2.$3") //Coloca ponto entre o quinto e o sexto dígitos
    v=v.replace(/\.(\d{3})(\d)/,".$1/$2")           //Coloca uma barra entre o oitavo e o nono dígitos
    v=v.replace(/(\d{4})(\d)/,"$1-$2")              //Coloca um hífen depois do bloco de quatro dígitos
    return v
}
    

</script>
<noscript>
<link rel="stylesheet" href="css/skel-noscript.css" />
<link rel="stylesheet" href="css/style.css" />
<link rel="stylesheet" href="css/style-wide.css" />
</noscript>
<!--[if lte IE 9]><link rel="stylesheet" href="css/ie9.css" /><![endif]-->
<!--[if lte IE 8]><link rel="stylesheet" href="css/ie8.css" /><![endif]-->
</head><body>

<div id='alerta'> </div>

<!-- Header -->
<div id="header" class="skel-panels-fixed">
  <div class="top">
    <!-- Nav -->
    <nav id="nav">
      <ul>
        <li><a href="#top" id="top-link" class="skel-panels-ignoreHref"><span class="fa fa-home">Sobre a AST</span></a></li>
        
        <li><a href="#cad_candidato" id="cad_candidato-link" class="skel-panels-ignoreHref"><span class="fa fa-lock">Cadastro de candidato</span></a></li>
        
        <li><a href="#cad_empresa" id="cad_empresa-link" class="skel-panels-ignoreHref"><span class="fa fa-lock">Cadastro de empresa</span></a></li>
       
        <li><a href="#acesso" id="acesso-link" class="skel-panels-ignoreHref"><span class="fa fa-gear">Acesse seu cadastro</span></a></li>
        <li><a href="acessoAdmin1.php" class="skel-panels-ignoreHref"><span class="fa fa-gear">Acesse administrador</span></a></li>
      </ul>
    </nav>
  </div>
  
</div>
<!-- Main -->
<div id="main">
  <a  class="image featured"><img src="banner.jpg" alt="" /></a>
  <!-- Intro -->
  <section id="top" class="one">
    <div class="container">      
      <header>
        <h3 class="alt" style="margin-bottom:20px;"><strong>Olá Profissional!</strong></h2>
        <h4>Para acessar às vagas disponíveis faça o seu "Cadastro de candidato."</h4>
        <p style="margin-top:10px;">
       Seja bem-vindo a este espaço, que é reservado para quem busca uma oportunidade no mercado de trabalho. Periodicamente temos vagas para diversas funções.
		</p>
        <p style="margin-top:-10px;">
		Especialista em Recrutamento & Seleção de profissionais temporários e efetivos, a gaúcha AST Facilities conta com experiência de 20 anos e já contratou mais de 141 mil profissionais.
		</p>
        
        <p>Alguns de nossos clientes:</p>
        <a  class="image featured"><img src="banner_2.jpg" alt="" /></a>
        
      </header>
    
    </div>
  </section>
  <!-- Contact -->
  <section id="cad_candidato" class="two">
    <div class="container">
      <header>
        <h2>Cadastro de <u>candidato</u></h2>
      </header>
      <p> Cadastre seu curriculo e venha construir seu futuro conosco!! </p>
      <form onSubmit="return false" id="cadastro_candidato">
        <div class="row half">
          <div class="6u">
            <input type="text" class="text" name="txtEmail" placeholder="E-mail" />
          </div>
          <div class="6u">
            <input type="text" class="text" name="txtCpf" placeholder="CPF" onkeypress='mascara(this,cpf)' maxlength='14'/>
          </div>
        </div>
        <div class="row half">
          <div class="6u">
            <input type="password" class="text" name="txtSenha" placeholder="Senha" />
          </div>
          <div class="6u">
            <input type="password" class="text" name="txtSenha2" placeholder="Digite sua senha novamente" />
          </div>
        </div>
        <!--
        <div class="row half">
            <div class="12u">
                <textarea name="message" placeholder="Message"></textarea>
            </div>
        </div>
        -->
        <div class="row">
          
      	   
          <div class="12u" id='btn_load_1'>
        		Processando...
	      </div>
            
    	  <div class="12u" id="btn_1"> 
                    	                      
          	<a class="button submit"  style="margin-top:10px;" 
            onclick='post("Controller/Cadastro.controller.php?op=1&tipo=1","btn_1","btn_load_1","retorno_1","cadastro_candidato")'>Finalizar o cadastro</a> 
            
            <div id='retorno_1' style="margin-top:15px;"></div> 
            
          </div>
           
        </div>
      </form>
    </div>
  </section>
  
  <section id="cad_empresa" class="three">
    <div class="container">
      <header>
        <h2>Cadastro de <u>empresa</u></h2>
      </header>
      <p> AST Facilities </p>
      <form onSubmit="return false" id="cadastro_empresa">
        <div class="row half">
          <div class="6u">
            <input type="text" class="text" name="txtEmail" placeholder="E-mail" />
          </div>
          <div class="6u">
            <input type="text" class="text" name="txtCnpj" placeholder="CNPJ" onkeypress='mascara(this,cnpj)' maxlength='18'/>
          </div>
        </div>
        <div class="row half">
          <div class="6u">
            <input type="password" class="text" name="txtSenha" placeholder="Senha" />
          </div>
          <div class="6u">
            <input type="password" class="text" name="txtSenha2" placeholder="Digite sua senha novamente" />
          </div>
        </div>
       
        <!--
        <div class="row half">
            <div class="12u">
                <textarea name="message" placeholder="Message"></textarea>
            </div>
        </div>
        -->       
        <div class="row">
          <div class="12u" id='btn_load_2'>
        		Processando...
	      </div>
            
    	  <div class="12u" id="btn_2"> 
			<a class="button submit" style="margin-top:10px;" 
            onclick='post("Controller/Cadastro.controller.php?op=1&tipo=2","btn_2","btn_load_2","retorno_2","cadastro_empresa")'>Finalizar o cadastro</a> 
            
            <div id='retorno_2' style="margin-top:15px;"></div> 
            
          </div>
        </div>
      </form>
    </div>
  </section>
  
  <!-- Portfolio -->
  <section id="acesso" class="four">
    <div class="container">
      <header>
        <h2>Acesse seu painel</h2>
      </header>
      <p><!--Acesse seu Painel e gerencie o seu futuro! Para atualizar seu currículo, acompanhar as vagas

abertas e os processos seletivos que você está participando, basta digitar o cpf e senha 

cadastrados. <br />Boa sorte!-->
	  </p>
      <form onSubmit="return false" id="login_sistema">
        
        <div id='reset_senha' style="margin-bottom:20px;">
               
             <div class="row half">
             
              <div class="12u"> 
                <h4>Selecione o tipo de usuário</h4>
                           
                <input class='check' name='rbFilho' type='radio' value='Sim' onClick='getId("Controller/Login.controller.php?op=1&tipo=candidato","input_acesso")' checked/> Candidato
                &nbsp;&nbsp;
                <input class='check' name='rbFilho' type='radio' value='Não' onClick='getId("Controller/Login.controller.php?op=1&tipo=empresa","input_acesso")'/> Empresa
              </div>
              
             </div>
            
             <div class="row half">
                
               <div class="6u" id='input_acesso'>
                <input type="text" class="text" name="txtLogin" placeholder="CPF" onkeypress='mascara(this,cpf)' maxlength='14'/>
               </div>
               <div class="6u">
                <input type="password" class="text" name="txtSenha" placeholder="Informe sua senha" />
               </div>
                  
             </div>
                      
            <!--
            <div class="row half">
                <div class="12u">
                    <textarea name="message" placeholder="Message"></textarea>
                </div>
            </div>
            -->
                        
          	<div class="row">          	              
                <div class="12u" id='btn_load_3'>
        		Processando...
		        </div>
            
	      	    <div class="12u" id="btn_3"> 
                    <a class="button submit" style="margin-top:10px;" 
                    onclick='post("Controller/Login.controller.php?op=1","btn_3","btn_load_3","retorno_3","login_sistema")'>Efetuar o acesso</a> 
                    
                    <div id='retorno_3' style="margin-top:15px;"></div>                    
                </div>
                                
                <div class="12u">
                   <a onClick='getId("Controller/Login.controller.php?op=2","reset_senha")' >Esqueceu sua senha?</a>
                </div>             
         	</div>
            
         </div>
         
      </form>
    </div>
  </section>
</div>
<!-- Footer -->
<div id="footer">
  <!-- Copyright -->
  <div class="copyright">    
    <ul class="menu">
      <li>AST Facilities.</li>
    </ul>
  </div>
</div>
</body>
<!-- #EndTemplate --></html>
