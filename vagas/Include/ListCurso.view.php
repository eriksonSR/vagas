<style>
#dataTables-example_length{
	display:none;
	
}

#popup_curso { 	 	
	display: none;
	position: absolute;
 	top:2%; 
	
	background:#FFFFFF;  
	z-index:100; /* Layering ( on-top of others), if you have lots of layers: I just maximized, you can change it yourself */
	/* additional features, can be omitted */
	border:2px solid #CCCCCC;  	
	padding:15px;  
	font-size:15px;  
	-moz-box-shadow: 0 0 5px #CCCCCC;
	-webkit-box-shadow: 0 0 5px #CCCCCC;
	box-shadow: 0 0 2px #999999;
}

@media screen and (min-width:975px){
	#popup_curso { 	
		left: 30%;			
		width:60%; 
	}
}

@media screen and (max-width:975px){
	#popup_curso { 	 	
		left: 5%;
		width:90%; 
	}
}
</style>

<script type="text/javascript">
	
	$(document).ready( function() {		
		
		// When site loaded, load the Popupbox First
		loadPopupBox();
	
		$('#popupBoxClose').click( function() {			
			unloadPopupBox();									  			 
		});
		
		function unloadPopupBox() {	// TO Unload the Popupbox
			$('#popup_curso').fadeOut("slow");	
			$("#popup_curso").remove(); 									 		
		}	
		
		function loadPopupBox() {	// To Load the Popupbox				
			$('#popup_curso').fadeIn("slow");
		}
		/**********************************************************/

	});
</script>

<script>
// Centraliza a div de poup no centro da tela independente do scroll
function id( el ){
	return document.getElementById( el );
}

var marginTopoInicial;//criando variavel de escopo global

calcMarginTop();

//levando em conta o scroll
id('popup_curso').style.marginTop = marginTopoInicial+window.pageYOffset+'px';

function calcMarginTop()
{
	var pop = id('popup_curso');
	var marginTopo = parseInt( getMarginTop( pop ) );
	var marginBaixo = parseInt( getMarginBottom( pop ) );

	if( pop.offsetTop < -1 )
		pop.style.marginTop = '-'+( parseInt( pop.offsetTop ) - marginTopo )+'px';
	else if( marginTopo!=marginBaixo )
	pop.style.marginTop = '-248px';
}

function getMarginTop( el )
{
	if( window.getComputedStyle )
		return document.defaultView.getComputedStyle(el, null).marginTop;
	else if( el.currentStyle )
		return el.currentStyle['marginTop'];
}
function getMarginBottom( el )
{
	if( window.getComputedStyle )
		return document.defaultView.getComputedStyle(el, null).marginBottom;
	else if( el.currentStyle )
		return el.currentStyle['marginBottom'];
}
//
</script>
<style>
#dataTables-example3_length{
	margin-bottom:-60px;
}
</style>
<?php	
	echo "<div id='popup_curso'>                    
			 <a id='popupBoxClose'><u>Fechar</u></a>
			 
			 <div style='margin-top:20px;' id='retorno_curso'>                    

               <table id='dataTables-example'>
					<caption><h2> Minhas formações acadêmicas </h2></caption>";
										
					if(isset($aviso)){
						echo "<caption style='margin-top:15px; margin-bottom:15px;'><h3>".utf8_encode($aviso)."</h3></caption>";
					}
					
	echo "			<thead>";
					
					if(count($list_curso) > 0){
						echo "<tr>
								<th>Nível</th>
								<th>Curso</th>
								<th>Escola</th>
								<th>Descrição</th>
								<th>Data inicio</th>
								<th>Data termino</th>
								<th>Ações</th>
	 						  </tr>";						
					}else{
						echo "<tr>
								<th>Aviso</th>
	 						  </tr>";
					}						
												
	echo "			</thead>
					<tbody>";
					if(count($list_curso) > 0){
						foreach($list_curso as $list){
							echo "<tr>
									<th>".$list->nivel."</th>
									<th>".$list->curso."</th>
									<th>".$list->escola."</th>";
									if(strlen($list->descricao) > 0){
										echo "<th>".$list->descricao."</th>";
									}else{
										echo "<th> - - - </th>";
									}
									
									if(strlen($list->data_inicio) > 0){
										echo "<th>".date('d/m/Y',strtotime($list->data_inicio))."</th>";
									}else{
										echo "<th> --- </th>";
									}									
							
									if(strlen($list->data_fim) > 0){
										echo "<th>".date('d/m/Y',strtotime($list->data_fim))."</th>";
									}else{
										echo "<th> --- </th>";
									}
									?>
									<th>   <a onClick='getId("Controller/Curso.controller.php?op=<?php echo sha1(2) ?>&id=<?php echo base64_encode($list->id) ?>","retorno_curso")'>&nbsp;&nbsp;<u>Excluir</u></a> 
                                    
                                    &nbsp;&nbsp;|&nbsp;&nbsp;
                                    <a onClick='getId("Controller/Curso.controller.php?op=<?php echo sha1(3) ?>&id=<?php echo base64_encode($list->id) ?>","edit_formacao")'>&nbsp;&nbsp;<u>Editar</u></a> 
                                    </th>
                                    
                                    <?php
							echo "</tr>";		
						}						
					}else{
						echo "<tr>
								<th>Não há cursos cadastrados.</th>
							  </tr>";
							
					}
	echo "			</tbody>
				 </table>
			  </div> 			
	  	   </div>";

?>

<script>
    $(document).ready(function() {
        $('#dataTables-example').dataTable();
    });
</script>
