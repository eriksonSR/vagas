<header>
<h2>Gestor de candidatos</h2>
<p>Procure o candidato que deseja acessar</p>
</header>
<form onSubmit="return false" id="get_candidato">

<div class="row half">   
     <div class="12u" id='input_full2'> <b>Auto imagem</b> <br />         
               
       <?php
       $infoCandidatoDAO = new InfoCandidatoDAO();
       
       $list_imagem = $infoCandidatoDAO->getListCod();
       
       $cont = 1;
       
       foreach($list_imagem as $list){		  
           echo "<input class='check' name='auto_".$list->auto_imagem."' type='checkbox' value='auto_".$list->auto_imagem."' />".$list->auto_imagem." &nbsp;";
           
           if($cont == 7){
                echo "<br />";
                
                $cont = 0;
           }
           
           $cont++;
       }
    
       ?>        
     
     </div> 
  
</div>
             
<div class="row half">   
   <div class="6u">
    <input type="text" class="text" name="txtCpf" placeholder="CPF" onkeypress='mascara(this,cpf)' maxlength='14'/>
  </div> 
       
  <div class="6u">
    <input type="text" class="text" name="txtEscola" placeholder="Escola" />
  </div>
</div>

<div class="row half">

  <div class="6u">
    <select name="cbNivel" onChange='cadastro("Controller/Curso.controller.php?op=3","curso_input","get_candidato")'>
        <option value=''> Nível do curso </option>                
        <option value='Ensino médio'>Ensino médio</option>
        <option value='Profissionalizante'>Profissionalizante</option>
        <option value='Técnicos'>Técnicos</option>
        <option value='Superior'>Superior</option>
        <option value='Pós'>Pós</option>                
    </select>
  </div>
  
  <div class="6u" id='curso_input'>
    <input type="text" class="text" placeholder="Selecione o nível do curso" disabled/>
  </div>

</div>

<div class="row half">        
  <div class="6u">
    <input type="text" class="text" name="txtNome" placeholder="Nome" />
  </div>

  <div class="6u">
    <input type="text" class="text" name="txtEmail" placeholder="E-mail" />
  </div>       
</div>		

<div class="row half">        
  <div class="6u">
    <input onkeypress='mascara(this,telefone)' maxlength='14' type="text" class="text" name="txtTel" placeholder="Telefone" />
  </div>

  <div class="6u">
    <input onkeypress='mascara(this,telefone)' maxlength='14' type="text" class="text" name="txtCel" placeholder="Celular" />
  </div>       
</div>		

<div class="row">
    <div class="12u" id='btn_load_2'>
      Processando...
    </div>
      
    <div class="12u" id="btn_2">  
          <a class="button submit" onclick='post("Controller/Admin.controller.php?op=1","btn_2","btn_load_2","gestor_candidato","get_candidato")'>Encontrar candidato</a>
    </div>           
</div>

</form> 
