<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>

<?php
echo "		<table>
				<caption><h2> Minhas experiencias profissionais. </h2></caption>";
				
echo "			<thead>";
				
				if(count($list_experiencia) > 0){
					echo "<tr>
							<th>Empresa</th>
							<th>Cargo</th>
							<th>Descrição</th>
							<th>Data inicio</th>
							<th>Data termino</th>
							<th>Ações</th>
						  </tr>";						
				}else{
					echo "<tr>
							<th>Aviso</th>
						  </tr>";
				}						
											
echo "			</thead>
				<tbody>";
				if(count($list_experiencia) > 0){
					foreach($list_experiencia as $list){
						echo "<tr>
								<th>".$list->empresa."</th>
								<th>".$list->cargo."</th>";
								if(strlen($list->descricao) > 0){
									echo "<th>".$list->descricao."</th>";
								}else{
									echo "<th> - - - </th>";
								}
						echo "	<th>".date('d/m/Y',strtotime($list->data_inicio))."</th>
								<th>".date('d/m/Y',strtotime($list->data_fim))."</th>";
								?>
								<th>   <a onClick='getId("Controller/Experiencia.controller.php?op=<?php echo sha1(2) ?>&id=<?php echo base64_encode($list->id) ?>","retorno_experiencia")'>&nbsp;&nbsp;<u>Excluir</u>&nbsp;&nbsp;</a> </th>
								<?php
						echo "</tr>";		
					}						
				}else{
					echo "<tr>
							<th>Não há experiencias cadastradas.</th>
						  </tr>";
						
				}
echo "			</tbody>
			 </table>";

?>
</body>
</html>
