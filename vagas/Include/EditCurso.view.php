<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>

<br />
<header>
<h2>Formação acadêmica</h2>
<p>O estágio é o complemento da sua trajetória acadêmica. <br />Cadastre todas as suas formações acadêmicas que poderão ajudar a garantir a vaga perfeita para o seu perfil.</p>
</header>

<form onSubmit="return false" id="form_edit_curso">

<div class="row half">
  <div class="12u">
    <select name="cbNivel" onChange='altNivelCurso("Controller/Curso.controller.php?op=2","curso_input","data_curso","edit_curso")'>
        <option value=''> Nível do curso </option>                
        <option value='Ensino médio'>Ensino médio</option>
        <option value='Profissionalizante'>Profissionalizante</option>
        <option value='Técnicos'>Técnicos</option>
        <option value='Superior'>Superior</option>
        <option value='Pós'>Pós</option>                
    </select>
    
    <script> 
		// Setando qual seleção da combo vai ficar marcada. (passa o valor dela) 
		form_edit_curso.cbNivel.value = "<?php echo $curso[0]->nivel ?>";  
	</script> 
        
  </div>
</div>

<div class="row half">
  
  
  <div class="6u" id='curso_input'>
    <?php
	if($curso[0]->nivel == "Ensino médio"){
	?>
	<input type="text" class="text" name="cbCurso" placeholder="Informe sua série" value="<?php echo $curso[0]->curso ?>"/>
	<?php
	}else if($curso[0]->nivel == "Profissionalizante"){
	?>
	<select name="cbCurso">
	
	  <option value="">Selecione o curso</option>
	  <option value="3D Studio Max">3D Studio Max</option>
	  <option value="Acordeon">Acordeon</option>
	  <option value="ActionScript MX">ActionScript MX</option>
	  <option value="Administrativo">Administrativo</option>
	  <option value="Aeromoça">Aeromoça</option>
	  <option value="Agente de turismo">Agente de turismo</option>
	  <option value="Artesanato em madeira">Artesanato em madeira</option>
	  <option value="Artes Plásticas">Artes Plásticas</option>
	  <option value="Assesoria fiscal e tributária">Assesoria fiscal e tributária</option>
	  <option value="ATP Poser 4">ATP Poser 4</option>
	  <option value="Autocad">Autocad</option>
	  <option value="Auxiliar clínica veterinária">Auxiliar clínica veterinária</option>
	  <option value="Auxiliar de enfermagem">Auxiliar de enfermagem</option>
	  <option value="Auxiliar de jardim de infância">Auxiliar de jardim de infância</option>
	  <option value="Auxiliar de odontologia">Auxiliar de odontologia</option>
	  <option value="Avaliação de empresas">Avaliação de empresas</option>
	  <option value="Cabeleireiro">Cabeleireiro</option>
	  <option value="Carpintaria">Carpintaria</option>
	  <option value="Cenografia">Cenografia</option>
	  <option value="Cobranças por telefone">Cobranças por telefone</option>
	  <option value="Colorimetria aplicada">Colorimetria aplicada</option>
	  <option value="Construção predial">Construção predial</option>
	  <option value="Consultor em segurança Microsoft 2003">Consultor em segurança Microsoft 2003</option>
	  <option value="Contabilidade analítica">Contabilidade analítica</option>
	  <option value="Contabilidade básica">Contabilidade básica</option>
	  <option value="Contabilidade de custos">Contabilidade de custos</option>
	  <option value="Contabilidade geral">Contabilidade geral</option>
	  <option value="Contabilidade para executivos">Contabilidade para executivos</option>
	  <option value="Contabilidade para servidores">Contabilidade para servidores</option>
	  <option value="Contabilidade pública">Contabilidade pública</option>
	  <option value="Contabilidade tributária">Contabilidade tributária</option>
	  <option value="Controle de gestão">Controle de gestão</option>
	  <option value="Corte e costura">Corte e costura</option>
	  <option value="Costura em couro">Costura em couro</option>
	  <option value="Criação/gestão de cursos de e-learning">Criação/gestão de cursos de e-learning</option>
	  <option value="Culinária">Culinária</option>
	  <option value="Curso de língua inglesa">Curso de língua inglesa</option>
	  <option value="Curso de línguas estrangeiras">Curso de línguas estrangeiras</option>
	  <option value="Dança">Dança</option>
	  <option value="Decoração">Decoração</option>
	  <option value="Desenho">Desenho</option>
	  <option value="Desenho 3Dstudio Max-2003">Desenho 3Dstudio Max-2003</option>
	  <option value="Desenho de moda">Desenho de moda</option>
	  <option value="Desenho em quadrinhos">Desenho em quadrinhos</option>
	  <option value="Desenho gráfico">Desenho gráfico</option>
	  <option value="Desenvolvimento do poder mental">Desenvolvimento do poder mental</option>
	  <option value="Design de interiores">Design de interiores</option>
	  <option value="Dietética e nutrição">Dietética e nutrição</option>
	  <option value="Direito comercial">Direito comercial</option>
	  <option value="Dreamweaver MX">Dreamweaver MX</option>
	  <option value="Educação meio-ambiental">Educação meio-ambiental</option>
	  <option value="EJB">EJB</option>
	  <option value="Eletricista">Eletricista</option>
	  <option value="Eletrônica e micro-eletrônica">Eletrônica e micro-eletrônica</option>
	  <option value="Encanador">Encanador</option>
	  <option value="Energia solar">Energia solar</option>
	  <option value="Especialização em audiovisuais">Especialização em audiovisuais</option>
	  <option value="Especialização em Comércio eletrônico">Especialização em Comércio eletrônico</option>
	  <option value="Especialização em Contabilidade Gerencial">Especialização em Contabilidade Gerencial</option>
	  <option value="Especialização em desenho em movimento">Especialização em desenho em movimento</option>
	  <option value="Especialização em direção comercial e marketing">Especialização em direção comercial e marketing</option>
	  <option value="Especialização em direito de novas tecnologias">Especialização em direito de novas tecnologias</option>
	  <option value="Especialização em prótese dentária">Especialização em prótese dentária</option>
	  <option value="Estética">Estética</option>
	  <option value="Fireworks MX">Fireworks MX</option>
	  <option value="Fiscalização de operações financeiras">Fiscalização de operações financeiras</option>
	  <option value="Flash MX">Flash MX</option>
	  <option value="Fotografia">Fotografia</option>
	  <option value="Gestão atuarial e financeira">Gestão atuarial e financeira</option>
	  <option value="Gestão de contas">Gestão de contas</option>
	  <option value="Gestão de tesouraria">Gestão de tesouraria</option>
	  <option value="Gestão em eventos culturais e desportivos">Gestão em eventos culturais e desportivos</option>
	  <option value="Gestão financeira de pequenas empresas">Gestão financeira de pequenas empresas</option>
	  <option value="Gestão meio-ambiental">Gestão meio-ambiental</option>
	  <option value="Guitarra">Guitarra</option>
	  <option value="Imposto de renda">Imposto de renda</option>
	  <option value="Impostos de produtos">Impostos de produtos</option>
	  <option value="Informática musical">Informática musical</option>
	  <option value="Iniciação à música">Iniciação à música</option>
	  <option value="Instalação/manutenção de PC's">Instalação/manutenção de PC's</option>
	  <option value="Instalador de gás">Instalador de gás</option>
	  <option value="Introdução ao Windows, Word e Internet">Introdução ao Windows, Word e Internet</option>
	  <option value="J2EE">J2EE</option>
	  <option value="JAVA 2003 Professional">JAVA 2003 Professional</option>
	  <option value="Locução e sonoplastia">Locução e sonoplastia</option>
	  <option value="Manutenção de ar condicionado">Manutenção de ar condicionado</option>
	  <option value="Master Web">Master Web</option>
	  <option value="Mecânico de motos">Mecânico de motos</option>
	  <option value="Mecânico do automóvel">Mecânico do automóvel</option>
	  <option value="Modos de pagamentos">Modos de pagamentos</option>
	  <option value="Montagem de cinema e produção TV">Montagem de cinema e produção TV</option>
	  <option value="Motores a diesel">Motores a diesel</option>
	  <option value="MS Project">MS Project</option>
	  <option value="Normas internacionais de contabilidade">Normas internacionais de contabilidade</option>
	  <option value="Oficina/chapista">Oficina/chapista</option>
	  <option value="Pedreiro">Pedreiro</option>
	  <option value="Photoshop 6.0">Photoshop 6.0</option>
	  <option value="PHP e MySQL">PHP e MySQL</option>
	  <option value="Piano">Piano</option>
	  <option value="Pigmentos e corantes">Pigmentos e corantes</option>
	  <option value="Pintura">Pintura</option>
	  <option value="Planejamento fiscal internacional">Planejamento fiscal internacional</option>
	  <option value="Produção multimídia">Produção multimídia</option>
	  <option value="Projetista mecânico">Projetista mecânico</option>
	  <option value="Proteção Civil">Proteção Civil</option>
	  <option value="Recepcionista/Telefonista">Recepcionista/Telefonista</option>
	  <option value="Roteiro de cinema e TV">Roteiro de cinema e TV</option>
	  <option value="Segurança e direito do trabalho">Segurança e direito do trabalho</option>
	  <option value="Serralheiro">Serralheiro</option>
	  <option value="Sistemas micro-informáticos">Sistemas micro-informáticos</option>
	  <option value="Soldador">Soldador</option>
	  <option value="Supervisor de obras">Supervisor de obras</option>
	  <option value="Teatro e espaço cênico">Teatro e espaço cênico</option>
	  <option value="Teclado">Teclado</option>
	  <option value="Técnico de alimentos">Técnico de alimentos</option>
	  <option value="Técnico em plásticos">Técnico em plásticos</option>
	  <option value="Técnico em tratamento de águas">Técnico em tratamento de águas</option>
	  <option value="Técnico meio-ambiente">Técnico meio-ambiente</option>
	  <option value="Telemarketing">Telemarketing</option>
	  <option value="Tradutor / Intérprete">Tradutor / Intérprete</option>
	  <option value="Tunning">Tunning</option>
	  <option value="Vigilante de segurança">Vigilante de segurança</option>
	  <option value="XML">XML</option>
	  
	</select>
	
	<?php
	}else if($curso[0]->nivel == "Técnicos"){
	?>
	
	<select name="cbCurso">
	
	  <option value="">Selecione o curso</option>
	  <option value="Administração">Administração</option>
	  <option value="Aeroportuário">Aeroportuário </option>
	  <option value="Agenciamento de viagem">Agenciamento de viagem</option>
	  <option value="Agricultura">Agricultura</option>
	  <option value="Agrimensura">Agrimensura </option>
	  <option value="Agroecologia">Agroecologia</option>
	  <option value="Agroindústria">Agroindústria</option>
	  <option value="Agronegócio">Agronegócio</option>
	  <option value="Agropecuária">Agropecuária</option>
	  <option value="Alimentos">Alimentos</option>
	  <option value="Análises clínicas">Análises clínicas</option>
	  <option value="Análises químicas">Análises químicas</option>
	  <option value="Apicultura">Apicultura </option>
	  <option value="Aquicultura">Aquicultura</option>
	  <option value="Arte circense">Arte circense</option>
	  <option value="Arte dramática">Arte dramática</option>
	  <option value="Artesanato">Artesanato </option>
	  <option value="Artes gráficas">Artes gráficas</option>
	  <option value="Artes visuais">Artes visuais</option>
	  <option value="Atividades físicas e desportivas">Atividades físicas e desportivas</option>
	  <option value="Automação industrial">Automação industrial</option>
	  <option value="Biblioteconomia">Biblioteconomia</option>
	  <option value="Calçados">Calçados </option>
	  <option value="Canto">Canto</option>
	  <option value="Celulose e papel">Celulose e papel </option>
	  <option value="Cerâmica">Cerâmica</option>
	  <option value="Citopatologia">Citopatologia</option>
	  <option value="Comércio">Comércio </option>
	  <option value="Comércio exterior">Comércio exterior</option>
	  <option value="Composição e arranjo">Composição e arranjo</option>
	  <option value="Comunicação visual">Comunicação visual</option>
	  <option value="Confeitaria">Confeitaria </option>
	  <option value="Conservação e restauro">Conservação e restauro</option>
	  <option value="Construção naval">Construção naval</option>
	  <option value="Contabilidade">Contabilidade</option>
	  <option value="Controle ambiental">Controle ambiental</option>
	  <option value="Cooperativismo">Cooperativismo </option>
	  <option value="Cozinha">Cozinha</option>
	  <option value="Curtimento">Curtimento</option>
	  <option value="Dança">Dança</option>
	  <option value="Departamento pessoal">Departamento pessoal</option>
	  <option value="Desenho">Desenho</option>
	  <option value="Desenho de construção civil">Desenho de construção civil</option>
	  <option value="Design de embalangens">Design de embalangens</option>
	  <option value="Design de interiores">Design de interiores</option>
	  <option value="Design de joias">Design de joias</option>
	  <option value="Design de móveis">Design de móveis</option>
	  <option value="Design gráfico">Design gráfico</option>
	  <option value="Documentação musical">Documentação musical</option>
	  <option value="Edificações">Edificações</option>
	  <option value="Eletroeletrônica">Eletroeletrônica</option>
	  <option value="Eletromecânica">Eletromecânica</option>
	  <option value="Eletrônica">Eletrônica</option>
	  <option value="Eletrotécnica">Eletrotécnica</option>
	  <option value="Enfermagem">Enfermagem</option>
	  <option value="Equipamentos biomédicos">Equipamentos biomédicos</option>
	  <option value="Estética">Estética</option>
	  <option value="Estradas">Estradas</option>
	  <option value="Eventos">Eventos</option>
	  <option value="Fabricação de instrumentos musicais">Fabricação de instrumentos musicais</option>
	  <option value="Fabricação mecânica">Fabricação mecânica</option>
	  <option value="Farmácia">Farmácia</option>
	  <option value="Finanças">Finanças</option>
	  <option value="Florestas">Florestas</option>
	  <option value="Fotografia">Fotografia</option>
	  <option value="Geodésia e cartografia">Geodésia e cartografia</option>
	  <option value="Geologia">Geologia </option>
	  <option value="Geoprocessamento">Geoprocessamento</option>
	  <option value="Gerência em saúde">Gerência em saúde</option>
	  <option value="Gráfica">Gráfica</option>
	  <option value="Guia de turismo">Guia de turismo</option>
	  <option value="Hidrologia">Hidrologia</option>
	  <option value="Hospedagem">Hospedagem</option>
	  <option value="Imagem pessoal">Imagem pessoal</option>
	  <option value="Imobilizações ortopédicas">Imobilizações ortopédicas</option>
	  <option value="Impressão gráfica">Impressão gráfica</option>
	  <option value="Impressão offset">Impressão offset </option>
	  <option value="Industrial">Industrial</option>
	  <option value="Informática">Informática</option>
	  <option value="Informática para internet">Informática para internet</option>
	  <option value="Infraestrutura escolar">Infraestrutura escolar</option>
	  <option value="Instrumentador cirúrgico">Instrumentador cirúrgico</option>
	  <option value="Instrumento musical">Instrumento musical</option>
	  <option value="Lazer">Lazer</option>
	  <option value="Logística">Logística</option>
	  <option value="Magistério">Magistério</option>
	  <option value="Manutenção automotiva">Manutenção automotiva</option>
	  <option value="Manutenção de aeronaves">Manutenção de aeronaves</option>
	  <option value="Manutenção e suporte em informática">Manutenção e suporte em informática</option>
	  <option value="Marketing">Marketing</option>
	  <option value="Massoterapia">Massoterapia</option>
	  <option value="Mecânica">Mecânica</option>
	  <option value="Mecatrônica">Mecatrônica</option>
	  <option value="Meio ambiente">Meio ambiente</option>
	  <option value="Metalurgia">Metalurgia</option>
	  <option value="Meteorologia">Meteorologia</option>
	  <option value="Mineração">Mineração</option>
	  <option value="Modelagem do vestuário">Modelagem do vestuário</option>
	  <option value="Móveis">Móveis</option>
	  <option value="Multimídia">Multimídia</option>
	  <option value="Nutrição e dietética">Nutrição e dietética</option>
	  <option value="Óptica">Óptica</option>
	  <option value="Paisagismo">Paisagismo</option>
	  <option value="Panificação">Panificação </option>
	  <option value="Petróleo e gás">Petróleo e gás</option>
	  <option value="Petroquímica">Petroquímica</option>
	  <option value="Plásticos">Plásticos</option>
	  <option value="Portos">Portos </option>
	  <option value="Pré-impressão">Pré-impressão</option>
	  <option value="Previdência social">Previdência social</option>
	  <option value="Processamento de pescado">Processamento de pescado</option>
	  <option value="Produção de áudio e vídeo">Produção de áudio e vídeo</option>
	  <option value="Produção de moda">Produção de moda</option>
	  <option value="Programação de jogos digitais">Programação de jogos digitais</option>
	  <option value="Prótese dentária">Prótese dentária</option>
	  <option value="Publicidade">Publicidade</option>
	  <option value="Qualidade">Qualidade</option>
	  <option value="Química">Química</option>
	  <option value="Rádio e televisão">Rádio e televisão</option>
	  <option value="Radiologia">Radiologia</option>
	  <option value="Recursos humanos">Recursos humanos</option>
	  <option value="Recursos minerais">Recursos minerais </option>
	  <option value="Recursos pesqueiros">Recursos pesqueiros </option>
	  <option value="Redes de computadores">Redes de computadores</option>
	  <option value="Refrigeração e climatização">Refrigeração e climatização</option>
	  <option value="Regência">Regência</option>
	  <option value="Registros e informações em saúde">Registros e informações em saúde</option>
	  <option value="Saneamento">Saneamento </option>
	  <option value="Saúde">Saúde</option>
	  <option value="Saúde bucal">Saúde bucal</option>
	  <option value="Secretariado">Secretariado</option>
	  <option value="Secretaria escolar">Secretaria escolar</option>
	  <option value="Segurança do trabalho">Segurança do trabalho</option>
	  <option value="Serviços de condomínio">Serviços de condomínio</option>
	  <option value="Serviços de restaurante e bar">Serviços de restaurante e bar</option>
	  <option value="Serviços públicos">Serviços públicos</option>
	  <option value="Sistemas a gás">Sistemas a gás</option>
	  <option value="Sistemas de comutação">Sistemas de comutação</option>
	  <option value="Sistemas de transmissão">Sistemas de transmissão</option>
	  <option value="Tabela de convergência">Tabela de convergência</option>
	  <option value="Tecelagem">Tecelagem</option>
	  <option value="Telecomunicações">Telecomunicações</option>
	  <option value="Tradutor e intérprete">Tradutor e intérprete</option>
	  <option value="Trânsito">Trânsito</option>
	  <option value="Transporte de cargas">Transporte de cargas</option>
	  <option value="Transporte ferroviário">Transporte ferroviário</option>
	  <option value="Transporte rodoviário">Transporte rodoviário</option>
	  <option value="Transportes">Transportes</option>
	  <option value="Vendas">Vendas</option>
	  <option value="Vestuário">Vestuário</option>
	  <option value="Veterinária">Veterinária</option>
	  <option value="Vigilância em saúde">Vigilância em saúde</option>
	  <option value="Viticultura e enologia">Viticultura e enologia</option>
	  <option value="Web design">Web design</option>
	  <option value="Zootecnia">Zootecnia</option>
	  
	</select>
	
	<?php
	}else if($curso[0]->nivel == "Superior"){
	?>
	<select name="cbCurso">
	
	  <option value="">Selecione o curso</option>
	  <option value="Administração de empresas">Administração de empresas</option>
	  <option value="Agrimensura">Agrimensura</option>
	  <option value="Agroecologia">Agroecologia</option>
	  <option value="Agroindústria">Agroindústria</option>
	  <option value="Agronegócio">Agronegócio</option>
	  <option value="Agronomia">Agronomia</option>
	  <option value="Análise e desenvolvimento de sistemas">Análise e desenvolvimento de sistemas</option>
	  <option value="Antropologia social e cultural">Antropologia social e cultural</option>
	  <option value="Aquicultura">Aquicultura</option>
	  <option value="Arqueologia">Arqueologia</option>
	  <option value="Arquitetura e urbanismo">Arquitetura e urbanismo</option>
	  <option value="Arquivologia">Arquivologia</option>
	  <option value="Automação industrial">Automação industrial</option>
	  <option value="Biblioteconomia">Biblioteconomia</option>
	  <option value="Biocombustíveis">Biocombustíveis</option>
	  <option value="Biologia">Biologia</option>
	  <option value="Biomedicina">Biomedicina</option>
	  <option value="Bioquímica">Bioquímica</option>
	  <option value="Cafeicultura">Cafeicultura</option>
	  <option value="Ciências ambientais">Ciências ambientais</option>
	  <option value="Ciências atuariais e financeiras">Ciências atuariais e financeiras</option>
	  <option value="Ciências contábeis">Ciências contábeis</option>
	  <option value="Ciências da computação">Ciências da computação</option>
	  <option value="Ciências da religião">Ciências da religião</option>
	  <option value="Ciências do trabalho">Ciências do trabalho</option>
	  <option value="Ciências políticas">Ciências políticas</option>
	  <option value="Ciências sanitárias">Ciências sanitárias</option>
	  <option value="Ciências sociais">Ciências sociais</option>
	  <option value="Cinema">Cinema</option>
	  <option value="Comércio exterior">Comércio exterior</option>
	  <option value="Comunicação audiovisual">Comunicação audiovisual</option>
	  <option value="Comunicações aeronáuticas">Comunicações aeronáuticas</option>
	  <option value="Construção de Edifícios">Construção de Edifícios</option>
	  <option value="Construção naval">Construção naval</option>
	  <option value="Dança">Dança</option>
	  <option value="Desenho de interiores">Desenho de interiores</option>
	  <option value="Desenho de moda">Desenho de moda</option>
	  <option value="Desenho de produto">Desenho de produto</option>
	  <option value="Desenho gráfico">Desenho gráfico</option>
	  <option value="Desenho industrial">Desenho industrial</option>
	  <option value="Design de Interiores">Design de Interiores</option>
	  <option value="Direito">Direito</option>
	  <option value="Economia">Economia</option>
	  <option value="Educação artística">Educação artística</option>
	  <option value="Educação física">Educação física</option>
	  <option value="Educação social">Educação social</option>
	  <option value="Eletrônica industrial">Eletrônica industrial</option>
	  <option value="Eletrotécnica industrial">Eletrotécnica industrial</option>
	  <option value="Enfermagem">Enfermagem</option>
	  <option value="Engenharia aeronáutica">Engenharia aeronáutica</option>
	  <option value="Engenharia Agrônoma">Engenharia Agrônoma</option>
	  <option value="Engenharia ambiental">Engenharia ambiental</option>
	  <option value="Engenharia cartográfica">Engenharia cartográfica</option>
	  <option value="Engenharia civil">Engenharia civil</option>
	  <option value="Engenharia da computação">Engenharia da computação</option>
	  <option value="Engenharia de alimentos">Engenharia de alimentos</option>
	  <option value="Engenharia de minas">Engenharia de minas</option>
	  <option value="Engenharia de produção/industrial">Engenharia de produção/industrial</option>
	  <option value="Engenharia de telecomunicações">Engenharia de telecomunicações</option>
	  <option value="Engenharia elétrica">Engenharia elétrica</option>
	  <option value="Engenharia eletrônica">Engenharia eletrônica</option>
	  <option value="Engenharia em Montes">Engenharia em Montes</option>
	  <option value="Engenharia geológica">Engenharia geológica</option>
	  <option value="Engenharia mecânica/mecatrônica">Engenharia mecânica/mecatrônica</option>
	  <option value="Engenharia metalúrgica e de materiais">Engenharia metalúrgica e de materiais</option>
	  <option value="Engenharia Naval">Engenharia Naval</option>
	  <option value="Engenharia organizacional">Engenharia organizacional</option>
	  <option value="Engenharia química">Engenharia química</option>
	  <option value="Engenharia técnica agrícola">Engenharia técnica agrícola</option>
	  <option value="Engenharia técnica de desenho industrial">Engenharia técnica de desenho industrial</option>
	  <option value="Engenharia técnica de obras públicas">Engenharia técnica de obras públicas</option>
	  <option value="Engenharia topográfica">Engenharia topográfica</option>
	  <option value="Enologia e viticultura">Enologia e viticultura</option>
	  <option value="Esporte e turismo">Esporte e turismo</option>
	  <option value="Estatística">Estatística</option>
	  <option value="Estudos internacionais e inter-culturais">Estudos internacionais e inter-culturais</option>
	  <option value="Eventos">Eventos</option>
	  <option value="Farmácia e Bioquímica">Farmácia e Bioquímica</option>
	  <option value="Filosofia">Filosofia</option>
	  <option value="Física">Física</option>
	  <option value="Fisioterapia">Fisioterapia</option>
	  <option value="Fonoaudiologia">Fonoaudiologia</option>
	  <option value="Fotografia">Fotografia</option>
	  <option value="Fotointeligência">Fotointeligência</option>
	  <option value="Gastronomia">Gastronomia</option>
	  <option value="Geografia">Geografia</option>
	  <option value="Geologia">Geologia</option>
	  <option value="Gestão ambiental">Gestão ambiental</option>
	  <option value="Gestão de qualidade">Gestão de qualidade</option>
	  <option value="Gestão de Recursos Humanos">Gestão de Recursos Humanos</option>
	  <option value="Gestão e administração pública">Gestão e administração pública</option>
	  <option value="Gestão Financeira">Gestão Financeira</option>
	  <option value="Gestão hospitalar">Gestão hospitalar</option>
	  <option value="Gestão portuária">Gestão portuária</option>
	  <option value="Hidrologia">Hidrologia</option>
	  <option value="História">História</option>
	  <option value="Hotelaria">Hotelaria</option>
	  <option value="Instrumentação e Controle">Instrumentação e Controle</option>
	  <option value="Investigação privada">Investigação privada</option>
	  <option value="Jogos digitais">Jogos digitais</option>
	  <option value="Jornalismo">Jornalismo</option>
	  <option value="Letras">Letras</option>
	  <option value="Letras - Alemão">Letras - Alemão </option>
	  <option value="Letras - Árabe">Letras - Árabe</option>
	  <option value="Letras - Espanhol">Letras - Espanhol</option>
	  <option value="Letras - Francês">Letras - Francês</option>
	  <option value="Letras - Inglês">Letras - Inglês</option>
	  <option value="Letras - Italiano">Letras - Italiano</option>
	  <option value="Letras - Línguas e culturas românicas">Letras - Línguas e culturas românicas</option>
	  <option value="Letras - Literatura e língua portuguesa">Letras - Literatura e língua portuguesa</option>
	  <option value="Lingüística">Lingüística</option>
	  <option value="Logística">Logística</option>
	  <option value="Marketing"> Marketing</option>
	  <option value="Matemática">Matemática</option>
	  <option value="Mecânica de precisão">Mecânica de precisão</option>
	  <option value="Medicina">Medicina</option>
	  <option value="Meteorologia e climatologia">Meteorologia e climatologia</option>
	  <option value="Náutica">Náutica</option>
	  <option value="Negócios imobiliários">Negócios imobiliários</option>
	  <option value="Nutrição">Nutrição</option>
	  <option value="Obstetrícia">Obstetrícia</option>
	  <option value="Oceanografia">Oceanografia</option>
	  <option value="Odontologia">Odontologia</option>
	  <option value="Oftálmica">Oftálmica</option>
	  <option value="Paisagismo">Paisagismo</option>
	  <option value="Papel e Celulose">Papel e Celulose</option>
	  <option value="Pedagogia">Pedagogia</option>
	  <option value="Pedagogia - Educação secundária">Pedagogia - Educação secundária</option>
	  <option value="Pedologia">Pedologia</option>
	  <option value="Petróleo e Gás">Petróleo e Gás</option>
	  <option value="Podologia">Podologia</option>
	  <option value="Polímeros">Polímeros</option>
	  <option value="Prevenção e segurança do trabalho">Prevenção e segurança do trabalho</option>
	  <option value="Prevenção e segurança integral">Prevenção e segurança integral</option>
	  <option value="Processos Gerenciais">Processos Gerenciais</option>
	  <option value="Processos Químicos">Processos Químicos</option>
	  <option value="Produção cultural">Produção cultural</option>
	  <option value="Produção de cachaça">Produção de cachaça</option>
	  <option value="Produção de Cosméticos">Produção de Cosméticos</option>
	  <option value="Produção fonográfica">Produção fonográfica</option>
	  <option value="Produção Industrial">Produção Industrial</option>
	  <option value="Produção joalheira">Produção joalheira</option>
	  <option value="Produção moveleira">Produção moveleira</option>
	  <option value="Produção multimídia">Produção multimídia</option>
	  <option value="Produção pesqueira">Produção pesqueira</option>
	  <option value="Produção sucroalcooleira">Produção sucroalcooleira</option>
	  <option value="Produção têxtil">Produção têxtil</option>
	  <option value="Psicologia">Psicologia</option>
	  <option value="Psicopedagogia">Psicopedagogia</option>
	  <option value="Publicidade e propaganda">Publicidade e propaganda</option>
	  <option value="Química">Química</option>
	  <option value="Radiologia">Radiologia</option>
	  <option value="Refrigeração e Climatização">Refrigeração e Climatização</option>
	  <option value="Relações Internacionais">Relações Internacionais</option>
	  <option value="Relações públicas">Relações públicas</option>
	  <option value="Saneamento Ambiental">Saneamento Ambiental</option>
	  <option value="Secretariado">Secretariado</option>
	  <option value="Serviço social">Serviço social</option>
	  <option value="Silvicultura">Silvicultura</option>
	  <option value="Sistemas de telecomunicações">Sistemas de telecomunicações</option>
	  <option value="Soldagem">Soldagem</option>
	  <option value="Teatro">Teatro</option>
	  <option value="Tecnologia da Informação">Tecnologia da Informação</option>
	  <option value="Telemática">Telemática</option>
	  <option value="Teologia">Teologia</option>
	  <option value="Terapia ocupacional">Terapia ocupacional</option>
	  <option value="Tradução e interpretação">Tradução e interpretação</option>
	  <option value="Transporte aéreo">Transporte aéreo</option>
	  <option value="Transporte marítimo">Transporte marítimo</option>
	  <option value="Turismo">Turismo</option>
	  <option value="Veterinária">Veterinária</option>
	  <option value="Zootécnia">Zootécnia</option>
	  
	</select>
	
	<?php
	}else if($curso[0]->nivel == "Pós"){
	?>
	<select name="cbCurso">
	  <option value="">Selecione o curso</option>
	  <option value="Administração de empresas">Administração de empresas</option>
	  <option value="Arqueologia e restauração">Arqueologia e restauração</option>
	  <option value="Atendimento preventivo">Atendimento preventivo</option>
	  <option value="Ciências médicas">Ciências médicas</option>
	  <option value="Dança-Educação">Dança-Educação</option>
	  <option value="Desenho do espaço do trabalho">Desenho do espaço do trabalho</option>
	  <option value="Desenho e estratégias de comunicação">Desenho e estratégias de comunicação</option>
	  <option value="Desenho e novos formatos expositivos">Desenho e novos formatos expositivos</option>
	  <option value="Desenho e produção gráfica">Desenho e produção gráfica</option>
	  <option value="Direção e administração em recursos humanos">Direção e administração em recursos humanos</option>
	  <option value="Direito">Direito</option>
	  <option value="Economia">Economia</option>
	  <option value="Engenharias">Engenharias</option>
	  <option value="Especialização em contabilidade e auditoria">Especialização em contabilidade e auditoria</option>
	  <option value="Física">Física</option>
	  <option value="Fonoaudiologia">Fonoaudiologia</option>
	  <option value="Gestão e alimentos">Gestão e alimentos</option>
	  <option value="História e sociologia">História e sociologia</option>
	  <option value="Hotelaria e Turismo">Hotelaria e Turismo</option>
	  <option value="Informática">Informática</option>
	  <option value="Ligüística, Letras e Artes">Ligüística, Letras e Artes</option>
	  <option value="Marketing">Marketing</option>
	  <option value="Matemática">Matemática</option>
	  <option value="Mediação de conflitos">Mediação de conflitos</option>
	  <option value="Melhora ambiental de produtos e processos">Melhora ambiental de produtos e processos</option>
	  <option value="Microeletrônica">Microeletrônica</option>
	  <option value="Neuro-psicologia clínica">Neuro-psicologia clínica</option>
	  <option value="Odontologia">Odontologia</option>
	  <option value="Operações logísticas">Operações logísticas</option>
	  <option value="Pedagogia da educação">Pedagogia da educação</option>
	  <option value="Petroquímica">Petroquímica</option>
	  <option value="Pós-graduação desenho e uso em Internet">Pós-graduação desenho e uso em Internet</option>
	  <option value="Psicologia clínica infanto-juvenil">Psicologia clínica infanto-juvenil</option>
	  <option value="Psicologia Social">Psicologia Social</option>
	  <option value="Psicomotricidade">Psicomotricidade</option>
	  <option value="Psico-oncologia">Psico-oncologia</option>
	  <option value="Psiquiatria">Psiquiatria</option>
	  <option value="Publicidade e Propaganda">Publicidade e Propaganda</option>
	  <option value="Química">Química</option>
	  <option value="Reabilitação cognitiva">Reabilitação cognitiva</option>
	  <option value="Relações Internacionais">Relações Internacionais</option>
	  <option value="Serviço social">Serviço social</option>
	  <option value="Técnicas de gestão empresarial">Técnicas de gestão empresarial</option>
	  <option value="Terapia sexual e de casal">Terapia sexual e de casal</option>
	  <option value="Transtornos da linguagem escrita. Dislexia">Transtornos da linguagem escrita. Dislexia</option>
	</select>
	
	<?php
	}else{
	?>
	
	<input type="text" class="text" placeholder="Selecione o nível do curso" disabled/>
	<?php
	}
	
	if($curso[0]->nivel != "Ensino médio"){
		?>
         <script> 
		// Setando qual seleção da combo vai ficar marcada. (passa o valor dela) 
		form_edit_curso.cbCurso.value = "<?php echo $curso[0]->curso ?>";  
		</script> 
        <?php
	}
	?>
  </div>
  
  
  <div class="6u">
    <input type="text" class="text" name="txtEscola" placeholder="Escola" value="<?php echo $curso[0]->escola ?>"/>
  </div>
</div>

<?php
if($curso[0]->nivel != "Ensino médio"){
?>
    <div class="row half" id='data_curso'>
      <div class="6u">
        <input type="text" class="text" name="txtDataInicio" placeholder="Data de início" onkeypress='mascara(this,data)' maxlength='10' value="<?php echo date('d/m/Y',strtotime($curso[0]->data_inicio )) ?>"/>
      </div>
      <div class="6u">
        <input type="text" class="text" name="txtDataFim" placeholder="Data de término" onkeypress='mascara(this,data)' maxlength='10' value="<?php echo date('d/m/Y',strtotime($curso[0]->data_fim )) ?>"/>
      </div>
    </div>
<?php
}
?>

<div class="row half">
    <div class="12u">
        <textarea name="txtMsg" placeholder="Descrição"><?php echo $curso[0]->descricao ?></textarea>
    </div>
</div>

<div class="row">
    <div class="12u">
        <a class="button submit" onclick='altPerfil("Controller/Curso.controller.php?op=4&id=<?php echo $_GET['id'] ?>","alerta","edit_formacao","form_edit_curso")'>Salvar formação acadêmica</a>                
    </div>
    <div class="12u">   
    	<a onClick='getId("Controller/Curso.controller.php?op=4","edit_formacao")'>Cancelar edição</a>
    </div>
</div>

</form> 
        
        
</body>
</html>
