<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
<?php
echo 	"<table>
				<caption><h2> Meus cursos. </h2></caption>
				<thead>";
				
				if(count($list_curso) > 0){
					echo "<tr>
							<th>Nível</th>
							<th>Curso</th>
							<th>Escola</th>
							<th>Descrição</th>
							<th>Data inicio</th>
							<th>Data termino</th>
							<th>Ações</th>
						  </tr>";						
				}else{
					echo "<tr>
							<th>Aviso</th>
						  </tr>";
				}						
											
echo "			</thead>
				<tbody>";
				if(count($list_curso) > 0){
					foreach($list_curso as $list){
						echo "<tr>
								<th>".$list->nivel."</th>
								<th>".$list->curso."</th>
								<th>".$list->escola."</th>";
								if(strlen($list->descricao) > 0){
									echo "<th>".$list->descricao."</th>";
								}else{
									echo "<th> - - - </th>";
								}
						echo "	<th>".date('d/m/Y',strtotime($list->data_inicio))."</th>
								<th>".date('d/m/Y',strtotime($list->data_fim))."</th>";
								?>
								<th>   <a onClick='getId("Controller/Curso.controller.php?op=<?php echo sha1(2) ?>&id=<?php echo base64_encode($list->id) ?>","retorno_curso")'>&nbsp;&nbsp;<u>Excluir</u>&nbsp;&nbsp;</a> </th>
								<?php
						echo "</tr>";		
					}						
				}else{
					echo "<tr>
							<th>Não há cursos cadastrados.</th>
						  </tr>";
						
				}
echo "			</tbody>
			</table>";
?>
</body>
</html>
