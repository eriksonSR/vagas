<style>
#dataTables-example2_length{
	display:none;
	
}
</style>

<header>
 <h2>Empresas encontradas</h2>
 <p>Acesse a empresa que deseja visualizar</p>
</header>

<?php	  

if(count($list_empresa) > 0){
	echo "<table id='dataTables-example2' style='margin-bottom:15px;'>
			<thead>
			  <tr>
				<th>Empresa</th>
				<th>Cnpj</th>
				<th>Ramu de atuação</th>
				<th>&nbsp;</th>
			  </tr>
			</thead>
			<tbody>";
			foreach($list_empresa as $list){
				echo "<tr>
						<th>".$list->nome."</th>
						<th>".$list->cnpj."</th>
						<th>".$list->ramu_atuacao."</th>";						
						?>
						<th>  <a href="Controller/Admin.controller.php?op=<?php echo sha1(2) ?>&id=<?php echo base64_encode($list->id_user) ?>">&nbsp;&nbsp;<u>Explorar</u>&nbsp;&nbsp;</a> </th>
						<?php					
				echo "</tr>";
			}					  
	echo "	</tbody>
		  </table>";
}else{
	echo "<table>
			<thead>
			  <tr>
				<th>Aviso</th>
			  </tr>
			</thead>
			<tbody>
			  <tr>
			  	<th>Não há empresas relacionados ao filtro utilizado.</th>
			  </tr>
			</tbody>
		  </table>";
}			 
?>

<div class="row">
    <div class="12u">
		<a onclick='getId("Controller/Admin.controller.php?op=3","gestor_empresa")'>Desejo filtrar novas empresas</a>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('#dataTables-example2').dataTable();
    });
</script>
