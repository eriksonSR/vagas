<style>
#dataTables-example3_length{
	display:none;
	
}

#popup_area_interesse { 	 	
	display: none;
	position: absolute;
 	top:2%; 
	
	background:#FFFFFF;  
	z-index:100; /* Layering ( on-top of others), if you have lots of layers: I just maximized, you can change it yourself */
	/* additional features, can be omitted */
	border:2px solid #CCCCCC;  	
	padding:15px;  
	font-size:15px;  
	-moz-box-shadow: 0 0 5px #CCCCCC;
	-webkit-box-shadow: 0 0 5px #CCCCCC;
	box-shadow: 0 0 2px #999999;
}

@media screen and (min-width:975px){
	#popup_area_interesse { 	
		left: 30%;			
		width:60%; 
	}
}

@media screen and (max-width:975px){
	#popup_area_interesse { 	 	
		left: 5%;
		width:90%; 
	}
}
</style>

<script type="text/javascript">
	
	$(document).ready( function() {		
		
		// When site loaded, load the Popupbox First
		loadPopupBox();
	
		$('#popupBoxClose').click( function() {			
			unloadPopupBox();									  			 
		});
		
		function unloadPopupBox() {	// TO Unload the Popupbox
			$('#popup_area_interesse').fadeOut("slow");	
			$("#popup_area_interesse").remove(); 									 		
		}	
		
		function loadPopupBox() {	// To Load the Popupbox				
			$('#popup_area_interesse').fadeIn("slow");
		}
		/**********************************************************/

	});
</script>

<script>
// Centraliza a div de poup no centro da tela independente do scroll
function id( el ){
	return document.getElementById( el );
}

var marginTopoInicial;//criando variavel de escopo global

calcMarginTop();

//levando em conta o scroll
id('popup_area_interesse').style.marginTop = marginTopoInicial+window.pageYOffset+'px';

function calcMarginTop()
{
	var pop = id('popup_area_interesse');
	var marginTopo = parseInt( getMarginTop( pop ) );
	var marginBaixo = parseInt( getMarginBottom( pop ) );

	if( pop.offsetTop < -1 )
		pop.style.marginTop = '-'+( parseInt( pop.offsetTop ) - marginTopo )+'px';
	else if( marginTopo!=marginBaixo )
	pop.style.marginTop = '-248px';
}

function getMarginTop( el )
{
	if( window.getComputedStyle )
		return document.defaultView.getComputedStyle(el, null).marginTop;
	else if( el.currentStyle )
		return el.currentStyle['marginTop'];
}
function getMarginBottom( el )
{
	if( window.getComputedStyle )
		return document.defaultView.getComputedStyle(el, null).marginBottom;
	else if( el.currentStyle )
		return el.currentStyle['marginBottom'];
}
//
</script>
<style>
#dataTables-example3_length{
	margin-bottom:-60px;
}
</style>
<?php	
	echo "<div id='popup_area_interesse'>                    
			 <a id='popupBoxClose'><u>Fechar</u></a>
			 
			 <div style='margin-top:20px;' id='retorno_area_interesse'>                    

               <table id='dataTables-example3'>
					<caption><h2> Meus interesses profissionais </h2></caption>";
										
					if(isset($aviso)){
						echo "<caption style='margin-top:15px; margin-bottom:15px;'><h3>".utf8_encode($aviso)."</h3></caption>";
					}
					
	echo "			<thead>";
					
					if(count($list_areaInteresse) > 0){
						echo "<tr>
								<th>Área de interesse</th>
								<th>Função</th>
								<th>Ações</th>
	 						  </tr>";						
					}else{
						echo "<tr>
								<th>Aviso</th>
	 						  </tr>";
					}						
												
	echo "			</thead>
					<tbody>";
					if(count($list_areaInteresse) > 0){
						foreach($list_areaInteresse as $list){
							echo "<tr>
									<th>".$list->cargo."</th>
									<th>".$list->funcao."</th>";									
									?>
									<th>   
                                    <a onClick='getId("Controller/AreaInteresse.controller.php?op=<?php echo sha1(2) ?>&id=<?php echo base64_encode($list->id) ?>","alerta")'>&nbsp;&nbsp;<u>Excluir</u></a> 
            						&nbsp;&nbsp;|&nbsp;&nbsp;                        
                                    <a onClick='getId("Controller/AreaInteresse.controller.php?op=<?php echo sha1(3) ?>&id=<?php echo base64_encode($list->id) ?>","edit_interesse")'>&nbsp;&nbsp;<u>Editar</u></a> 
                                    </th>
                                    <?php
							echo "</tr>";		
						}						
					}else{
						echo "<tr>
								<th>Não há interesses profissionais cadastrados.</th>
							  </tr>";
							
					}
	echo "			</tbody>
				 </table>
				 
			  </div> 			
	  	   </div>";

?>

<script>
    $(document).ready(function() {
        $('#dataTables-example3').dataTable();
    });
</script>

