<style>
#dataTables-example_length{
	display:none;
	
}
</style>

<header>
 <h2>Candidatos encontrados</h2>
 <p>Acesse o candidato que deseja visualizar</p>
</header>

<?php	  

if(count($list_candidato) > 0){
	echo "<table id='dataTables-example' style='margin-bottom:15px;'>
			<thead>
			  <tr>
				<th>Nome</th>
				<th>Idade</th>
				<th>Cidade</th>
				<th>Auto imagem</th>
				<th>&nbsp;</th>
			  </tr>
			</thead>
			<tbody>";
			foreach($list_candidato as $list){
				echo "<tr>
						<th>".$list->nome."</th>";
						
						if(strlen($list->nascimento) > 0){
							$hoje = date('Y');
							
							$ano_idade = date('Y',strtotime($list->nascimento));
								
							$idade =  $hoje - $ano_idade;
							
							echo "<th>".$idade."</th>";
						}else{
							echo "<th> --- </th>";
						}
						
						if(strlen($list->cidade) > 0){
							echo "<th>".$list->cidade."</th>";
						}else{
							echo "<th> --- </th>";
						}
						
						if(strlen($list->auto_imagem) > 0){
							echo "<th>".$list->auto_imagem."</th>";
						}else{
							echo "<th> --- </th>";
						}
						
						?>
						<th>  <a href="Controller/Candidato.controller.php?op=<?php echo sha1(2) ?>&id=<?php echo base64_encode($list->id_user) ?>">&nbsp;&nbsp;<u>Explorar</u>&nbsp;&nbsp;</a> </th>
						<?php
				echo "</tr>";
			}
	echo "	</tbody>
		  </table>";
}else{
	echo "<table>
			<thead>
			  <tr>
				<th>Aviso</th>
			  </tr>
			</thead>
			<tbody>
			  <tr>
			  	<th>Não há candidatos relacionados ao filtro utilizado.</th>
			  </tr>
			</tbody>
		  </table>";
}			 
?>

<div class="row">
    <div class="12u">
    	<?php
        if(isset($_SESSION['ct_empresa'])){
		?>
		    <a onclick='getId("Controller/Candidato.controller.php?op=1","gestor_candidato")'>Desejo filtrar novos candidatos</a>
        <?php
        }else{
        ?>
		    <a onclick='getId("Controller/Admin.controller.php?op=1","gestor_candidato")'>Desejo filtrar novos candidatos</a>    		    
        <?php	
        }
		?>
    </div>
</div>

 <script>
    $(document).ready(function() {
        $('#dataTables-example').dataTable();
    });
 </script>