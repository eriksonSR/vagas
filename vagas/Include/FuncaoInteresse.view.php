<?php
$list_funcao = array();

if($_POST['cbCargo'] == "Arquitetura"){
	$list_funcao = 
	array(
	'Função',
	'ARQUITETO',
	'COORDENADOR / SUPERVISOR',
	'ESTÁGIO'
	);
}else if($_POST['cbCargo'] == "Cobrança"){
	$list_funcao = 
	array(
	'COORDENADOR / SUPERVISOR',
	'ASSIST. RECUP. DE CREDITO',
	'ESTÁGIO'
	);
}else if($_POST['cbCargo'] == "Comercial"){
	$list_funcao = 
	array(
	'GERENTE COMERCIAL',
	'GERENTE COMERCIAL DAS FALCULDADES',
	'GERENTE DE EQUIPE COMERCIAL',
	'ESPECIALISTA EM PLANEJAMENTO DE VENDAS',
	'ANALISTA DE PLANEJAMENTO DE VENDAS',
	'COORDENADOR ADMINISTRATIVO COMERCIAL',
	'AGENDADOR(A)',
	'ATENDENTE DE CHAT',
	'DIVULGADOR (A)',
	'AUXILIAR DE ATENDIMENTO',
	'VENDEDOR(A)',
	'ESTÁGIO'
	);
}else if($_POST['cbCargo'] == "CPD"){
	$list_funcao = 
	array(
	'ESTAGIÁRIO',
	'AUXILIAR DE CPD'
	);
}else if($_POST['cbCargo'] == "Departamento pessoal"){
	$list_funcao = 
	array(
	'ANALISTA / AUXILIAR / ASSITENTE',
	'COORDENADOR / SUPERVISOR',
	'ESTÁGIO'
	);
}else if($_POST['cbCargo'] == "Educação"){

	$list_funcao = 
	array(
	'DIRETOR DE FACULDADE',
	'DIRETOR(A) DE ESCOLA',
	'VICE DIRETOR(A) FACULDADE',
	'DIRIGENTE REGIONAL',
	'GERENTE DE FACULDADE',
	'GERENTE DE FILIAL',
	'GESTOR(A) AREA IDIOMA',
	'COORDENADOR DE GRADUAÇÃO',
	'COORDENADOR DE  PÓS GRADUAÇÃO',
	'COORDENADOR EDUCAÇÃO TÉCNICA',
	'COORDENADOR EDUCAÇÃO CURSOS LIVRES',
	'COORDENADOR (A) NADD',
	'COORDENADOR DE IDIOMAS',
	'PROF. NIVEL SUPERIOR',
	'PROFESSOR(A) DE NIVEL TECNICO ADM',
	'PROFESSOR(A) DE NIVEL TECNICO INFO',
	'PROF. TITULAR DE ADMINISTRAÇÃO',
	'TUTOR',
	'INSTRUTOR DE CURSOS LIVRES',
	'INSTRUTOR DE IDIOMAS',
	'GERENTE DE PLANEJAMENTO DE TURMAS',
	'ASSISTENTE DE PLANEJAMENO DE TURMAS',
	'SECRETARIA ACADEMICA',
	'SECRETARIO(A)  GERAL',
	'SECRETARIO(A) DE ESCOLA',
	'INTERPRETE DE LIBRAS',
	'ESTÁGIO'
	);

}else if($_POST['cbCargo'] == "Financeiro"){

	$list_funcao = 
	array(
	'GERENTE ADMINISTRATIVO FINANCEIRO',
	'ANALISTA / AUXILIAR / ASSISTENTE',
	'COORDENADOR / SUPERVISOR',
	'ESTÁGIO',
	'COORDENADOR / SUPERVISOR'
	);

}else if($_POST['cbCargo'] == "Juridico"){

	$list_funcao = 
	array(
	'ADVOGADO (A)',
	'COORDENADOR JURIDICO',
	'ESTÁGIO'
	);

}else if($_POST['cbCargo'] == "Legislação academica"){

	$list_funcao = 
	array(
	'GERENTE DE LEGISLAÇÃO ESCOLA',
	'SUPERVISOR ESCOLAR',
	'ASSIST. DE SUPERV. ESCOLAS',
	'ASSISTENTE PEDAGOGICO',
	'BIBLIOTECARIO',
	'ESTÁGIO'
	);

}else if($_POST['cbCargo'] == "Manutenção"){

	$list_funcao = 
	array(
	'COORDENADOR DE MANUTENÇÃO',
	'SUPERVISOR DE MANUTENÇÃO',
	'AUXILIAR DE MANUTENÇÃO',
	'ESTOFADOR',
	'MECANICO DE REFRIGERACAO'
	);

}else if($_POST['cbCargo'] == "Marketing"){

	$list_funcao = 
	array(
	'GERENTE DE MARKETING',
	'DIRETOR DE CRIAÇÃO',
	'ASSITENTE DE CRIAÇÃO',
	'ASSISTENTE DE MARKETING DIGITAL',
	'ASSITENTE DE MARKETING DE EVENTOS',
	'ESTAGIÁRIO'
	);

}else if($_POST['cbCargo'] == "Recrutamento e seleção"){

	$list_funcao = 
	array(
	'COORDENADOR / SUPERVISOR',
	'ANALISTA / AUXILIAR / ASSISTENTE',
	'ESTÁGIO'
	);

}else if($_POST['cbCargo'] == "Serviços gerais"){

	$list_funcao = 
	array(
	'ZELADOR',
	'AUXILIAR DE LIMPEZA'
	);

}else if($_POST['cbCargo'] == "Suprimentos"){

	$list_funcao = 
	array(
	'COORDENADOR / SUPERVISOR',
	'ANALISTA / AUXILIAR / ASSISTENTE',
	'ARQUITETO',
	'ESTÁGIO'
	);

}else if($_POST['cbCargo'] == "TI - Desenvolvimento"){

	$list_funcao = 
	array(
	'COORDENADOR / SUPERVISOR',
	'ANALISTA / AUXILIAR / ASSISTENTE',
	'ESTÁGIO'
	);

}else if($_POST['cbCargo'] == "TI - Infraestrutura"){

	$list_funcao = 
	array(
	'COORDENADOR DE T&D',
	'ANALISTA DE T&D',
	'ESTÁGIO'
	);

}else if($_POST['cbCargo'] == "Treinamento"){

	$list_funcao = 
	array(
	'CORDENADOR DE T&D',
	'ANALISTA DE T&D',
	'ESTAGIÁRIO',
	'SECRETARIA EXECUTIVA'
	);
}

echo '<select name="cbFuncao" >';
 
echo "<option value=''> Selecione a área </option> ";
 
foreach($list_funcao as $funcao){
	echo "<option value='".$funcao."'> ".$funcao." </option>";
}

echo '</select>';
?>
