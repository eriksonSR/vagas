<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
 <br />
<header>
<h2>Minhas experiências</h2>
<p>Estágio é o seu momento de aprendizado, experiência anterior não é obrigatória, mas pode ser um diferencial para garantir a sua vaga.</p>
</header>
<form onSubmit="return false" id="cad_experiencia">

<div class="row half">
  <div class="12u" id='input_full_2'> 
    <h4>Você está vivenciando essa experiência?</h4>
               
    <input class='check' name='rbAtual' type='radio' value='0' onClick='getId("Controller/Experiencia.controller.php?op=3&tipo=0","data_experiencia")' /> Sim, esta experiência é atual.
    &nbsp;&nbsp;
    <input class='check' name='rbAtual' type='radio' value='1' onClick='getId("Controller/Experiencia.controller.php?op=3&tipo=1","data_experiencia")' checked/> Não, já passei por essa experiência.				                                          
  </div>
  
</div>

<div class="row half">
    <div class="6u">
         <input type="text" class="text" name="txtEmpresa" placeholder="Empresa" />
    </div>
    <div class="6u">
    <select name="txtCargo">
        <option value=''> Área em que trabalhou </option>
        
        <option value='Comercial, Vendas'>Comercial, Vendas</option>
        <option value='Administração'>Administração</option>
        <option value='Industrial, Produção, Fábrica'>Industrial, Produção, Fábrica</option>
        <option value='Logística'>Logística</option>
        <option value='Informática, TI, Telecomunicação'>Informática, TI, Telecomunicação</option>
        <option value='Construção, Manutenção'>Construção, Manutenção</option>
        <option value='Contabil, Finanças, Economia'>Contabil, Finanças, Economia</option>
        <option value='Alimentação, Gastronomia'>Alimentação, Gastronomia</option>
        <option value='Engenharia'>Engenharia</option>
        <option value='Telemarketing'>Telemarketing</option>
        <option value='Saúde'>Saúde</option>
        <option value='Recursos Humanos'>Recursos Humanos</option>
        <option value='Transportes'>Transportes</option>
        <option value='Educação, Ensino, Idiomas'>Educação, Ensino, Idiomas</option>
        <option value='Compras, Almoxarifado, Materiais, Suprimentos'>Compras, Almoxarifado, Materiais, Suprimentos</option>
        <option value='Jurídica'>Jurídica</option>
        <option value='Marketing'>Marketing</option>
        <option value='Hotelaria, Turismo'>Hotelaria, Turismo</option>
        <option value='Segurança'>Segurança</option>
        <option value='Qualidade'>Qualidade</option>
        <option value='Arquitetura, Decoração, Designer'>Arquitetura, Decoração, Designer</option>
        <option value='Estética'>Estética</option>
        <option value='Comunicação, TV, Cinema'>Comunicação, TV, Cinema</option>
        <option value='Química, Petroquímica'>Química, Petroquímica</option>
        <option value='Comércio Exterior, Importação, Exportação'>Comércio Exterior, Importação, Exportação</option>
        <option value='Cultura, Lazer, Entretenimento'>Cultura, Lazer, Entretenimento</option>
        <option value='Agricultura, Pecuária, Veterinária'>Agricultura, Pecuária, Veterinária</option>
        <option value='Moda'>Moda</option>
        <option value='Auditoria'>Auditoria</option>
        <option value='Artes'>Artes</option>
        <option value='Serviços Sociais, Comunitários'>Serviços Sociais, Comunitários</option>
        <option value='Ciências, Pesquisa'>Ciências, Pesquisa</option>
        <option value='Meio Ambiente, Ecologia'>Meio Ambiente, Ecologia</option>
        
        <option value='Administracao'>Administração</option>
        <option value='Arquitetura'>Arquitetura</option>
        
        <option value='TI - Infra estrutura'>TI - Infra estrutura</option>
        <option value='TI - Desenvolvimento'>TI - Desenvolvimento</option>
                
        <option value='Financeiro'>Financeiro</option>
        <option value='Departamento pessoal'>Departamento pessoal</option>
        <option value='Recrutamento e Selecao'>Recrutamento e Seleção</option>
        <option value='Compras'>Compras</option>
        <option value='Manutencao predial'>Manutenção predial</option>
        <option value='Juridico'>Jurídico</option>
        <option value='Suprimentos e compras'>Suprimentos e compras</option>
        <option value='Marketing - comunicacao'>Marketing - comunicação</option>
        <option value='Logistica academica'>Logística acadêmica</option>
        <option value='Relacionamento com cliente'>Relacionamento com cliente</option>
        <option value='Secretaria escolar'>Secretaria escolar</option>
        <option value='Professor - Tec em informatica'>Professor - Tec em informática</option>
        <option value='Professor - Tec em administracao'>Professor - Tec em administração</option>
        <option value='Instrutor em linguagem inglesa'>Instrutor em linguagem inglesa</option>
        <option value='Instrutor profissional'>Instrutor profissional</option>
         
     </select>     
    

  </div>
</div>

<div class="row half">
  <div class="6u">
    <input type="text" class="text" name="txtTel" placeholder="Telefone da empresa" onkeypress='mascara(this,telefone)' maxlength='14'/>
  </div>
  <div class="6u">
    <input type="text" class="text" name="txtEmail" placeholder="E-mail da empresa" />
  </div>
</div>

<div class="row half" id='data_experiencia'>
  <div class="6u">
    <input type="text" class="text" name="txtDataInicio" placeholder="Data de início" onkeypress='mascara(this,data)' maxlength='10'/>
  </div>
  <div class="6u">
    <input type="text" class="text" name="txtDataFim" placeholder="Data de término" onkeypress='mascara(this,data)' maxlength='10'/>
  </div>
</div>

<div class="row half">
    <div class="12u">
        <textarea name="txtMsg" placeholder="Descrição"></textarea>
    </div>
</div>

<div class="row">
    <div class="12u">
        <a class="button submit" onclick='cadastro("Controller/Experiencia.controller.php?op=1","alerta","cad_experiencia")'>Cadastrar experiência</a>
    </div>
    <div class="12u">
		<a onClick='getId("Controller/Experiencia.controller.php?op=<?php echo sha1(1) ?>","alerta")'>Visualizar as experiências já cadastradas</a>
    </div>
</div>

</form> 
        
</body>
</html>
