<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
<?php
echo "        <table>
				<caption><h2> Meus interesses profissionais.</h2></caption>";
									
				if(isset($aviso)){
					echo "<caption><h3>".$aviso."</h3></caption>";
				}
				
echo "			<thead>";
				
				if(count($list_areaInteresse) > 0){
					echo "<tr>
							<th>Área de interesse</th>
							<th>Remuneração pretendida</th>
							<th>Ações</th>
						  </tr>";						
				}else{
					echo "<tr>
							<th>Aviso</th>
						  </tr>";
				}						
											
echo "			</thead>
				<tbody>";
				if(count($list_areaInteresse) > 0){
					foreach($list_areaInteresse as $list){
						echo "<tr>
								<th>".$list->cargo."</th>
								<th> R$ ".number_format($list->salario, 2, ',', '.')."</th>";									
								?>
								<th>   <a onClick='getId("Controller/AreaInteresse.controller.php?op=<?php echo sha1(2) ?>&id=<?php echo base64_encode($list->id) ?>","retorno_area_interesse")'>&nbsp;&nbsp;<u>Excluir</u>&nbsp;&nbsp;</a> </th>
								<?php
						echo "</tr>";		
					}						
				}else{
					echo "<tr>
							<th>Não há interesses profissionais cadastrados.</th>
						  </tr>";
						
				}
echo "			</tbody>
			 </table>";
?>
</body>
</html>
