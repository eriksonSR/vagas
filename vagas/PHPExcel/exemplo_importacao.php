<?php
session_start();


					 
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('Europe/London');


/** Include PHPExcel */
require_once 'Classes/PHPExcel.php';
	
// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
							 ->setLastModifiedBy("Maarten Balliauw")
							 ->setTitle("Office 2007 XLSX Test Document")
							 ->setSubject("Office 2007 XLSX Test Document")
							 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
							 ->setKeywords("office 2007 openxml php")
							 ->setCategory("Test result file");		
// Criando os titulos.
$objPHPExcel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);
		
$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('B1', 'Nome');	
				
$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('B2', 'registro_1');	

$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('B3', 'registro_2');		

		
// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('Simple');


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="modelo_importacao.xlsx"');
header('Cache-Control: max-age=0');

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;


?>