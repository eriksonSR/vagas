<?php
session_start();
include '../Banco/Conexao.class.php';
include '../Model/RetornoAtendimento.class.php';
include '../Persistencia/UserDAO.class.php';
include '../Model/CategoriaUser.class.php';

if(isset($_SESSION['crm_excel_foco_retorno'])){	
	
	$userDAO = new UserDAO();	
	
	$list_foco_retorno = unserialize($_SESSION['crm_excel_foco_retorno']);
					 
	error_reporting(E_ALL);
	ini_set('display_errors', TRUE);
	ini_set('display_startup_errors', TRUE);
	date_default_timezone_set('Europe/London');	
	
	/** Include PHPExcel */
	require_once 'Classes/PHPExcel.php';
		
	// Create new PHPExcel object
	$objPHPExcel = new PHPExcel();
	
	// Set document properties
	$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
								 ->setLastModifiedBy("Maarten Balliauw")
								 ->setTitle("Office 2007 XLSX Test Document")
								 ->setSubject("Office 2007 XLSX Test Document")
								 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
								 ->setKeywords("office 2007 openxml php")
								 ->setCategory("Test result file");		

	// Criando os titulos.
	$objPHPExcel->getActiveSheet()->getStyle('B2')->getFont()->setBold(true);			
	$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('B2', 'Usuário');	
	
	$objPHPExcel->getActiveSheet()->getStyle('C2')->getFont()->setBold(true);			
	$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('C2', 'Categoria');	
		
	$objPHPExcel->getActiveSheet()->getStyle('D2')->getFont()->setBold(true);			
	$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('D2', 'Cliente');
	
	$objPHPExcel->getActiveSheet()->getStyle('E2')->getFont()->setBold(true);			
	$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('E2', 'Cidade');	
	
	$objPHPExcel->getActiveSheet()->getStyle('F2')->getFont()->setBold(true);			
	$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('F2', 'Telefone');	
	
	$objPHPExcel->getActiveSheet()->getStyle('G2')->getFont()->setBold(true);			
	$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('G2', 'Telefone');	
	
	$objPHPExcel->getActiveSheet()->getStyle('H2')->getFont()->setBold(true);			
	$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('H2', 'Status');	
					
	$objPHPExcel->getActiveSheet()->getStyle('I2')->getFont()->setBold(true);			
	$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('I2', 'Motivo');	
					
	$objPHPExcel->getActiveSheet()->getStyle('J2')->getFont()->setBold(true);			
	$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('J2', 'Data');	
					
	$objPHPExcel->getActiveSheet()->getStyle('L2')->getFont()->setBold(true);			
	$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('K2', 'Hora');	
	
					
	// Adicionando campos da tabela
	
	if(count($list_foco_retorno) > 0){
			
		$cont = 3;		
			
		foreach($list_foco_retorno as $retorno){
						
			$list_categoriaUser = $userDAO->getListCategoriaId($retorno->id_user);
			
			$list_cate = "";
			
			if(count($list_categoriaUser) > 0){
				foreach($list_categoriaUser as $cate){
					$list_cate .= $cate->nome.",";
				}
				
				$list_cate = substr($list_cate, 0, -1); 
			}
		
			$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('B'.$cont.'', $retorno->nome_user);	

			$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('C'.$cont.'', $list_cate);	
					
			$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('D'.$cont.'', $retorno->nome_cliente);	
					
			$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('E'.$cont.'', $retorno->cidade_cliente);	

			$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('F'.$cont.'', $retorno->cel_cliente);	

			$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('G'.$cont.'', $retorno->tel_cliente);	
										
			$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('H'.$cont.'', $retorno->titulo_status);	
			
			$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('I'.$cont.'', $retorno->titulo_motivo);	
			
			$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('J'.$cont.'', date('d/m/Y',strtotime($retorno->data)));	
	
			$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('K'.$cont.'', $retorno->hora);
			
			$cont++;		
		
		}
	}	

	// Rename worksheet
	$objPHPExcel->getActiveSheet()->setTitle('Simple');
		
	// Set active sheet index to the first sheet, so Excel opens this as the first sheet
	$objPHPExcel->setActiveSheetIndex(0);
		
	// Redirect output to a client’s web browser (Excel2007)
	header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment;filename="atendimentos.xlsx"');
	header('Cache-Control: max-age=0');
	
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	$objWriter->save('php://output');
	exit;
}

?>