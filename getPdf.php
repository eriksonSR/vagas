<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>
<body>
<?php
session_start();

include("geraPdf/mpdf.php");
include 'Banco/Conexao.class.php';
include 'Model/User.class.php';
include 'Model/Curso.class.php';
include 'Persistencia/CursoDAO.class.php';
include 'Model/Experiencia.class.php';
include 'Persistencia/ExperienciaDAO.class.php';
include 'Model/AreaInteresse.class.php';
include 'Persistencia/AreaInteresseDAO.class.php';
include 'Model/InfoCandidato.class.php';
include 'Persistencia/InfoCandidatoDAO.class.php';

if(isset($_SESSION['ct_user']) || isset($_SESSION['foco_candidato'])){
	
	$cursoDAO = new CursoDAO();
	$experienciaDAO = new ExperienciaDAO();
	$areaInteresseDAO = new AreaInteresseDAO();	
	$infoCandidatoDAO = new InfoCandidatoDAO();
	
	if(isset($_SESSION['ct_user'])){
		$user = unserialize($_SESSION['ct_user']);
		
		$infoCandidato = $infoCandidatoDAO->getUser($user[0]->id);
	}else{
		$infoCandidato = unserialize($_SESSION['foco_candidato']);
		
		$user_aux->id = $infoCandidato[0]->id_user;
		
		$user = array($user_aux);		
	}
						
	if(count($infoCandidato) > 0){
		$html = '			
				<h5> Dados pessoais</h5>
				<p style="position:absolute; right:93px;">
				<b>Nascido em: &nbsp;</b>'.date('d/m/Y',strtotime($infoCandidato[0]->nascimento)).'
				</p>			
				
				<p>
				<b>Nome: &nbsp;</b>'.$infoCandidato[0]->nome.'
				</p>
				
				<p style="position:absolute; right:93px;">
				<b>Sexo: &nbsp;</b>'.htmlentities($infoCandidato[0]->sexo).'
				</p>	
															
				<p>
				<b>Estado civil: &nbsp;</b>'.$infoCandidato[0]->estado_civil.'
				</p>
								
				<p style="position:absolute; right:93px;">
				<b>CNH: &nbsp;</b>'.$infoCandidato[0]->cnh.'
				</p>			
				
				<p>
				<b>Idiomas: &nbsp;</b>'.$infoCandidato[0]->idioma.'
				</p>
				
				<p style="position:absolute; right:93px;">
				<b>Telefone: &nbsp;</b>'.$infoCandidato[0]->tel.'
				</p>			
				
				<p>
				<b>Celular: &nbsp;</b>'.$infoCandidato[0]->cel.'
				</p>
								
				<p>
				<b>E-mail: &nbsp;</b>'.$infoCandidato[0]->email.'
				</p>			
				
				<p>
				<b>Dias disponíveis:</b><br />'.htmlentities($infoCandidato[0]->dia_disponivel).'
				</p>
				
				<p>
				<b>Turnos disponíveis:</b><br />'.$infoCandidato[0]->turno_disponivel.'
				</p>			
												
				<br />
				<h5>Endereço</h5>
												
				<p style="position:absolute; right:93px;">
				<b>Bairro: &nbsp;</b>'.$infoCandidato[0]->bairro.'
				</p>			
				
				<p>
				<b>Estado:</b> Rio Grande do Sul&nbsp;
				</p>
				
				<p style="position:absolute; right:93px;">
				<b>Rua / Numero: &nbsp;</b>'.$infoCandidato[0]->rua.'				
				</p>
				
				<p>
				<b>Cidade: &nbsp;</b>'.$infoCandidato[0]->cidade.'
				</p>';
	}else{
				$html = '			
				<h5> Dados pessoais</h5>
				
				<p style="position:absolute; right:93px;">
				<b>Nascido em: &nbsp;</b>
				</p>			
				
				<p>
				<b>Nome: &nbsp;</b>
				</p>
				
				<p style="position:absolute; right:93px;">
				<b>Sexo: &nbsp;</b>
				</p>	
															
				<p>
				<b>Estado civil: &nbsp;</b>
				</p>
								
				<p style="position:absolute; right:93px;">
				<b>CNH: &nbsp;</b>
				</p>			
				
				<p>
				<b>Idiomas: &nbsp;</b>
				</p>
				
				<p style="position:absolute; right:93px;">
				<b>Telefone: &nbsp;</b>
				</p>			
				
				<p>
				<b>Celular: &nbsp;</b>
				</p>
								
				<p>
				<b>E-mail: &nbsp;</b>
				</p>			
				
				<p>
				<b>Dias disponíveis:</b>
				</p>
				
				<p>
				<b>Turnos disponíveis:</b>
				</p>				
												
				<br />
				<h5>Endereço</h5>
				
				<p style="position:absolute; right:93px;">
				<b>Bairro: &nbsp;</b>
				</p>			
				
				<p>
				<b>Estado:</b> Rio Grande do Sul&nbsp;
				</p>
				
				<p style="position:absolute; right:93px;">
				<b>Rua / Numero: &nbsp;</b>
				</p>
				
				<p>
				<b>Cidade: &nbsp;</b>
				</p>';
	}
	
	// LIstando os cursos	
	$list_curso = $cursoDAO->getAll($user[0]);		
	
	if(count($list_curso) > 0){
		$html .= '				
				<br />
				<h2>Cursos</h2>
				
				<table border="1" style="margin-top:8px; width:100%;">
				<tbody>';
				
				$html .= '<tr style="background:#CCCCCC;">
							<th>Nível do curso</th>
							<th>Titulo do curso</th>
							<th>Escola</th>
							<th>Descrição</th>
							<th>Data inicio</th>
							<th>Data termino</th>
						  </tr>';
						  
				foreach($list_curso as $list){
					$html .= "<tr>
								<td>".$list->nivel."</td>
								<td>".$list->curso."</td>
								<td>".$list->escola."</td>";
							if(strlen($list->descricao) > 0){
								$html .=  "<td>".$list->descricao."</td>";
							}else{
								$html .=  "<td> </td>";
							}
					$html .= "	<td>".date('d/m/Y',strtotime($list->data_inicio))."</td>
								<td>".date('d/m/Y',strtotime($list->data_fim))."</td>";							
					$html .= "</tr>";		
				}					
				
		$html .= '</tbody>
				</table>';
	}
	// />
	
	// Listando as experiencias profissionais
	$list_experiencia = $experienciaDAO->getAll($user[0]);
	
	if(count($list_experiencia) > 0){
		$html .= '
				<br />
				<h2>Experiencias profissionais</h2>
				
				<table border="1" style="margin-top:8px; width:100%;">
				<tbody>';
								
				$html .= '<tr style="background:#CCCCCC;">
							<th>Empresa</th>
							<th>Área que trabalhei</th>
							<th>Descrição</th>
							<th>Data inicio</th>
							<th>Data termino</th>
						  </tr>';
						  
				foreach($list_experiencia as $list){
					$html .= "<tr>
								<td>".$list->empresa."</td>
								<td>".$list->cargo."</td>";
							if(strlen($list->descricao) > 0){
								$html .=  "<td>".$list->descricao."</td>";
							}else{
								$html .=  "<td> </td>";
							}
					$html .= "	<td>".date('d/m/Y',strtotime($list->data_inicio))."</td>
								<td>".date('d/m/Y',strtotime($list->data_fim))."</td>";							
					$html .= "</tr>";		
				}									
				
		$html .= '</tbody>
				</table>';
	}
	// />	
		
	// Listando as áreas de interesse
		
	$list_areaInteresse = $areaInteresseDAO->getAll($user[0]);
	
	if(count($list_areaInteresse) > 0){
		$html .= '
				<br />
				<h2>Interesses profissionais</h2>
				
				<table border="1" style="margin-top:8px; width:100%;">
				<tbody>';
								
				$html .= '<tr style="background:#CCCCCC;">
							<th>Área de interesse</th>
							<th>Remuneração pretendida</th>
						  </tr>';
						  
				foreach($list_areaInteresse as $list){
					$html .= "<tr>
								<td>".$list->cargo."</td>
								<td> R$ ".number_format($list->salario, 2, ',', '.')."</td>";
					$html .= "</tr>";		
				}	
								
		$html .= '</tbody>
				</table>';
	}
	// />	
	
	// Gerando PDF
	
	$mpdf=new mPDF('c','A4','','',32,25,27,25,16,13); 

	$mpdf->SetDisplayMode('fullpage');
	
	$mpdf->list_indent_first_level = 0;	// 1 or 0 - whether to indent the first level of a list
	
	// LOAD a stylesheet
	$stylesheet = file_get_contents('geraPdf/mpdfstyletables.css');
	$mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text
	$mpdf->WriteHTML($html,2);
	
	$mpdf->Output('curriculo.pdf','I');

	exit;
	// />
}else{
	session_unset();
	header('location:index.php');
}

?>
</body>
</html>
