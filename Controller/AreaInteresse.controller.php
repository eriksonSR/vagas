<?php
session_start();

date_default_timezone_set('America/Sao_Paulo');	
include '../Banco/Conexao.class.php';
include '../Model/User.class.php';
include '../Model/AreaInteresse.class.php';
include '../Persistencia/AreaInteresseDAO.class.php';
include '../Model/Validacao.class.php';

if($_POST){
	if($_GET['op'] == 1){
		
		$areaInteresseDAO = new AreaInteresseDAO();
		
		$cont_erro = 0;

		if(strlen($_POST['cbCargo']) == 0){
			$erro = 'Preencha a área de interesse.';
			
			$cont_erro++;
		}
		
		if(strlen($_POST['cbFuncao']) == 0){
			$erro = 'Preencha a função de interesse.';
			
			$cont_erro++;
		}
		
														
		if($cont_erro == 0){
			
			$user = unserialize($_SESSION['ct_user']);
				
			$areaInteresse->id_user = $user[0]->id;
			$areaInteresse->cargo = $_POST['cbCargo'];			
			$areaInteresse->funcao = $_POST['cbFuncao'];
								
			$areaInteresseDAO->cadastro($areaInteresse);

			$aviso = "Interesse profissional cadastrado com sucesso";
			
			// Listando os interesses ja cadastrados			
			$list_areaInteresse = $areaInteresseDAO->getAll($user[0]);
			include '../Include/ListAreaInteresse.view.php';
			// />
			
			$id_form = 'cad_interesse';
			
			include '../Include/ResetForm/FormAll.view.php';	
			
		}else{		
			echo $erro;
		}
	}else if($_GET['op'] == 2){
		
		$areaInteresseDAO = new AreaInteresseDAO();
		
		$cont_erro = 0;
		
		
		if(strlen($_POST['cbCargo']) == 0){
			$_SESSION['aviso'] = 'Preencha a área de interesse.';
			
			$cont_erro++;
		}
		
		if(strlen($_POST['cbFuncao']) == 0){
			$_SESSION['aviso'] = 'Preencha a função de interesse.';
			
			$cont_erro++;
		}
														
		if($cont_erro == 0){
			
			$user = unserialize($_SESSION['ct_user']);
			
			$areaInteresse = $areaInteresseDAO->getId(base64_decode($_GET['id']));
			
			$areaInteresse[0]->cargo = $_POST['cbCargo'];
			$areaInteresse[0]->funcao = $_POST['cbFuncao'];
			
			$areaInteresseDAO->alt($areaInteresse[0]);

			$aviso = "Interesse profissional alterado com sucesso";
			
			// Listando os interesses ja cadastrados			
			$list_areaInteresse = $areaInteresseDAO->getAll($user[0]);
			include '../Include/ListAreaInteresse.view.php';
			// />
			
			echo "<list>";
			include '../Include/CadAreaInteresse.view.php';
			echo "<list>";
			
		}else{		
			include '../Include/Aviso.view.php';			
			echo "<list>
				  <list>";
		}
	}else if($_GET['op'] == 3){		
		include '../Include/FuncaoInteresse.view.php';		
	}else{
		session_unset();
		header('location:../index.php');		
	}
}else if($_GET['op'] == sha1(1)){
	$areaInteresseDAO = new AreaInteresseDAO();
		
	$user = unserialize($_SESSION['ct_user']);
	
	$list_areaInteresse = $areaInteresseDAO->getAll($user[0]);
	
	include '../Include/ListAreaInteresse.view.php';
}else if($_GET['op'] == sha1(2)){
	$areaInteresseDAO = new AreaInteresseDAO();
		
	$user = unserialize($_SESSION['ct_user']);
	
	$areaInteresseDAO->excluir(base64_decode($_GET['id']));
	
	$list_areaInteresse = $areaInteresseDAO->getAll($user[0]);
									
	include '../Include/ListAreaInteresse.view.php';
}else if($_GET['op'] == sha1(3)){
	
	$areaInteresseDAO = new AreaInteresseDAO();

	$areaInteresse = $areaInteresseDAO->getId(base64_decode($_GET['id']));
	
	include '../Include/EditAreaInteresse.view.php';
	
	$div = "popup_area_interesse";
	
	include '../Include/ResetForm/ClosedAllPopup.view.php';

}else if($_GET['op'] == 4){
	include '../Include/CadAreaInteresse.view.php';
}else{
	session_unset();
	header('location:../index.php');
}

?>