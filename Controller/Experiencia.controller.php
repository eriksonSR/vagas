<?php
session_start();

date_default_timezone_set('America/Sao_Paulo');	
include '../Banco/Conexao.class.php';
include '../Model/User.class.php';
include '../Model/Experiencia.class.php';
include '../Persistencia/ExperienciaDAO.class.php';
include '../Model/Validacao.class.php';

if($_POST){
	if($_GET['op'] == 1){
		
		$experienciaDAO = new ExperienciaDAO();
		
		$cont_erro = 0;
						
		if(strlen($_POST['txtDataInicio']) > 0){
		
			$aux = str_replace("/","-",$_POST['txtDataInicio']);
			$aux = date("d/m/Y",strtotime($aux));				
			$aux = str_replace("-","/",$aux);	
						
			$aux = explode('/',$aux);
			
			$dia = $aux[0];
			$mes = $aux[1];
			$ano = $aux[2];
																	
			if(Validacao::data($mes,$dia,$ano)){
				$experiencia->data_inicio = date($ano."-".$mes."-".$dia);
			}else{
				$erro = 'Data de inicio da experiencia invalida.';
			
				$cont_erro++;
			}
			
		}else{
			$erro = 'Data de inicio da experiencia invalida.';
			
			$cont_erro++;
		}
	
		if($_POST['rbAtual'] == 1){
			if(strlen($_POST['txtDataFim']) > 0){
			
				$aux = str_replace("/","-",$_POST['txtDataFim']);
				$aux = date("d/m/Y",strtotime($aux));				
				$aux = str_replace("-","/",$aux);	
							
				$aux = explode('/',$aux);
				
				$dia = $aux[0];
				$mes = $aux[1];
				$ano = $aux[2];
																		
				if(Validacao::data($mes,$dia,$ano)){
					$experiencia->data_fim = date($ano."-".$mes."-".$dia);
					
					if(strtotime($experiencia->data_inicio) > strtotime($experiencia->data_fim)){
						$erro = 'Data de inicio e termino incompativeis';		
				
						$cont_erro++;
					}
					
				}else{
					$erro = 'Data de termino da experiencia invalida.';		
				
					$cont_erro++;	
				}		
			}else{		
				$erro = 'Data de termino da experiencia invalida.';		
				
				$cont_erro++;	
			}	
		}else{
			$experiencia->data_fim = NULL;
		}
		
		
		if(strlen($_POST['txtEmail']) > 0){
			if(!Validacao::email($_POST['txtEmail'])){
				$erro = "E-mail invalido.";
				
				$cont_erro++;
			}	
		}
		
		if(strlen($_POST['txtTel']) == 11){
			$erro = 'Preencha o cargo.';
			
			$cont_erro++;
		}
		
		if(strlen($_POST['txtCargo']) == 0){
			$erro = 'Preencha o cargo.';
			
			$cont_erro++;
		}
									
		if(strlen($_POST['txtEmpresa']) == 0){
			$erro = 'Preencha o nome da empresa.';
			
			$cont_erro++;
		}
		
		
		if($cont_erro == 0){
			
			$user = unserialize($_SESSION['ct_user']);
				
			$experiencia->id_user = $user[0]->id;
			$experiencia->empresa = $_POST['txtEmpresa'];
			$experiencia->cargo = $_POST['txtCargo'];
			$experiencia->email = $_POST['txtEmail'];
			$experiencia->tel = $_POST['txtTel'];		
			$experiencia->descricao = $_POST['txtMsg'];
			$experiencia->atual = $_POST['rbAtual'];
			
			$experienciaDAO->cadastro($experiencia);

			$aviso = "Seu experiencia foi cadastrada com sucesso";
			
			// Listando as experiencias cadastradas no sistema
			$list_experiencia = $experienciaDAO->getAll($user[0]);	
			include '../Include/ListExperiencia.view.php';
			// />
			
			$id_form = 'cad_experiencia';
			
			include '../Include/ResetForm/FormExperiencia.view.php';												
			
		}else{		
			echo $erro;
		}
	
	}else if($_GET['op'] == 2){
		
		$experienciaDAO = new ExperienciaDAO();
		
		$experiencia = $experienciaDAO->getId(base64_decode($_GET['id']));
		
		$cont_erro = 0;
						
		if(strlen($_POST['txtDataInicio']) > 0){
		
			$aux = str_replace("/","-",$_POST['txtDataInicio']);
			$aux = date("d/m/Y",strtotime($aux));				
			$aux = str_replace("-","/",$aux);	
						
			$aux = explode('/',$aux);
			
			$dia = $aux[0];
			$mes = $aux[1];
			$ano = $aux[2];
																	
			if(Validacao::data($mes,$dia,$ano)){
				$experiencia[0]->data_inicio = date($ano."-".$mes."-".$dia);
			}else{
				$_SESSION['aviso'] = 'Data de inicio da experiencia invalida.';
			
				$cont_erro++;
			}
			
		}else{
			$_SESSION['aviso'] = 'Data de inicio da experiencia invalida.';
			
			$cont_erro++;
		}
	
		if($_POST['rbAtual'] == 1){
			if(strlen($_POST['txtDataFim']) > 0){
			
				$aux = str_replace("/","-",$_POST['txtDataFim']);
				$aux = date("d/m/Y",strtotime($aux));				
				$aux = str_replace("-","/",$aux);	
							
				$aux = explode('/',$aux);
				
				$dia = $aux[0];
				$mes = $aux[1];
				$ano = $aux[2];
																		
				if(Validacao::data($mes,$dia,$ano)){
					$experiencia[0]->data_fim = date($ano."-".$mes."-".$dia);
					
					if(strtotime($experiencia[0]->data_inicio) > strtotime($experiencia[0]->data_fim)){
						$_SESSION['aviso'] = 'Data de inicio e termino incompativeis';		
				
						$cont_erro++;
					}
					
				}else{
					$_SESSION['aviso'] = 'Data de termino da experiencia invalida.';		
				
					$cont_erro++;	
				}		
			}else{		
				$_SESSION['aviso'] = 'Data de termino da experiencia invalida.';		
				
				$cont_erro++;	
			}	
		}else{
			$experiencia[0]->data_fim = NULL;
		}
		
		
		if(strlen($_POST['txtEmail']) > 0){
			if(!Validacao::email($_POST['txtEmail'])){
				$_SESSION['aviso'] = "E-mail invalido.";
				
				$cont_erro++;
			}	
		}
		
		if(strlen($_POST['txtTel']) == 11){
			$_SESSION['aviso'] = 'Preencha o cargo.';
			
			$cont_erro++;
		}
		
		if(strlen($_POST['txtCargo']) == 0){
			$_SESSION['aviso'] = 'Preencha o cargo.';
			
			$cont_erro++;
		}
									
		if(strlen($_POST['txtEmpresa']) == 0){
			$_SESSION['aviso'] = 'Preencha o nome da empresa.';
			
			$cont_erro++;
		}
				
		if($cont_erro == 0){
		
			$user = unserialize($_SESSION['ct_user']);
			
			$experiencia[0]->empresa = $_POST['txtEmpresa'];
			$experiencia[0]->cargo = $_POST['txtCargo'];
			$experiencia[0]->email = $_POST['txtEmail'];
			$experiencia[0]->tel = $_POST['txtTel'];
			$experiencia[0]->descricao = $_POST['txtMsg'];
			$experiencia[0]->atual = $_POST['rbAtual'];
			
			$experienciaDAO->alt($experiencia[0]);

			$aviso = "Seu experiencia foi alterada com sucesso";
			
			// Listando as experiencias cadastradas no sistema
			$list_experiencia = $experienciaDAO->getAll($user[0]);	
			include '../Include/ListExperiencia.view.php';
			// />
			
			echo "<list>";
			include '../Include/CadExperiencia.view.php';
			echo "<list>";	
					
		}else{		
			include '../Include/Aviso.view.php';	
			echo "<list>";
			echo "<list>";	
		}
		
	}else{
		session_unset();
		header('location:../index.php');		
	}
}else if($_GET['op'] == sha1(1)){
	$experienciaDAO = new ExperienciaDAO();
		
	$user = unserialize($_SESSION['ct_user']);
	
	$list_experiencia = $experienciaDAO->getAll($user[0]);
	
	include '../Include/ListExperiencia.view.php';	
}else if($_GET['op'] == sha1(2)){
	$experienciaDAO = new ExperienciaDAO();
		
	$user = unserialize($_SESSION['ct_user']);
	
	$experienciaDAO->excluir(base64_decode($_GET['id']));
	
	$list_experiencia = $experienciaDAO->getAll($user[0]);
	
	include '../Include/ListExperiencia.view.php';	
	
}else if($_GET['op'] == 3 && isset($_GET['tipo'])){

	include '../Include/FormDataExperiencia.view.php';
	
}else if($_GET['op'] == sha1(4)){
	$experienciaDAO = new ExperienciaDAO();
		
	$experiencia = $experienciaDAO->getId(base64_decode($_GET['id']));
		
	include '../Include/EditExperiencia.view.php';
	
	$div = "popup_experiencia";
	
	include '../Include/ResetForm/ClosedAllPopup.view.php';
}else if($_GET['op'] == 5){
	
	include '../Include/CadExperiencia.view.php';
}else{
	session_unset();
	header('location:../index.php');
}

?>