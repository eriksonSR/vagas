<?php
session_start();

date_default_timezone_set('America/Sao_Paulo');	
include '../Banco/Conexao.class.php';
include '../Model/User.class.php';
include '../Model/Curso.class.php';
include '../Persistencia/CursoDAO.class.php';
include '../Model/Validacao.class.php';

if($_POST){
	if($_GET['op'] == 1){
		
		$cursoDAO = new CursoDAO();
		
		$cont_erro = 0;
		
		if($_POST['cbNivel'] != utf8_encode("Ensino m�dio")){
			if(strlen($_POST['txtDataInicio']) > 0){
			
				$aux = str_replace("/","-",$_POST['txtDataInicio']);
				$aux = date("d/m/Y",strtotime($aux));				
				$aux = str_replace("-","/",$aux);	
							
				$aux = explode('/',$aux);
				
				$dia = $aux[0];
				$mes = $aux[1];
				$ano = $aux[2];
																		
				if(Validacao::data($mes,$dia,$ano)){
					$curso->data_inicio = date($ano."-".$mes."-".$dia);
				}else{
					$erro = 'Data de inicio do curso invalida.';
				
					$cont_erro++;
				}
				
			}else{
				$erro = 'Data de inicio do curso invalida.';
				
				$cont_erro++;
			}
		
			if(strlen($_POST['txtDataFim']) > 0){
			
				$aux = str_replace("/","-",$_POST['txtDataFim']);
				$aux = date("d/m/Y",strtotime($aux));				
				$aux = str_replace("-","/",$aux);	
							
				$aux = explode('/',$aux);
				
				$dia = $aux[0];
				$mes = $aux[1];
				$ano = $aux[2];
																		
				if(Validacao::data($mes,$dia,$ano)){
					$curso->data_fim = date($ano."-".$mes."-".$dia);
					
					if(strtotime($curso->data_inicio) > strtotime($curso->data_fim)){
						$erro = 'Data de inicio e t�rmino incompativeis';		
				
						$cont_erro++;
					}
					
				}else{
					$erro = 'Data de t�rmino do curso invalida.';		
				
					$cont_erro++;	
				}		
			}else{		
				$erro = 'Data de t�rmino do curso invalida.';		
				
				$cont_erro++;	
			}	
		}else{
			$curso->data_inicio = NULL;
			$curso->data_fim = NULL;			
		}
		
		if(strlen($_POST['txtEscola']) == 0){
			$erro = 'Preencha a escola em que realizou o curso.';			
			
			$cont_erro++;
		}								
		
		if(strlen($_POST['cbCurso']) == 0){
			if($_POST['cbNivel'] != utf8_encode("Ensino m�dio")){
				$erro = 'Selecione o curso.';
			}else{
				$erro = htmlentities('Informe sua s�rie.');
			}
						
			$cont_erro++;
		}
		
		if(strlen($_POST['cbNivel']) == 0){
			$erro = 'Selecione o nivel curso.';
			
			$cont_erro++;
		}
		
		if($cont_erro == 0){
			
			$user = unserialize($_SESSION['ct_user']);
				
			$curso->id_user = $user[0]->id;
			$curso->nivel = $_POST['cbNivel'];
			$curso->curso = $_POST['cbCurso'];
			$curso->escola = $_POST['txtEscola'];		
			$curso->descricao = $_POST['txtMsg'];
			
			$cursoDAO->cadastro($curso);

			$aviso = "Sua forma��o acad�mica foi cadastrada com sucesso";
			
			// Listando os cursos cadastrados
			$list_curso = $cursoDAO->getAll($user[0]);	
			include '../Include/ListCurso.view.php';
			// />
			
			$id_form = 'cad_curso';
			
			include '../Include/ResetForm/FormAll.view.php';										
			
		}else{				
			echo $erro;
		}	
		
	}else if($_GET['op'] == 2){
		$nivel = $_POST['cbNivel'];
		
		include '../Include/setCurso.view.php';		
		
		echo "<list>";
		
		if($_POST['cbNivel'] != utf8_encode("Ensino m�dio")){
			include '../Include/FormDataCurso.view.php';
		}
		
		echo "<list>";
	}else if($_GET['op'] == 3){
		$nivel = $_POST['cbNivel'];
		
		include '../Include/setCurso.view.php';		

	}else if($_GET['op'] == 4){
		
		$cursoDAO = new CursoDAO();
		
		$cont_erro = 0;
		
		if($_POST['cbNivel'] != utf8_encode("Ensino m�dio")){
			if(strlen($_POST['txtDataInicio']) > 0){
			
				$aux = str_replace("/","-",$_POST['txtDataInicio']);
				$aux = date("d/m/Y",strtotime($aux));				
				$aux = str_replace("-","/",$aux);	
							
				$aux = explode('/',$aux);
				
				$dia = $aux[0];
				$mes = $aux[1];
				$ano = $aux[2];
																		
				if(Validacao::data($mes,$dia,$ano)){
					$curso->data_inicio = date($ano."-".$mes."-".$dia);
				}else{
					$_SESSION['aviso'] = 'Data de inicio do curso invalida.';
				
					$cont_erro++;
				}
				
			}else{
				$_SESSION['aviso'] = 'Data de inicio do curso invalida.';
				
				$cont_erro++;
			}
		
			if(strlen($_POST['txtDataFim']) > 0){
			
				$aux = str_replace("/","-",$_POST['txtDataFim']);
				$aux = date("d/m/Y",strtotime($aux));				
				$aux = str_replace("-","/",$aux);	
							
				$aux = explode('/',$aux);
				
				$dia = $aux[0];
				$mes = $aux[1];
				$ano = $aux[2];
																		
				if(Validacao::data($mes,$dia,$ano)){
					$curso->data_fim = date($ano."-".$mes."-".$dia);
					
					if(strtotime($curso->data_inicio) > strtotime($curso->data_fim)){
						$_SESSION['aviso'] = 'Data de inicio e t�rmino incompativeis';		
				
						$cont_erro++;
					}
					
				}else{
					$_SESSION['aviso'] = 'Data de t�rmino do curso invalida.';		
				
					$cont_erro++;	
				}		
			}else{		
				$_SESSION['aviso'] = 'Data de t�rmino do curso invalida.';		
				
				$cont_erro++;	
			}	
		}else{
			$curso->data_inicio = NULL;
			$curso->data_fim = NULL;			
		}
		
		if(strlen($_POST['txtEscola']) == 0){
			$_SESSION['aviso'] = 'Preencha a escola em que realizou o curso.';			
			
			$cont_erro++;
		}								
		
		if(strlen($_POST['cbCurso']) == 0){
			if($_POST['cbNivel'] != utf8_encode("Ensino m�dio")){
				$_SESSION['aviso'] = 'Selecione o curso.';
			}else{
				$_SESSION['aviso'] = htmlentities('Informe sua s�rie.');
			}
						
			$cont_erro++;
		}
		
		if(strlen($_POST['cbNivel']) == 0){
			$_SESSION['aviso'] = 'Selecione o nivel curso.';
			
			$cont_erro++;
		}
		
		if($cont_erro == 0){
			$user = unserialize($_SESSION['ct_user']);
				
			$curso = $cursoDAO->getId(base64_decode($_GET['id']));

			$curso[0]->nivel = $_POST['cbNivel'];
			$curso[0]->curso = $_POST['cbCurso'];
			$curso[0]->escola = $_POST['txtEscola'];		
			$curso[0]->descricao = $_POST['txtMsg'];
			
			$cursoDAO->alt($curso[0]);

			$aviso = "Sua forma��o acad�mica foi alterada com sucesso";
			
			// Listando os cursos cadastrados
			$list_curso = $cursoDAO->getAll($user[0]);	
			include '../Include/ListCurso.view.php';
			// />			
			
			echo "<list>";
			include '../Include/CadCurso.view.php';
			echo "<list>";
		}else{				
			include '../Include/Aviso.view.php';
			echo "<list>";
			echo "<list>";
		}		
				
	}else{
		session_unset();
		header('location:../index.php');
	}
}else if($_GET['op'] == sha1(1)){
	$cursoDAO = new CursoDAO();
		
	$user = unserialize($_SESSION['ct_user']);
	
	$list_curso = $cursoDAO->getAll($user[0]);
	
	include '../Include/ListCurso.view.php';
}else if($_GET['op'] == sha1(2)){
	$cursoDAO = new CursoDAO();
		
	$user = unserialize($_SESSION['ct_user']);
	
	$cursoDAO->excluir(base64_decode($_GET['id']));
	
	$list_curso = $cursoDAO->getAll($user[0]);
	
	include '../Include/ValorListCurso.view.php';
}else if($_GET['op'] == sha1(3)){
	
	$cursoDAO = new CursoDAO();
	
	$curso = $cursoDAO->getId(base64_decode($_GET['id']));
	
	include '../Include/EditCurso.view.php';
	
	$div = "popup_curso";
	
	include '../Include/ResetForm/ClosedAllPopup.view.php';

}else if($_GET['op'] == 4){
	include '../Include/CadCurso.view.php';
	
}else{
	session_unset();
	header('location:../index.php');
}

?>