<?php
session_start();

date_default_timezone_set('America/Sao_Paulo');	
include '../Banco/Conexao.class.php';
include '../Model/User.class.php';
include '../Persistencia/UserDAO.class.php';
include '../Model/Validacao.class.php';
include '../phpmailer/class.phpmailer.php';	

if($_POST){
				
    if($_GET['op'] == 1){
		$userDAO = new UserDAO();
		
		$cont_erro = 0;

		if(strlen($_POST['txtSenha']) == 0){
			$erro = "Preencha uma senha.";

			$cont_erro++;
		}else if($_POST['txtSenha'] != $_POST['txtSenha2']){
			$erro = "Confirme a senha corretamente.";
			
			$cont_erro++;
		}
		
		if(isset($_POST['txtCpf'])){
			if(strlen($_POST['txtCpf']) == 0){
				$erro = "CPF invalido.";
				
				$cont_erro++;
			}else if($userDAO->getContCpf($_POST['txtCpf']) > 0){
				$erro = htmlentities("CPF já cadastrado.");
				
				$cont_erro++;
			}
		}else if(isset($_POST['txtCnpj'])){
			if(strlen($_POST['txtCnpj']) == 0){
				$erro = "CNPJ invalido.";
				
				$cont_erro++;
			}else if($userDAO->getContCnpj($_POST['txtCnpj']) > 0){
				$erro = htmlentities("CNPJ já cadastrado.");
				
				$cont_erro++;
			}
		}	
						
		if(!Validacao::email($_POST['txtEmail'])){
			$erro = "E-mail invalido.";
			
			$cont_erro++;
		}else if($userDAO->getContEmail($_POST['txtEmail']) > 0){
			$erro = htmlentities("E-mail já cadastrado.");
			
			$cont_erro++;
		}
		
		if($cont_erro == 0){
			$user->email = $_POST['txtEmail'];
			
			if(isset($_POST['txtCpf'])){
				$user->cpf = $_POST['txtCpf'];
				$user->tipo = 1;
			}else{
				$user->cpf = NULL;
			}
			
			if(isset($_POST['txtCnpj'])){
				$user->cnpj = $_POST['txtCnpj'];
				$user->tipo = 2;
			}else{
				$user->cnpj = NULL;
			}
			
			$user->senha = sha1($_POST['txtSenha']);						
			
			$user->data = date('Y-m-d'); 
			$user->hora = date('G:i:s');
			
			$userDAO->cadastro($user);
									
			$_SESSION['aviso_login'] = 'Cadastro efetuado com sucesso';
			
			//include '../Include/ResetForm/FormCandidato.view.php';
			
			if($user->tipo == 1){
				$user_aux = $userDAO->getLogin($user);
				
				$_SESSION['ct_user'] = serialize($user_aux);
			
				$url = "PerfilCandidato.php";
				
				include '../Include/Direcionamento.view.php';
			}else{
				// So para utilizar a busca, estamos utilizando o campo de cpf so para passar a informação pq o metodo get ta validando em cima da variavel cpf.
				
				$user->cpf = $user->cnpj;
				
				$user_aux = $userDAO->getLogin($user);
				
				$_SESSION['ct_empresa'] = serialize($user_aux);
			
				$url = "PerfilEmpresa.php";
				
				include '../Include/Direcionamento.view.php';
			}
			
			
			// Começando script para e-mail. 	
			/*					
			$nome = iconv('utf8', 'ISO-8859-1', "Escolas e Faculdades QI"); // E-MAIL DO REMETENTE
			
			$de = "recrutamento@qi.edu.br"; // E-MAIL DO REMETENTE
			
			$envio = $_POST['txtEmail']; // E-MAIL DO DESTINATÁRIO
						
			$mensagem = '<div><h3 style="outline:none;font-family:museo300;font-size:20px;margin:0px;width:540px;line-height:45px">Prezado(a).</h3><p style="outline:none;font-family:museo500;font-size:18px;color:rgb(1,1,1);width:690px;margin:25px 0px 30px">Para que possamos dar continuidade ao processo, necessitamos que seja preenchido o levantamento de perfil o qual o link segue abaixo.</p><p>PARA FAZER O LEVANTAMENTO DE PERFIL ACESSE:<br />
http://www.auto-imagem.com.br/sistema/autoimagem_qi.asp</p><br /><p>Desde já agradecemos seu interesse em fazer parte de nossa instituição. <br /><br />QI Escolas e Faculdades </p></p></div>';
			
			$msg = "<html><body>".iconv('utf8', 'ISO-8859-1',  $mensagem)." </body></html>";  // Mensagem
			
			$assunto = iconv('utf8', 'ISO-8859-1', "Cadastro para recrutamento"); //PEGA O ASSUNTO DO E-MAIL

			$mail = new PHPMailer(); //Instanciamos a classe PHPMailer
			
			$mail->IsSMTP(true); //Caso queira utilizar o programa de e-mail do seu servidor unix/linux para o envio de e-mail. Caso seja servidor windows o e-mail precisa ser enviado via STMP, então ai invéz de utilizar a função $mail->IsMail() utilizamos $mail->IsSMTP()
			
			$mail->IsHTML(true); //Ativa o envio de e-mail no formato html, se false desativa
			
			$mail->From = $de; //E-mail do remente da mensagem
			
			$mail->FromName = $nome; //Nome do remente da mensagem
			
			$mail->AddAddress("$envio"); //E-mail que a mensagem será enviada
			
			$mail->Subject = $assunto; //Assunto da mensagem
			
			$mail->Body = $msg; //Corpo da mensagem										
			
			$mail->Send();	
			*/
						
		}else{		
			echo $erro;
		}
		
	}else if($_GET['op'] == 2){
		
		if(strlen($_POST['txtSenha']) == 0){
			$_SESSION['aviso'] = "Preencha uma senha.";
		}else if($_POST['txtSenha'] != $_POST['txtSenha2']){
			$_SESSION['aviso'] = "Confirme a senha corretamente.";			
		}else{
			$userDAO = new UserDAO();
			
			if(isset($_SESSION['ct_empresa'])){
				$user = unserialize($_SESSION['ct_empresa']);
			}else{
				$user = unserialize($_SESSION['ct_user']);
			}
			
			$user[0]->senha = sha1($_POST['txtSenha']);
			
			$userDAO->alt($user[0]);
			
			$_SESSION['aviso'] = "Senha alterada com sucesso";			
		}
		
		include '../Include/Aviso.view.php';
		
	}else{
		session_unset();
		header('location:../index.php');
	}
	
}else if($_GET['op'] == 1){
	include '../Include/AltSenha.view.php';	
}else{
	session_unset();
	header('location:../index.php');
}
?>