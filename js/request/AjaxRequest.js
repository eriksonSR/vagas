function generateXMLHttp() {
	if (typeof XMLHttpRequest != "undefined"){
		return new XMLHttpRequest();
	}
	else{	
	 	if (window.ActiveXObject){
			var versions = ["MSXML2.XMLHttp.5.0", 
		                	"MSXML2.XMLHttp.4.0", 
                        	"MSXML2.XMLHttp.3.0",
		                	"MSXML2.XMLHttp", 
		                	"Microsoft.XMLHttp"
		               		];
		}
	}
	for (var i=0; i<versions.length; i++){
		try{
			return new ActiveXObject(versions[i]);
		}catch(e){}
	}
	alert('Seu navegador não pode trabalhar com Ajax!')
}

// Get simples
function getId(url,div) {
	/* Abrindo alerta de carregamento */
	if(document.getElementById('pop') != undefined){
		document.getElementById('pop').style.display = "block";						
	}
		
	var result = document.getElementById(div);
  
	var XMLHttp = generateXMLHttp();
	XMLHttp.open("get", url, true);
	XMLHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
 
	XMLHttp.onreadystatechange = function (){		 		
		
		if(XMLHttp.readyState == 4){		
			
			/* Fechando alerta de carregamento */
			if(document.getElementById('pop') != undefined ){
				document.getElementById('pop').style.display = "none";
			}
			
			// Exibindo resposta	
			result.innerHTML = XMLHttp.responseText;
			
			// executa scripts
			extraiScript(XMLHttp.responseText);
		}
	};
	
	XMLHttp.send(null);
}

// Get simples
function getId2(url,div,div2) {
	/* Abrindo alerta de carregamento */
	if(document.getElementById('pop') != undefined){
		document.getElementById('pop').style.display = "block";						
	}
		
	var result = document.getElementById(div);
  
	var XMLHttp = generateXMLHttp();
	XMLHttp.open("get", url, true);
	XMLHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
 
	XMLHttp.onreadystatechange = function (){		 		
		
		if(XMLHttp.readyState == 4){		
			
			/* Fechando alerta de carregamento */
			if(document.getElementById('pop') != undefined ){
				document.getElementById('pop').style.display = "none";
			}
			
			var retorno = XMLHttp.responseText;

			var list = retorno.split("<list>");

			// Exibe o retorno

			result.innerHTML = list[0];
																				
			document.getElementById(div2).innerHTML = list[1];			
			
			// executa scripts
			extraiScript(XMLHttp.responseText);
		}
	};
	
	XMLHttp.send(null);
}
// />

// Get simples
function getId3(url,div,div2,div3) {
	/* Abrindo alerta de carregamento */
	if(document.getElementById('pop') != undefined){
		document.getElementById('pop').style.display = "block";						
	}
		
	var result = document.getElementById(div);
  
	var XMLHttp = generateXMLHttp();
	XMLHttp.open("get", url, true);
	XMLHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
 
	XMLHttp.onreadystatechange = function (){		 		
		
		if(XMLHttp.readyState == 4){		
			
			/* Fechando alerta de carregamento */
			if(document.getElementById('pop') != undefined ){
				document.getElementById('pop').style.display = "none";
			}
			
			var retorno = XMLHttp.responseText;

			var list = retorno.split("<list>");

			// Exibe o retorno

			result.innerHTML = list[0];
																				
			document.getElementById(div2).innerHTML = list[1];			
			
			document.getElementById(div3).innerHTML = list[2];			
			
			// executa scripts
			extraiScript(XMLHttp.responseText);
		}
	};
	
	XMLHttp.send(null);
}
// />


// Cadastro e edição de registros
function altNivelCurso(url,div,div2,n_form){
	/* Abrindo alerta de carregamento */

	if(document.getElementById('pop') != undefined){
		document.getElementById('pop').style.display = "block";						
	}
	
	var formInsert   = document.forms[n_form];
	var fieldsValues = generateFieldsValues(formInsert);
	var result       = document.getElementById(div);
   
	var XMLHttp = generateXMLHttp();
	XMLHttp.open("post", url, true);
	XMLHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
 
	XMLHttp.onreadystatechange = function (){
				
		if(XMLHttp.readyState == 4){			
			
			/* Fechando alerta de carregamento */
			
			if(document.getElementById('pop') != undefined ){
				document.getElementById('pop').style.display = "none";
			}
			
			var retorno = XMLHttp.responseText;

			var list = retorno.split("<list>");

			// Exibe o retorno

			result.innerHTML = list[0];
																				
			document.getElementById(div2).innerHTML = list[1];
									
			// executa scripts
			extraiScript(XMLHttp.responseText);							
		}
	};
	
	XMLHttp.send(fieldsValues);
}

// Cadastro e edição de registros
function cadastroRapido(url,div,n_form){
	/* Abrindo alerta de carregamento */
	
	if(document.getElementById('pop') != undefined){
		document.getElementById('pop').style.display = "block";						
	}
	
	var formInsert   = document.forms[n_form];
	var fieldsValues = generateFieldsValues(formInsert);
	var result       = document.getElementById(div);
   
	var XMLHttp = generateXMLHttp();
	XMLHttp.open("post", url, true);
	XMLHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
 
	XMLHttp.onreadystatechange = function (){
				
		if(XMLHttp.readyState == 4){			
			
			/* Fechando alerta de carregamento */
			
			if(document.getElementById('pop') != undefined ){
				document.getElementById('pop').style.display = "none";
			}
			
			// Exibe o retorno
			result.innerHTML = XMLHttp.responseText;
									
			// executa scripts
			extraiScript(XMLHttp.responseText);							
		}
	};
	
	XMLHttp.send(fieldsValues);
}

// Cadastro e edição de registros
function post(url,btn,btn_load,div,n_form){
		
	document.getElementById(btn).style.display = "none";
	document.getElementById(btn_load).style.display = "block";

	var formInsert   = document.forms[n_form];
	var fieldsValues = generateFieldsValues(formInsert); 	
	var result       = document.getElementById(div);
   
	var XMLHttp = generateXMLHttp();
	XMLHttp.open("post", url, true);
	XMLHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

	XMLHttp.onreadystatechange = function (){

		if(XMLHttp.readyState == 4){			
			
			document.getElementById(btn).style.display = "block";
			document.getElementById(btn_load).style.display = "none";						
			
			// Exibe o retorno
			result.innerHTML = XMLHttp.responseText;
									
			// executa scripts
			extraiScript(XMLHttp.responseText);			
		}
	};
	
	XMLHttp.send(fieldsValues);
}

function altPerfil(url,div,div2,n_form){
	/* Abrindo alerta de carregamento */
	
	if(document.getElementById('pop') != undefined){
		document.getElementById('pop').style.display = "block";						
	}

	var formInsert   = document.forms[n_form];
	var fieldsValues = generateFieldsValues(formInsert); 	
	var result       = document.getElementById(div);
   
	var XMLHttp = generateXMLHttp();
	XMLHttp.open("post", url, true);
	XMLHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
 
	XMLHttp.onreadystatechange = function (){
				
		if(XMLHttp.readyState == 4){			
		
			if(document.getElementById('pop') != undefined ){
				document.getElementById('pop').style.display = "none";
			}
			
			var retorno = XMLHttp.responseText;

			var list = retorno.split("<list>");

			// Exibe o retorno

			result.innerHTML = list[0];
			
			if(list[1].length > 1){
				document.getElementById(div2).innerHTML = list[1];
			}
			
			// executa scripts
			extraiScript(XMLHttp.responseText);			
		}
	};
	
	XMLHttp.send(fieldsValues);
}


function altVaga(url,div,n_form){
	/* Abrindo alerta de carregamento */
	
	if(document.getElementById('pop') != undefined){
		document.getElementById('pop').style.display = "block";						
	}

	var formInsert   = document.forms[n_form];
	var fieldsValues = generateFieldsValues(formInsert); 	
	var result       = document.getElementById(div);
   
	var XMLHttp = generateXMLHttp();
	XMLHttp.open("post", url, true);
	XMLHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
 
	XMLHttp.onreadystatechange = function (){
				
		if(XMLHttp.readyState == 4){			
			
			/* Fechando alerta de carregamento */
			if(document.getElementById('pop') != undefined ){
				document.getElementById('pop').style.display = "none";
			}

			var retorno = XMLHttp.responseText;

			var list = retorno.split("<list>");

			// Exibe o retorno
			if(list[0].length > 5){
				result.innerHTML = list[0];
			}

			document.getElementById('title').innerHTML = list[1];
			
			document.getElementById('nome_vaga').innerHTML = list[1];
									
			// executa scripts
			extraiScript(XMLHttp.responseText);	
			
		}
	};
	
	XMLHttp.send(fieldsValues);
}

// Gerando post
function generateFieldsValues(formInsert){
	var strReturn = new Array();
 
	for(var i=0; i< formInsert.elements.length; i++){
						
		if(formInsert.elements[i].checked == true || (formInsert.elements[i].type != "checkbox" && formInsert.elements[i].type != "radio")){		
			var str = encodeURIComponent(formInsert.elements[i].name);
			str    += "=";
			str    += encodeURIComponent(formInsert.elements[i].value);
			strReturn.push(str);			
		}		
	}
	
	return strReturn.join("&");
}
// />


// Executando os scripts da pagina carregada
function extraiScript(texto){
	var ini, pos_src, fim, codigo;
	var objScript = null;
	ini = texto.indexOf('<script', 0)
	while (ini!=-1){
		var objScript = document.createElement("script");
		//Busca se tem algum src a partir do inicio do script
		pos_src = texto.indexOf(' src', ini)
		ini = texto.indexOf('>', ini) + 1;

		//Verifica se este e um bloco de script ou include para um arquivo de scripts
		if (pos_src < ini && pos_src >=0){//Se encontrou um "src" dentro da tag script, esta e um include de um arquivo script
			//Marca como sendo o inicio do nome do arquivo para depois do src
			ini = pos_src + 4;
			//Procura pelo ponto do nome da extencao do arquivo e marca para depois dele
			fim = texto.indexOf('.', ini)+4;
			//Pega o nome do arquivo
			codigo = texto.substring(ini,fim);
			//Elimina do nome do arquivo os caracteres que possam ter sido pegos por engano
			codigo = codigo.replace("=","").replace(" ","").replace("\"","").replace("\"","").replace("\'","").replace("\'","").replace(">","");
			// Adiciona o arquivo de script ao objeto que sera adicionado ao documento
			objScript.src = codigo;
		}else{//Se nao encontrou um "src" dentro da tag script, esta e um bloco de codigo script
			// Procura o final do script
			fim = texto.indexOf('</script>', ini);
			// Extrai apenas o script
			codigo = texto.substring(ini,fim);
			// Adiciona o bloco de script ao objeto que sera adicionado ao documento
			objScript.text = codigo;
		}

		//Adiciona o script ao documento
		document.body.appendChild(objScript);
		// Procura a proxima tag de <script
		ini = texto.indexOf('<script', fim);

		//Limpa o objeto de script
		objScript = null;
	}
}
// />


// Centraliza a div de poup no centro da tela independente do scroll
function id( el ){
	return document.getElementById( el );
}

var marginTopoInicial;//criando variavel de escopo global
window.onload = function(){
	calcMarginTop();
	marginTopoInicial = parseInt( getMarginTop( id('pop') ) );//recuperando o marginTop inicial		
}
window.onscroll = function(){
	id('pop').style.marginTop = marginTopoInicial+window.pageYOffset+'px';//levando em conta o scroll
}
window.onresize = function(){
	calcMarginTop();
}

function calcMarginTop()
{
	var pop = id('pop');
	var marginTopo = parseInt( getMarginTop( pop ) );
	var marginBaixo = parseInt( getMarginBottom( pop ) );

	if( pop.offsetTop < -1 )
		pop.style.marginTop = '-'+( parseInt( pop.offsetTop ) - marginTopo )+'px';
	else if( marginTopo!=marginBaixo )
	pop.style.marginTop = '-248px';
}

function getMarginTop( el )
{
	if( window.getComputedStyle )
		return document.defaultView.getComputedStyle(el, null).marginTop;
	else if( el.currentStyle )
		return el.currentStyle['marginTop'];
}
function getMarginBottom( el )
{
	if( window.getComputedStyle )
		return document.defaultView.getComputedStyle(el, null).marginBottom;
	else if( el.currentStyle )
		return el.currentStyle['marginBottom'];
}
//
