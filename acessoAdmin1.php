<!DOCTYPE HTML>

<html><!-- #BeginTemplate "/Templates/AcessoAdmin1Template.dwt" --><!-- DW6 -->
<head>

<title>Bem-vindo</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600" rel="stylesheet" type="text/css" />
<!--[if lte IE 8]><script src="js/html5shiv.js"></script><![endif]-->
<script src="js/request/AjaxRequest.js"></script>
<script src="js/jquery.min.js"></script>
<script src="js/skel.min.js"></script>
<script src="js/skel-panels.min.js"></script>
<script src="js/init.js"></script> 
<!-- Mascaras para formulario" -->
<script>
function mascara(o,f){
	v_obj=o
	v_fun=f
	setTimeout("execmascara()",1)
}

function execmascara(){
	v_obj.value=v_fun(v_obj.value)
}

function leech(v){
	v=v.replace(/o/gi,"0")
	v=v.replace(/i/gi,"1")
	v=v.replace(/z/gi,"2")
	v=v.replace(/e/gi,"3")
	v=v.replace(/a/gi,"4")
	v=v.replace(/s/gi,"5")
	v=v.replace(/t/gi,"7")
	return v
}

///////////////////////////////////////////////////


function cpf(v){
	v=v.replace(/\D/g,"")                    //Remove tudo o que não é dígito
	v=v.replace(/(\d{3})(\d)/,"$1.$2")       //Coloca um ponto entre o terceiro e o quarto dígitos
	v=v.replace(/(\d{3})(\d)/,"$1.$2")       //Coloca um ponto entre o terceiro e o quarto dígitos
											 //de novo (para o segundo bloco de números)
	v=v.replace(/(\d{3})(\d{1,2})$/,"$1-$2") //Coloca um hífen entre o terceiro e o quarto dígitos
	return v
}

function cnpj(v){
    v=v.replace(/\D/g,"")                           //Remove tudo o que não é dígito
    v=v.replace(/^(\d{2})(\d)/,"$1.$2")             //Coloca ponto entre o segundo e o terceiro dígitos
    v=v.replace(/^(\d{2})\.(\d{3})(\d)/,"$1.$2.$3") //Coloca ponto entre o quinto e o sexto dígitos
    v=v.replace(/\.(\d{3})(\d)/,".$1/$2")           //Coloca uma barra entre o oitavo e o nono dígitos
    v=v.replace(/(\d{4})(\d)/,"$1-$2")              //Coloca um hífen depois do bloco de quatro dígitos
    return v
}
    

</script>
<noscript>
<link rel="stylesheet" href="css/skel-noscript.css" />
<link rel="stylesheet" href="css/style.css" />
<link rel="stylesheet" href="css/style-wide.css" />
</noscript>
<!--[if lte IE 9]><link rel="stylesheet" href="css/ie9.css" /><![endif]-->
<!--[if lte IE 8]><link rel="stylesheet" href="css/ie8.css" /><![endif]-->
</head><body>

<div id='alerta'> </div>

<!-- Header -->
<div id="header" class="skel-panels-fixed">
  <div class="top">
    <!-- Nav -->
    <nav id="nav">
      <ul>
        <li><a href="#top" id="top-link" class="skel-panels-ignoreHref"><span class="fa fa-home">Sobre o projeto</span></a></li>
        <li><a href="#acesso" id="acesso-link" class="skel-panels-ignoreHref"><span class="fa fa-gear">Acesse seu painel</span></a></li>
      </ul>
    </nav>
  </div>
  <div class="bottom">
    <!-- Social Icons -->
    <ul class="icons">
      <li><a href="#" class="fa fa-facebook solo"><span>Facebook</span></a></li>
      <li><a href="#" class="fa fa-envelope solo"><span>Email</span></a></li>
      <!--
        <li><a href="#" class="fa fa-github solo"><span>Github</span></a></li>
        <li><a href="#" class="fa fa-dribbble solo"><span>Dribbble</span></a></li>
        <li><a href="#" class="fa fa-envelope solo"><span>Email</span></a></li>
       -->
    </ul>
  </div>
</div>
<!-- Main -->
<div id="main">
  <!-- Intro -->
  <section id="top" class="one">
    <div class="container">
       <a  class="image featured"><img src="banner.jpg" alt="" /></a>
      <header>
        <h2 class="alt" style="margin-top:-50px;"><strong>Bem Vindo!!</strong></h2>
        <h3>Venha construir seu Futuro Conosco! Na QI você terá a oportunidade de crescer junto com uma das maiores Instituições de Educação Tecnológica do Sul do País!!</h3>
      </header>
    </div>
  </section>
  <!-- Contact -->

  <!-- Portfolio -->
  <section id="acesso" class="four">
    <div class="container">
      <header>
        <h2>Acesse seu painel</h2>
      </header>
      <p><!--Acesse seu Painel e gerencie o seu futuro! Para atualizar seu currículo, acompanhar as vagas

abertas e os processos seletivos que você está participando, basta digitar o cpf e senha 

cadastrados. <br />Boa sorte!--></p>
      <form onSubmit="return false" id="login_sistema">

        <div class="row half">
          <div class="6u" id='input_acesso'>
            <input type="text" class="text" name="txtLogin" placeholder="E-mail de acesso" o/>
          </div>
          <div class="6u">
            <input type="password" class="text" name="txtSenha" placeholder="Informe sua senha" />
          </div>
        </div>
        <!--
        <div class="row half">
            <div class="12u">
                <textarea name="message" placeholder="Message"></textarea>
            </div>
        </div>
        -->
        <div class="row">
          <div class="12u" id='btn_load_1'>
        		Processando...
	      </div>
            
    	  <div class="12u" id="btn_1"> 
          	
            <a class="button submit" style="margin-top:10px;" onclick='post("Controller/Login.controller.php?op=2","btn_1","btn_load_1","retorno_1","login_sistema")'>Efetuar o acesso</a> 
            
            <div id='retorno_1' style="margin-top:15px;"></div>
             
          </div>
        </div>
      </form>
    </div>
  </section>
</div>
<!-- Footer -->
<div id="footer">
  <!-- Copyright -->
  <div class="copyright">
    <ul class="menu">
      <li>AST Facilities.</li>
    </ul>
  </div>
</div>
</body>
<!-- #EndTemplate --></html>
