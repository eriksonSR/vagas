<style>
#dataTables-example4_length{
	display:none;
	
}

#dataTables-example4{
	margin-bottom:15px;
}
</style>

<header>
 <h2>Vagas encontradas</h2>
 <p>Acesse a vaga que deseja visualizar</p>
</header>

<?php	
echo "<table id='dataTables-example4'>";
				
echo "<thead>";
		
		if(count($list_vaga) > 0){
			echo "<tr>					
					<th>Vaga</th>								
					<th>Área</th>
					<th>Remuneração</th>
					<th>Cidade</th>								
					<th>Candidatos</th>
					<th>Nível</th>											
					<th>&nbsp;</th>
				  </tr>";						
		}else{
			echo "<tr>
					<th>Aviso</th>
				  </tr>";
		}						
												
echo "</thead>
	  <tbody>";
		if(count($list_vaga) > 0){
			
			$relaVagaDAO = new RelaVagaDAO();
			$recrutadorDAO = new RecrutadorDAO();
			
			foreach($list_vaga as $list){
				
				$recrutador = $recrutadorDAO->getId($list->id_recrutador);
			
				echo "<tr>						
						<th>".$list->nome."</th>									
						<th>".$list->area."</th>									
						<th> R$ ".number_format($list->salario, 2, ',', '.')."</th>
						<th>".$list->cidade."</th>
						<th><b>".$relaVagaDAO->getContCandidatoVaga($list->id)."</b></th>";
						if($list->nivel == 1){
							echo "<th><b style='color:green;'>Captação</b></th>";
						}else if($list->nivel == 2){
							echo "<th><b style='color:blue;'>Concluida</b></th>";										
						}else if($list->nivel == 3){
							echo "<th><b style='color:red;'>Cancelada</b></th>";
						}									
												
						?>
						
						<th> <a href="Controller/Vaga.controller.php?op=<?php echo sha1(2) ?>&id=<?php echo base64_encode($list->id) ?>">&nbsp;&nbsp;<u>Explorar</u>&nbsp;&nbsp;</a> </th>
						<?php
				echo "</tr>";		
			}						
		}else{
			echo "<tr>
					<th>Não vagas cadastrados.</th>
				  </tr>";
				
		}
echo "	</tbody>
	  </table>";

?>

<div class="row" style="margin-top:25px;">
    <div class="12u">
	    <a class="button submit" onClick='getId("Controller/Vaga.controller.php?op=<?php echo sha1(1) ?>","gestor_vaga")'>Filtrar novas vagas</a>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('#dataTables-example4').dataTable();
    });
</script>
