<div class="row half" style="margin-top:-15px;">                  

  <div class="6u" id='input_full'> <b style="color:#000000;">Beneficios</b> <br />
   <?php
   echo substr($vaga[0]->beneficio,0,strlen($vaga[0]->beneficio) - 1);
   ?>
  </div>  
  
  <div class="6u" id='input_full'> <b style="color:#000000;">Remuneração</b> <br />
   <?php
   echo "R$ ".number_format($vaga[0]->salario, 2, ',', '.');
   ?>
  </div>  
</div>

<div class="row half" style="margin-top:-15px;">
  <div class="12u" id='input_full_2'> <b style="color:#000000;">Candidatos com experiência em</b> <br />
   <?php
    echo $vaga[0]->area_experiencia;
   ?>
  </div>  
</div> 

<div class="row half" style="margin-top:-15px;">
  <div class="6u" id='input_full'> <b style="color:#000000;">Área da vaga</b> <br />
   <?php
   echo $vaga[0]->area;
   ?>
  </div>          
  
  <div class="6u" id='input_full'> <b style="color:#000000;">Atividade da área</b> <br />
   <?php
   echo $vaga[0]->atividade;
   ?>
  </div>
</div>

<div class="row half">
  
  <div class="6u" id='input_full'> <b style="color:#000000;">Nível da vaga</b> <br />
   <?php
   echo substr($vaga[0]->escolaridade,0,strlen($vaga[0]->escolaridade) - 1);
   ?>
  </div>   
  
  <div class="6u" id='input_full'> <b style="color:#000000;">Idiomas desejados</b> <br />
   <?php
   echo substr($vaga[0]->idioma,0,strlen($vaga[0]->idioma) - 1);
   ?>
  </div>      
</div>

<div class="row half">
  <div class="6u" id='input_full'> <b style="color:#000000;">Cidade</b> <br />
   <?php
   echo $vaga[0]->cidade;
   ?>
  </div>
  
  <div class="6u" id='input_full'> <b style="color:#000000;">Horário</b> <br />
   <?php
   echo $vaga[0]->horario;
   ?>
  </div>          
</div>

<div class="row half">
  <div class="6u" id='input_full'> <b style="color:#000000;">Data de cadastro</b> <br />
   <?php
   echo date('d/m/Y',strtotime($vaga[0]->data));
   ?>
  </div>
  
  <div class="6u" id='input_full'> <b style="color:#000000;">Hora de cadastro</b> <br />
   <?php
   echo $vaga[0]->hora;
   ?>
  </div>          
</div>

<div class="row half">
  <div class="12u" id='input_full_2'> <b style="color:#000000;">Informações adicionais</b> <br />
   <?php
   echo $vaga[0]->informacao_adicional;
   ?>
  </div>                    
</div>

<div class="row">
				
	<?php
    if($vaga[0]->nivel != 2){
    
        echo "<div class='12u' id='botao_vaga'>";									
        
        ?>
        <a class="button submit" onClick='getId("Controller/Vaga.controller.php?op=12","alerta")'>Concluir vaga</a> &nbsp		              		<?php
                    
        if($vaga[0]->nivel == 1){
            ?>
            <a class="button submit" onClick='getId2("Controller/Vaga.controller.php?op=11&nv=3","botao_vaga","nivel_vaga")'>Cancelar vaga</a> 
            <?php
        }else if($vaga[0]->nivel == 3){
            ?>
            <a class="button submit" onClick='getId2("Controller/Vaga.controller.php?op=11&nv=1","botao_vaga","nivel_vaga")'>Ativar vaga</a> 
            <?php
        }
                    
        echo "</div>";
    }
    ?>
                
  	<div class="12u">  
      <a onclick='getId("Controller/Vaga.controller.php?op=3","dados_vaga")'> Desejo editar esta vaga</a>
  	</div>
</div>
