<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
<br />
<header>
<h2>Meus interesses profissionais</h2>
<p>Conte-nos seus interesses para que possamos indicar a vaga certa para o seu perfil.</p>
</header>
<form onSubmit="return false" id="cad_interesse">

<div class="row half">
  <div class="6u">            
     <select name="cbCargo" onChange='cadastroRapido("Controller/AreaInteresse.controller.php?op=3","return_funcao","cad_interesse")'>
     
        <option value=''> Área de interesse</option>
        
        <option value='Arquitetura'>Arquitetura</option>
        <option value='Cobrança'>Cobrança</option>               
        <option value='Comercial'>Comercial</option>
        <option value='CPD'>CPD</option>
        <option value='Departamento pessoal'>Departamento pessoal</option>
        <option value='Educação'>Educação</option>
        <option value='Financeiro'>Financeiro</option>
        <option value='Juridico'>Jurídico</option>
        <option value='Legislação academica'>Legislação acadêmica</option>                                
        <option value='Manutenção'>Manutenção</option>
        <option value='Marketing'>Marketing</option>
        <option value='Recrutamento e seleção'>Recrutamento e seleção</option>
        <option value='Serviços gerais'>Serviços gerais</option>
        <option value='Suprimentos'>Suprimentos</option>                                                                
        <option value='Juridico'>Jurídico</option>
        <option value='TI - Desenvolvimento'>TI - Desenvolvimento</option>
        <option value='TI - Infraestrutura'>TI - Infraestrutura</option>
        <option value='Treinamento'>Treinamento</option>                
     </select>                      
    
  </div>
  <div class="6u" id='return_funcao'>
    <select name="cbFuncao" disabled>
     
        <option value=''> Selecione a área </option>                                
     
     </select>
  </div>
</div>

<div class="row">
    <div class="12u">
        <a class="button submit" onclick='cadastro("Controller/AreaInteresse.controller.php?op=1","alerta","cad_interesse")'>Cadastrar interesse</a>
    </div>
    <div class="12u">
	   <a onClick='getId("Controller/AreaInteresse.controller.php?op=<?php echo sha1(1) ?>","alerta")'>Visualizar meus interesses profissionais</a>
    </div>
</div>

</form> 
</body>
</html>
