<style>
#dataTables-example4_length{
	display:none;
	
}

#dataTables-example4{
	margin-bottom:15px;
}
</style>

<header>
 <h2>Vagas encontradas</h2>
 <p>Acesse a vaga que deseja visualizar</p>
</header>
<?php	  

echo "<table id='dataTables-example4'>";
				
echo "	<thead>";
				
			if(count($list_vaga) > 0){
				echo "<tr>
						<th>Vaga</th>
						<th>Área</th>
						<th>Remuneração</th>
						<th>Cidade</th>														
						<th>&nbsp;</th>
					  </tr>";						
			}else{
				echo "<tr>
						<th>Aviso</th>
					  </tr>";
			}						
											
echo "	</thead>
		<tbody>";
			if(count($list_vaga) > 0){
				foreach($list_vaga as $list){
					
					echo "<tr>
							<th>".$list->nome."</th>
							<th>".$list->area."</th>
							<th> R$ ".number_format($list->salario, 2, ',', '.')."</th>
							<th>".$list->cidade."</th>";
							?>
							<th> <a href="Controller/Vaga.controller.php?op=<?php echo sha1(2) ?>&id=<?php echo base64_encode($list->id) ?>">&nbsp;&nbsp;<u>Explorar</u>&nbsp;&nbsp;</a> </th>
							<?php
					echo "</tr>";		
				}						
			}else{
				echo "<tr>";
						if($_GET['op'] == 6){
							echo "<th>Você não se candidatou a uma vaga ainda.</th>";						
						}else{
							echo "<th>Não encontramos vagas com aos dados fornecidos.</th>";
						}
				echo "</tr>";
					
			}
echo "	</tbody>
	 </table>";
			 
?>

<div class="row" style="margin-top:25px;">
    <div class="12u">
	    <a class="button submit" onclick='getId("Controller/Vaga.controller.php?op=4","gestor_vaga")'>Filtrar novas vagas</a>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('#dataTables-example4').dataTable();
    });
</script>
