<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
<style>
#popup_foco_rela_vaga { 	 	
	display: none;
	position: absolute;
 	top:2%; 
	
	background:#FFFFFF;  
	z-index:100; /* Layering ( on-top of others), if you have lots of layers: I just maximized, you can change it yourself */
	/* additional features, can be omitted */
	border:2px solid #CCCCCC;  	
	padding:15px;  
	font-size:15px;  
	-moz-box-shadow: 0 0 5px #CCCCCC;
	-webkit-box-shadow: 0 0 5px #CCCCCC;
	box-shadow: 0 0 2px #999999;
}

@media screen and (min-width:975px){
	#popup_foco_rela_vaga { 	
		left: 30%;			
		width:60%; 
	}
}

@media screen and (max-width:975px){
	#popup_foco_rela_vaga { 	 	
		left: 5%;
		width:90%; 
	}
}
</style>

<script type="text/javascript">
	
	$(document).ready( function() {		
		
		// When site loaded, load the Popupbox First
		loadPopupBox();
	
		$('#popupBoxClose').click( function() {			
			unloadPopupBox();									  			 
		});
		
		function unloadPopupBox() {	// TO Unload the Popupbox
			$('#popup_foco_rela_vaga').fadeOut("slow");	
			$("#popup_foco_rela_vaga").remove(); 									 		
		}	
		
		function loadPopupBox() {	// To Load the Popupbox				
			$('#popup_foco_rela_vaga').fadeIn("slow");
		}
		/**********************************************************/

	});
</script>

<script>
// Centraliza a div de poup no centro da tela independente do scroll
function id( el ){
	return document.getElementById( el );
}

var marginTopoInicial;//criando variavel de escopo global

calcMarginTop();

//levando em conta o scroll
id('popup_foco_rela_vaga').style.marginTop = marginTopoInicial+window.pageYOffset+'px';

function calcMarginTop()
{
	var pop = id('popup_foco_rela_vaga');
	var marginTopo = parseInt( getMarginTop( pop ) );
	var marginBaixo = parseInt( getMarginBottom( pop ) );

	if( pop.offsetTop < -1 )
		pop.style.marginTop = '-'+( parseInt( pop.offsetTop ) - marginTopo )+'px';
	else if( marginTopo!=marginBaixo )
	pop.style.marginTop = '-248px';
}

function getMarginTop( el )
{
	if( window.getComputedStyle )
		return document.defaultView.getComputedStyle(el, null).marginTop;
	else if( el.currentStyle )
		return el.currentStyle['marginTop'];
}
function getMarginBottom( el )
{
	if( window.getComputedStyle )
		return document.defaultView.getComputedStyle(el, null).marginBottom;
	else if( el.currentStyle )
		return el.currentStyle['marginBottom'];
}
//
</script>
<style>
#dataTables-example_length{
	margin-bottom:-60px;
}
</style>
<?php	
	echo "<div id='popup_foco_rela_vaga'>                    
			 <a id='popupBoxClose'><u>Fechar</u></a>
	
			 <div style='margin-top:20px;' id='retorno_curso'>                    

               <table id='dataTables-example'>
					<caption><h2> Candidatos relacionados a vagas. </h2></caption>";

	echo "			<thead>";
					
					if(count($list_rela_vaga) > 0){
						echo "<tr>								
								<th>Candidato</th>
								<th>Código</th>
								<th>Vaga</th>
								<th>Bolsa-auxílio</th>
								<th>Data</th>														
								<th>Hora</th>
						     </tr>";						
					}else{
						echo "<tr>
								<th>Aviso</th>
	 						  </tr>";
					}						
												
	echo "			</thead>
					<tbody>";
					if(count($list_rela_vaga) > 0){
						foreach($list_rela_vaga as $list){							
							echo "<tr>
									<th>".$list->nome_candidato."</th>
									<th>".$list->codigo_vaga."</th>
									<th>".$list->nome_vaga."</th>
									<th> R$ ".number_format($list->salario_vaga, 2, ',', '.')."</th>									
									<th>".date('d/m/Y',strtotime($list->data))."</th>
									<th>".$list->hora."</th>
								 </tr>";		
						}						
					}else{
						echo "<tr>
								<th>Não há candidatos relacionados a vaga.</th>
							  </tr>";
							
					}
	echo "			</tbody>
				 </table>
			  </div> 			
	  	   </div>";

?>

<script>
    $(document).ready(function(){
        $('#dataTables-example').dataTable();
    });
</script>
</body>
</html>
