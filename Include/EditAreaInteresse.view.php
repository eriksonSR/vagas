<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
<br />
<header>
<h2>Meus interesses profissionais</h2>
<p>Conte-nos seus interesses para que possamos indicar a vaga certa para o seu perfil.</p>
</header>
<form onSubmit="return false" id="form_edit_interesse">

<div class="row half">
  <div class="6u">            
     <select name="cbCargo" onChange='cadastroRapido("Controller/AreaInteresse.controller.php?op=3","return_funcao","form_edit_interesse")'>
       
        <option value=''> Área de interesse</option>
            
            <option value='Arquitetura'>Arquitetura</option>
            <option value='Cobrança'>Cobrança</option>               
            <option value='Comercial'>Comercial</option>
            <option value='CPD'>CPD</option>
            <option value='Departamento pessoal'>Departamento pessoal</option>
            <option value='Educação'>Educação</option>
            <option value='Financeiro'>Financeiro</option>
            <option value='Juridico'>Jurídico</option>
            <option value='Legislação academica'>Legislação acadêmica</option>                                
            <option value='Manutenção'>Manutenção</option>
            <option value='Marketing'>Marketing</option>
            <option value='Recrutamento e seleção'>Recrutamento e seleção</option>
            <option value='Serviços gerais'>Serviços gerais</option>
            <option value='Suprimentos'>Suprimentos</option>                                                                
            <option value='Juridico'>Jurídico</option>
            <option value='TI - Desenvolvimento'>TI - Desenvolvimento</option>
            <option value='TI - Infraestrutura'>TI - Infraestrutura</option>
            <option value='Treinamento'>Treinamento</option>               
             
     	</select>                      
	 
     <script>
		// Setando qual seleção da combo vai ficar marcada. (passa o valor dela)
		form_edit_interesse.cbCargo.value = "<?php echo $areaInteresse[0]->cargo ?>";	 		
	 </script>

  </div>
  <div class="6u" id='return_funcao'>
  	
    <?php
	$list_funcao = array();
	
	if($areaInteresse[0]->cargo == "Arquitetura"){
		$list_funcao = 
		array(
		'Função',
		'ARQUITETO',
		'COORDENADOR / SUPERVISOR',
		'ESTÁGIO'
		);
	}else if($areaInteresse[0]->cargo == "Cobrança"){
		$list_funcao = 
		array(
		'COORDENADOR / SUPERVISOR',
		'ASSIST. RECUP. DE CREDITO',
		'ESTÁGIO'
		);
	}else if($areaInteresse[0]->cargo == "Comercial"){
		$list_funcao = 
		array(
		'GERENTE COMERCIAL',
		'GERENTE COMERCIAL DAS FALCULDADES',
		'GERENTE DE EQUIPE COMERCIAL',
		'ESPECIALISTA EM PLANEJAMENTO DE VENDAS',
		'ANALISTA DE PLANEJAMENTO DE VENDAS',
		'COORDENADOR ADMINISTRATIVO COMERCIAL',
		'AGENDADOR(A)',
		'ATENDENTE DE CHAT',
		'DIVULGADOR (A)',
		'AUXILIAR DE ATENDIMENTO',
		'VENDEDOR(A)',
		'ESTÁGIO'
		);
	}else if($areaInteresse[0]->cargo == "CPD"){
		$list_funcao = 
		array(
		'ESTAGIÁRIO',
		'AUXILIAR DE CPD'
		);
	}else if($areaInteresse[0]->cargo == "Departamento pessoal"){
		$list_funcao = 
		array(
		'ANALISTA / AUXILIAR / ASSITENTE',
		'COORDENADOR / SUPERVISOR',
		'ESTÁGIO'
		);
	}else if($areaInteresse[0]->cargo == "Educação"){
	
		$list_funcao = 
		array(
		'DIRETOR DE FACULDADE',
		'DIRETOR(A) DE ESCOLA',
		'VICE DIRETOR(A) FACULDADE',
		'DIRIGENTE REGIONAL',
		'GERENTE DE FACULDADE',
		'GERENTE DE FILIAL',
		'GESTOR(A) AREA IDIOMA',
		'COORDENADOR DE GRADUAÇÃO',
		'COORDENADOR DE  PÓS GRADUAÇÃO',
		'COORDENADOR EDUCAÇÃO TÉCNICA',
		'COORDENADOR EDUCAÇÃO CURSOS LIVRES',
		'COORDENADOR (A) NADD',
		'COORDENADOR DE IDIOMAS',
		'PROF. NIVEL SUPERIOR',
		'PROFESSOR(A) DE NIVEL TECNICO ADM',
		'PROFESSOR(A) DE NIVEL TECNICO INFO',
		'PROF. TITULAR DE ADMINISTRAÇÃO',
		'TUTOR',
		'INSTRUTOR DE CURSOS LIVRES',
		'INSTRUTOR DE IDIOMAS',
		'GERENTE DE PLANEJAMENTO DE TURMAS',
		'ASSISTENTE DE PLANEJAMENO DE TURMAS',
		'SECRETARIA ACADEMICA',
		'SECRETARIO(A)  GERAL',
		'SECRETARIO(A) DE ESCOLA',
		'INTERPRETE DE LIBRAS',
		'ESTÁGIO'
		);
	
	}else if($areaInteresse[0]->cargo == "Financeiro"){
	
		$list_funcao = 
		array(
		'GERENTE ADMINISTRATIVO FINANCEIRO',
		'ANALISTA / AUXILIAR / ASSISTENTE',
		'COORDENADOR / SUPERVISOR',
		'ESTÁGIO',
		'COORDENADOR / SUPERVISOR'
		);
	
	}else if($areaInteresse[0]->cargo == "Juridico"){
	
		$list_funcao = 
		array(
		'ADVOGADO (A)',
		'COORDENADOR JURIDICO',
		'ESTÁGIO'
		);
	
	}else if($areaInteresse[0]->cargo == "Legislação academica"){
	
		$list_funcao = 
		array(
		'GERENTE DE LEGISLAÇÃO ESCOLA',
		'SUPERVISOR ESCOLAR',
		'ASSIST. DE SUPERV. ESCOLAS',
		'ASSISTENTE PEDAGOGICO',
		'BIBLIOTECARIO',
		'ESTÁGIO'
		);
	
	}else if($areaInteresse[0]->cargo == "Manutenção"){
	
		$list_funcao = 
		array(
		'COORDENADOR DE MANUTENÇÃO',
		'SUPERVISOR DE MANUTENÇÃO',
		'AUXILIAR DE MANUTENÇÃO',
		'ESTOFADOR',
		'MECANICO DE REFRIGERACAO'
		);
	
	}else if($areaInteresse[0]->cargo == "Marketing"){
	
		$list_funcao = 
		array(
		'GERENTE DE MARKETING',
		'DIRETOR DE CRIAÇÃO',
		'ASSITENTE DE CRIAÇÃO',
		'ASSISTENTE DE MARKETING DIGITAL',
		'ASSITENTE DE MARKETING DE EVENTOS',
		'ESTAGIÁRIO'
		);
	
	}else if($areaInteresse[0]->cargo == "Recrutamento e seleção"){
	
		$list_funcao = 
		array(
		'COORDENADOR / SUPERVISOR',
		'ANALISTA / AUXILIAR / ASSISTENTE',
		'ESTÁGIO'
		);
	
	}else if($areaInteresse[0]->cargo == "Serviços gerais"){
	
		$list_funcao = 
		array(
		'ZELADOR',
		'AUXILIAR DE LIMPEZA'
		);
	
	}else if($areaInteresse[0]->cargo == "Suprimentos"){
	
		$list_funcao = 
		array(
		'COORDENADOR / SUPERVISOR',
		'ANALISTA / AUXILIAR / ASSISTENTE',
		'ARQUITETO',
		'ESTÁGIO'
		);
	
	}else if($areaInteresse[0]->cargo == "TI - Desenvolvimento"){
	
		$list_funcao = 
		array(
		'COORDENADOR / SUPERVISOR',
		'ANALISTA / AUXILIAR / ASSISTENTE',
		'ESTÁGIO'
		);
	
	}else if($areaInteresse[0]->cargo == "TI - Infraestrutura"){
	
		$list_funcao = 
		array(
		'COORDENADOR DE T&D',
		'ANALISTA DE T&D',
		'ESTÁGIO'
		);
	
	}else if($areaInteresse[0]->cargo == "Treinamento"){
	
		$list_funcao = 
		array(
		'CORDENADOR DE T&D',
		'ANALISTA DE T&D',
		'ESTAGIÁRIO',
		'SECRETARIA EXECUTIVA'
		);
	}
	
	echo '<select name="cbFuncao" >';
							 
	foreach($list_funcao as $funcao){
		echo "<option value='".$funcao."'> ".$funcao." </option>";
	}
	
	echo '</select>';
	?>

	 
     <script>
		// Setando qual seleção da combo vai ficar marcada. (passa o valor dela)
		form_edit_interesse.cbFuncao.value = "<?php echo $areaInteresse[0]->funcao ?>";	 		
	 </script> 
  </div>
</div>

<div class="row">
    <div class="12u">
        <a class="button submit" onclick='altPerfil("Controller/AreaInteresse.controller.php?op=2&id=<?php echo $_GET['id'] ?>","alerta","edit_interesse","form_edit_interesse")'>Editar interesse</a>
    </div>
    <div class="12u">    
	    <a href="#" onclick='getId("Controller/AreaInteresse.controller.php?op=4","edit_interesse")' >Cancelar edição</a>
    </div>
</div>

</form> 

</body>
</html>
