<div class="container" style="width:95%;">
  <header>
    <h2>Vagas pendentes de aprovação</h2>
  </header>
  <?php	  
      $vagaDAO = new VagaDAO();
      
      $list_vaga = $vagaDAO->getPendente();
      
      echo "<table id='dataTables-example4'>";
                      
      echo "	<thead>";
                      
                  if(count($list_vaga) > 0){
                      echo "<tr>
                              <th>Vaga</th>
                              <th>Escola</th>
                              <th>Bolsa-auxílio</th>
                              <th>Data</th>
                              <th>Atendimento</th>
                              <th>&nbsp;</th>
                              <th>&nbsp;</th>
                            </tr>";						
                  }else{
                      echo "<tr>
                              <th>Aviso</th>
                            </tr>";
                  }						
                                                  
      echo "	</thead>
              <tbody>";
                  if(count($list_vaga) > 0){
                      foreach($list_vaga as $list){
                          
                          echo "<tr>
                                  <th>".$list->nome."</th>
                                  <th>".$list->escola."</th>
                                  <th> R$ ".number_format($list->salario, 2, ',', '.')."</th>
                                  <th>".date('d/m/Y',strtotime($list->data))."</th>
                                  <th>".$list->atendimento."</th>";
                                  ?>
                                  <th> <a href="Controller/Vaga.controller.php?op=<?php echo sha1(2) ?>&id=<?php echo base64_encode($list->id) ?>">&nbsp;&nbsp;<u>Explorar</u>&nbsp;&nbsp;</a> </th>
                                  <th> 
                                      <a onclick='getId("Controller/Vaga.controller.php?op=15&id=<?php echo base64_encode($list->id) ?>","vaga_pendente")'> &nbsp;&nbsp;<u>Cancelar</u>&nbsp;&nbsp;</a> 
                                  </th>
                                  <?php
                          echo "</tr>";		
                      }	
                                          
                  }else{
                      echo "<tr>
                              <th>Não encontramos vagas pendentes.</th>
                           </tr>";
                          
                  }
      echo "	</tbody>
           </table>";					 
      ?>
</div>