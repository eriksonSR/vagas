<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
<?php
$relaVagaDAO = new RelaVagaDAO();

// Capturando o registro de relacionamento da vaga			
$vaga = unserialize($_SESSION['foco_vaga']);

$relaVagax->id_vaga = $vaga[0]->id;			
$relaVagax->id_user = $infoCandidato[0]->id_user;

$relaVaga = $relaVagaDAO->getRela($relaVagax);
// />
?>
<header>
	<h2> 
	<?php
       echo $infoCandidato[0]->nome;
    ?>
    </h2>
	<p>Informações expecificas do candidato.</p>
</header>

<div class="container" style="margin-bottom:20px; margin-top:-5px;">       
    <form onSubmit="return false" id="alt_nota">
        <div class="row half">
            <div class="12u">
                <textarea name="txtNota" placeholder="Crie uma anotação sobre o candidato" onBlur='cadastroRapido("Controller/Candidato.controller.php?op=3","alerta","alt_nota")'><?php 
                if(strlen($infoCandidato[0]->anotacao) > 0){
                    echo $infoCandidato[0]->anotacao;
                } ?></textarea>
            </div>
        </div>
    </form>
</div>
        
<div id='retorno_candidato'>
      
    
    <?php									
	if($relaVaga[0]->nivel == 2){
	?>  
        <div class="row half" style="margin-top:-15px;">
          <div class="6u" id='input_full'> <b style="color:#000000;">Telefone</b> <br />
           <?php
           echo $infoCandidato[0]->tel;
           ?>
          </div>          
        
          <div class="6u" id='input_full'> <b style="color:#000000;">Celular</b> <br />
           <?php
           echo $infoCandidato[0]->cel;
           ?>
          </div>
        </div>
    
    <?php
	}
	?>
    
    <div class="row half" style="margin-top:-15px;">
      <div class="6u" id='input_full'> <b style="color:#000000;">Facebook</b> <br />
       <?php
       echo $infoCandidato[0]->facebook;
       ?>
      </div>          
    
      <div class="6u" id='input_full'> <b style="color:#000000;">Linkedin</b> <br />
       <?php
       echo $infoCandidato[0]->linkedin;
       ?>
      </div>
    </div>
    
    <div class="row half">
      <div class="6u" id='input_full'> <b style="color:#000000;">Data de nascimento</b> <br />
       <?php
	   if(strlen($infoCandidato[0]->nascimento) > 0){
       	echo date('d/m/Y',strtotime($infoCandidato[0]->nascimento));
	   }
       ?>
      </div>          
    
      <div class="6u" id='input_full'> <b style="color:#000000;">Conhecimento em informatica</b> <br />
       <?php
       echo $infoCandidato[0]->informatica;
       ?>
      </div>
    </div>
    
    <div class="row half">
      <div class="6u" id='input_full'> <b style="color:#000000;">CNH</b> <br />
       <?php
        echo substr($infoCandidato[0]->cnh,0,strlen($infoCandidato[0]->cnh) - 1);   
       ?>
      </div>          
    
      <div class="6u" id='input_full'> <b style="color:#000000;">Idiomas que domina</b> <br />
       <?php
        echo substr($infoCandidato[0]->idioma,0,strlen($infoCandidato[0]->idioma) - 1);   
       ?>
      </div>
    </div>
    
    <div class="row half">
      <div class="6u" id='input_full'> <b style="color:#000000;">Estado civil</b> <br />
       <?php
       echo $infoCandidato[0]->estado_civil;
       ?>
      </div>          
    
      <div class="6u" id='input_full'> <b style="color:#000000;">Escolaridade</b> <br />
       <?php
       echo $infoCandidato[0]->escolaridade;
       ?>
      </div>
    </div>
    
    <div class="row half">
      <div class="6u" id='input_full'> <b style="color:#000000;">Possui filhos</b> <br />
       <?php
       echo $infoCandidato[0]->filho_pequeno;
       ?>
      </div>          
    
      <div class="6u" id='input_full'> <b style="color:#000000;">Sexo</b> <br />
       <?php
       echo $infoCandidato[0]->sexo;
       ?>
      </div>
    </div>
    
    <div class="row half">
      <div class="6u" id='input_full'> <b style="color:#000000;">Dias disponíveis</b> <br />
       <?php
       $infoCandidato[0]->dia_disponivel = utf8_encode($infoCandidato[0]->dia_disponivel);
       
       echo substr($infoCandidato[0]->dia_disponivel,0,strlen($infoCandidato[0]->dia_disponivel) - 2);   
     
       ?>
      </div>          
    
      <div class="6u" id='input_full'> <b style="color:#000000;">Turnos disponíveis</b> <br />
       <?php
       echo substr($infoCandidato[0]->turno_disponivel,0,strlen($infoCandidato[0]->turno_disponivel) - 1);      
       ?>
      </div>
    </div>
	
    <div class="row half">
      <div class="6u" id='input_full'> <b style="color:#000000;">Estado</b> <br />
       <?php
       echo $infoCandidato[0]->estado;
       ?>
      </div>          
    
      <div class="6u" id='input_full'> <b style="color:#000000;">Cidade</b> <br />
       <?php
       echo $infoCandidato[0]->cidade;
       ?>
      </div>
    </div>
    
    <div class="row half">
      <div class="6u" id='input_full'> <b style="color:#000000;">Bairro</b> <br />
       <?php
       echo $infoCandidato[0]->bairro;
       ?>
      </div>          
    
      <div class="6u" id='input_full'> <b style="color:#000000;">Rua / Número</b> <br />
       <?php
       echo $infoCandidato[0]->rua;
       ?>
      </div>
    </div>
    
    
	<br />
    
    <?php
	$cursoDAO = new CursoDAO();
	$userx = new User();
	
	$userx->id = base64_decode($_GET['id']);
	
	$list_curso = $cursoDAO->getAll($userx);
	
	echo " <table>
				<caption><h3> Cursos cadastrados</h3></caption>
			
			<thead>";
				
			if(count($list_curso) > 0){
				echo "<tr>
						<th>Nível</th>
						<th>Curso</th>
						<th>Escola</th>
						<th>Descrição</th>
						<th>Data inicio</th>
						<th>Data termino</th>
					  </tr>";						
			}else{
				echo "<tr>
						<th>Aviso</th>
					  </tr>";
			}						
											
	echo "	</thead>
			<tbody>";
			if(count($list_curso) > 0){
				foreach($list_curso as $list){
					echo "<tr>
							<th>".$list->nivel."</th>
							<th>".$list->curso."</th>
							<th>".$list->escola."</th>";
							if(strlen($list->descricao) > 0){
								echo "<th>".$list->descricao."</th>";
							}else{
								echo "<th> - - - </th>";
							}
					echo "	<th>".date('d/m/Y',strtotime($list->data_inicio))."</th>
							<th>".date('d/m/Y',strtotime($list->data_fim))."</th>";

					echo "</tr>";		
				}						
			}else{
				echo "<tr>
						<th>Não há cursos cadastrados.</th>
					  </tr>";
					
			}
	echo "	</tbody>
		 </table>";
	
	$experienciaDAO = new ExperienciaDAO();		
	
	$list_experiencia = $experienciaDAO->getAll($userx);
	
	echo "	 <table>
				<caption><h3>Experiencias profissionais</h3></caption>";
									
				if(isset($aviso)){
					echo "<caption><h3>".$aviso."</h3></caption>";
				}
					
	echo "			<thead>";
					
					if(count($list_experiencia) > 0){
						echo "<tr>
								<th>Empresa</th>
								<th>Cargo</th>
								<th>Descrição</th>
								<th>Data inicio</th>
								<th>Data termino</th>
	 						  </tr>";						
					}else{
						echo "<tr>
								<th>Aviso</th>
	 						  </tr>";
					}						
												
	echo "			</thead>
					<tbody>";
					if(count($list_experiencia) > 0){
						foreach($list_experiencia as $list){
							echo "<tr>
									<th>".$list->empresa."</th>
									<th>".$list->cargo."</th>";
									if(strlen($list->descricao) > 0){
										echo "<th>".$list->descricao."</th>";
									}else{
										echo "<th> - - - </th>";
									}
							echo "	<th>".date('d/m/Y',strtotime($list->data_inicio))."</th>
									<th>".date('d/m/Y',strtotime($list->data_fim))."</th>";

							echo "</tr>";		
						}						
					}else{
						echo "<tr>
								<th>Não há experiencias cadastradas.</th>
							  </tr>";
							
					}
	echo "			</tbody>
				 </table>";				 
	?>
    
	
	<div class="row">  
      
      	<?php									
		if($relaVaga[0]->nivel != 3){
		?>
            <div class="12u">       		
                <?php									
                if($relaVaga[0]->nivel == 1){
                ?>
                    <a class="button submit" onclick='getId("Controller/Vaga.controller.php?op=10&id_vaga=<?php echo base64_encode($relaVaga[0]->id) ?>&id=<?php echo base64_encode($infoCandidato[0]->id_user) ?>","info_candidato")'>Vou entrevistar</a>
                    &nbsp;
                <?php
                }
                ?> 
                <a class="button submit" onclick='getId("Controller/Vaga.controller.php?op=9&id=<?php echo base64_encode($infoCandidato[0]->id_user) ?>","info_candidato")'>Retirar da vaga</a>
            </div>                                    
        <?php
		}
		?>
        
        <div class="12u" style="margin-top:15px;">    
	        <a href="getPdf.php" target="_blank">Gerar currículo em PDF</a>            
        </div>
        
        <div class="12u" style="margin-top:15px;">    
	        <a onclick='getId("Controller/Vaga.controller.php?op=8","info_candidato")'>Retornar para listagem de candidatos</a>    
        </div>
	</div>

</div>


</body>
</html>
