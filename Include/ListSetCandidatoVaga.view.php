<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
<style>
#popup_candidato_vaga { 	 	
	display: none;
	position: absolute;
 	top:2%; 
	
	background:#FFFFFF;  
	z-index:100; /* Layering ( on-top of others), if you have lots of layers: I just maximized, you can change it yourself */
	/* additional features, can be omitted */
	border:2px solid #CCCCCC;  	
	padding:15px;  
	font-size:15px;  
	-moz-box-shadow: 0 0 5px #CCCCCC;
	-webkit-box-shadow: 0 0 5px #CCCCCC;
	box-shadow: 0 0 2px #999999;
}

@media screen and (min-width:975px){
	#popup_candidato_vaga { 	
		left: 30%;			
		width:60%; 
	}
}

@media screen and (max-width:975px){
	#popup_candidato_vaga { 	 	
		left: 5%;
		width:90%; 
	}
}
</style>

<script type="text/javascript">
	
	$(document).ready( function() {		
		
		// When site loaded, load the Popupbox First
		loadPopupBox();
	
		$('#popupBoxClose').click( function() {			
			unloadPopupBox();									  			 
		});
		
		function unloadPopupBox() {	// TO Unload the Popupbox
			$('#popup_candidato_vaga').fadeOut("slow");	
			$("#popup_candidato_vaga").remove(); 									 		
		}	
		
		function loadPopupBox() {	// To Load the Popupbox				
			$('#popup_candidato_vaga').fadeIn("slow");
		}
		/**********************************************************/

	});
</script>

<script>
// Centraliza a div de poup no centro da tela independente do scroll
function id( el ){
	return document.getElementById( el );
}

var marginTopoInicial;//criando variavel de escopo global

calcMarginTop();

//levando em conta o scroll
id('popup_candidato_vaga').style.marginTop = marginTopoInicial+window.pageYOffset+'px';

function calcMarginTop()
{
	var pop = id('popup_candidato_vaga');
	var marginTopo = parseInt( getMarginTop( pop ) );
	var marginBaixo = parseInt( getMarginBottom( pop ) );

	if( pop.offsetTop < -1 )
		pop.style.marginTop = '-'+( parseInt( pop.offsetTop ) - marginTopo )+'px';
	else if( marginTopo!=marginBaixo )
	pop.style.marginTop = '-248px';
}

function getMarginTop( el )
{
	if( window.getComputedStyle )
		return document.defaultView.getComputedStyle(el, null).marginTop;
	else if( el.currentStyle )
		return el.currentStyle['marginTop'];
}
function getMarginBottom( el )
{
	if( window.getComputedStyle )
		return document.defaultView.getComputedStyle(el, null).marginBottom;
	else if( el.currentStyle )
		return el.currentStyle['marginBottom'];
}
//
</script>
<?php	
	echo "<div id='popup_candidato_vaga'>                    
			 <a id='popupBoxClose'><u>Fechar</u></a>
			 
			 <div style='margin-top:20px;' id='retorno_vaga'>";                  
	
			$relaVagaDAO = new RelaVagaDAO();
			
			$list_info_candidato = $relaVagaDAO->getUserRecrutado($vaga[0]->id);
			
			if(count($list_info_candidato) > 0){
				echo "<table>
						<thead>
						  <tr>
							<th>Nome</th>
							<th>Idade</th>
							<th>Cidade</th>
							<th>&nbsp;</th>
						  </tr>
						</thead>
						<tbody>";
						foreach($list_info_candidato as $list){
							echo "<tr>
									<th>".$list->nome."</th>";
									
									if(strlen($list->nascimento) > 0){
										$hoje = date('Y-d-m');
									
										$idade =  $hoje - $list->nascimento;
										
										echo "<th>".$idade."</th>";
									}else{
										echo "<th> --- </th>";
									}
									
									if(strlen($list->cidade) > 0){
										echo "<th>".$list->cidade."</th>";
									}else{
										echo "<th> --- </th>";
									}									
									
									?>
									<th> <a onClick='getId3("Controller/Vaga.controller.php?op=<?php echo sha1(13) ?>&id=<?php echo base64_encode($list->id_user) ?>","botao_vaga_all","nivel_vaga","retorno_candidato")' >&nbsp;&nbsp;<u>Candidato selecionado</u>&nbsp;&nbsp;</a> </th>
									<?php
									
							echo "</tr>";
						}					  
				echo "	</tbody>
					  </table>";
			}else{
				echo "<table>
						<thead>
						  <tr>
							<th>Aviso</th>
						  </tr>
						</thead>
						<tbody>
						  <tr>
							<th>Não há candidatos recrutados.</th>
						  </tr>
						</tbody>
					  </table>";
			}
	  
      
				 
	echo "  </div> 			
	  	   </div>";

?>


</body>
</html>
