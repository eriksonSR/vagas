<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
<?php

if(!isset($filtro_candidato)){
	$relaVagaDAO = new RelaVagaDAO();
	
	$vaga = unserialize($_SESSION['foco_vaga']);
	
	$list_info_candidato = $relaVagaDAO->getUser($vaga[0]->id);
}

if(count($list_info_candidato) > 0){
	if(isset($_SESSION['ct_empresa'])){
		echo "<table>
				<thead>
				  <tr>
					<th>Nome</th>
					<th>Idade</th>
					<th>Cidade</th>
					<th>Nível</th>
					<th>&nbsp;</th>
				  </tr>
				</thead>
				<tbody>";
				foreach($list_info_candidato as $list){
					echo "<tr>
							<th>".$list->nome."</th>";
							
							if(strlen($list->nascimento) > 0){
								$hoje = date('Y-d-m');
							
								$idade =  $hoje - $list->nascimento;
								
								echo "<th>".$idade."</th>";
							}else{
								echo "<th> --- </th>";
							}
							
							if(strlen($list->cidade) > 0){
								echo "<th>".$list->cidade."</th>";
							}else{
								echo "<th> --- </th>";
							}
																									
							if($list->nivel_candidatura == 1){
								echo "<th style='color:red;'> Candidato </th>";
							}else if($list->nivel_candidatura == 2){
								echo "<th style='color:green;'> Selecionado </th>";
							}else if($list->nivel_candidatura == 3){
								echo "<th style='color:blue;'> Recrutado </th>";
							}
							
							?>
							<th> <a onclick='getId("Controller/Vaga.controller.php?op=<?php echo sha1(7) ?>&id=<?php echo base64_encode($list->id_user) ?>","info_candidato")' >&nbsp;&nbsp;<u>Explorar</u>&nbsp;&nbsp;</a> </th>
							<?php
						
					echo "</tr>";
				}					  
		echo "	</tbody>
			  </table>";
	}else{
		echo "<table>
				<thead>
				  <tr>
					<th>Candidatos</th>							
				  </tr>
				</thead>
				<tbody>
				<tr>
					<th>".count($list_info_candidato)."</th>
				</tr>
				</tbody>
			  </table>";
	}
}else{
	echo "<table>
			<thead>
			  <tr>
				<th>Aviso</th>
			  </tr>
			</thead>
			<tbody>
			  <tr>";
			  	if(isset($filtro_candidato)){
					echo "<th>Não há candidatos relacionados ao filtro utilizado.</th>";
				}else{
					echo "<th>Não há candidatos relacionados a vaga.</th>";
				}
	echo "	  </tr>
			</tbody>
		  </table>";
}
?>
</body>
</html>
