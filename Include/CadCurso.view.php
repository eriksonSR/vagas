<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
 <br />
<header>
<h2>Formação acadêmica</h2>
<p>O estágio é o complemento da sua trajetória acadêmica. <br />Cadastre todas as suas formações acadêmicas que poderão ajudar a garantir a vaga perfeita para o seu perfil.</p>
</header>
<form onSubmit="return false" id="cad_curso">

<div class="row half">
  <div class="12u">
    <select name="cbNivel" onChange='altNivelCurso("Controller/Curso.controller.php?op=2","curso_input","data_curso","cad_curso")'>
        <option value=''> Nível do curso </option>                
        <option value='Ensino médio'>Ensino médio</option>
        <option value='Profissionalizante'>Profissionalizante</option>
        <option value='Técnicos'>Técnicos</option>
        <option value='Superior'>Superior</option>
        <option value='Pós'>Pós</option>                
    </select>
  </div>
</div>

<div class="row half">
  <div class="6u" id='curso_input'>
    <input type="text" class="text" placeholder="Selecione o nível do curso" disabled/>
  </div>
  <div class="6u">
    <input type="text" class="text" name="txtEscola" placeholder="Escola" />
  </div>
</div>

<div class="row half" id='data_curso'>
  <div class="6u">
    <input type="text" class="text" name="txtDataInicio" placeholder="Data de início" onkeypress='mascara(this,data)' maxlength='10'/>
  </div>
  <div class="6u">
    <input type="text" class="text" name="txtDataFim" placeholder="Data de término" onkeypress='mascara(this,data)' maxlength='10'/>
  </div>
</div>

<div class="row half">
    <div class="12u">
        <textarea name="txtMsg" placeholder="Descrição"></textarea>
    </div>
</div>

<div class="row">
    <div class="12u">
        <a class="button submit" onclick='cadastro("Controller/Curso.controller.php?op=1","alerta","cad_curso")'>Cadastrar formação acadêmica</a>                
    </div>
    <div class="12u">   
		<a onClick='getId("Controller/Curso.controller.php?op=<?php echo sha1(1) ?>","alerta")'><b>Visualizar as formações acadêmicas já cadastradas</b></a>
    </div>
</div>

</form> 
</body>
</html>
