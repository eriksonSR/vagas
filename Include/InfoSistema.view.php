<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
<?php
$userDAO = new UserDAO();
$vagaDAO = new VagaDAO();
$relaVagaDAO = new RelaVagaDAO();
?>

<div class="row half">
          
  <div class="12u">             
    <h3>Candidatos cadastrados</h3>                                         
  </div>        

  <div class="6u" id='input_full'> <b style="color:#000000;">Cadastro completo - liberados</b> <br />
   <a onClick='getId("Controller/Admin.controller.php?op=8&cmd=getUserCompAtivo&tp=user","box_view")'>
   <?php
    echo $userDAO->getContUserCompAtivo($data);
   ?>
   </a>
  </div>
    
  <div class="6u" id='input_full'> <b style="color:#000000;">Cadastro incompleto - liberados</b> <br />
   <a onClick='getId("Controller/Admin.controller.php?op=8&cmd=getUserIncompAtivo&tp=user","box_view")'>
   <?php
    echo $userDAO->getContUserIncompAtivo($data);
   ?>
   </a>   
  </div>    
        
  <div class="6u" id='input_full'> <b style="color:#000000;">Cadastro completo - Desativados</b> <br />
   <a onClick='getId("Controller/Admin.controller.php?op=8&cmd=getUserCompDesativo&tp=user","box_view")'>  
   <?php
    echo $userDAO->getContUserCompDesativo($data);
   ?>
   </a>
  </div>

  <div class="6u" id='input_full'> <b style="color:#000000;">Cadastro incompleto - Desativados</b> <br />
   <a onClick='getId("Controller/Admin.controller.php?op=8&cmd=getUserIncompDesativo&tp=user","box_view")'>    
   <?php
    echo $userDAO->getContUserIncompDesativo($data);
   ?>
   </a>
  </div>
  
</div>

<div class="row half" style="margin-top:-15px;">  
  <div class="12u">             
	<h3>Vagas cadastradas</h3>                                         
  </div>
</div>

<div class="row half">   
  
  <div class="6u" id='input_full'> <b style="color:#000000;">Total cadastrado</b> <br />
   <a onClick='getId("Controller/Admin.controller.php?op=8&cmd=getTotal&tp=vaga","box_view")'>
   <?php
	echo $vagaDAO->getContTotal($data,$id_recrutador);
   ?>
   </a>
  </div>          
  
  <div class="6u" id='input_full'> <b style="color:#000000;">Em captação</b> <br />
   <a onClick='getId("Controller/Admin.controller.php?op=8&cmd=getCaptacao&tp=vaga","box_view")'>  
   <?php
   echo $vagaDAO->getContCaptacao($data,$id_recrutador);
   ?>
   </a>
  </div>          

</div>

<div class="row half">   
  
  <div class="6u" id='input_full'> <b style="color:#000000;">Concluida</b> <br />
   <a onClick='getId("Controller/Admin.controller.php?op=8&cmd=getConcluido&tp=vaga","box_view")'>    
   <?php
   echo $vagaDAO->getContConcluido($data,$id_recrutador);
   ?>
   </a>
  </div>          

  <div class="6u" id='input_full'> <b style="color:#000000;">Desativada</b> <br />
   <a onClick='getId("Controller/Admin.controller.php?op=8&cmd=getCancelado&tp=vaga","box_view")'>      
   <?php
   echo $vagaDAO->getContCancelado($data,$id_recrutador);
   ?>
   </a>
  </div>
</div>

<div class="row half">   
          
  <div class="12u" id='input_full2'> <b style="color:#000000;">Pendente para aprovação</b> <br />
   <a onClick='getId("Controller/Admin.controller.php?op=8&cmd=getPendente&tp=vaga","box_view")'>    
   <?php
   echo $vagaDAO->getContPendente($data,$id_recrutador);
   ?>
   </a>
  </div>          

</div>

<div class="row half">          
  <div class="12u">             
	<h3>Candidatos relacionados a vagas</h3>                                         
  </div>        
</div>

<div class="row half">   
 
  <div class="6u" id='input_full'> <b style="color:#000000;">Total cadastrado</b> <br />
   <a onClick='getId("Controller/Admin.controller.php?op=8&cmd=getTotal&tp=relaVaga","box_view")'>        
   <?php
	 echo $relaVagaDAO->getContTotal($data,$id_recrutador);
   ?>
   </a>
  </div>          
  
  <div class="6u" id='input_full'> <b style="color:#000000;">Em candidatura</b> <br />
   <a onClick='getId("Controller/Admin.controller.php?op=8&cmd=getCandidato&tp=relaVaga","box_view")'>          
   <?php
	 echo $relaVagaDAO->getContCandidato($data,$id_recrutador);  
   ?>
   </a>
  </div>
</div>

<div class="row half">   
 
  <div class="6u" id='input_full'> <b style="color:#000000;">Recrutados</b> <br />
   <a onClick='getId("Controller/Admin.controller.php?op=8&cmd=getRecrutado&tp=relaVaga","box_view")'>            
   <?php
   echo $relaVagaDAO->getContRecrutado($data,$id_recrutador);          
   ?>
   </a>
  </div>          
  <div class="6u" id='input_full'> <b style="color:#000000;">Selecionados</b> <br />
   <a onClick='getId("Controller/Admin.controller.php?op=8&cmd=getSelecionado&tp=relaVaga","box_view")'>            
   <?php
   echo $relaVagaDAO->getContSelecionado($data,$id_recrutador);
   ?>
   </a>
  </div>
</div>
</body>
</html>
