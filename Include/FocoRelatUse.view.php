<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
<style>
#popup_foco_user { 	 	
	display: none;
	position: absolute;
 	top:2%; 
	
	background:#FFFFFF;  
	z-index:100; /* Layering ( on-top of others), if you have lots of layers: I just maximized, you can change it yourself */
	/* additional features, can be omitted */
	border:2px solid #CCCCCC;  	
	padding:15px;  
	font-size:15px;  
	-moz-box-shadow: 0 0 5px #CCCCCC;
	-webkit-box-shadow: 0 0 5px #CCCCCC;
	box-shadow: 0 0 2px #999999;
}

@media screen and (min-width:975px){
	#popup_foco_user { 	
		left: 30%;			
		width:60%; 
	}
}

@media screen and (max-width:975px){
	#popup_foco_user { 	 	
		left: 5%;
		width:90%; 
	}
}
</style>

<script type="text/javascript">
	
	$(document).ready( function() {		
		
		// When site loaded, load the Popupbox First
		loadPopupBox();
	
		$('#popupBoxClose').click( function() {			
			unloadPopupBox();									  			 
		});
		
		function unloadPopupBox() {	// TO Unload the Popupbox
			$('#popup_foco_user').fadeOut("slow");	
			$("#popup_foco_user").remove(); 									 		
		}	
		
		function loadPopupBox() {	// To Load the Popupbox				
			$('#popup_foco_user').fadeIn("slow");
		}
		/**********************************************************/

	});
</script>

<script>
// Centraliza a div de poup no centro da tela independente do scroll
function id( el ){
	return document.getElementById( el );
}

var marginTopoInicial;//criando variavel de escopo global

calcMarginTop();

//levando em conta o scroll
id('popup_foco_user').style.marginTop = marginTopoInicial+window.pageYOffset+'px';

function calcMarginTop()
{
	var pop = id('popup_foco_user');
	var marginTopo = parseInt( getMarginTop( pop ) );
	var marginBaixo = parseInt( getMarginBottom( pop ) );

	if( pop.offsetTop < -1 )
		pop.style.marginTop = '-'+( parseInt( pop.offsetTop ) - marginTopo )+'px';
	else if( marginTopo!=marginBaixo )
	pop.style.marginTop = '-248px';
}

function getMarginTop( el )
{
	if( window.getComputedStyle )
		return document.defaultView.getComputedStyle(el, null).marginTop;
	else if( el.currentStyle )
		return el.currentStyle['marginTop'];
}
function getMarginBottom( el )
{
	if( window.getComputedStyle )
		return document.defaultView.getComputedStyle(el, null).marginBottom;
	else if( el.currentStyle )
		return el.currentStyle['marginBottom'];
}
//
</script>
<style>
#dataTables-example_length{
	margin-bottom:-60px;	
}
</style>
<?php	
	echo "<div id='popup_foco_user'>                    
			 <a id='popupBoxClose'><u>Fechar</u></a>
	
			 <div style='margin-top:20px;' id='retorno_curso'>                    

               <table id='dataTables-example'>
					<caption><h2> Registros encontrados. </h2></caption>";

	echo "			<thead>";
					
					if(count($list_user) > 0){
						echo "<tr>
								<th>E-mail</th>";
								if($list_user[0]->tipo == 1){
									echo "<th>CPF</th>";
								}else{
									echo "<th>CNPJ</th>";
								}
								
	 					echo "	<th>Data</th>
								<th>Hora</th>";
								if($explorar == 0){
									echo "<th>&nbsp;</th>";
								}
						echo " </tr>";						
					}else{
						echo "<tr>
								<th>Aviso</th>
	 						  </tr>";
					}						
												
	echo "			</thead>
					<tbody>";
					if(count($list_user) > 0){
						foreach($list_user as $list){
							echo "<tr>
									<th>".$list->email."</th>";
									
									if($list->tipo == 1){
										echo "<th>".$list->cpf."</th>";
									}else{
										echo "<th>".$list->cnpj."</th>";
									}
									
							echo "	<th>".date('d/m/Y',strtotime($list->data))."</th>
									<th>".$list->hora."</th>";		
									
									if($explorar == 0){
										if($tipo == "user"){
											?>
											<th>  <a href="Controller/Candidato.controller.php?op=<?php echo sha1(2) ?>&id=<?php echo base64_encode($list->id) ?>">&nbsp;&nbsp;<u>Explorar</u>&nbsp;&nbsp;</a> </th>
											<?php
										}else{
											?>                                        
											<th>  <a href="Controller/Admin.controller.php?op=<?php echo sha1(2) ?>&id=<?php echo base64_encode($list->id_user) ?>">&nbsp;&nbsp;<u>Explorar</u>&nbsp;&nbsp;</a> </th>
											<?php
										}				
									}			
							echo "</tr>";		
						}						
					}else{
						echo "<tr>
								<th>Não há registro cadastrado.</th>
							  </tr>";
							
					}
	echo "			</tbody>
				 </table>
			  </div> 			
	  	   </div>";

?>

<script>
    $(document).ready(function(){
        $('#dataTables-example').dataTable();
    });
</script>
</body>
</html>
