<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
 <header>
<h2>Gestor de empresas</h2>
<p>Procure a empresa que deseja acessar</p>
</header>
<form onSubmit="return false" id="get_empresa">

<div class="row half">        
  <div class="12u">
    <input type="text" class="text" name="txtCnpj" placeholder="CNPJ" onkeypress='mascara(this,cnpj)' maxlength='18'/>
  </div> 
</div>	

<div class="row half">        
  <div class="6u">
    <input type="text" class="text" name="txtNome" placeholder="Empresa" />
  </div>

  <div class="6u">
    <input type="text" class="text" name="txtEmail" placeholder="E-mail" />
  </div>       
</div>		
       
<div class="row">
    <div class="12u" id='btn_load_3'>
      Processando...
    </div>
      
    <div class="12u" id="btn_3"> 
          <a class="button submit" onclick='post("Controller/Admin.controller.php?op=2","btn_3","btn_load_3","gestor_empresa","get_empresa")'>Encontrar empresa</a>
    </div>           
</div>

</form> 
</body>
</html>
