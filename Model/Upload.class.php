<?php
class Upload{
	private $tamanho;
	private $tipo;
	private $nome;
	private $nomeTemporario;
	private $erro;
	private $diretorio;
	private $formatos;
	private $tamanhoMaximo;
	
	public function __construct($arquivo){
		$this->nome = $arquivo['name'];
		$this->tamanho = $arquivo['size'];
		$this->nomeTemporario = $arquivo['tmp_name'];
		$this->tipo = $arquivo['type'];
		$this->erro = $arquivo['error'];
		$this->tamanhoMaximo = 2097152;
		$this->formatos = array();			
	}
	
	public function __set($atributo,$valor){
		$this->$atributo = $valor;
	}
	
	public function __get($atributo){
		return $this->$atributo;
	}	
	
	public function testeTamanho(){
		if($this->tamanho <= $this->tamanhoMaximo){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	
	public function testeTipo(){
		return in_array($this->tipo,$this->formatos);
	}
	
	public function obterErro(){
		if($this->erro == 1){
			return 'Ultrapassou o limite de tramanho do servidor';
		}else if($this->erro == 2){
			return 'Ultrapassou o limite de arquivo do formulario';
		}else if($this->erro == 3){
			return 'O upload não foi concluido';
		}else{
			return 'o upload não foi feito'; 
		}
	}
	
	public function upload($nome){
		$ponto = strrpos($this->nome,".");
		$ext = substr($this->nome,$ponto);
		$destino = $this->diretorio . "/" . $nome . $ext;
		move_uploaded_file($this->nomeTemporario,$destino);
		return 	$nome.$ext;
	}
}
?> 