<?php
session_start();

include '../Banco/Conexao.class.php';
include '../Persistencia/ValorCampoDAO.class.php';
include '../Model/ValorCampo.class.php';
include '../Model/ListCampo.class.php';
include '../Persistencia/ListCampoDAO.class.php';
include '../Model/Prospect.class.php';
include '../Persistencia/CategoriaProspectDAO.class.php';
include '../Model/CategoriaProspect.class.php';
include '../Model/Endereco.class.php';
include '../Persistencia/EnderecoDAO.class.php';

// Buscando os prospects da categoria

if(isset($_SESSION['crm_foco_categoria_prospect'])){
	
	$categoriaProspectDAO = new CategoriaProspectDAO();
	$listCampoDAO = new ListCampoDAO();
	
	$foco_categoriaProspect = unserialize($_SESSION['crm_foco_categoria_prospect']);

	$list_prospect = $categoriaProspectDAO->getAllListProspect($foco_categoriaProspect[0]);	
	
	$list_campo = $listCampoDAO->getCateProspect($foco_categoriaProspect[0]);
					 
	error_reporting(E_ALL);
	ini_set('display_errors', TRUE);
	ini_set('display_startup_errors', TRUE);
	date_default_timezone_set('Europe/London');
	
	
	/** Include PHPExcel */
	require_once 'Classes/PHPExcel.php';
		
	// Create new PHPExcel object
	$objPHPExcel = new PHPExcel();
	
	// Set document properties
	$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
								 ->setLastModifiedBy("Maarten Balliauw")
								 ->setTitle("Office 2007 XLSX Test Document")
								 ->setSubject("Office 2007 XLSX Test Document")
								 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
								 ->setKeywords("office 2007 openxml php")
								 ->setCategory("Test result file");		
	// Criando os titulos.
	$objPHPExcel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);
			
	$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('B1', 'Nome');	

	// Por incrivel que pareça asuashu o alfabeto tmb aceita ++ igual numero asuhuashuashua
	$letra = "C";
	
	if($foco_categoriaProspect[0]->tel != 0){		
		$objPHPExcel->getActiveSheet()->getStyle(''.$letra.'1')->getFont()->setBold(true);
		
		$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue(''.$letra.'1', 'Telefone');	
					
		$letra++;
	}
	
	 if($foco_categoriaProspect[0]->cel != 0){
	 	$objPHPExcel->getActiveSheet()->getStyle(''.$letra.'1')->getFont()->setBold(true);
		
		$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue(''.$letra.'1', 'Celular');	
		
		$letra++;
	}
	
	if($foco_categoriaProspect[0]->email != 0){
		$objPHPExcel->getActiveSheet()->getStyle(''.$letra.'1')->getFont()->setBold(true);
		
		$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue(''.$letra.'1', 'E-mail');	
		
		$letra++;
	}
	
	if($foco_categoriaProspect[0]->site != 0){
		$objPHPExcel->getActiveSheet()->getStyle(''.$letra.'1')->getFont()->setBold(true);
		
		$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue(''.$letra.'1', 'Site');	
		
		$letra++;
	}
	
	if($foco_categoriaProspect[0]->rg != 0){
		$objPHPExcel->getActiveSheet()->getStyle(''.$letra.'1')->getFont()->setBold(true);
		
		 $objPHPExcel->setActiveSheetIndex(0)
					->setCellValue(''.$letra.'1', 'RG');	
		
		$letra++;
	}
	
	if($foco_categoriaProspect[0]->cnpj != 0){
		$objPHPExcel->getActiveSheet()->getStyle(''.$letra.'1')->getFont()->setBold(true);
		
		$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue(''.$letra.'1', 'CNPJ');	
					
		$letra++;
	}
	
	if($foco_categoriaProspect[0]->cpf != 0){
		$objPHPExcel->getActiveSheet()->getStyle(''.$letra.'1')->getFont()->setBold(true);
		
		$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue(''.$letra.'1', 'CPF');	
		
		$letra++;
	}
	
	if($foco_categoriaProspect[0]->dt_nascimento != 0){
		$objPHPExcel->getActiveSheet()->getStyle(''.$letra.'1')->getFont()->setBold(true);
		
		$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue(''.$letra.'1', 'Data de nascimento');	
		
		$letra++;
	}
	
	if($foco_categoriaProspect[0]->cep != 0){
		$objPHPExcel->getActiveSheet()->getStyle(''.$letra.'1')->getFont()->setBold(true);
		
	 	$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue(''.$letra.'1', 'CEP');	
					
		$letra++;
	}
	
	if($foco_categoriaProspect[0]->estado != 0){
		$objPHPExcel->getActiveSheet()->getStyle(''.$letra.'1')->getFont()->setBold(true);
		
		$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue(''.$letra.'1', 'Estado');	
					
		$letra++;
	}
	
	if($foco_categoriaProspect[0]->cidade != 0){
		$objPHPExcel->getActiveSheet()->getStyle(''.$letra.'1')->getFont()->setBold(true);
		
	 	$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue(''.$letra.'1', 'Cidade');	
					
		$letra++;
	}
	
	if($foco_categoriaProspect[0]->bairro != 0){
		$objPHPExcel->getActiveSheet()->getStyle(''.$letra.'1')->getFont()->setBold(true);
		
		$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue(''.$letra.'1', 'Bairro');	
		
		$letra++;
	}
	
	if($foco_categoriaProspect[0]->rua != 0 || $foco_categoriaProspect[0]->numero != 0){
		$objPHPExcel->getActiveSheet()->getStyle(''.$letra.'1')->getFont()->setBold(true);
		
		$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue(''.$letra.'1', 'Rua / Numero');	
		
		$letra++;
	}
	
	foreach($list_campo as $list){
		$objPHPExcel->getActiveSheet()->getStyle(''.$letra.'1')->getFont()->setBold(true);
		
		$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue(''.$letra.'1', $list->nome);	
		
		$letra++;
	}
				
	// Adicionando campos da tabela
	if(count($list_prospect) > 0){
		
		$enderecoDAO = new EnderecoDAO();
		$valorCampoDAO = new ValorCampoDAO();
		
		$cont = 2;		
			
		foreach($list_prospect as $list){
			
			$endx->id_prospect = $list->id;
 			$end = $enderecoDAO->getProspect($endx);	
				 			
			$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('B'.$cont.'', $list->nome);	
			
			$letra = "C";
			
			if($foco_categoriaProspect[0]->tel != 0){
				$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue(''.$letra.$cont.'', $list->tel);							
				
				$letra++;
			}
			
			 if($foco_categoriaProspect[0]->cel != 0){
				$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue(''.$letra.$cont.'', $list->cel);			
				
				$letra++;
			}
			
			if($foco_categoriaProspect[0]->email != 0){
				$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue(''.$letra.$cont.'', $list->email);		
				
				$letra++;
			}
			
			if($foco_categoriaProspect[0]->site != 0){
				$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue(''.$letra.$cont.'', $list->site);		
				
				$letra++;
			}
			
			if($foco_categoriaProspect[0]->rg != 0){
				 $objPHPExcel->setActiveSheetIndex(0)
					->setCellValue(''.$letra.$cont.'', $list->rg);			
				
				$letra++;
			}
			
			if($foco_categoriaProspect[0]->cnpj != 0){
				$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue(''.$letra.$cont.'', $list->cnpj);		
							
				$letra++;
			}
			
			if($foco_categoriaProspect[0]->cpf != 0){
				$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue(''.$letra.$cont.'', $list->cpf);		
				
				$letra++;
			}
			
			if($foco_categoriaProspect[0]->dt_nascimento != 0){
				if(strlen($list->dt_nascimento) > 0){
					$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue(''.$letra.$cont.'', date('d/m/Y',strtotime($list->dt_nascimento)));		
				}
				
				$letra++;
			}
			
			if($foco_categoriaProspect[0]->cep != 0){
				if(count($end) > 0){
					$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue(''.$letra.$cont.'', $end[0]->cep);		
				}
				
				$letra++;
			}
			
			if($foco_categoriaProspect[0]->estado != 0){
				if(count($end) > 0){
					$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue(''.$letra.$cont.'', $end[0]->estado);		
				}
							
				$letra++;
			}
			
			if($foco_categoriaProspect[0]->cidade != 0){
				if(count($end) > 0){
					$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue(''.$letra.$cont.'', $end[0]->cidade);		
				}
							
				$letra++;
			}
			
			if($foco_categoriaProspect[0]->bairro != 0){
				if(count($end) > 0){
					$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue(''.$letra.$cont.'', $end[0]->bairro);		
				}
				
				$letra++;
			}
			
			if($foco_categoriaProspect[0]->rua != 0 || $foco_categoriaProspect[0]->numero != 0){
				if(count($end) > 0){
					$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue(''.$letra.$cont.'', $end[0]->rua." / ".$end[0]->numero);		
				}
				
				$letra++;
			}
			
			foreach($list_campo as $campo){
				$valorCampox->id_list_campo = $campo->id;
				$valorCampox->id_prospect = $list->id;
				
				$valorCampo = $valorCampoDAO->getValorProspect($valorCampox);
			
				if(count($valorCampo) > 0){
					$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue(''.$letra.$cont.'', $valorCampo[0]->valor);		
				}
							
				$letra++;
			}
																
			$cont++;			
		}
	}	
	
	// Rename worksheet
	$objPHPExcel->getActiveSheet()->setTitle('Simple');
	
	
	// Set active sheet index to the first sheet, so Excel opens this as the first sheet
	$objPHPExcel->setActiveSheetIndex(0);
	
	
	// Redirect output to a client’s web browser (Excel2007)
	header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment;filename="registro.xlsx"');
	header('Cache-Control: max-age=0');
	
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	$objWriter->save('php://output');
	exit;
}

?>