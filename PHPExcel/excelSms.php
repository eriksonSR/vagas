<?php
session_start();

include '../Model/InboxSms.class.php';

// Buscando os prospects da categoria

if(isset($_SESSION['its_list_retorno_sms'])){

	$list_retorno_sms = unserialize($_SESSION['its_list_retorno_sms']);
 
	error_reporting(E_ALL);
	ini_set('display_errors', TRUE);
	ini_set('display_startup_errors', TRUE);
	date_default_timezone_set('Europe/London');
	
	
	/** Include PHPExcel */
	require_once 'Classes/PHPExcel.php';
		
	// Create new PHPExcel object
	$objPHPExcel = new PHPExcel();
	
	// Set document properties
	$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
								 ->setLastModifiedBy("Maarten Balliauw")
								 ->setTitle("Office 2007 XLSX Test Document")
								 ->setSubject("Office 2007 XLSX Test Document")
								 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
								 ->setKeywords("office 2007 openxml php")
								 ->setCategory("Test result file");		
	
	// Criando os titulos.
	$objPHPExcel->getActiveSheet()->getStyle('B2')->getFont()->setBold(true);			
	$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('B2', 'Celular');	
	
	$objPHPExcel->getActiveSheet()->getStyle('C2')->getFont()->setBold(true);			
	$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('C2', 'Mensagem');	

	$objPHPExcel->getActiveSheet()->getStyle('D2')->getFont()->setBold(true);			
	$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('D2', 'Data');	

	$objPHPExcel->getActiveSheet()->getStyle('E2')->getFont()->setBold(true);			
	$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('E2', 'Hora');	

	$objPHPExcel->getActiveSheet()->getStyle('F2')->getFont()->setBold(true);			
	$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('F2', 'Anotação');	

	$objPHPExcel->getActiveSheet()->getStyle('G2')->getFont()->setBold(true);			
	$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('G2', 'Concluido');	
	
						
	// Adicionando campos da tabela
	if(count($list_retorno_sms) > 0){

		$cont = 3;		
			
		foreach($list_retorno_sms as $inbox){
						
			$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('B'.$cont.'', $inbox->cel);	
			
			$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('C'.$cont.'', $inbox->mensagem);	
					
			$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('D'.$cont.'', date('d/m/Y',strtotime($inbox->data)));	
			
			$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('E'.$cont.'', $inbox->hora);	
			
			$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('F'.$cont.'', $inbox->anotacao);	
			
			if($inbox->concluido == 1){
				$concluido = "Sim";
			}else{
				$concluido = "Não";			
			}										
			
			$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('G'.$cont.'', $concluido);
			
			$cont++;			
		}
	}	
	
	// Rename worksheet
	$objPHPExcel->getActiveSheet()->setTitle('Simple');
	
	
	// Set active sheet index to the first sheet, so Excel opens this as the first sheet
	$objPHPExcel->setActiveSheetIndex(0);
	
	
	// Redirect output to a client’s web browser (Excel2007)
	header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment;filename="retorno.xlsx"');
	header('Cache-Control: max-age=0');
	
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	$objWriter->save('php://output');
	exit;
}

?>